<?php
mb_internal_encoding("UTF-8");

/** comment out the following four lines when deployed to production */
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
ini_set('display_errors', 1);
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'dev');

/** Global language config */
defined('YII_DEFAULT_LANGUAGE') or define('YII_DEFAULT_LANGUAGE', 'ru');
defined('YII_LANGUAGES') or define('YII_LANGUAGES', [
    'Русский' => 'ru',
    'Українська' => 'uk',
    'English' => 'en',
    'Deutsch' => 'de',
]);

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
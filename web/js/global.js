(function ($) {

    $('.b-header__mobile-menu a, #b-adapt-navigation-close').click(function () {
        $('.b-header__bottom').slideToggle('fast');
        return false;
    });
    if ($(window).width() <= 992) {
        $('.b-nav li i').click(function () {

            if ($(this).attr('status') === 'close') {
                $(this).attr('status', 'open').removeClass('fa-plus-circle').addClass('fa-minus-circle');

            } else if ($(this).attr('status') === 'open') {
                $(this).attr('status', 'close').removeClass('fa-minus-circle').addClass('fa-plus-circle');
            }

            if ($(this).parent().find('ul').length) {
                $(this).parent().find('ul').eq(0).slideToggle('fast');
            }

            return false;
        });
    }

    var sidebar = $('.js-sidebar');
    if (sidebar.length) {
        var url = window.location.href;
        $(sidebar).find('ul li a').each(function () {
            if (url === this.href) {
                $(this).addClass('-active')
            }
        });
    }

    if (+localStorage.getItem('is_user_agrees') !== 1) {
        $('.warning-popup-modal').fadeIn();
    }

    $('.js-agreement-button').on('click', function () {
        if (+localStorage.getItem('is_user_agrees') !== 1) {
            localStorage.setItem('is_user_agrees', 1);
            $('.warning-popup-modal').fadeOut();
            $(this).off('click');
        }
    })

})(jQuery);
function initMap() {
    var $input_address = $('.js-address-field');
    var $input_lat = $('.js-map-lat-field');
    var $input_lng = $('.js-map-lng-field');
    var location;

    if($input_lat.val() !== '' && $input_lng.val() !== '') {
        location = new google.maps.LatLng($input_lat.val(), $input_lng.val());
    } else {
        location = new google.maps.LatLng(55.66441, 37.64225);
    }

    var geocoder = new google.maps.Geocoder();
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: location
    });
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        draggable:true
    });

    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    google.maps.event.addListener(searchBox, 'places_changed', function() {
        searchBox.set('map', null);

        var places = searchBox.getPlaces();
        var bounds = new google.maps.LatLngBounds();
        var i, place;

        for (i = 0; place = places[i]; i++) {
            (function(place) {
                $input_lat.val(place.geometry.location.lat());
                $input_lng.val(place.geometry.location.lng());

                var marker = new google.maps.Marker({
                    position: place.geometry.location,
                    map: map,
                    draggable:true
                });
                marker.bindTo('map', searchBox, 'map');
                google.maps.event.addListener(marker, 'dragend', function (event) {
                    var pos = this.getPosition();
                    $input_lat.val(pos.lat());
                    $input_lng.val(pos.lng());
                    geocodePosition(pos)
                });
                google.maps.event.addListener(marker, 'map_changed', function() {
                    if (!this.getMap()) {
                        this.unbindAll();
                    }
                });
                bounds.extend(place.geometry.location);
            }(place));
        }
        map.fitBounds(bounds);
        searchBox.set('map', map);
        map.setZoom(Math.min(map.getZoom(),12));

    });

    google.maps.event.addListener(marker, 'dragend', function (event) {
        var pos = this.getPosition();
        $input_lat.val(pos.lat());
        $input_lng.val(pos.lng());
        geocodePosition(pos)
    });

    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
                $input_address.val(responses[0].formatted_address);
            } else {
                console.log('Cannot determine address at this location.');
            }
        });
    }
}

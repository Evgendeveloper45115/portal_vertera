function initMap() {
    $('.js-contact-map').each(function() {
        initMapByEl(this)
    });
}

function initMapByEl(el) {
    if($(el).data('lat') === '' || $(el).data('lng') === '') {
        return;
    }
    var location = new google.maps.LatLng($(el).data('lat'), $(el).data('lng'));
    var map = new google.maps.Map(el, {
        zoom: 8,
        center: location
    });
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
}


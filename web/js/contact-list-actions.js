(function ($) {

    var timer;

    $('#contact-search-form').on('submit', function (e) {
        e.preventDefault();
        return false;
    }).find('[type="text"]').on('keyup change', function () {
        if(timer) {
            clearTimeout(timer);
        }

        var pattern = new RegExp(this.value.trim().replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"), "gi");

        timer = setTimeout(function () {
            var $rows = $('#contact-search-list').find('.js-search-result > tr');

            $rows.each(function () {
                if(pattern.test($(this).text()) === true) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
                console.log(pattern.test($(this).text()));
            });
        }, 500);
    });

})(jQuery);
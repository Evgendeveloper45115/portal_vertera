(function($) {

    var $modal = $('#lang-form-modal');

    $('[data-translate-url]').on('click', function (e) {
        if(e.ctrlKey === true || this.tagName.toLowerCase() === 'button') {
            if($(this).closest('form').data('is-new-record') !== undefined) {
                alert('Перевод доступен только при редактировании!');
                return;
            }

            if($(this).data('label') !== undefined) {
                $modal.find('.modal-header h3 > span').html($(this).data('label'));
            }
            $modal.modal('show').find('.modal-body').empty();

            $.get($(this).data('translate-url'), function (response) {
                $modal.find('.modal-body').html('<div>' + response + '</div>');
            });
        }
    });

    $(document).on('submit', '.field-translate-form', function () {
        var action = $(this).prop('action');

        $.post($(this).prop('action'), $(this).serialize(), function (response) {
            if(response.error === false) {
                alert('Переводы сохранены');
                $modal.find('[data-dismiss="modal"]').trigger('click');
                $modal.find('.modal-body').empty();
            }
        });

        return false;
    });

})(jQuery);
(function ($) {

    var $form = $('#calendar-form');

    $('[data-pjax-container]').on('pjax:send', function () {
        $(this).find('> .pjax-container-loader').addClass('loading')
    }).on('pjax:complete', function () {
        $(this).find('> .pjax-container-loader').removeClass('loading')
    });

    $(document).on('click', function () {
        $('.sort-dropdown-input ul.sort__select-drop').hide();
    });

    $('.sort-dropdown-input ul.sort__select > li > a').on('click', function () {
        $('.sort-dropdown-input ul.sort__select-drop').not(this).hide();
        $(this).next().show();

        return false;
    });

    $('.sort-dropdown-input ul.sort__select-drop > li > a').on('click', function () {
        var v = $(this).data('value');
        var t = $(this).text();

        if (v === undefined) {
            alert('Не найдено выбранное значение....');

            return false;
        }

        var $parent = $(this).closest('.sort-dropdown-input');

        $parent.find('.sort__input-selected').val(v);
        $parent.find('.sort__select > li > a.active').text(t);
        $parent.find('.sort__select-drop').hide();

        $form.submit();

        return false;
    });

})(jQuery);
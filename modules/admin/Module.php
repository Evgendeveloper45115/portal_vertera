<?php

namespace app\modules\admin;
use app\models\User;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     */
    public function beforeAction($action)
    {
        /** @var $user User */
        $webUser = \Yii::$app->getUser();

        if ($webUser->getIsGuest() || null === ($user = $webUser->getIdentity())) {
            return \Yii::$app->controller->redirect(['/site/login']);
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::$app->language = YII_DEFAULT_LANGUAGE;
        $this->layout = 'main.php';
        $this->defaultRoute = 'item';
    }
}

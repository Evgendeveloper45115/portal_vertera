<?php

namespace app\modules\admin\extensions;

use Yii;
use yii\db\ActiveRecord;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use app\models\FieldTranslation;

/**
 * Class TrController
 * @package app\modules\admin\extensions
 */
class TrController extends Controller
{
    /**
     * @param $id
     * @param $field
     * @param $model_key
     * @param $lang_code
     * @return array|string
     * @throws HttpException
     * @throws \HttpInvalidParamException
     */
    public function actionTranslateSingle($id, $field, $model_key, $lang_code)
    {
        /**
         * @var ActiveRecord $cls
         * @var ActiveRecord|object $model
         */
        $cls = FieldTranslation::$model_keys[$model_key] ?? null;

        if (null === $cls || null === $id || null === ($model = $cls::findOne($id))) {
            throw new HttpException(404, 'Not Found');
        }

        if (!$lang = FieldTranslation::getLangName($lang_code)) {
            throw new \HttpInvalidParamException(404, "Language '{$lang}' not Found");
        }

        if (YII_DEFAULT_LANGUAGE == $lang) {
            throw new \HttpInvalidParamException(404, "Language '{$lang}' is default");
        }

        $translation = FieldTranslation::find()->where([
                'rel_id' => $model->id,
                'model_key' => $model_key,
                'field_name' => $field,
                'language' => $lang_code,
            ])->one() ?: new FieldTranslation([
                'rel_id' => $model->id,
                'model_key' => $model_key,
                'field_name' => $field,
                'language' => $lang_code,
            ]);

        if ($translation->load(Yii::$app->getRequest()->post())) {
            $translation->save();
            Yii::$app->getResponse()->format = Response::FORMAT_JSON;

            return ['error' => false];
        }

        return $this->renderAjax('/field-translation/edit-single-editor', ['translation' => $translation]);
    }

    /**
     * @param $id
     * @param $field
     * @param $model_key
     * @return array|string
     * @throws HttpException
     */
    public function actionTranslate($id, $field, $model_key)
    {
        /**
         * @var ActiveRecord $cls
         * @var ActiveRecord|object $model
         * @var FieldTranslation[] $translations
         */
        $translations = [];
        $cls = FieldTranslation::$model_keys[$model_key] ?? null;

        if (null === $cls || null === $id || null === ($model = $cls::findOne($id))) {
            throw new HttpException(404, 'Not Found');
        }

        foreach (YII_LANGUAGES as $v) {
            if (YII_DEFAULT_LANGUAGE == $v || !$code = FieldTranslation::getLangCode($v))
                continue;

            $translations[$code] = FieldTranslation::find()->where([
                'rel_id' => $model->id,
                'model_key' => $model_key,
                'field_name' => $field,
                'language' => $code,
            ])->one() ?: new FieldTranslation([
                'rel_id' => $model->id,
                'model_key' => $model_key,
                'field_name' => $field,
                'language' => $code,
            ]);
        }

        if (Yii::$app->getRequest()->isPost && FieldTranslation::loadMultiple($translations, Yii::$app->getRequest()->post())) {
            foreach ($translations as $translation) {
                $translation->save();
            }
            Yii::$app->getResponse()->format = Response::FORMAT_JSON;

            return ['error' => false];
        }

        return $this->renderAjax('/field-translation/edit', ['translations' => $translations]);
    }
}
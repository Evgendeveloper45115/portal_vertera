<?php

namespace app\modules\admin\controllers;

use Yii;
use goodizer\helpers\GridSearchHelper;
use app\models\ObjectFile;
use app\modules\admin\extensions\TrController;
use app\models\Speaker;

/**
 * Class SpeakerController
 * @package app\modules\admin\controllers
 */
class SpeakerController extends TrController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchData = GridSearchHelper::search(new Speaker(), [
            'pagination' => ['pageSize' => 0],
        ]);

        return $this->render('index', ['searchData' => $searchData]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        return $this->_edit();
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \HttpException
     */
    public function actionUpdate($id)
    {
        return $this->_edit($id);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->_getRecord($id);

        if ($model->delete()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('system', 'Deleted success'));

            return $this->redirect(['index']);
        }
        return $this->render('edit', ['model' => $model]);
    }

    /**
     * @param null $id
     * @return Speaker
     * @throws \HttpException
     */
    private function _getRecord($id = null)
    {
        /** @var Speaker $model */
        if (null !== $id && null === ($model = Speaker::findOne($id))) {
            throw new \HttpException(404, 'Not Found');
        } elseif (null === $id) {
            $model = new Speaker();
        }

        return $model;
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \HttpException
     */
    private function _edit($id = null)
    {
        $model = $this->_getRecord($id);

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->save()) {
                if (ObjectFile::dynamicSave($model)) {
                    $model->save(false);
                }

                Yii::$app->getSession()->setFlash('success', Yii::t('system', 'Success'));

                return $this->redirect(['index']);
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('system', 'Failed'));
            }
        }

        return $this->render('edit', ['model' => $model]);
    }
}
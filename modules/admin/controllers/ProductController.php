<?php

namespace app\modules\admin\controllers;

use app\models\ProductHasSubCategory;
use app\models\ProductSubCategory;
use Yii;
use goodizer\helpers\GridSearchHelper;
use app\modules\admin\extensions\TrController;
use app\models\Product;
use app\models\ObjectFile;
use yii\helpers\VarDumper;

/**
 * Class ProductController
 * @package app\modules\admin\controllers
 */
class ProductController extends TrController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchData = GridSearchHelper::search(new Product(), [
            'pagination' => ['pageSize' => 0],
        ]);

        return $this->render('index', ['searchData' => $searchData]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        return $this->_edit();
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \HttpException
     */
    public function actionUpdate($id)
    {
        return $this->_edit($id);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->_getRecord($id);

        if ($model->delete()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('system', 'Deleted success'));

            return $this->redirect(['index']);
        }

        return $this->render('edit', ['model' => $model]);
    }
    
    /**
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionDeleteCertificate()
    {
        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
        }
        $model = ObjectFile::findOne(Yii::$app->request->post('key'));
        
        if (is_null($model)) {
            return ['error' => true];
        }
        
        $file = $model->getPath();
        
        if (empty($file) || !file_exists($file)) {
            return ['error' => true];
        }
        
        if (!unlink($file)) {
            return ['error' => true];
        }
        
        if ($model->delete()) {
            return ['success' => true];
        }
        
        return false;
        
    }

    /**
     * @param $id
     * @param $field
     * @return string|\yii\web\Response
     * @throws \HttpException
     */
    public function actionEditor($id, $field)
    {
        $model = $this->_getRecord($id);
        $content = $model->{$field};

        if (null !== $model->{$field} = Yii::$app->request->post('Editor_content')) {
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', Yii::t('system', 'Success'));
                return $this->redirect(['/admin/product/update', 'id' => $model->id]);
            }

        }
        return $this->render('editor', ['content' => $content]);
    }

    /**
     * @param null $id
     * @return Product
     * @throws \HttpException
     */
    private function _getRecord($id = null)
    {
        /** @var Product $model */
        if (null !== $id && null === ($model = Product::findOne($id))) {
            throw new \HttpException(404, 'Not Found');
        } elseif (null === $id) {
            $model = new Product();
        }

        return $model;
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \HttpException
     */
    private function _edit($id = null)
    {
        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
        }
        
        $model = $this->_getRecord($id);
        $model->setScenario('admin-edit');
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                ProductHasSubCategory::deleteAll(['product_id' => $model->id]);
                foreach ($model->categories as $category_id) {
                    $PSC = new ProductHasSubCategory();
                    $PSC->product_id = $model->id;
                    $PSC->sub_category_id = $category_id;
                    $PSC->save(false);
                }
                if (ObjectFile::dynamicSave($model)) {
                    $model->save(false);
                }

                ObjectFile::saveByLanguage($model, 'image_id');
                ObjectFile::saveByLanguage($model, 'description_image_id');

                if (Yii::$app->request->isAjax)
                {
                    return ['success' => true];
                    
                }
                Yii::$app->getSession()->setFlash('success', Yii::t('system', 'Success'));
                

                return $this->redirect(['index']);
            } else {
                
                if (Yii::$app->request->isAjax)
                {
                    return ['error' => true];
                    
                }
                
                Yii::$app->getSession()->setFlash('error', Yii::t('system', 'Failed'));
            }
        }

        return $this->render('edit', ['model' => $model]);
    }
}
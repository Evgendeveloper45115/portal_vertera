<?php

namespace app\modules\admin\controllers;

use Yii;
use goodizer\helpers\GridSearchHelper;
use app\models\Business;
use app\models\BusinessHasSpeaker;
use app\models\ObjectFile;
use app\modules\admin\extensions\TrController;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class BusinessController
 * @package app\modules\admin\controllers
 */
class BusinessController extends TrController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchData = GridSearchHelper::search(new Business(), [
            'pagination' => ['pageSize' => 0],
        ]);

        return $this->render('index', ['searchData' => $searchData]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        return $this->_edit();
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \HttpException
     */
    public function actionUpdate($id)
    {
        return $this->_edit($id);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->_getRecord($id);

        if ($model->delete()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('system', 'Deleted success'));

            return $this->redirect(['index']);
        }
        return $this->render('edit', ['model' => $model]);
    }

    /**
     * @param null $id
     * @return Business
     * @throws \HttpException
     */
    private function _getRecord($id = null)
    {
        /** @var Business $model */
        if (null !== $id && null === ($model = Business::findOne($id))) {
            throw new \HttpException(404, 'Not Found');
        } elseif (null === $id) {
            $model = new Business();
        }

        return $model;
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \HttpException
     */
    private function _edit($id = null)
    {
        $model = $this->_getRecord($id);
        $model->setScenario('admin-edit');

        if ($model->load(\Yii::$app->request->post())) {
            $model->start_date = date('Y-m-d H:i:s', strtotime($model->start_date));

            if ($model->save()) {

                if (!empty($model->speakers)) {
                    foreach ($model->getBusinessHasSpeakers()->all() as $bhs) {
                        $bhs->delete();
                    }
                    foreach ($model->speakers as $speaker_id) {
                        $bhs = new BusinessHasSpeaker();
                        $bhs->business_id = $model->id;
                        $bhs->speaker_id = $speaker_id;
                        $bhs->save(false);
                    }
                }

                if (ObjectFile::dynamicSave($model)) {
                    $model->save(false);
                }

                ObjectFile::saveByLanguage($model, 'image_id');
                Yii::$app->getSession()->setFlash('success', Yii::t('system', 'Success'));

                return $this->redirect(['index']);
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('system', 'Failed'));
            }
        }

        return $this->render('edit', ['model' => $model]);
    }
}
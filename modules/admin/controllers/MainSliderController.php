<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use himiklab\sortablegrid\SortableGridAction;
use goodizer\helpers\GridSearchHelper;
use app\models\MainSliderHeader;
use app\models\ObjectFile;

/**
 * Class MainSliderController
 * @package app\modules\admin\controllers
 */
class MainSliderController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::class,
                'modelName' => MainSliderHeader::class,
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchData = GridSearchHelper::search(new MainSliderHeader(), [
            'pagination' => ['pageSize' => 0],
        ]);
        $searchData->dataProvider->getSort()->defaultOrder = ['sort_order' => SORT_ASC];

        return $this->render('index', ['searchData' => $searchData]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        return $this->_edit();
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \HttpException
     */
    public function actionUpdate($id)
    {
        return $this->_edit($id);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->_getRecord($id);

        if ($model->delete()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('system', 'Deleted success'));

            return $this->redirect(['index']);
        }

        return $this->render('edit', ['model' => $model]);
    }

    /**
     * @param null $id
     * @return MainSliderHeader
     * @throws \HttpException
     */
    private function _getRecord($id = null)
    {
        /** @var MainSliderHeader $model */
        if (null !== $id && null === ($model = MainSliderHeader::findOne($id))) {
            throw new \HttpException(404, 'Not Found');
        } elseif (null === $id) {
            $model = new MainSliderHeader();
        }

        return $model;
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \HttpException
     */
    private function _edit($id = null)
    {
        $model = $this->_getRecord($id);
        $model->setScenario('admin-edit');

        if ($model->isNewRecord) {
            $model->sort_order = (int)MainSliderHeader::find()
                ->orderBy(['sort_order' => SORT_DESC])
                ->limit(1)
                ->scalar();
            $model->sort_order++;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->start_date = date('Y-m-d', strtotime($model->start_date));
            $model->end_date = date('Y-m-d', strtotime($model->end_date));

            if ($model->save()) {
                if (ObjectFile::dynamicSave($model)) {
                    $model->save(false);
                }

                ObjectFile::saveByLanguage($model, 'image_id');
                Yii::$app->getSession()->setFlash('success', Yii::t('system', 'Success'));

                return $this->redirect(['index']);
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('system', 'Failed'));
            }
        }

        return $this->render('edit', ['model' => $model]);
    }
}
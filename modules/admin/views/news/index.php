<?php
/**
 * @var $this yii\web\View
 * @var $searchData \goodizer\helpers\SearchData
 * @var $user \yii\web\User
 */

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use \app\models\News;

$this->title = 'Список новостей';
$this->params['breadcrumbs'] = [
    $this->title,
];
?>
    <h1><?= $this->title ?></h1>
<?php
echo Html::a('Создать', ['/admin/news/create'], [
    'class' => 'btn btn-success'
]);

echo '<hr />';

Pjax::begin();
echo \yii\grid\GridView::widget([
    'filterModel' => $searchData->filterModel,
    'dataProvider' => $searchData->dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-cell-valign-middle'
    ],
    'columns' => [
        'id' => [
            'attribute' => 'id',
        ],
        [
            'attribute' => 'image_id',
            'format' => ['html'],
            'value' => function (News $model) {
                return Html::img(\app\models\ObjectFile::getThumbnail($model, 'image', [200, 400]));
            },
        ],
        [
            'attribute' => 'title',
            'value' => function (News $model) {
                return $model->title;
            },
        ],
        [
            'attribute' => 'start_date',
            'filter' => DatePicker::widget([
                'model' => $searchData->filterModel,
                'attribute' => 'start_date',
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Выбрать дату',
                ],
                'pickerButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true,
                ]
            ]),
            'headerOptions' => [
                'style' => 'min-width: 200px;'
            ],
            'value' => function (News $model) {
                return date('Y-m-d', strtotime($model->start_date));
            },
        ],
        [
            'attribute' => 'in_top',
            'format' => ['html'],
            'filter' => false,
            'contentOptions' => [
                'class' => 'text-center'
            ],
            'value' => function (News $model) {
                $cls = $model->in_top ? 'success' : 'danger';

                return Html::tag('span', $model->in_top ? 'Да' : 'Нет', ['class' => "label label-{$cls}"]);
            },
        ],
        [
            'attribute' => 'status',
            'format' => ['html'],
            'filter' => News::getStatuses(),
            'value' => function (News $model) {
                if ($model->status === null)
                    return null;

                $cls = $model->status == News::STATUS_ACTIVE ? 'success' : 'danger';

                return Html::tag('span', News::getStatuses($model->status), ['class' => "label label-{$cls}"]);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => '&#160;',
            'contentOptions' => [
                'style' => 'text-align: right; width: 70px;'
            ],
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', [
                        '/admin/news/update',
                        'id' => $model->id
                    ], [
                        'title' => \Yii::t('system', 'Edit'),
                        'style' => 'margin-right: 5px',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                        '/admin/news/delete',
                        'id' => $model->id
                    ], [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                },
            ]
        ]
    ]
]);
Pjax::end();
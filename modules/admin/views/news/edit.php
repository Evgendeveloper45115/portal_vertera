<?php
/**
 * @var $this yii\web\View
 * @var $model News|object
 */

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use dosamigos\ckeditor\CKEditor;
use app\models\News;
use app\helpers\H;

$this->title = $model->isNewRecord ? 'Добавление Новости' : 'Редактирование новости';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Список новостей',
        'url' => ['/admin/news'],
    ],
    $this->title,
];
$this->registerJsFile(Yii::getAlias('@web/js/field-translate-actions.js'), [
    'depends' => [\app\assets\AdminAsset::class]
]);
?>
<div class="Project-index">
    <h1><?= $this->title ?></h1>
    <small>Для редактирования перевода поля, зажмите клавишу - <b>ctrl</b> и <b>левой кнопкой мышки</b> кликните по
        полю.
    </small>
    <hr/>
    <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => [
        'enctype' => 'multipart/form-data',
        'data-is-new-record' => $model->getIsNewRecord()
    ]]) ?>

    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'title')->textInput($model->buildOptions('title')) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'sub_description')->textarea($model->buildOptions('sub_description')) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= H::render_languages_editor_field($form, $model, 'description') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'start_date')->widget(DatePicker::class, [
                'options' => ['placeholder' => 'Выбрать дату'],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true
                ]
            ]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'in_top')->dropDownList([0 => 'Нет', 1 => 'Да']) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'status')->dropDownList($model::getStatuses()) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php H::render_language_file_field($form, $model, 'image_id') ?>
        </div>
    </div>
    <hr/>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

    <?php $form->end() ?>
</div>
<?php
Modal::begin([
    'header' => '<h3>Переводы для поля "<span></span>"</h3>',
    'size' => Modal::SIZE_LARGE,
    'options' => [
        'id' => 'lang-form-modal'
    ],
]);
Modal::end();
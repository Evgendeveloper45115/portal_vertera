<?php
/**
 * @var $this yii\web\View
 * @var $searchData \goodizer\helpers\SearchData
 * @var $user \yii\web\User
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductSubCategory;

$this->title = 'Список продуктов';
$this->params['breadcrumbs'] = [
    $this->title,
];

$sub_categories = ProductSubCategory::find()->filterWhere([
    'parent_id' => $searchData->filterModel->category_id
])->all();
?>
    <h1><?= $this->title ?></h1>
<?php
echo Html::a('Создать', ['/admin/product/create'], [
    'class' => 'btn btn-success'
]);

echo '<hr />';

Pjax::begin();
echo \yii\grid\GridView::widget([
    'filterModel' => $searchData->filterModel,
    'dataProvider' => $searchData->dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-cell-valign-middle'
    ],
    'columns' => [
        'id' => [
            'attribute' => 'id',
        ],
        'category_id' => [
            'attribute' => 'category_id',
            'value' => function (Product $model) {
                foreach ($model->getSubCategory()->all() as $subCategory) {
                    $p = ProductSubCategory::findOne($subCategory->sub_category_id)->getParent();
                    if($p) {
                        return $p->name;
                    };
                }

                return 'Не заданно';
            },
        ],
        'categories' => [
            'attribute' => 'categories',
            'format' => ['html'],
            'value' => function (Product $model) {
                $arr = [];
                foreach ($model->getSubCategory()->all() as $subCategory) {
                    $arr[] = ProductSubCategory::findOne($subCategory->sub_category_id)->name;
                }
                if (!empty($arr)) {
                    return implode(';<br>', $arr);
                }
                return 'Не заданно';
            },
        ],
        'title' => [
            'attribute' => 'title',
            'value' => function (Product $model) {
                return $model->title;
            },
        ],
        'cost' => [
            'attribute' => 'cost',
            'value' => function (Product $model) {
                return $model->cost;
            },
        ],
//        'point' => [
//            'attribute' => 'point',
//            'value' => function (Product $model) {
//                return $model->point;
//            },
//        ],
//        'amount' => [
//            'attribute' => 'amount',
//            'value' => function (Product $model) {
//                return $model->amount;
//            },
//        ],
//        'vendor_code' => [
//            'attribute' => 'vendor_code',
//            'value' => function (Product $model) {
//                return $model->vendor_code;
//            },
//        ],
        'created_at' => [
            'attribute' => 'created_at',
            'filter' => DatePicker::widget([
                'model' => $searchData->filterModel,
                'attribute' => 'created_at',
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Выбрать дату',
                ],
                'pickerButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true,
                ]
            ]),
            'headerOptions' => [
                'style' => 'min-width: 200px;'
            ],
            'value' => function (Product $model) {
                return date('Y-d-m', strtotime($model->created_at));
            },
        ],
        [
            'attribute' => 'in_top',
            'format' => ['html'],
            'filter' => false,
            'contentOptions' => [
                'class' => 'text-center'
            ],
            'value' => function (Product $model) {
                $cls = $model->in_top ? 'success' : 'danger';

                return Html::tag('span', $model->in_top ? 'Да' : 'Нет', ['class' => "label label-{$cls}"]);
            },
        ],
        'status' => [
            'attribute' => 'status',
            'format' => ['html'],
            'filter' => Product::getStatuses(),
            'value' => function (Product $model) {
                if ($model->status === null)
                    return null;

                $classes = [
                    Product::STATUS_ACTIVE => 'success',
                    Product::STATUS_DISABLED => 'danger',
                    Product::STATUS_NEW => 'warning',
                ];

                $cls = $classes[$model->status] ?? Product::STATUS_DISABLED;

                return Html::tag('span', Product::getStatuses($model->status), ['class' => "label label-{$cls}"]);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => '&#160;',
            'contentOptions' => [
                'style' => 'text-align: right; min-width: 50px;'
            ],
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', [
                        '/admin/product/update',
                        'id' => $model->id
                    ], [
                        'title' => Yii::t('system', 'Edit'),
                        'style' => 'margin-right: 5px',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                        '/admin/product/delete',
                        'id' => $model->id
                    ], [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                },
            ]
        ]
    ]
]);
Pjax::end();
<?php
/**
 * @var $this yii\web\View
 * @var $content
 */

use dosamigos\ckeditor\CKEditorInline;
use yii\helpers\Html;

?>
<hr/>
<?php
$form = \kartik\widgets\ActiveForm::begin();
echo Html::submitButton('Save', ['class' => 'js-submit-content btn btn-success']);
?>
<hr/>
<?php
echo \dosamigos\ckeditor\CKEditor::widget([
    'options' => ['rows' => 6],
    'preset' => 'full',
    'name' => 'Editor_content',
    'value' => $content,
]);
?>
<hr/>
<?= Html::submitButton('Save', ['class' => 'js-submit-content btn btn-success']) ?>
<?php $form->end() ?>


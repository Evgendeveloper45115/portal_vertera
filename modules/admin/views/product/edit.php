<?php
/**
 * @var $this yii\web\View
 * @var $model Product|\app\behaviors\TranslationBehavior
 */

use yii\helpers\{
    ArrayHelper,
    Html
};
use yii\bootstrap\{
    ActiveForm,
    Modal
};
use kartik\widgets\TouchSpin;
use app\models\{
    Product,
    ProductSubCategory
};
use app\helpers\FileInput;
use app\helpers\H;
use app\models\ObjectFile;
use yii\helpers\Url;

$this->title = $model->isNewRecord ? 'Добавление продукта' : 'Редактирование продукта';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Список продуктов',
        'url' => ['/admin/product'],
    ],
    $this->title,
];
$this->registerJsFile(Yii::getAlias('@web/js/field-translate-actions.js'), [
    'depends' => [\app\assets\AdminAsset::class]
]);
?>
    <div class="Project-index">
        <h1><?= $this->title ?></h1>
        <small>Для редактирования перевода поля, зажмите клавишу - <b>ctrl</b> и <b>левой кнопкой мышки</b> кликните по
            полю.
        </small>
        <div style="margin: 5px">
            <?php echo \yii\bootstrap\Html::a(Yii::t('admin', 'View product on the website'), Url::to(['/product/view/?id=' . $model->id]), ['class' => 'btn btn-success'])?>
        </div>
        <hr/>
        <?php $form = ActiveForm::begin(['options' => [
            'enctype' => 'multipart/form-data',
            'data-is-new-record' => $model->getIsNewRecord()
        ]]) ?>

        <div class="row">
            <div class="col-md-12">
                <?php
                $model->categories = ArrayHelper::map($model->getCategories(), 'sub_category_id', 'sub_category_id');
                echo $form->field($model, 'categories')->widget(\kartik\select2\Select2::class, [
                    'data' => ArrayHelper::map(ProductSubCategory::find()->all(), 'id', 'name', 'parent.name'),
                    'language' => Yii::$app->language,
                    'options' => [
                        'multiple' => true,
                        'placeholder' => 'Выберите из списка...'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'status')->dropDownList(Product::getStatuses()) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'url_name') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'in_top')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>
            </div>
        </div>

        <hr/>

        <div class="row">
            <div class="col-md-6">
                <?php H::render_language_file_field($form, $model, 'image_id') ?>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'title')->textInput($model->buildOptions('title')) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><?= $form->field($model, 'title_description')
                            ->textarea($model->buildOptions('title_description')) ?></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><?= $form->field($model, 'vendor_code') ?></div>
                </div>
                <div class="row">
                    <div class="col-sm-3"><?= $form->field($model, 'cost')->widget(TouchSpin::class, [
                            'pluginOptions' => [
                                'min' => 0,
                                'max' => 999999,
                                'verticalbuttons' => true,
                            ],
                        ]) ?></div>
                    <div class="col-sm-3"><?= $form->field($model, 'cost_original')->widget(TouchSpin::class, [
                            'pluginOptions' => [
                                'min' => 0,
                                'max' => 999999,
                                'verticalbuttons' => true,
                            ],
                        ]) ?></div>
                    <div class="col-sm-3"><?= $form->field($model, 'point')->widget(TouchSpin::class, [
                            'pluginOptions' => [
                                'min' => 0,
                                'max' => 999999,
                                'verticalbuttons' => true,
                            ],
                        ]) ?></div>
                    <div class="col-sm-3"><?= $form->field($model, 'amount')->textInput($model->buildOptions('amount')) ?></div>
                </div>
            </div>
        </div>

        <hr/>

        <div class="row">
            <div class="col-md-12">
                <?= H::render_languages_editor_field($form, $model, 'tab_description') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= H::render_languages_editor_field($form, $model, 'tab_composition') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= H::render_languages_editor_field($form, $model, 'tab_use') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php H::render_language_file_field($form, $model, 'description_image_id') ?>
            </div>
        </div>

        <hr/>

        <div class="row">
            <div class="col-md-12"><?= $form->field($model, 'info_text')->textarea($model->buildOptions('info_text')) ?></div>
        </div>

        <div class="row">
            <div class="col-md-12"><?= $form->field($model, 'info_image_id')->widget(FileInput::class, [
                    'options' => ['accept' => 'image/*'],
                    'objectFiles' => $model->getInfo_image(),
                ]) ?></div>
        </div>

        <hr/>

        <div class="row">
            <div class="col-md-12"><?= $form->field($model, 'certificates')->widget(FileInput::class, [
                    'id' => 'certicates-field',
                    'deleteUrlCallback' => Url::to(['/admin/product/delete-certificate']),
                    'options' => [
                        'accept' => 'image/*',
                        'multiple' => true
                    ],
//                     'pluginOptions' => [
//                         'uploadUrl' => Url::to(['/admin/product/update?id=' . $model->id]),
//                         'maxFileCount' => 10
//                     ],
                    'objectFiles' => $model->getCertificates(),
                ]) ?></div>
        </div>

        <hr/>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php $form->end() ?>
    </div>
<?php
Modal::begin([
    'header' => '<h3>Переводы для поля "<span></span>"</h3>',
    'size' => Modal::SIZE_LARGE,
    'options' => [
        'id' => 'lang-form-modal',
    ],
]);
Modal::end();
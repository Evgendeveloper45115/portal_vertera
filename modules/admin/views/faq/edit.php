<?php
/**
 * @var $this yii\web\View
 * @var $model \app\models\Faq|object
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\FaqCategory;

$this->title = $model->isNewRecord
    ? 'Добавление faq'
    : 'Редактирование faq';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Список faq',
        'url' => ['/admin/faq'],
    ],
    $this->title,
];
$this->registerJsFile(Yii::getAlias('@web/js/field-translate-actions.js'), [
    'depends' => [\app\assets\AdminAsset::class]
]);
?>
<div class="Project-index">
    <h1><?= $this->title ?></h1>
    <small>Для редактирования перевода поля, зажмите клавишу - <b>ctrl</b> и <b>левой кнопкой мышки</b> кликните по
        полю.
    </small>
    <hr/>
    <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => [
        'enctype' => 'multipart/form-data',
        'data-is-new-record' => $model->getIsNewRecord()
    ]]) ?>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'theme')->textInput($model->buildOptions('theme')) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'client_information')->textInput($model->buildOptions('client_information')) ?></div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'category_id')->dropDownList(
                ArrayHelper::map(FaqCategory::findAll(['status' => FaqCategory::STATUS_ACTIVE]), 'id', 'name')
            ) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'in_top')->dropDownList([0 => 'Нет', 1 => 'Да']) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'status')->dropDownList(FaqCategory::getStatuses()) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'question')->textarea($model->buildOptions('question')) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'answer')->textarea($model->buildOptions('answer')) ?></div>
    </div>
    <hr/>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

    <?php $form->end() ?>
</div>
<?php
\yii\bootstrap\Modal::begin([
    'header' => '<h3>Переводы для поля "<span></span>"</h3>',
    'options' => [
        'id' => 'lang-form-modal'
    ],
]);
\yii\bootstrap\Modal::end();
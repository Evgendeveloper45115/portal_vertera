<?php
/**
 * @var $this yii\web\View
 * @var $searchData \goodizer\helpers\SearchData
 * @var $user \yii\web\User
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use app\models\Faq;
use app\models\FaqCategory;

$this->title = 'Список faq';
$this->params['breadcrumbs'] = [
    $this->title,
];
?>
    <h1><?= $this->title ?></h1>
<?php
echo Html::a('Создать', ['/admin/faq/create'], [
    'class' => 'btn btn-success'
]);

echo '<hr />';

Pjax::begin();
echo \yii\grid\GridView::widget([
    'filterModel' => $searchData->filterModel,
    'dataProvider' => $searchData->dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-cell-valign-middle'
    ],
    'columns' => [
        'id',
        [
            'attribute' => 'category_id',
            'value' => 'category.name',
            'filter' => ArrayHelper::map(FaqCategory::find()->all(), 'id', 'name'),
        ],
        [
            'attribute' => 'theme',
            'value' => function (Faq $model) {
                return $model->theme;
            },
        ],
        [
            'attribute' => 'client_information',
            'value' => function (Faq $model) {
                return $model->client_information;
            },
        ],
        [
            'attribute' => 'created_at',
            'filter' => DatePicker::widget([
                'model' => $searchData->filterModel,
                'attribute' => 'created_at',
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Выбрать дату',
                ],
                'pickerButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true,
                ]
            ]),
            'headerOptions' => [
                'style' => 'min-width: 200px;'
            ],
            'value' => function (Faq $model) {
                return $model->created_at;
            },
        ],
        [
            'attribute' => 'in_top',
            'format' => ['html'],
            'filter' => false,
            'contentOptions' => [
                'class' => 'text-center'
            ],
            'value' => function (Faq $model) {
                $cls = $model->in_top ? 'success' : 'danger';

                return Html::tag('span', $model->in_top ? 'Да' : 'Нет', ['class' => "label label-{$cls}"]);
            },
        ],
        [
            'attribute' => 'status',
            'format' => ['html'],
            'filter' => Faq::getStatuses(),
            'value' => function (Faq $model) {
                if ($model->status === null)
                    return null;

                $cls = $model->status == Faq::STATUS_ACTIVE ? 'success' : 'danger';

                return Html::tag('span', Faq::getStatuses($model->status), ['class' => "label label-{$cls}"]);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => '&#160;',
            'contentOptions' => [
                'style' => 'text-align: right; width: 70px;'
            ],
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', [
                        '/admin/faq/update',
                        'id' => $model->id
                    ], [
                        'title' => \Yii::t('system', 'Edit'),
                        'style' => 'margin-right: 5px',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                        '/admin/faq/delete',
                        'id' => $model->id
                    ], [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                },
            ]
        ]
    ]
]);
Pjax::end();
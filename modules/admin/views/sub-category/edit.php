<?php
/**
 * @var $this yii\web\View
 * @var $model app\models\ProductSubCategory|object
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use app\models\ProductCategory;
use app\models\ProductSubCategory;

$this->title = $model->isNewRecord ? 'Добавление подкатегории' : 'Редактирование подкатегории';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Список подкатегорий',
        'url' => ['/admin/sub-category'],
    ],
    $this->title,
];
$this->registerJsFile(Yii::getAlias('@web/js/field-translate-actions.js'), [
    'depends' => [\app\assets\AdminAsset::class]
]);
?>
    <div class="Project-index">
        <h1><?= $this->title ?></h1>
        <small>Для редактирования перевода поля, зажмите клавишу - <b>ctrl</b> и <b>левой кнопкой мышки</b> кликните по полю.</small>

        <hr/>
        <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => [
            'enctype' => 'multipart/form-data',
            'data-is-new-record' => $model->getIsNewRecord()
        ]]) ?>

        <div class="row">
            <div class="col-md-4"><?= $form
                    ->field($model, 'parent_id')
                    ->dropDownList(ArrayHelper::map(ProductCategory::find()->all(), 'id', 'name')) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'name')->textInput($model->buildOptions('name')) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'status')->dropDownList(ProductSubCategory::getStatuses()) ?></div>
        </div>
        <div class="row">
            <div class="col-md-12"><?= $form->field($model, 'description')->textInput($model->buildOptions('description')) ?></div>
        </div>
        <hr/>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php $form->end() ?>
    </div>
<?php
Modal::begin([
    'header' => '<h3>Переводы для поля "<span></span>"</h3>',
    'options' => [
        'id' => 'lang-form-modal'
    ],
]);
Modal::end();
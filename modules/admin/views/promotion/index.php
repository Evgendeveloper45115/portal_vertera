<?php
/**
 * @var $this yii\web\View
 * @var $searchData \goodizer\helpers\SearchData
 * @var $user \yii\web\User
 */

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\Product;
use app\models\Promotion;

$this->title = 'Список акций';
$this->params['breadcrumbs'] = [
    $this->title,
];
?>
    <h1><?= $this->title ?></h1>
<?php
echo Html::a('Создать', ['/admin/promotion/create'], [
    'class' => 'btn btn-success'
]);

echo '<hr />';

Pjax::begin();
echo \yii\grid\GridView::widget([
    'filterModel' => $searchData->filterModel,
    'dataProvider' => $searchData->dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-cell-valign-middle'
    ],
    'columns' => [
        'id' => [
            'attribute' => 'id',
        ],
        [
            'attribute' => 'image_id',
            'format' => ['html'],
            'value' => function (Promotion $model) {
                return Html::img(\app\models\ObjectFile::getThumbnail($model, 'image', [200, 400]));
            },
        ],
        'product_id' => [
            'attribute' => 'product_id',
            'value' => function (Promotion $model) {
                if(!$p = Product::findOne($model->product_id))
                    return null;

                return $p->title;
            },
        ],
        'title' => [
            'attribute' => 'title',
            'value' => function (Promotion $model) {
                return $model->title;
            },
        ],
        'start_date' => [
            'attribute' => 'start_date',
            'value' => function (Promotion $model) {
                return $model->start_date;
            },
        ],
        'end_date' => [
            'attribute' => 'end_date',
            'value' => function (Promotion $model) {
                return $model->end_date;
            },
        ],
        'status' => [
            'attribute' => 'status',
            'value' => function (Promotion $model) {
                return Promotion::getStatuses($model->status);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => '&#160;',
            'contentOptions' => [
                'style' => 'text-align: right; width: 70px;'
            ],
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', [
                        '/admin/promotion/update',
                        'id' => $model->id
                    ], [
                        'title' => \Yii::t('system', 'Edit'),
                        'style' => 'margin-right: 5px',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                        '/admin/promotion/delete',
                        'id' => $model->id
                    ], [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                },
            ]
        ]
    ]
]);
Pjax::end();
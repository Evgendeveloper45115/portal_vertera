<?php
/**
 * @var $this yii\web\View
 * @var $model MainSliderHeader
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\{
    TouchSpin,
    DatePicker
};
use app\helpers\H;
use app\models\MainSliderHeader;

$this->title = $model->isNewRecord
    ? 'Добавление обьекта для слайдера на главной'
    : 'Редактирование обьекта для слайдера на главной';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Список обьектов для слайдера',
        'url' => ['/admin/main-slider'],
    ],
    $this->title,
];
?>
<div class="Project-index">
    <h1><?= $this->title ?></h1>
    <hr/>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'url') ?></div>
        <div class="col-md-3"><?= $form->field($model, 'status')->dropDownList($model::getStatuses()) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'sort_order')->widget(TouchSpin::class, [
                'options' => [
                    'placeholder' => '№',
                ],
                'pluginOptions' => [
                    'min' => 0,
                    'max' => 999,
                    'verticalbuttons' => true,
                ],
            ]) ?></div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'start_date')->widget(DatePicker::class, [
                'options' => [
                    'placeholder' => 'Выбрать дату',
                    'autocomplete' => 'off',
                ],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'end_date')->widget(DatePicker::class, [
                'options' => [
                    'placeholder' => 'Выбрать дату',
                    'autocomplete' => 'off',
                ],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php H::render_language_file_field($form, $model, 'image_id') ?>
        </div>
    </div>
    <hr/>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

    <?php $form->end() ?>
</div>
<?php
/**
 * @var $this yii\web\View
 * @var $searchData \goodizer\helpers\SearchData
 * @var $user \yii\web\User
 */

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\MainSliderHeader;
use app\models\ObjectFile;

$this->title = 'Список обьектов для слайдера';
$this->params['breadcrumbs'] = [
    $this->title,
];
?>
    <h1><?= $this->title ?></h1>
<?php
echo Html::a('Создать', ['/admin/main-slider/create'], [
    'class' => 'btn btn-success'
]);

echo '<hr />';

Pjax::begin();
echo \himiklab\sortablegrid\SortableGridView::widget([
    'filterModel' => $searchData->filterModel,
    'dataProvider' => $searchData->dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-cell-valign-middle'
    ],
    'columns' => [
//        'sort_order' => [
//            'attribute' => 'sort_order',
//        ],
        'id' => [
            'attribute' => 'id',
        ],
        [
            'attribute' => 'image_id',
            'format' => ['html'],
            'value' => function (MainSliderHeader $model) {
                return Html::img(ObjectFile::getThumbnail($model, 'image', [200, 400]));
            },
        ],
        [
            'attribute' => 'url',
            'value' => function (MainSliderHeader $model) {
                return $model->url;
            },
        ],
        'start_date' => [
            'attribute' => 'start_date',
            'value' => function (MainSliderHeader $model) {
                return date('Y-m-d', strtotime($model->start_date));
            },
        ],
        'end_date' => [
            'attribute' => 'end_date',
            'value' => function (MainSliderHeader $model) {
                return date('Y-m-d', strtotime($model->end_date));
            },
        ],
        [
            'attribute' => 'status',
            'format' => ['html'],
            'filter' => MainSliderHeader::getStatuses(),
            'value' => function (MainSliderHeader $model) {
                if ($model->status === null)
                    return null;

                $cls = $model->status == MainSliderHeader::STATUS_ACTIVE ? 'success' : 'danger';

                return Html::tag('span', MainSliderHeader::getStatuses($model->status), ['class' => "label label-{$cls}"]);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => '&#160;',
            'contentOptions' => [
                'style' => 'text-align: right; width: 70px;'
            ],
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', [
                        '/admin/main-slider/update',
                        'id' => $model->id
                    ], [
                        'title' => \Yii::t('system', 'Edit'),
                        'style' => 'margin-right: 5px',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                        '/admin/main-slider/delete',
                        'id' => $model->id
                    ], [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                },
            ]
        ]
    ]
]);
Pjax::end();
<?php
/**
 * @var $this yii\web\View
 * @var $translation FieldTranslation
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use app\models\FieldTranslation;

?>
<div class="field-translate-container">
    <?php $form = ActiveForm::begin(['options' => [
        'class' => 'field-translate-form',
        'enctype' => 'multipart/form-data',
    ]]) ?>

    <div class="row">
        <div class="col-md-12"><?= $form->field($translation, "field_value")->widget(CKEditor::class, [
                    'options' => ['rows' => 4],
                    'preset' => 'full',
                ])->label($translation->getFullLanguage()) ?></div>
    </div>
    <hr/>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

    <?php $form->end() ?>
</div>
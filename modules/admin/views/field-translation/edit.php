<?php
/**
 * @var $this yii\web\View
 * @var $translations FieldTranslation[]
 */

use yii\helpers\Html;
use app\models\FieldTranslation;

?>
<div class="field-translate-container">
    <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => [
        'class' => 'field-translate-form',
        'enctype' => 'multipart/form-data',
    ]]) ?>

    <div class="row">
        <?php foreach ($translations as $model) { ?>
            <div class="col-md-12"><?= $form
                    ->field($model, "[{$model->language}]field_value")
                    ->textarea()
                    ->label($model->getFullLanguage()) ?></div>
        <?php } ?>
    </div>
    <hr/>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

    <?php $form->end() ?>
</div>
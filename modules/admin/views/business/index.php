<?php
/**
 * @var $this yii\web\View
 * @var $searchData \goodizer\helpers\SearchData
 * @var $user \yii\web\User
 */

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use app\models\Business;
use app\models\ObjectFile;

$this->title = 'Список бизнес-событий';
$this->params['breadcrumbs'] = [
    $this->title,
];
?>
    <h1><?= $this->title ?></h1>
<?php
echo Html::a('Создать', ['/admin/business/create'], [
    'class' => 'btn btn-success'
]);

echo '<hr />';

Pjax::begin();
echo \yii\grid\GridView::widget([
    'filterModel' => $searchData->filterModel,
    'dataProvider' => $searchData->dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-cell-valign-middle'
    ],
    'columns' => [
        [
            'attribute' => 'id',
        ],
        [
            'attribute' => 'image_id',
            'format' => ['html'],
            'enableSorting' => false,
            'value' => function (Business $model) {
                return Html::img(ObjectFile::getThumbnail($model, 'image', [200, 80]), ['height' => 80]);
            },
        ],
        [
            'attribute' => 'title',
            'value' => function (Business $model) {
                return $model->title;
            },
        ],
        [
            'attribute' => 'total_speakers',
            'format' => ['html'],
            'value' => function (Business $model) {
                return $model->getSpeakersQuery()->where(['bhs.business_id' => $model->id])->count();
            },
        ],
        [
            'attribute' => 'type',
            'filter' => Business::$types,
            'value' => function (Business $model) {
                return Business::$types[$model->type];
            },
        ],
        [
            'attribute' => 'conference_type',
            'filter' => Business::$conference_types,
            'value' => function (Business $model) {
                return Business::$conference_types[$model->conference_type];
            },
        ],
        [
            'attribute' => 'start_date',
            'filter' => DatePicker::widget([
                'model' => $searchData->filterModel,
                'attribute' => 'start_date',
                'options' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Выбрать дату',
                ],
                'pickerButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true,
                ]
            ]),
            'headerOptions' => [
                'style' => 'min-width: 200px;'
            ],
            'value' => function (Business $model) {
                return date('Y-m-d', strtotime($model->start_date));
            },
        ],
        'in_top' => [
            'attribute' => 'in_top',
            'format' => ['html'],
            'filter' => false,
            'contentOptions' => [
                'class' => 'text-center'
            ],
            'value' => function (Business $model) {
                $cls = $model->in_top ? 'success' : 'danger';

                return Html::tag('span', $model->in_top ? 'Да' : 'Нет', ['class' => "label label-{$cls}"]);
            },
        ],
        [
            'attribute' => 'status',
            'format' => ['html'],
            'filter' => Business::getStatuses(),
            'value' => function (Business $model) {
                if ($model->status === null)
                    return null;

                $cls = $model->status == Business::STATUS_ACTIVE ? 'success' : 'danger';

                return Html::tag('span', Business::getStatuses($model->status), ['class' => "label label-{$cls}"]);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => '&#160;',
            'headerOptions' => [
                'style' => 'min-width: 50px;'
            ],
            'contentOptions' => [
                'style' => 'text-align: right;'
            ],
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', [
                        '/admin/business/update',
                        'id' => $model->id
                    ], [
                        'title' => \Yii::t('system', 'Edit'),
                        'style' => 'margin-right: 5px',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                        '/admin/business/delete',
                        'id' => $model->id
                    ], [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                },
            ]
        ]
    ]
]);
Pjax::end();
<?php
/**
 * @var $this yii\web\View
 * @var $model Business|object
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use kartik\widgets\DateTimePicker;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use app\models\Business;
use app\models\Speaker;
use app\helpers\H;

$this->title = $model->isNewRecord ? 'Добавление бизнес-события' : 'Редактирование бизнес-события';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Список бизнес-событий',
        'url' => ['/admin/business'],
    ],
    $this->title,
];
$this->registerJsFile(Yii::getAlias('@web/js/field-translate-actions.js'), [
    'depends' => [\app\assets\AdminAsset::class]
]);
?>
    <div class="Project-index">
        <h1><?= $this->title ?></h1>
        <small>Для редактирования перевода поля, зажмите клавишу - <b>ctrl</b> и <b>левой кнопкой мышки</b> кликните по
            полю.
        </small>
        <hr/>
        <?php $form = ActiveForm::begin(['options' => [
            'enctype' => 'multipart/form-data',
            'data-is-new-record' => $model->getIsNewRecord()
        ]]) ?>

        <div class="row">
            <div class="col-md-6"><?= $form->field($model, 'title')->textInput($model->buildOptions('title')) ?></div>
            <div class="col-md-6">
                <?php
                $model->speakers = ArrayHelper::map($model->getSpeakers(), 'id', 'id');
                echo $form->field($model, 'speakers')->widget(Select2::class, [
                    'data' => ArrayHelper::map(Speaker::find()->all(), 'id', 'full_name'),
                    'language' => Yii::$app->language,
                    'options' => [
                        'multiple' => true,
                        'placeholder' => 'Выберите из списка...'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"><?= $form->field($model, 'sub_description')->textarea($model->buildOptions('sub_description')) ?></div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= H::render_languages_editor_field($form, $model, 'description') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"><?= $form->field($model, 'type')->dropDownList(Business::$types) ?></div>
            <div class="col-md-6"><?= $form->field($model, 'conference_type')->dropDownList(Business::$conference_types) ?></div>
        </div>
        <div class="row">
            <div class="col-md-6"><?= $form->field($model, 'button_text')->textInput($model->buildOptions('button_text')) ?></div>
            <div class="col-md-6"><?= $form->field($model, 'button_url')->textInput($model->buildOptions('button_url')) ?></div>
        </div>
        <div class="row">
            <div class="col-md-4"><?= $form->field($model, 'start_date')->widget(DateTimePicker::class, [
                    'removeButton' => false,
                    'pickerButton' => ['icon' => 'time'],
                    'options' => ['placeholder' => 'Выбрать дату'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy hh:ii',
                        'todayHighlight' => true,
                    ]
                ]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'start_date_text') ?></div>
            <div class="col-md-2"><?= $form->field($model, 'status')->dropDownList($model::getStatuses()) ?></div>
            <div class="col-md-2"><?= $form->field($model, 'in_top')->dropDownList([0 => 'Нет', 1 => 'Да']) ?></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php H::render_language_file_field($form, $model, 'image_id') ?>
            </div>
        </div>
        <hr/>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php $form->end() ?>
    </div>
<?php
Modal::begin([
    'header' => '<h3>Переводы для поля "<span></span>"</h3>',
    'size' => Modal::SIZE_LARGE,
    'options' => [
        'id' => 'lang-form-modal'
    ],
]);
Modal::end();
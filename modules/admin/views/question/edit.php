<?php
/**
 * @var $this yii\web\View
 * @var $model Question|object
 */

use yii\helpers\Html;
use app\models\Question;

$this->title = $model->isNewRecord
    ? 'Добавление вопроса'
    : 'Редактирование вопросв';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Список вопросов',
        'url' => ['/admin/question'],
    ],
    $this->title,
];
$this->registerJsFile(Yii::getAlias('@web/js/field-translate-actions.js'), [
    'depends' => [\app\assets\AdminAsset::class]
]);
?>
<div class="Project-index">
    <h1><?= $this->title ?></h1>
    <small>Для редактирования перевода поля, зажмите клавишу - <b>ctrl</b> и <b>левой кнопкой мышки</b> кликните по
        полю.
    </small>
    <hr/>
    <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => [
        'enctype' => 'multipart/form-data',
        'data-is-new-record' => $model->getIsNewRecord()
    ]]) ?>

    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'name')->textInput($model->buildOptions('name')) ?></div>
        <div class="col-md-8"><?= $form->field($model, 'email') ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'type')->dropDownList(Question::$types) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'status')->dropDownList(Question::getStatuses()) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'subject')->textarea($model->buildOptions('subject')) ?></div>
    </div>
    <hr/>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

    <?php $form->end() ?>
</div>
<?php
\yii\bootstrap\Modal::begin([
    'header' => '<h3>Переводы для поля "<span></span>"</h3>',
    'options' => [
        'id' => 'lang-form-modal'
    ],
]);
\yii\bootstrap\Modal::end();
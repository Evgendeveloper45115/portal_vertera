<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;
use app\widgets\Alert;

/**
 * @var $this \yii\web\View
 * @var $content string
 * @var $user app\models\User
 */

AdminAsset::register($this);

$ctrl = Yii::$app->controller;
$ctrlName = $ctrl->id;
$actionName = $ctrl->action->id;
$am = Yii::$app->getAssetManager();

$user = Yii::$app->user->identity;
$type = isset($ctrl->actionParams['type']) ? $ctrl->actionParams['type'] : null;

$this->registerCssFile($am->publish(Yii::getAlias('@app/modules/admin/assets/css/custom.css'))[1], [
    'depends' => [AdminAsset::class]
]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="wrapper" class="content-wrapper">
    <?php
    NavBar::begin();
    echo Nav::widget([
        'items' => [
            [
                'label' => 'Товары',
                'url' => ['/admin/product'],
                'active' => $ctrlName == 'product' || $ctrlName == 'category' || $ctrlName == 'sub-category',
                'items' => [
                    [
                        'label' => 'Список',
                        'url' => ['/admin/product'],
                        'active' => $ctrlName == 'product',
                    ],
                    [
                        'label' => 'Категории',
                        'url' => ['/admin/category'],
                        'active' => $ctrlName == 'category',
                    ],
                    [
                        'label' => 'Подкатегории',
                        'url' => ['/admin/sub-category'],
                        'active' => $ctrlName == 'sub-category',
                    ],
                ],
            ],
            [
                'label' => 'Акции',
                'url' => ['/admin/promotion'],
                'active' => $ctrlName == 'promotion',
            ],
            [
                'label' => 'Бизнес',
                'url' => ['/admin/business'],
                'active' => $ctrlName == 'business',
            ],
            [
                'label' => 'Новости',
                'url' => ['/admin/news'],
                'active' => $ctrlName == 'news',
            ],
            [
                'label' => 'Спикеры',
                'url' => ['/admin/speaker'],
                'active' => $ctrlName == 'speaker',
            ],
            [
                'label' => 'Контакты',
                'url' => ['/admin/contact'],
                'active' => $ctrlName == 'contact',
            ],
            [
                'label' => 'Баннеры',
                'url' => ['/admin/main-slider'],
                'active' => $ctrlName == 'main-slider',
            ],
            [
                'label' => 'Вопросы',
                'url' => ['/admin/question'],
                'active' => $ctrlName == 'question',
            ],
            [
                'label' => 'FAQ',
                'url' => ['/admin/faq'],
                'active' => $ctrlName == 'faq' || $ctrlName == 'faq-category',
                'items' => [
                    [
                        'label' => 'Список',
                        'url' => ['/admin/faq'],
                        'active' => $ctrlName == 'faq',
                    ],
                    [
                        'label' => 'Категории',
                        'url' => ['/admin/faq-category'],
                        'active' => $ctrlName == 'faq-category',
                    ],
                ]
            ],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]);
    echo Html::tag('div', '', ['class' => 'navbar-nav--divider']);
    echo Nav::widget([
        'items' => [
            [
                'label' => 'Перейти на клиента',
                'url' => ['/'],
            ],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]);
    echo Nav::widget([
        'items' => [
            [
                'label' => Yii::t('labels', 'Login'),
                'url' => ['/site/login'],
                'visible' => Yii::$app->getUser()->isGuest,
            ],
            [
                'label' => 'Выход(' . ($user ? $user->username : null) . ')',
                'url' => ['/site/logout'],
                'visible' => !Yii::$app->getUser()->isGuest,
            ],
        ],
        'options' => ['class' => 'navbar-nav pull-right'],
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Alert::widget() ?>
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'url' => \yii\helpers\Url::to(['/admin']),
                'label' => Yii::t('yii', 'Home'),
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>
<div class="footer-wrapper">
    <div class="footer" style="padding: 20px; margin-top: 10px; border-top: 1px solid #e7e7e7;">
        <p class="pull-left">&copy; vertera <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php
/**
 * @var $this yii\web\View
 * @var $model Contact|object
 */

use yii\helpers\Html;
use app\models\Contact;

$this->title = $model->isNewRecord ? 'Добавление контакта' : 'Редактирование контакта';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Список контактов',
        'url' => ['/admin/contact'],
    ],
    $this->title,
];
$apiKey = 'AIzaSyBnVNkAhczDSQjXtZhzQzy6laG6OX6rGMs';
$this->registerJsFile(Yii::getAlias('@web/js/field-translate-actions.js'), [
    'depends' => [\app\assets\AdminAsset::class]
]);
$this->registerJsFile("https://maps.googleapis.com/maps/api/js?key={$apiKey}&libraries=places&callback=initMap", [
    'async' => true, 'defer' => true, 'depends' => [\app\assets\AppAsset::class]
]);
$this->registerJsFile(Yii::getAlias('@web/js/contact-map.js'), [
    'depends' => [\app\assets\AppAsset::class]
]);
?>
    <div class="Project-index">
        <h1><?= $this->title ?></h1>
        <small>Для редактирования перевода поля, зажмите клавишу - <b>ctrl</b> и <b>левой кнопкой мышки</b> кликните по
            полю.
        </small>
        <hr/>
        <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => [
            'enctype' => 'multipart/form-data',
            'data-is-new-record' => $model->getIsNewRecord()
        ]]) ?>
        <?= Html::activeHiddenInput($model, 'map_lat', ['class' => 'js-map-lat-field']) ?>
        <?= Html::activeHiddenInput($model, 'map_lng', ['class' => 'js-map-lng-field']) ?>
        <div class="row">
            <div class="col-md-3"><?= $form->field($model, 'country_name')->textInput($model->buildOptions('country_name')) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'city_name')->textInput($model->buildOptions('city_name')) ?></div>
            <div class="col-md-5"><?= $form->field($model, 'address')->textInput(['class' => 'form-control js-address-field']) ?></div>
        </div>
        <div class="row">
            <div class="col-md-12"><?= $form->field($model, 'map_enable')->checkbox([
                    'class' => 'js-enable-map',
                    'onchange' => 'this.checked ? $("#map").show() : $("#map").hide()',
                ]) ?></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input id="pac-input" class="map-search-controls" type="text" placeholder="Поиск...">
                <div id="map" style="height: 400px; display: <?= $model->map_enable ? 'block' : 'none'?>;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"><?= $form->field($model, 'phone') ?></div>
            <div class="col-md-4"><?= $form->field($model, 'email') ?></div>
            <div class="col-md-4"><?= $form->field($model, 'status')->dropDownList(Contact::getStatuses()) ?></div>
        </div>
        <hr/>
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

        <?php $form->end() ?>
    </div>
<?php
\yii\bootstrap\Modal::begin([
    'header' => '<h3>Переводы для поля "<span></span>"</h3>',
    'options' => [
        'id' => 'lang-form-modal'
    ],
]);
\yii\bootstrap\Modal::end();
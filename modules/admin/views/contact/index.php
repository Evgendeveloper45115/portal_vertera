<?php
/**
 * @var $this yii\web\View
 * @var $searchData \goodizer\helpers\SearchData
 * @var $user \yii\web\User
 */

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\Contact;

$this->title = 'Список контактов';
$this->params['breadcrumbs'] = [
    $this->title,
];
?>
    <h1><?= $this->title ?></h1>
<?php
echo Html::a('Создать', ['/admin/contact/create'], [
    'class' => 'btn btn-success'
]);

echo '<hr />';

Pjax::begin();
echo \yii\grid\GridView::widget([
    'filterModel' => $searchData->filterModel,
    'dataProvider' => $searchData->dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-cell-valign-middle'
    ],
    'columns' => [
        'id' => [
            'attribute' => 'id',
        ],
        'country_name' => [
            'attribute' => 'country_name',
            'value' => function (Contact $model) {
                return $model->country_name;
            },
        ],
        'city_name' => [
            'attribute' => 'city_name',
            'value' => function (Contact $model) {
                return $model->city_name;
            },
        ],
        'address' => [
            'attribute' => 'address',
            'value' => function (Contact $model) {
                return $model->address;
            },
        ],
        'phone' => [
            'attribute' => 'phone',
            'value' => function (Contact $model) {
                return $model->phone;
            },
        ],
        'email' => [
            'attribute' => 'email',
            'value' => function (Contact $model) {
                return $model->email;
            },
        ],
        'map_enable' => [
            'attribute' => 'map_enable',
            'filter' => [1 => 'Да', 0 => 'Нет'],
            'value' => function (Contact $model) {
                return $model->map_enable ? 'Да' : 'Нет';
            },
        ],
        'status' => [
            'attribute' => 'status',
            'filter' => Contact::getStatuses(),
            'value' => function (Contact $model) {
                if (!$model->status) {
                    return null;
                }
                return Contact::getStatuses($model->status);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => '&#160;',
            'contentOptions' => [
                'style' => 'text-align: right; width: 70px;'
            ],
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', [
                        '/admin/contact/update',
                        'id' => $model->id
                    ], [
                        'title' => \Yii::t('system', 'Edit'),
                        'style' => 'margin-right: 5px',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                        '/admin/contact/delete',
                        'id' => $model->id
                    ], [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                },
            ]
        ]
    ]
]);
Pjax::end();
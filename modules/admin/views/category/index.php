<?php
/**
 * @var $this yii\web\View
 * @var $searchData \goodizer\helpers\SearchData
 * @var $user \yii\web\User
 */

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\ProductCategory;

$this->title = 'Список категорий';
$this->params['breadcrumbs'] = [
    $this->title,
];
?>
    <h1><?= $this->title ?></h1>
<?php
echo Html::a('Создать', ['/admin/category/create'], [
    'class' => 'btn btn-success'
]);

echo '<hr />';

Pjax::begin();
echo \yii\grid\GridView::widget([
    'filterModel' => $searchData->filterModel,
    'dataProvider' => $searchData->dataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-cell-valign-middle'
    ],
    'columns' => [
        'id' => [
            'attribute' => 'id',
        ],
        'name' => [
            'attribute' => 'name',
            'value' => function (ProductCategory $model) {
                return $model->name ;
            },
        ],
        'status' => [
            'attribute' => 'status',
            'filter' => ProductCategory::getStatuses(),
            'value' => function (ProductCategory $model) {
                return ProductCategory::getStatuses($model->status);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => '&#160;',
            'contentOptions' => [
                'style' => 'text-align: right; width: 70px;'
            ],
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', [
                        '/admin/category/update',
                        'id' => $model->id
                    ], [
                        'title' => \Yii::t('system', 'Edit'),
                        'style' => 'margin-right: 5px',
                        'data-pjax' => '0',
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                        '/admin/category/delete',
                        'id' => $model->id
                    ], [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                },
            ]
        ]
    ]
]);
Pjax::end();
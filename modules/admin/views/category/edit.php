<?php
/**
 * @var $this yii\web\View
 * @var $model app\models\ProductCategory|object
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;
use app\models\ProductCategory;

$this->title = $model->isNewRecord ? 'Добавление категории' : 'Редактирование категории';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Список категорий',
        'url' => ['/admin/category'],
    ],
    $this->title,
];
$this->registerJsFile(Yii::getAlias('@web/js/field-translate-actions.js'), [
    'depends' => [\app\assets\AdminAsset::class]
]);
?>
<div class="Project-index">
    <h1><?= $this->title ?></h1>
    <small>Для редактирования перевода поля, зажмите клавишу - <b>ctrl</b> и <b>левой кнопкой мышки</b> кликните по полю.</small>

    <hr/>
    <?php $form = \yii\bootstrap\ActiveForm::begin(['options' => [
        'enctype' => 'multipart/form-data',
        'data-is-new-record' => $model->getIsNewRecord()
    ]]) ?>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'name')->textInput($model->buildOptions('name')) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'status')->dropDownList(ProductCategory::getStatuses()) ?></div>
    </div>
    <div class="row">
        <div class="col-md-12"><?= $form->field($model, 'description')->textInput($model->buildOptions('description')) ?></div>
    </div>
    <hr/>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

    <?php $form->end() ?>
</div>
<?php
Modal::begin([
    'header' => '<h3>Переводы для поля "<span></span>"</h3>',
    'options' => [
        'id' => 'lang-form-modal'
    ],
]);
Modal::end();
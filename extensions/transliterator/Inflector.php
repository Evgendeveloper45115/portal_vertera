<?php
namespace app\extensions\transliterator;

/**
 * Class Inflector
 * @package extensions
 */
class Inflector extends \yii\helpers\Inflector
{
    /**
     * @var string language
     */
    public static $language;

    /**
     * @param $string
     * @param string $replacement
     * @param bool $lowercase
     * @return string
     */
    public static function t($string, $replacement = '_', $lowercase = true)
    {
        static::initTransliteration();

        return static::slug($string, $replacement, $lowercase);
    }

    public static function initTransliteration()
    {
        if(null === static::$language) {
            static::$language = \Yii::$app->language;

            $path = \Yii::getAlias(strtr('@app/extensions/transliterator/translations/{lang}/transliteration.php', [
                '{lang}' => static::$language,
            ]));

            if(file_exists($path)) {
                static::$transliteration = require_once($path);
            }
        }
    }

}
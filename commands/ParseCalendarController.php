<?php

namespace app\commands;

require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'RollingCurl.class.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'AngryCurl.class.php');

use yii\console\Controller;
use yii\helpers\Json;
use \AngryCurl;
use app\models\Business;
use app\models\ObjectFile;
use app\models\BusinessHasSpeaker;
use app\models\Speaker;

/**
 * Class ParseCalendarController
 * @package app\commands
 */
class ParseCalendarController extends Controller
{
    public $url = 'https://vertera.org';

    /**
     * Synchronization
     */
    public function actionGetData()
    {
        \Yii::$app->getDb()->createCommand()->truncateTable(Business::tableName())->query();
        \Yii::$app->getDb()->createCommand()->truncateTable(Speaker::tableName())->query();
        \Yii::$app->getDb()->createCommand()->truncateTable(BusinessHasSpeaker::tableName())->query();
        ObjectFile::deleteAll(['type' => [ObjectFile::TYPE_BUSINESS_IMAGE, ObjectFile::TYPE_SPEAKER_IMAGE]]);

        $context = stream_context_create([
            'http' => [
                'method' => "GET",
                'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36\r\n"
            ]
        ]);

        $json = file_get_contents("{$this->url}/calendar/export", null, $context);

        if (is_array($data = Json::decode($json))) {
            foreach ($data as $calendar_data) {
                $attributes = $calendar_data['attributes'];
                $speakers_data = $calendar_data['speakers_data'];
                $image_url = $calendar_data['image_url'];

                $business = new Business();
                $business->title = $attributes['title'] ?? null;
                $business->description = $attributes['description'] ?? null;
                $business->sub_description = $attributes['sub_description'] ?? null;
                $business->button_text = $attributes['button_text'] ?? null;
                $business->button_url = $attributes['button_url'] ?? null;
                $business->city_name = $attributes['city_name'] ?? null;
                $business->start_date = $attributes['start_date'] ?? null;
                $business->start_date_text = $attributes['start_date_text'] ?? null;
                $business->type = $attributes['type'] ?? null;
                $business->conference_type = $attributes['conference_type'] ?? null;
                $business->in_top = $attributes['in_top'] ?? null;
                $business->status = $attributes['status'] ?? null;

                $business->save(false);

                if ($image_url) {
                    $info = pathinfo($this->url . $image_url);
                    $objectFile = new ObjectFile();
                    $objectFile->setOwnerId($business->id);
                    $objectFile->name = md5(microtime(true) . $info['basename']) . '.' . $info['extension'];
                    $objectFile->original_name = $info['basename'];
                    $objectFile->type = ObjectFile::TYPE_BUSINESS_IMAGE;
                    $objectFile->ext = ObjectFile::EXT_IMAGE;

                    $path = $objectFile->getPath();

                    if (!is_dir(dirname($path))) {
                        mkdir(dirname($path), 0755, true);
                    }

                    if (($str = file_get_contents($this->url . $image_url)) !== false && $objectFile->save()) {
                        $business->image_id = $objectFile->id;
                        $business->save(false);
                        file_put_contents($path, $str);
                    }
                }

                foreach ($speakers_data as $speaker_data) {
                    if (!$speaker = Speaker::findOne(['full_name' => $speaker_data['attributes']['full_name']])) {
                        $speaker = new Speaker();
                        $speaker->full_name = $speaker_data['attributes']['full_name'];
                        $speaker->status = $speaker_data['attributes']['status'];

                        $speaker->save(false);

                        if ($speaker_data['image_url']) {
                            $path_info = pathinfo($this->url . $speaker_data['image_url']);
                            $objectFile = new ObjectFile();
                            $objectFile->setOwnerId($speaker->id);
                            $objectFile->name = md5(microtime(true) . $path_info['basename']) . '.' . $path_info['extension'];
                            $objectFile->original_name = $path_info['basename'];
                            $objectFile->type = ObjectFile::TYPE_SPEAKER_IMAGE;
                            $objectFile->ext = ObjectFile::EXT_IMAGE;

                            $path = $objectFile->getPath();

                            if (!is_dir(dirname($path))) {
                                mkdir(dirname($path), 0755, true);
                            }

                            if (($str = file_get_contents($this->url . $speaker_data['image_url'])) !== false
                                && $objectFile->save()
                            ) {
                                $speaker->image_id = $objectFile->id;
                                $speaker->save(false);
                                file_put_contents($path, $str);
                            }
                        }
                    }

                    $BHS = new BusinessHasSpeaker();
                    $BHS->business_id = $business->id;
                    $BHS->speaker_id = $speaker->id;
                    $BHS->save(false);
                }
            }
        }
    }

    /**
     * Reads all events links
     */
    public function actionGetUrls()
    {
        $context = stream_context_create([
            'http' => [
                'method' => "GET",
                'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36\r\n"
            ]
        ]);
        $dates_arr = [
            'января' => '01',
            'февраля' => '02',
            'марта' => '03',
            'апреля' => '04',
            'мая' => '05',
            'июня' => '06',
            'июля' => '07',
            'августа' => '08',
            'сентября' => '09',
            'октября' => '10',
            'ноября' => '11',
            'декабря' => '12',
        ];
        $full_url = $this->url . '/calendar/?' . http_build_query([
                'date_from' => 'all',
                'calendar_subject_id' => '0',
                'calendar_speaker_id' => '0',
                'type' => '0',
            ]);
        $items = [];
        $document = \phpQuery::newDocumentHTML(file_get_contents($full_url, null, $context));

        foreach ($document->find('.b-calendar-list > .row .b-calendar__item')->elements as $item) {
            $conference_type = Business::WEBINAR;
            $type = null;
            $date = null;
            $city_name = trim(pq($item)->find(".b-calendar__item-sem-town")->text());
            $date_text = trim(pq($item)->find(".b-calendar__item-web-date")->text());
            $time = preg_replace(
                '/[^\d|:]/',
                '',
                trim(pq($item)->find('.b-calendar__item-web-time')->text())
            );

            if (!$link = pq($item)->find(".b-calendar__item-web-subtitle > a")->attr('href')) {
                $conference_type = Business::SEMINAR;
                $link = pq($item)->find(".b-calendar__item-sem-subtitle > a")->attr('href');
            }

            if (strpos($link, 'event') !== false) {
                $type = Business::TYPE_EVENT;
            }

            if (strpos($link, 'calendar') !== false) {
                $type = Business::TYPE_CALENDAR;
            }

            if (strpos($link, $this->url) === false) {
                $link = $this->url . $link;
            }

            if ($type === null) {
                echo "Skipped url: {$link}" . PHP_EOL;
                echo PHP_EOL;
                continue;
            }

            if (sizeof($parts = explode(' ', $date_text)) >= 3) {
                if (sizeof($parts) >= 4)
                    array_pop($parts);

                if (sizeof($b = explode('-', $parts[0])) != 2)
                    $date_text = null;

                $day = strlen($b[0]) === 1 ? "0{$b[0]}" : $b[0];
                $date = implode('-', [$parts[2], $dates_arr[mb_strtolower($parts[1], 'utf-8')], $day]);

                if ($time) {
                    $date .= " {$time}:00";
                    if (!$this->validate_date($date)) {
                        $date = null;
                    }
                } else {
                    if (!$this->validate_date($date, 'Y-m-d')) {
                        $date = null;
                    }
                }
            }

            $items[$link] = [
                'type' => $type,
                'conference_type' => $conference_type,
                'city_name' => $city_name,
                'start_date' => $date,
                'start_date_text' => $date_text,
            ];
        }

        file_put_contents(\Yii::getAlias('@runtime/calendar-items.json'), Json::encode($items));
    }

    /**
     * Reads data in event view and save it to db
     *
     * @param int $truncate
     */
    public function actionPopItems($truncate = 0)
    {
        if ($truncate) {
            \Yii::$app->getDb()->createCommand()->truncateTable(Speaker::tableName())->query();
            ObjectFile::deleteAll(['type' => ObjectFile::TYPE_SPEAKER_IMAGE]);
            \Yii::$app->getDb()->createCommand()->truncateTable(BusinessHasSpeaker::tableName())->query();
        }

        if (!$items = Json::decode(file_get_contents(\Yii::getAlias('@runtime/calendar-items.json')))) {
            echo "No items found in '" . \Yii::getAlias('@runtime/calendar-items.json') . "'" . PHP_EOL;
            return;
        }

        $arr = [];
        $dates_arr = [
            'января' => '01',
            'февраля' => '02',
            'марта' => '03',
            'апреля' => '04',
            'мая' => '05',
            'июня' => '06',
            'июля' => '07',
            'августа' => '08',
            'сентября' => '09',
            'октября' => '10',
            'ноября' => '11',
            'декабря' => '12',
        ];
        $AC = new AngryCurl(function ($response, $info) use ($items, &$arr, $dates_arr) {
            if ($info['http_code'] != 200) {
                $arr[] = [
                    'url' => $info['url'],
                    'http_code' => $info['http_code'],
                ];
            }
            $data = $items[$info['url']];

            if ($data['type'] == Business::TYPE_EVENT) {
                /** If event-link, find record by title and set type -> both */
                $document = \phpQuery::newDocumentHTML($response)->find('.b-news');

                if (!$title = trim(pq($document)->find('.b-news__title')->text())) {
                    echo "No title" . PHP_EOL;
                    echo PHP_EOL;
                    return;
                }

                if ($business = Business::findOne(['title' => $title])) {
                    $business->type = Business::TYPE_BOTH;
                    $business->save(false);
                }
            } else {
                /** If calendar-link - Business::TYPE_CALENDAR*/
                $document = \phpQuery::newDocumentHTML($response)->find('.site-event');
                $is_new = false;
                $date = null;

                if (!$title = trim(pq($document)->find('.site-event-title')->text())) {
                    echo "No title in url [{$info['url']}]" . PHP_EOL;
                    echo PHP_EOL;
                    return;
                }

                if (!$data['start_date']) {
                    $date_str = trim(pq($document)->find('.site-event-name .site-event-name-date')->text());
                    $time = preg_replace(
                        '/[^\d|:]/',
                        '',
                        trim(pq($document)->find('.site-event-info .site-event-info-time')->text())
                    );

                    if (preg_match('/^(\d{1,2})\s(\w+)\s(\d{4})/iu', $date_str, $matches)
                        && isset($dates_arr[mb_strtolower($matches[2], 'utf-8')])
                    ) {
                        $date = implode('-', [
                                '0' . ltrim($matches[3], '0'),
                                $dates_arr[mb_strtolower($matches[2], 'utf-8')],
                                $matches[1]
                            ]) . ($time ? (' ' . $time . ':00') : '');
                    }

                    if (!$date) {
                        echo "No date [{$date_str}] in url [{$info['url']}];" . PHP_EOL;
                        echo PHP_EOL;
                        return;
                    }
                } else {
                    $date = $data['start_date'];
                }

                if (!$business = Business::findOne(array_filter([
                    'title' => $title,
                    'start_date' => $date,
                    'city_name' => $data['city_name'] ?: null,
                ]))
                ) {
                    $is_new = true;
                    $business = new Business();
                    $business->in_top = 0;
                }

                if (($a = pq($document)->find('.site-event-info-cat a'))->length) {
                    $business->button_url = $a->attr('href') ?: null;
                    $business->button_text = $a->text() ?: null;
                }

                $business->title = $title;
                $business->start_date = $date;
                $business->start_date_text = $data['start_date_text'];
                $business->city_name = $data['city_name'] ?: null;
                $business->description = trim(pq($document)->find('.site-event-content')->html());
                $business->type = Business::TYPE_CALENDAR;
                $business->conference_type = $data['conference_type'];
                $business->status = Business::STATUS_ACTIVE;

                $business->save(false);

                if ($is_new) {
                    foreach (pq($document)->find('.site-event-author')->elements as $info) {
                        $speaker_name = preg_replace(
                            '/<br>/',
                            ' ',
                            pq($info)->find('.site-event-author-name')->html()
                        );
                        if (!$speaker = Speaker::findOne(['full_name' => $speaker_name])) {
                            $speaker = new Speaker();
                            $speaker->full_name = $speaker_name;
                            $speaker->status = Speaker::STATUS_ACTIVE;

                            $speaker->save(false);

                            if ($image_url = pq($info)->find('.site-event-author-img img')->attr('src')) {
                                $path_info = pathinfo($this->url . $image_url);
                                $objectFile = new ObjectFile();
                                $objectFile->setOwnerId($speaker->id);
                                $objectFile->name = md5(microtime(true) . $path_info['basename']) . '.' . $path_info['extension'];
                                $objectFile->original_name = $path_info['basename'];
                                $objectFile->type = ObjectFile::TYPE_SPEAKER_IMAGE;
                                $objectFile->ext = ObjectFile::EXT_IMAGE;

                                $path = $objectFile->getPath();

                                if (!is_dir(dirname($path))) {
                                    mkdir(dirname($path), 0755, true);
                                }

                                if (($str = file_get_contents($this->url . $image_url)) !== false && $objectFile->save()) {
                                    $speaker->image_id = $objectFile->id;
                                    $speaker->save(false);
                                    file_put_contents($path, $str);
                                }
                            }
                        }

                        $BHS = new BusinessHasSpeaker();
                        $BHS->business_id = $business->id;
                        $BHS->speaker_id = $speaker->id;
                        $BHS->save(false);
                    }
                }
            }
        });

        $AC->init_console();

        foreach ($items as $link => $data) {
            $AC->post($link);
        }

        $AC->execute(100);

        print_r($arr);
    }

    /**
     * @param $date
     * @param string $format
     * @return bool
     */
    private function validate_date($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}
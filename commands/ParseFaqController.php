<?php

namespace app\commands;

require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'RollingCurl.class.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'AngryCurl.class.php');

use yii\console\Controller;
use app\models\Faq;
use app\models\FaqCategory;

/**
 * Class ParseFaqController
 * @package app\commands
 */
class ParseFaqController extends Controller
{
    public $url = 'https://vertera.org';

    /**
     * Reads all events links
     *
     * @param int $truncate
     */
    public function actionPopItems($truncate = 0)
    {
        if($truncate) {
            \Yii::$app->getDb()->createCommand()->truncateTable(Faq::tableName())->query();
            \Yii::$app->getDb()->createCommand()->truncateTable(FaqCategory::tableName())->query();
        }

        $context = stream_context_create([
            'http' => [
                'method' => "GET",
                'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36\r\n"
            ]
        ]);
        $content = file_get_contents($this->url . '/faq/', null, $context);

        foreach (\phpQuery::newDocumentHTML($content)->find('.b-sidebar ul li:not(:first-child) a')->elements as $a) {
            $page_html = file_get_contents($this->url . pq($a)->attr('href'), null, $context);

            if(!$cat = FaqCategory::findOne(['name' => trim(pq($a)->text())])) {
                $cat = new FaqCategory();
                $cat->name = trim(pq($a)->text());
                $cat->status = FaqCategory::STATUS_ACTIVE;
                $cat->save(false);
            }

            foreach (\phpQuery::newDocumentHTML($page_html)->find('.b-faq__item')->elements as $item) {
                $faq = new Faq();
                $faq->category_id = $cat->id;
                $faq->client_information = pq($item)->find('.b-faq-item__author')->text();
                $faq->question = pq($item)->find('.b-question-expert-item__subtitle')->html();
                $faq->answer = pq($item)->find('.b-faq-item__text-descr')->html();
                $faq->status = Faq::STATUS_ACTIVE;
                $faq->save(false);
            }
        }
    }
}
<?php
namespace app\commands;

use yii\console\Controller;
use goodizer\helpers\DbSyncHelper;

/**
 * Class DbSyncController
 * @package app\commands
 */
class DbSyncController extends Controller
{
    public function actionIndex()
    {
        (new DbSyncHelper([
            'app\models'
        ]))->run();
    }
}

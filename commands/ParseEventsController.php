<?php

namespace app\commands;

require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'RollingCurl.class.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'AngryCurl.class.php');

use yii\console\Controller;
use yii\helpers\Json;
use \AngryCurl;
use app\models\Business;
use app\models\ObjectFile;

/**
 * Class DbSyncController
 * @package app\commands
 */
class ParseEventsController extends Controller
{
    public $url = 'https://vertera.org';

    /**
     * Reads all events links
     */
    public function actionGetUrls()
    {
        $context = stream_context_create([
            'http' => [
                'method' => "GET",
                'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36\r\n"
            ]
        ]);
        $items = [];
        $content = file_get_contents($this->url . '/event/', null, $context);

        foreach (\phpQuery::newDocumentHTML($content)
                     ->find('.b-news-list-items > .row .b-news-list__item')
                     ->elements as $sub_a) {
            $items[$this->url . pq($sub_a)->find('.b-news-list__subtitle > a')->attr('href')] = trim(pq($sub_a)->find('.b-news-list__text')->text());
        }

        file_put_contents(\Yii::getAlias('@runtime/event-items.json'), Json::encode($items));
    }

    /**
     * Reads data in event view and save it to db
     *
     * @param int $truncate
     */
    public function actionPopItems($truncate = 0)
    {
        if($truncate) {
            \Yii::$app->getDb()->createCommand()->truncateTable(Business::tableName())->query();
            ObjectFile::deleteAll(['type' => ObjectFile::TYPE_BUSINESS_IMAGE]);
        }

        if(!$items = Json::decode(file_get_contents(\Yii::getAlias('@runtime/event-items.json')))) {
            echo "No items found in '" . \Yii::getAlias('@runtime/event-items.json') . "'" . PHP_EOL;
            return;
        }

        $arr = [];
        $dates_arr = [
            'января' => '01',
            'февраля' => '02',
            'марта' => '03',
            'апреля' => '04',
            'мая' => '05',
            'июня' => '06',
            'июля' => '07',
            'августа' => '08',
            'сентября' => '09',
            'октября' => '10',
            'ноября' => '11',
            'декабря' => '12',
        ];
        $AC = new AngryCurl(function ($response, $info) use ($items, &$arr, $dates_arr) {
            if ($info['http_code'] != 200) {
                $arr[] = [
                    'url' => $info['url'],
                    'http_code' => $info['http_code'],
                ];
            }

            $is_new = false;
            $document = \phpQuery::newDocumentHTML($response)->find('.b-news');

            if (!$title = trim(pq($document)->find('.b-news__title')->text())) {
                echo "No title in url [{$info['url']}]" . PHP_EOL;
                echo PHP_EOL;
                return;
            }

            $date = null;
            $date_str = trim(pq($document)->find('.b-news__date')->text());

            if (preg_match('/^(\d{1,2})\s(\w+)\s(\d{4})/iu', $date_str, $matches)
                && isset($dates_arr[mb_strtolower($matches[2], 'utf-8')])
            ) {
                $date = implode('-', [$matches[3], $dates_arr[mb_strtolower($matches[2], 'utf-8')], $matches[1]]);
            }

            if (!$date) {
                echo "No date [{$date_str}] in url [{$info['url']}];" . PHP_EOL;
                echo PHP_EOL;
                return;
            }

            if (!$business = Business::findOne(['title' => $title])) {
                $business = new Business();
                $business->in_top = 0;
                $is_new = true;
            }

            $business->title = $title;
            $business->start_date = date('Y-m-d', strtotime($date));
            $business->description = trim(pq($document)->find('div.b-news__text')->html());
            $business->type = Business::TYPE_EVENT;
            $business->conference_type = Business::SEMINAR;
            $business->status = Business::STATUS_ACTIVE;

            if(isset($items[$info['url']])) {
                $business->sub_description = $items[$info['url']];
            }

            $business->save(false);

            if ($is_new) {
                $image_url = $this->url . pq($document)->find('.b-news__img img')->attr('src');
                $info = pathinfo($image_url);
                $objectFile = new ObjectFile();
                $objectFile->setOwnerId($business->id);
                $objectFile->name = md5(microtime(true) . $info['basename']) . '.' . $info['extension'];
                $objectFile->original_name = $info['basename'];
                $objectFile->type = ObjectFile::TYPE_BUSINESS_IMAGE;
                $objectFile->ext = ObjectFile::EXT_IMAGE;

                $path = $objectFile->getPath();

                if (!is_dir(dirname($path))) {
                    mkdir(dirname($path), 0755, true);
                }

                if (($str = file_get_contents($image_url)) !== false && $objectFile->save()) {
                    $business->image_id = $objectFile->id;
                    $business->save(false);
                    file_put_contents($path, $str);
                }
            }
        });

        $AC->init_console();

        foreach ($items as $link => $short_desc) {
            $AC->post($link);
        }

        $AC->execute(100);

        print_r($arr);
    }
}
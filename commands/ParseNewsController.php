<?php

namespace app\commands;

require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'RollingCurl.class.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'AngryCurl.class.php');

use yii\console\Controller;
use yii\helpers\Json;
use \AngryCurl;
use app\models\ObjectFile;
use app\models\News;

/**
 * Class ParseNewsController
 * @package app\commands
 */
class ParseNewsController extends Controller
{
    public $url = 'https://vertera.org';

    /**
     * Synchronization with dev
     */
    public function actionGetData()
    {
        \Yii::$app->getDb()->createCommand()->truncateTable(News::tableName())->query();
        ObjectFile::deleteAll(['type' => ObjectFile::TYPE_NEWS_IMAGE]);

        $context = stream_context_create([
            'http' => [
                'method' => "GET",
                'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36\r\n"
            ]
        ]);

        $json = file_get_contents("{$this->url}/news/export", null, $context);

        if (is_array($data = Json::decode($json))) {
            foreach ($data as $news_data) {
                $attributes = $news_data['attributes'];
                $image_url = $news_data['image_url'];

                $news = new News();
                $news->title = $attributes['title'] ?? null;
                $news->description = $attributes['description'] ?? null;
                $news->sub_description = $attributes['sub_description'] ?? null;
                $news->start_date = $attributes['start_date'] ?? null;
                $news->in_top = $attributes['in_top'] ?? null;
                $news->status = $attributes['status'] ?? null;

                $news->save(false);

                if ($image_url) {
                    $info = pathinfo($this->url . $image_url);
                    $objectFile = new ObjectFile();
                    $objectFile->setOwnerId($news->id);
                    $objectFile->name = md5(microtime(true) . $info['basename']) . '.' . $info['extension'];
                    $objectFile->original_name = $info['basename'];
                    $objectFile->type = ObjectFile::TYPE_NEWS_IMAGE;
                    $objectFile->ext = ObjectFile::EXT_IMAGE;

                    $path = $objectFile->getPath();

                    if (!is_dir(dirname($path))) {
                        mkdir(dirname($path), 0755, true);
                    }

                    if (($str = file_get_contents($this->url . $image_url)) !== false && $objectFile->save()) {
                        $news->image_id = $objectFile->id;
                        $news->save(false);
                        file_put_contents($path, $str);
                    }
                }
            }
        }
    }

    /**
     * Reads all news links
     */
    public function actionGetUrls()
    {
        $context = stream_context_create([
            'http' => [
                'method' => "GET",
                'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36\r\n"
            ]
        ]);
        $items = [];
        $content = file_get_contents($this->url . '/news/', null, $context);

        foreach (\phpQuery::newDocumentHTML($content)
                     ->find('.b-news-list-items > .row .b-news-list__item')
                     ->elements as $sub_a) {
            $items[$this->url . pq($sub_a)->find('.b-news-list__subtitle > a')->attr('href')] = trim(pq($sub_a)->find('.b-news-list__text')->text());
        }

        file_put_contents(\Yii::getAlias('@runtime/news-items.json'), Json::encode($items));
    }

    /**
     * Reads data in news view and save it to db
     *
     * @param int $truncate
     */
    public function actionPopItems($truncate = 0)
    {
        if ($truncate) {
            \Yii::$app->getDb()->createCommand()->truncateTable(News::tableName())->query();
        }

        if (!$items = Json::decode(file_get_contents(\Yii::getAlias('@runtime/news-items.json')))) {
            echo "No items found in '" . \Yii::getAlias('@runtime/news-items.json') . "'" . PHP_EOL;
            return;
        }

        $arr = [];
        $dates_arr = [
            'января' => '01',
            'февраля' => '02',
            'марта' => '03',
            'апреля' => '04',
            'мая' => '05',
            'июня' => '06',
            'июля' => '07',
            'августа' => '08',
            'сентября' => '09',
            'октября' => '10',
            'ноября' => '11',
            'декабря' => '12',
        ];
        $AC = new AngryCurl(function ($response, $info) use ($items, &$arr, $dates_arr) {
            if ($info['http_code'] != 200) {
                $arr[] = $info;
            }
            $is_new = false;
            $document = \phpQuery::newDocumentHTML($response)->find('.b-news');
            if (!$title = trim(pq($document)->find('.b-news__title')->text())) {
                echo "No title" . PHP_EOL;
                echo PHP_EOL;
                return;
            }
            $date = null;
            $date_str = trim(pq($document)->find('.b-news__date')->text());
            if (preg_match('/^(\d{2})\s(\w+)\s(\d{4})/iu', $date_str, $matches)
                && isset($dates_arr[mb_strtolower($matches[2], 'utf-8')])
            ) {
                $date = implode('-', [$matches[3], $dates_arr[mb_strtolower($matches[2], 'utf-8')], $matches[1]]);
            }
            if (!$date) {
                echo "No date [{$date_str}];" . PHP_EOL;
                echo PHP_EOL;
                return;
            }

            if (!$news = News::findOne(['title' => $title])) {
                $is_new = true;
                $news = new News();
                $news->title = $title;
                $news->start_date = $date;
                $news->description = mb_convert_encoding(trim(pq($document)->find('div.b-news__text')->html()), "UTF-8");
                $news->in_top = 0;
                $news->status = News::STATUS_ACTIVE;

                if (isset($items[$info['url']])) {
                    $news->sub_description = $items[$info['url']];
                }

                $news->save(false);
            }

            if ($is_new) {
                $image_url = $this->url . pq($document)->find('.b-news__img img')->attr('src');
                $info = pathinfo($image_url);
                $objectFile = new ObjectFile();
                $objectFile->setOwnerId($news->id);
                $objectFile->name = md5(microtime(true) . $info['basename']) . '.' . $info['extension'];
                $objectFile->original_name = $info['basename'];
                $objectFile->type = ObjectFile::TYPE_NEWS_IMAGE;
                $objectFile->ext = ObjectFile::EXT_IMAGE;

                $path = $objectFile->getPath();

                if (!is_dir(dirname($path))) {
                    mkdir(dirname($path), 0755, true);
                }

                if (($str = file_get_contents($image_url)) !== false && $objectFile->save()) {
                    $news->image_id = $objectFile->id;
                    $news->save(false);
                    file_put_contents($path, $str);
                }
            }
        });

        $AC->init_console();

        foreach ($items as $link => $short_desc) {
            $AC->post($link);
        }

        $AC->execute(100);

        print_r($arr);
    }
}
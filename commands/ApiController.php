<?php

namespace app\commands;

use app\models\ProductHasSubCategory;
use yii\console\Controller;
use app\models\ProductCategory;
use app\components\os\ApiComponent;
use app\models\ObjectFile;
use app\models\Product;
use app\models\Contact;
use app\models\ProductSubCategory;
use yii\helpers\ArrayHelper;

/**
 * Class ApiController
 * @package app\commands
 */
class ApiController extends Controller
{
    public function actionAddProducts()
    {
        /**
         * @var $api ApiComponent
         */
        $excluded_categories = [
            'стартовый набор',
            'полиграфия',
            'фирменная продукция',
            'промонаборы гермес'
        ];
        $api = \Yii::$app->get('api');
        $productApi = $api->getProductApi();

        /** @var $products Product[] */
        $products = ArrayHelper::index(Product::find()->all(), 'vendor_code');
        $categories = [];
        $sub_categories = [];

        foreach (ProductCategory::find()->all() as $c) {
            /** @var $c ProductCategory */
            $categories[mb_strtolower($c->name, 'utf-8')] = $c->id;
        }
        foreach (ProductSubCategory::find()->all() as $c) {
            /** @var $c ProductCategory */
            $sub_categories[mb_strtolower($c->name, 'utf-8')] = $c->id;
        }

        $default_sub_category = ProductSubCategory::find()->select(['id'])->where(['name' => 'Умное питание'])->one();

        foreach ($productApi->getList() as $item) {
            if (in_array(mb_strtolower($item['params']['category_name'], 'utf-8'), $excluded_categories)) {
                continue;
            }

            $is_new = false;

            if (isset($products[$item['vendor_code']])) {
                $product = $products[$item['vendor_code']];
                unset($products[$item['vendor_code']]);
            } else {
                $is_new = true;
                $product = new Product();
                $product->amount = $item['amount'];
                $product->point = $item['points'];
            }

            $product->api_product_id = $item['api_product_id'];
            $product->vendor_code = $item['vendor_code'];
            $product->title = $item['title'];
            $product->cost = $item['cost'];
            $product->cost_original = $item['cost_original'];
            $product->url_name = $item['params']['url_name'];

            if ($is_new) {
                $product->status = Product::STATUS_NEW;
            }

            $product->save(false);

            if ($is_new) {
                $cat_name = mb_strtolower($item['params']['category_name'], 'utf-8');
                if ($cat_name == 'чай') {
                    $cat_name = 'чай белояр';
                }

                if ($cat_name == 'основной каталог') {
                    $cat_name = mb_strtolower($default_sub_category->name, 'utf-8');
                }

                $sub_category_id = $sub_categories[$cat_name] ?? null;

                if (!$sub_category_id) {
                    if ($category_id = $categories[$cat_name] ?? null) {
                        $sub_category = ProductSubCategory::find()
                            ->where(['parent_id' => $category_id])
                            ->orderBy(['parent_id' => SORT_ASC])
                            ->one();
                        if ($sub_category) {
                            $sub_category_id = $sub_category->id;
                        } else {
                            $sub_category = new ProductSubCategory();
                            $sub_category->parent_id = $default_sub_category->parent_id;
                            $sub_category->name = $cat_name;

                            if ($sub_category->save(false)) {
                                $sub_category_id = $sub_category->id;
                            }
                        }
                    }
                }

                if (!$sub_category_id) {
                    $sub_category_id = $default_sub_category->id;
                }

//                print_r([$cat_name => $sub_category_id]);
//                echo PHP_EOL;
//                continue;

                $PHSC = new ProductHasSubCategory();
                $PHSC->product_id = $product->id;
                $PHSC->sub_category_id = $sub_category_id;
                $PHSC->save(false);
            }

            if ($is_new) {
                $url = $item['params']['img_url'];
                $info = pathinfo($url);

                $objectFile = new ObjectFile();
                $objectFile->setOwnerId($product->id);
                $objectFile->name = md5(microtime(true) . $info['basename']) . '.' . $info['extension'];
                $objectFile->original_name = $info['basename'];
                $objectFile->type = ObjectFile::TYPE_PRODUCT_IMAGE;
                $objectFile->ext = ObjectFile::EXT_IMAGE;

                $path = $objectFile->getPath();

                if (!is_dir(dirname($path))) {
                    mkdir(dirname($path), 0755, true);
                }

                $str = file_get_contents($url);

                if ($str !== false && $objectFile->save()) {
                    $product->image_id = $objectFile->id;
                    $product->save(false);
                    file_put_contents($path, $str);
                }
            }
        }


        echo PHP_EOL;
        print_r(sizeof($products));
        echo PHP_EOL;

        foreach ($products as $product) {
            $product->status = Product::STATUS_DISABLED;
            $product->save(false);
        }
    }

    /**
     * @param int $truncate
     */
    public function actionAddStorages($truncate = 1)
    {
        if ($truncate) {
            \Yii::$app->getDb()->createCommand()->truncateTable(Contact::tableName())->query();
        }

        /**
         * @var $api ApiComponent
         */
        $api = \Yii::$app->get('api');
        $storageApi = $api->getStorageApi();

        foreach ($storageApi->getStorageList() as $item) {
            if ($data = $storageApi->getStorage(['sid' => $item['id']])) {
                $contact = new Contact();
                $contact->address = $data['address'];
                $contact->country_name = $item['country'];
                $contact->city_name = $item['city'];
                $contact->email = $data['email'];

                $n = preg_replace('/[^\d]/', '', $data['phone']);
                if (strlen($n) == 11) {
                    $contact->phone = preg_replace('/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/', '+$1 $2 $3 $4 $5', $n);
                } elseif (strlen($n) == 12) {
                    $contact->phone = preg_replace('/(\d{2})(\d{3})(\d{3})(\d{2})(\d{2})/', '+$1 $2 $3 $4 $5', $n);
                } else {
                    $contact->phone = trim(chunk_split($n, 3, ' '));
                }

                $contact->type = $data['type'];
                $contact->status = Contact::STATUS_ACTIVE;

                if ($data['is_official']) {
                    $contact->map_lat = $data['latitude'];
                    $contact->map_lng = $data['longitude'];
                    $contact->map_enable = Contact::MAP_ENABLED;
                } else if ($data['is_public']) {
                    $contact->map_enable = Contact::MAP_DISABLED;
                } else {
                    continue;
                }

                $contact->save(false);
            }
        }
    }
}
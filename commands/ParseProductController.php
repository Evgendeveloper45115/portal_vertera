<?php

namespace app\commands;

require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'RollingCurl.class.php');
require_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'AngryCurl' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'AngryCurl.class.php');

use app\models\ProductCategory;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use \AngryCurl;
use app\models\ObjectFile;
use app\models\ProductHasFiles;
use app\models\ProductHasSubCategory;
use app\models\ProductSubCategory;
use app\models\Product;

/**
 * Class ParseProductController
 * @package app\commands
 */
class ParseProductController extends Controller
{
    public $url = 'https://vertera.org';

    /**
     * Reads all sub categories and prepared their products links
     */
    public function actionGetUrls()
    {
        $context = stream_context_create([
            'http' => [
                'method' => "GET",
                'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36\r\n"
            ]
        ]);
        $items = [];
        $sub_cats = ArrayHelper::map(ProductSubCategory::find()->all(), 'name', 'id');
        $content = file_get_contents($this->url, null, $context);

        foreach (\phpQuery::newDocumentHTML($content)
                     ->find('nav.b-nav.js-nav > ul > li:first-child .b-nav__second-lvl > li .b-nav__third-lvl:last-child > li > a')
                     ->elements as $sub_a) {
            if (!isset($sub_cats[$name = trim(pq($sub_a)->text())])) {
                continue;
            }

            $content = file_get_contents($this->url . pq($sub_a)->attr('href'), null, $context);
            foreach (\phpQuery::newDocumentHTML($content)
                         ->find('.b-catalog .col-sm-9 .row > .col-sm-4')
                         ->elements as $key => $item) {
                $link = $this->url . pq($item)->find('a')->attr('href');
                $items[$sub_cats[$name]][$link] = trim(pq($item)->find('.b-catalog__item_text')->text());
            }
        }

        file_put_contents(\Yii::getAlias('@runtime/product-items.json'), Json::encode($items));
    }

    /**
     * Reads data in product view and save it to db
     *
     * @param int $truncate
     */
    public function actionPopItems($truncate = 0)
    {
        if($truncate) {
            \Yii::$app->getDb()->createCommand()->truncateTable(Product::tableName())->query();
            ObjectFile::deleteAll(['type' => [
                ObjectFile::TYPE_PRODUCT_IMAGE,
                ObjectFile::TYPE_PRODUCT_INFO_IMAGE,
                ObjectFile::TYPE_PRODUCT_DESCRIPTION_IMAGE,
                ObjectFile::TYPE_PRODUCT_CERTIFICATE_FILE,
            ]]);
            \Yii::$app->getDb()->createCommand()->truncateTable(ProductHasSubCategory::tableName())->query();
            \Yii::$app->getDb()->createCommand()->truncateTable(ProductHasFiles::tableName())->query();
        }

        if (!$items = Json::decode(file_get_contents(\Yii::getAlias('@runtime/product-items.json')))) {
            echo "No items found in '" . \Yii::getAlias('@runtime/product-items.json') . "'" . PHP_EOL;
            return;
        }

        $arr = [];
        $AC = new AngryCurl(function ($response, $info) use ($items, &$arr) {
            if ($info['http_code'] != 200) {
                $arr[] = [
                    'url' => $info['url'],
                    'http_code' => $info['http_code'],
                ];
            }
            $desc_elms = [
                'tab_description',
                'tab_composition',
                'tab_use',
            ];
            $is_new = false;
            $document = \phpQuery::newDocumentHTML($response)->find('.b-product-full.js-product-full');

            if (!$title = trim(pq($document)->find('.b-product-full__title')->text())) {
                echo "No title in url '{$info['url']}'" . PHP_EOL;
                echo PHP_EOL;
                return;
            }

            if (!$product = Product::findOne(['title' => $title])) {
                $is_new = true;
                $product = new Product();
                $product->title = $title;
                $product->title_description = trim(pq($document)->find('.b-product-full__shot-descr')->text());
                $product->cost = preg_replace("/[^0-9]/", '', pq($document)->find('.b-product-full__price')->text());
                $product->point = preg_replace("/[^0-9]/", '', pq($document)->find('.b-product-full__balls')->text());
                $product->amount = pq($document)->find('.b-product-full__number.hidden-xs p:last-child')->text();
                $product->vendor_code = preg_replace("/[^0-9]/", '', pq($document)->find('.b-product-full__article')->text());
                
                $info_text = trim(pq($document)->find('div.b-product-full-add-info-bg__text')->text());
                
                $product->info_text =  iconv('utf-8', 'utf-8//IGNORE', $info_text);;
                $product->in_top = 0;
                $product->status = Product::STATUS_ACTIVE;
                $elms = pq($document)->find('.b-product-full-tabs__wrapper.js-tab-content')->elements;

                foreach ($elms as $ai => $value) {
                    $product->{$desc_elms[$ai]} = trim(pq($value)->html());
                }

                $product->save(false);
            }

            foreach ($items as $sub_cat_id => $item) {
                foreach ($item as $link => $desc) {
                    if ($link == $info['url']) {
                        if (!ProductHasSubCategory::find()->where([
                            'product_id' => $product->id,
                            'sub_category_id' => $sub_cat_id,
                        ])->one()
                        ) {
                            $PHSC = new ProductHasSubCategory();
                            $PHSC->product_id = $product->id;
                            $PHSC->sub_category_id = $sub_cat_id;
                            $PHSC->save(false);
                        }
                        if ($is_new) {
                            $product->info_text = $desc;
                            $product->save(false);
                        }
                        break;
                    }
                }
            }

            if ($is_new) {
                $obj['image_id'] = pq($document)->find('.b-product-full__image img')->attr('src');
                $obj['description_image_id'] = pq($document)->find('.b-product-full-info-img img')->attr('src');
                preg_match('/\((.*)\)/i', pq($document)->find('.b-product-full__add-info-bg')->attr('style'), $image);
                $obj['info_image_id'] = $image[1];
                foreach (pq($document)->find('.b-certificates__item')->elements as $key_cert => $value_img) {
                    $obj['certificates'][$key_cert]['src'] = pq($value_img)->find('img')->attr('src');
                    $obj['certificates'][$key_cert]['text'] = pq($value_img)
                        ->find('.b-certificates__item-title > a')->text();
                }
                $this->saveParseImages($obj, $product);
                $product->save(false);
            }
        });
        $AC->init_console();

        foreach ($items as $sub_cat_id => $links) {
            foreach ($links as $link => $short_desc) {
                $AC->post($link);
            }
        }
        $AC->execute(100);

        print_r($arr);
    }

    /**
     * @param array $paths
     * @param Product $product
     */
    private function saveParseImages($paths = [], Product $product)
    {
        foreach ($paths as $fieldName => $url) {
            if (!is_array($url)) {
                $url = $this->url . $url;
                $info = pathinfo($url);
                $objectFile = new ObjectFile();
                $objectFile->setOwnerId($product->id);
                $objectFile->name = md5(microtime(true) . $info['basename']) . '.' . $info['extension'];
                $objectFile->original_name = $info['basename'];
                $objectFile->type = ObjectFile::TYPE_PRODUCT_IMAGE;
                $objectFile->ext = ObjectFile::EXT_IMAGE;

                $path = $objectFile->getPath();
                if (!is_dir(dirname($path))) {
                    mkdir(dirname($path), 0755, true);
                }

                if (($str = file_get_contents($url)) !== false && $objectFile->save()) {
                    $product->{$fieldName} = $objectFile->id;
                    $product->save(false);
                    file_put_contents($path, $str);
                }
            } else {
                foreach ($url as $path_cert) {
                    $path_c = $this->url . $path_cert['src'];
                    $info = pathinfo($path_c);
                    $objectFile = new ObjectFile();
                    $objectFile->setOwnerId($product->id);
                    $objectFile->name = md5(microtime(true) . $info['basename']) . '.' . $info['extension'];
                    $objectFile->original_name = $path_cert['text'];
                    $objectFile->type = ObjectFile::TYPE_PRODUCT_CERTIFICATE_FILE;
                    $objectFile->ext = ObjectFile::EXT_IMAGE;

                    $path = $objectFile->getPath();
                    if (!is_dir(dirname($path))) {
                        mkdir(dirname($path), 0755, true);
                    }

                    if (($str = file_get_contents($path_c)) !== false && $objectFile->save()) {
                        $relModel = new ProductHasFiles();
                        $relModel->product_id = $product->id;
                        $relModel->file_id = $objectFile->id;
                        $relModel->save();
                        file_put_contents($path, $str);
                    }
                }
            }
        }
    }

    /**
     * Creates categories and sub cats
     */
    public function actionCheckCategories()
    {
        //Uncomment for dev
        \Yii::$app->getDb()->createCommand()->truncateTable(ProductCategory::tableName())->query();
        \Yii::$app->getDb()->createCommand()->truncateTable(ProductSubCategory::tableName())->query();

        $context = stream_context_create([
            'http' => [
                'method' => "GET",
                'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36\r\n"
            ]
        ]);

        echo "Parsing started;" . PHP_EOL;

        foreach (\phpQuery::newDocumentHTML(file_get_contents($this->url, null, $context))
                     ->find('nav.b-nav.js-nav > ul > li:first-child .b-nav__second-lvl > li:not(:first-child)')
                     ->elements as $li) {
            $name = pq($li)->find('> a.js-second-lvl-controller')->text();

            if (!($cat = ProductCategory::findOne(['name' => trim($name)]))
                && ($page = \phpQuery::newDocumentHTML(file_get_contents(
                    $this->url . pq($li)->find('> a.js-second-lvl-controller')->attr('href'),
                    null,
                    $context
                )))->length
            ) {
                $cat = new ProductCategory();
                $cat->name = $name;
                $cat->description = trim($page->find('.b-descr-page__text')->text());
                $cat->status = ProductCategory::STATUS_ACTIVE;
                if($cat->save(false)) {
                    echo "\tCreate new category '{$name}';" . PHP_EOL;
                }
            }

            $els = pq($li)->find('.b-nav__third-lvl:last-child > li > a')->elements;

            foreach (array_reverse($els) as $a) {
                if (($sub_name = trim(pq($a)->text())) == 'VERTERA SPORT ACTIVE') {
                    continue;
                }

                if (!($sub_cat = ProductSubCategory::findOne(['name' => $sub_name]))
                    && ($sub_page = \phpQuery::newDocumentHTML(file_get_contents(
                        $this->url . pq($a)->attr('href'),
                        null,
                        $context
                    )))->length
                ) {
                    $sub_cat = new ProductSubCategory();
                    $sub_cat->parent_id = $cat->id;
                    $sub_cat->name = $sub_name;
                    $sub_cat->description = trim($sub_page->find('.b-descr-page__text')->text());
                    $sub_cat->status = ProductSubCategory::STATUS_ACTIVE;
                    if($sub_cat->save(false)) {
                        echo "\t\tCreate new sub category '{$sub_name}';" . PHP_EOL;
                    }
                }
            }
        }

        echo "End;" . PHP_EOL;
    }
}
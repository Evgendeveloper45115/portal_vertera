<?php

namespace app\forms;

use Yii;
use yii\base\Model;

/**
 * Class ConsentForm
 * @package app\forms
 */
class ConsentForm extends Model
{
    public $i_agree = true;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['i_agree'], 'required', 'requiredValue' => 1, 'message' => 'Поставте флажок'],
            ['i_agree', 'boolean'],
        ];
    }
}

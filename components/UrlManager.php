<?php
namespace app\components;

use app\extensions\transliterator\Inflector;
use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductSubCategory;

class UrlManager extends \klisl\languages\UrlManager
{
    public $route;

    /**
     * @param $params array
     * @return array
     */
    private function prepareParams($params)
    {
        return empty($params) ? [$this->route, $params] : [$this->route] + $params;
    }

    /**
     * @param array|string $params
     * @return string
     */
    public function createUrl($params)
    {
        $params = (array)$params;
        $this->route = trim($params[0], '/');
        switch ($this->route) {
            case 'product/view':
                /** @var Product $p */
                if (isset($params['id']) && $p = Product::findOne($params['id'])) {
                    $new = $params;
                    $new['id'] = Inflector::t($p->title);
                    /** @var ProductCategory $c */
                    if (isset($params['cat_id']) && $c = ProductCategory::findOne($params['cat_id'])) {
                        $new['cat_id'] = Inflector::t($c->name);
                    }
                    /** @var ProductSubCategory $sub_c */
                    if (isset($params['sub_cat_id']) && $sub_c = ProductSubCategory::findOne($params['sub_cat_id'])) {
                        $new['sub_cat_id'] = Inflector::t($sub_c->name);
                    }

                    return $this->setData($new, $params);
                }
                break;
            case 'category/view':
                /** @var ProductCategory $c */
                if (isset($params['id']) && ($c = ProductCategory::findOne($params['id']))) {
                    $new = $params;
                    $new['id'] = Inflector::t($c->name);
                    /** @var ProductSubCategory $sub_c */
                    if (isset($params['sub_id']) && ($sub_c = ProductSubCategory::findOne($params['sub_id']))) {
                        $new['sub_id'] = Inflector::t($sub_c->name);
                    }
                    return $this->setData($new, $params);
                }
                break;
        }
        return parent::createUrl($params);
    }

    /**
     * @param \yii\web\Request $request
     * @return array|bool
     */
    public function parseRequest($request)
    {
        if (false === $result = parent::parseRequest($request)) {
            return false;
        }

        list($this->route, $params) = $result;

        if (in_array($this->route, [
            'product/view',
            'category/view',
        ])) {
            $params = $this->getData($this->prepareParams($params));
        }
        return [(string)$this->route, $params];
    }

    /**
     * @param array $new
     * @param array $old
     * @return string
     */
    public function setData(array $new, array $old)
    {
        $cache = \Yii::$app->getCache();
        $params = $this->getData($new);

        if (!empty($params)) {
            return parent::createUrl($new);
        } else {
            $params[$cache->buildKey($new)] = $old;
        }

        $params = $this->getData() + $params;
        $cache->set('__urlData', $params);

        return parent::createUrl($new);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getData(array $params = [])
    {
        $cache = \Yii::$app->getCache();
        $urlData = $cache->get('__urlData');

        if (null === $urlData) {
            $cache->set('__urlData', []);
            return [];
        }

        if (empty($params)) {
            return $urlData ?: [];
        } else {
            $cKey = $cache->buildKey($params);
        }

        return isset($urlData[$cKey]) ? $urlData[$cKey] : [];
    }
}
<?php
namespace app\components\os;

/**
 * @author Aleksandr Mitasov
 * 
 * @date 10-19-2017
 * 
 * @brief Компонент взаимодействия с OS
 * 
 * @detailed Компонент реализует все методы, описанные API OS для получения и отправки данных в OS
 *  посредством включения классов UserApi, ProductApi, OrderApi, StorageApi
 */

use app\components\os\api\OrderApi;
use yii\base\Component;
// use Guzzle\Service\Exception\ValidationException;
use app\components\os\api\UserApi;
use app\components\os\api\StorageApi;
use app\components\os\api\ProductApi;

class ApiComponent extends Component
{
    public $magicKey = '';
    
    public $osBaseUrl = '';
    
    public $osEngine = '';
    
    public $rules;
    
    public function init()
    {
        $this->rules = new Rules();
    }
    
    public function getUserApi()
    {
        return new UserApi($this);
    }

    public function getStorageApi()
    {
        return new StorageApi($this);
    }
    
    public function getProductApi()
    {
        return new ProductApi($this);
    }
    
    public function getOrderApi()
    {
        return new OrderApi($this);
    }
    
    public function request($method, $type, $params = [], $format = 'json')
    {
        $params_request = [
            'method' => $method,
            'format' => $format,
            'magic_key' => $this->magicKey
        ];
        
        $options = array(
            CURLOPT_URL            => $this->osBaseUrl . $this->osEngine,
            CURLOPT_CUSTOMREQUEST  => $type,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        );
        
        $ch = curl_init();
        if ($type == 'GET') {
            $params = array_merge($params_request, $params);
            
            $options[CURLOPT_URL] .= '?' . http_build_query($params);
        }
        else {
            $options[CURLOPT_URL] .= '?' . http_build_query($params_request);
            $options[CURLOPT_POST] = true;
            $options[CURLOPT_POSTFIELDS] = $params;
        }

        curl_setopt_array($ch, $options);
        
        $response = curl_exec($ch);
        
        $error = curl_error($ch);
        
        curl_close($ch);
        
        if ($error) {
            throw new \Exception($error);
        }

        return $response;
    }
    
    public function validateParams($params, $rules)
    {
        $model = new OsDynamicModel($params);
        foreach ($rules as $rule) {
            $options = [];
            if (isset($rule['on'])) {
                $model->addScenario([$rule['on'] => $rule[0]]);
                $options['on'] = $rule['on'];
            }
            if (isset($rule['range'])) {
                $options['range'] = $rule['range'];
            }

            if (isset($rule['strict'])) {
                $options['strict'] = $rule['strict'];
            }
            if (isset($rule['pattern'])) {
                $options['pattern'] = $rule['pattern'];
            }
            if (isset($rule['value'])) {
                $options['value'] = $rule['value'];
            }
            
            $model->addRule($rule[0], $rule[1], $options);
        }

        if ($model->validate()) {
            return true;
        }
        else {
            $e = self::modelErrorsException($model->errors);

            throw $e;
        }
    }
    
    public static function modelErrorsException($errors)
    {
        $data = [];
        foreach ($errors as $error) {
            $data[] = implode(";", $error);
        }
        
//         $e = new ValidationException('Validation errors: ' . implode("\n", $data));
//         $e->setErrors($errors);
        $e = new \Exception('Validation errors: ' . implode("\n", $data));
        
        return $e;
    }
}
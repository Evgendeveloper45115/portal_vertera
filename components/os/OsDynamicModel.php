<?php
namespace app\components\os;


use yii\base\DynamicModel;

class OsDynamicModel extends DynamicModel
{
    protected $scenarios = [];
    
    public function addScenario($scenario)
    {
        $this->scenarios = array_merge_recursive($this->scenarios, $scenario);
    }
    
    public function scenarios()
    {
        $scenarios = [self::SCENARIO_DEFAULT => []];
        foreach ($this->getValidators() as $validator) {
            foreach ($validator->on as $scenario) {
                $scenarios[$scenario] = [];
            }
            foreach ($validator->except as $scenario) {
                $scenarios[$scenario] = [];
            }
        }
        $names = array_keys($scenarios);
        
        foreach ($this->getValidators() as $validator) {
            if (empty($validator->on) && empty($validator->except)) {
                foreach ($names as $name) {
                    foreach ($validator->attributes as $attribute) {
                        $scenarios[$name][$attribute] = true;
                    }
                }
            } elseif (empty($validator->on)) {
                foreach ($names as $name) {
                    if (!in_array($name, $validator->except, true)) {
                        foreach ($validator->attributes as $attribute) {
                            $scenarios[$name][$attribute] = true;
                        }
                    }
                }
            } else {
                foreach ($validator->on as $name) {
                    foreach ($validator->attributes as $attribute) {
                        $scenarios[$name][$attribute] = true;
                    }
                }
            }
        }
        
        foreach ($scenarios as $scenario => $attributes) {
            if (!empty($attributes)) {
                $scenarios[$scenario] = array_keys($attributes);
            }
        }
        
        $this->scenarios = array_merge_recursive($this->scenarios, $scenarios);
        
        return $this->scenarios;
    }


    public function beforeValidate()
    {
        $this->fillNotIssetAttributes();
        
        return parent::beforeValidate();
    }
    
    private function fillNotIssetAttributes()
    {
        $notIssetAttributes = array_diff($this->activeAttributes(), $this->attributes());
        if (!empty($notIssetAttributes)) {
            foreach ($notIssetAttributes as $attribute)
            {
                $this->defineAttribute($attribute);
            }
        }
    }
}
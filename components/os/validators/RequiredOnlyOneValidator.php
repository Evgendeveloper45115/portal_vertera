<?php
namespace app\components\os\validators;


use yii\validators\Validator;

/**
 * 
 * @author Alexandr Mitasov
 * @date 20 окт. 2017 г.
 *
 * @brief \app\web\VerteraTheme\module\os\validators\RequiredOnlyOneValidator
 * @detailed Проверяет один ли из параметров существует и не пуст ли он
 *
 */
class RequiredOnlyOneValidator extends Validator
{
//     public $skipOnError = false;
    
    public function validateAttribute($model, $attribute)
    {
        if ($this->isEmpty($model->$attribute)) {
            $this->addError($model, $attribute, '{attribute} can not be empty');
        }
    }
    
    public function validateAttributes($model, $attributes = null)
    {
        if (count($model->attributes) != 1) {
            $this->addError($model, current(array_keys($model->attributes)), 'In the array of parameters should be one element.');
            return;
        }
        else {
            if (count(array_intersect($attributes, array_keys($model->attributes))) == 1) {
                
                $toDelete = array_diff($attributes, array_keys($model->attributes));
                foreach ($toDelete as $key => $attributeName) {
                    unset($attributes[$key]);
                }
            }
            $model->scenario = current($attributes);
        }
        if (is_array($attributes)) {
            $newAttributes = [];
            $attributeNames = $this->getAttributeNames();
            foreach ($attributes as $attribute) {
                if (in_array($attribute, $attributeNames, true)) {
                    $newAttributes[] = $attribute;
                }
            }
            $attributes = $newAttributes;
        } else {
            $attributes = $this->getAttributeNames();
        }
        foreach ($attributes as $attribute) {
            $skip = $this->skipOnError && $model->hasErrors($attribute)
            || $this->skipOnEmpty && $this->isEmpty($model->$attribute);
            if (!$skip) {
                if ($this->when === null || call_user_func($this->when, $model, $attribute)) {
                    $this->validateAttribute($model, $attribute);
                }
            }
        }
    }
}
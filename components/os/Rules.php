<?php
namespace app\components\os;

// use app\web\VerteraTheme\module\components\os\validators\RequiredOnlyOneValidator;

/**
 * 
 * @author Alexandr Mitasov
 * @date 20 окт. 2017 г.
 *
 * @brief \app\web\VerteraTheme\module\os\Rules
 * @detailed Содержит коллекцию правил валидации для каждого метода OS API
 *
 */
class Rules
{
    private $rules = [];
    
    public function __construct()
    {
        $this->rules = $this->setRules();
    }

    /**
     * returns array of rules for api-method
     * @param string $method
     * @return mixed|boolean
     */
    public function getRules($method) 
    {
        if (isset($this->rules[$method])) {
            return $this->rules[$method];
        }
        
        return false;
    }
    
    private function setRules()
    {
        return [
            //user
            'add_user' => [
                [['cl_name', 'cl_lastname'], 'required'],
                [['cl_name', 'cl_lastname', 'cpass', 'ref', 'cl_flags'], 'string'],
                [['cl_phone'], 'match', 'pattern' => '/^[0-9]{10,12}/i'],
                ['cl_email', 'email'],
                [['cl_email', 'ref'], 'trim'],
                ['ref', 'default', 'value' => 'ad'],
                ['cl_flags', 'default', 'value' => 'WOR'],
            ],
            'user_get_profile' => [
                [['uid', 'cards'], 'integer'],
                [['phone'], 'match', 'pattern' => '/^[0-9]{10,12}/i'],
            ],
            'user_get_tree' => [
                [['uid', 'structure'], 'required'],
                [['uid', 'level', 'depth'], 'integer'],
                ['structure', 'in', 'range' => ['mentor', 'sponsor']],
                ['mode', 'in', 'range' => ['up', 'down']]
            ],
            'user_set_agreement' => [
                [['uid'], 'required'],
                [['uid'], 'integer'],
                [['date'], 'string'],
            ],
            //storage
            'storage_get_profile' => [
                ['sid', 'integer'],
            ],
            //products
            'storage_get_goods' => [
                [['sid', 'currency'], 'required'],
                [['sid', 'uid', 'currency'], 'integer'],
                
            ],
            'get_product' => [
                [['sid', 'currency', 'product_id'], 'required'],
                [['sid', 'currency', 'product_id'], 'integer'],
            ],

            'get_catalog' => [
                [['uid', 'sid', 'currency'], 'required'],
                [['uid', 'sid', 'currency', 'price_group'], 'integer'],
                ['coupon', 'string']
            ],
            //orders
            'order_create' => [
                [['uid', 'addr', 'items'], 'required'],
                [['uid', 'currency', 'delivery', 'storage', 'bonus'], 'integer'],
                ['delivery_price', 'number'],
                ['params', 'string'], //'params' => ['delivery_method', 'order_source']
                [['date', 'comment', 'addr'], 'string'],
            ],
            'order_set_state' => [
                [['oid', 'state'], 'required'],
                [['oid', 'state'], 'integer'],
            ],
            'order_get_pay_url' => [
                [['oid'], 'required'],
                [['oid'], 'integer'],
            ],
            'order_get' => [
                [['oid'], 'required'],
                [['oid'], 'integer'],
            ],
        ];
    }
}
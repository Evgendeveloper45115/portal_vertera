<?php
namespace app\components\os\api;

use Yii;
use yii\helpers\Json;
use app\components\os\Rules;
use app\components\os\ApiComponent;

/**
 * 
 * @author Alexandr Mitasov
 * @date 31 окт. 2017 г.
 *
 * @brief app\web\VerteraTheme\module\components\os\api\ProductApi
 * @detailed Реализует методы взаимодействия с OS в контексте продуктов
 *
 */
class ProductApi 
{
    /** @var ApiComponent */
    private $_api;

    /** @var Rules */
    private $_rules;
    
    /**
     * ProductApi constructor.
     * @param ApiComponent $api
     */
    public function __construct(ApiComponent $api) {
        $this->_api = $api;
        $this->_rules = $api->rules;
    }

    /**
     * @param array $params
     * @return array|false
     */
    public function getList(array $params = ['sid' => 10, 'currency' => 1])
    {
        $rules = $this->_rules->getRules('storage_get_goods');
        
        if ($this->_api->validateParams($params, $rules)) {
            
            $cacheKey = 'ApiProductList:sid-' . $params['sid'] . ':currency-' . $params['currency'];
            
            if (false === $request = Yii::$app->cache->get($cacheKey)) {
                $request = $this->_api->request('storage_get_goods', 'GET', $params);
                if ($request != 'false') {
                    Yii::$app->cache->set(
                        $cacheKey,
                        $request,
                        300
                        );
                }
                else {
                    return false;
                }
            }

            if (!$result = Json::decode($request)) {
                return false;
            }

            $products = [];

            foreach ($result as $key => $item) {
                $products[$key] = [
                    'api_product_id' => $item['gid'],
                    'title' => $item['name_orig'],
                    'vendor_code' => $item['art'],
                    'cost' => $item['price'],
                    'cost_original' => $item['price_original'],
                    'points' => $item['points'],
                    'amount' => $item['params']['weight'] ?? null,

                    'params' => [
                        'url_name' => $item['slug'],
                        'category_id' => $item['cat_id'],
                        'category_name' => $item['cat_name'],
                        'img_url' => $this->_api->osBaseUrl . $item['img'],
                        'bn' => $item['bn'],
                    ],
                ];
            }

            return $products;
        }
        
        return false;
    }

    /**
     * @param array $params
     * @return array|false
     */
    public function getProduct(array $params)
    {
        if (!isset($params['sid'])) {
            $params['sid'] = 10;
        }
        
        if (!isset($params['currency'])) {
            $params['currency'] = 1;
        }
        
        $rules = $this->_rules->getRules('get_product');
        
        if ($this->_api->validateParams($params, $rules)) {
            if ($products = $this->getList($params)) {
                return $products[$params['product_id']] ?? false;
            }
        }
        
        return false;
    }

    /**
     * @param array $params must contain 'uid' (3), 'sid' (storage_id - default 10), 'currency',
     * 'price_group' (4 - for clients, 6 - for partners)
     * @return array|false
     */
    public function getCatalog($params)
    {
        if (!isset($params['sid'])) {
            $params['sid'] = 10;
        }
        
        if (!isset($params['currency'])) {
            $params['currency'] = 1;
        }

        if (!isset($params['uid'])) {
            $params['uid'] = 3;
        }
        
        $rules = $this->_rules->getRules('get_catalog');
        
        if ($this->_api->validateParams($params, $rules)) {
            $request = $this->_api->request('get_catalog', 'GET', $params);

            return Json::decode($request);
        }
        
        return false;
    }
}
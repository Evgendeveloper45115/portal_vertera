<?php

namespace app\components\os\api;

use app\components\os\ApiComponent;
use app\components\os\Rules;
use yii\helpers\Json;

/**
 *
 * @author Alexandr Mitasov
 * @date 19 окт. 2017 г.
 *
 * @brief UserApi
 * @detailed Реализует методы взаимодействия с OS в контексте пользователя
 *
 */
class UserApi
{
    private $_api;

    /** @var Rules */
    private $_rules;

    public function __construct(ApiComponent $api)
    {
        $this->_api = $api;
        $this->_rules = $api->rules;
    }

    public function createUser(array $params)
    {
        $rules = $this->_rules->getRules('add_user');

        if ($this->_api->validateParams($params, $rules)) {
            $request = $this->_api->request('add_user', 'POST', array_merge($params, ['cl_type' => 2]));

            $result = Json::decode($request);

            return $this->getUser(['uid' => $result['uid']]);
        }
    }

    /*
     * @params array, that has 'uid' element
     */
    public function getUser(array $params)
    {
        $rules = $this->_rules->getRules('user_get_profile');

        if ($this->_api->validateParams($params, $rules)) {

        }

        $request = $this->_api->request('user_get_profile', 'GET', $params);

        $result = Json::decode($request);

        if (isset($result['error'])) {
            return $result['error'];
        }

        $user = [];

        $user['id'] = $result['id'];
        $user['personal_number'] = $result['user_card'];
        $user['storage_id'] = $result['storage'] != 0 ? (int)$result['storage'] : NULL;
        $user['lastname'] = $result['meta']['cl_lastname'];
        $user['firstname'] = $result['meta']['cl_name'];
        $user['rank'] = $result['type_show'];
        $user['state'] = $result['state'] != 1 ? 0 : $result['state'];
        $user['rank_string'] = $result['type_dizz'];
        $user['avatar'] = $result['img'] ?? '/market/img/avatar_man.png';

        $user['type'] = !is_null($result['agr_date']) ? 'partner' : 'client';

        $user['email'] = NULL;
        $user['phone'] = NULL;

        if (is_array($result['email'])) {
            foreach ($result['email'] as $k => $item) {
                if ($item['base'] == 1 && $item['confirmed'] == 1) {
                    $user['email'] = strtolower($item['email']);
                }
            }
        }

        if (is_array($result['phone'])) {
            foreach ($result['phone'] as $k => $item) {
                if ($item['base'] == 1 && $item['confirmed'] == 1 && $item['mobile'] == 1) {
                    $user['phone'] = strtolower($item['phone']);
                }
            }
        }

        // Получаем наставника
        $mentor = $this->getTree([
            'uid' => $result['id'],
            'structure' => 'mentor',
            'depth' => 1,
            'mode' => 'up'
        ]);

        if (is_array($mentor) && !empty($mentor)) {
            $mentor = array_shift($mentor);
            $user['mentor_id'] = (int)$mentor['user_id'];
        }

        // Получаем спонсора
        $sponsor = $this->getTree([
            'uid' => $result['id'],
            'structure' => 'sponsor',
            'depth' => 1,
            'mode' => 'up'
        ]);

        if (is_array($sponsor) && !empty($sponsor)) {
            $sponsor = array_shift($sponsor);
            $user['sponsor_id'] = (int)$sponsor['user_id'];
        }

        return $user;
    }

    /*
     * @params array, that has 'uid' element
     */
    public function getUserName(array $params)
    {
        $rules = $this->_rules->getRules('user_get_profile');

        if ($this->_api->validateParams($params, $rules)) {
            $request = $this->_api->request('user_get_profile', 'GET', $params);

            $result = Json::decode($request);

            $user = [];

            $user['id'] = $result['id'];
            $user['lastname'] = $result['meta']['cl_lastname'];
            $user['firstname'] = $result['meta']['cl_name'];

            return $user;
        }

        return false;
    }

    public function getTree(array $params)
    {
        $rules = $this->_rules->getRules('user_get_tree');

        if ($this->_api->validateParams($params, $rules)) {

            $request = $this->_api->request('user_get_tree', 'GET', $params);

            $result = Json::decode($request);

            if (is_array($result['tree'])) {
                $tree = [];

                foreach ($result['tree'] as $item) {
                    if ($item['level'] > 0) {
                        $user_id = $item['uid'] ?? $item['puid'];

                        $tree[] = ['level' => $item['level'], 'user_id' => $user_id];
                    }
                }

                return $tree;
            } else {
                return [];
            }
        }

        return false;
    }

    public function setStorage(array $params)
    {

    }

    public function setAgreement(array $params)
    {
        $rules = $this->_rules->getRules('user_set_agreement');

        if ($this->_api->validateParams($params, $rules)) {

            $request = $this->_api->request('user_set_agreement', 'POST', $params);

            $result = Json::decode($request);

            return $result;

        }

        return false;
    }

}
<?php
namespace app\components\os\api;

use app\models\Order;
use app\components\os\ApiComponent;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 *
 * @author Aleksandra Litvinova
 * @date 15 ноября 2017 г.
 *
 * @brief app\web\VerteraTheme\module\components\os\api\OrderApi
 * @detailed Реализует методы взаимодействия с OS в контексте заказов
 *
 */
class OrderApi
{
    const STATE_NEW = 1;
    const STATE_ON_PERFORMANCE = 3;
    const STATE_FULFILLED = 99;
    const STATE_CANCELED = 98;
    
    private $_api;
    /**
     * @var \app\components\os\Rules
     */
    private $_rules;

    public function __construct(ApiComponent $api)
    {
        $this->_api = $api;
        $this->_rules = $api->rules;
    }

    public function createOrder(Order $order, $state = self::STATE_NEW)
    {
        if (!$order->user->osUser) {
            return;
        }

        $items = ArrayHelper::map($order->items, 'product_id', 'quantity');
        $deliveryInformation = $order->contragent->deliveryInformation;
        if ($deliveryInformation->address) {
            $address = $deliveryInformation->address . ', ' . $deliveryInformation->flat . ', '
                . $deliveryInformation->zip_code;
        } else {
            $address = $deliveryInformation->storage->address . ', ' . $deliveryInformation->storage->city->name . ', '
                . $deliveryInformation->storage->country->name;
        }
        // TODO: currency нужно брать с заказа
        $params = [
            'uid' => $order->user->osUser->id,
            'date' => $order->start_date,
            'currency' => 1,
            'delivery' => 0,
            'storage' => $deliveryInformation->storage_id,
            'comment' => '',
            'addr' => $address,
            'bonus' => 0,
            'items' => json_encode($items),
            'delivery_price' => $order->orderDeliveryInformation->shipping_price_total,
            'params' => json_encode([
                'delivery_method' => $order->shippingOption->slug,
                'order_source' => 'vertera_market'
            ]),
        ];
        $rules = $this->_rules->getRules('order_create');
        // TODO: сделать фильтр центров в зависимости от товаров!!!
        if ($this->_api->validateParams($params, $rules)) {
            $request = $this->_api->request('order_create', 'POST', $params);

            $result = Json::decode($request);
            if (is_array($result) && key_exists('oid', $result)) {
                
                if ($state != self::STATE_NEW) {
                    $this->setStateOrder(['oid' => $result['oid'], 'state' => $state]);
                }
                
                $order->pay_url = $this->getPayUrl(['oid' => $result['oid']]);
                $order->os_order_id = $result['oid'];
                $order->save(false);
            }
        }
    }
    
    public function setStateOrder($params)
    {
        $rules = $this->_rules->getRules('order_set_state');
        
        if ($this->_api->validateParams($params, $rules)) {
            $this->_api->request('order_set_state', 'POST', $params);
        }
    }
    
    public function getPayUrl($params)
    {
        $rules = $this->_rules->getRules('order_get_pay_url');
        
        if ($this->_api->validateParams($params, $rules)) {
            $request = $this->_api->request('order_get_pay_url', 'POST', $params);
            
            $result = Json::decode($request);
            
            if ($result['status'] == 'OK') {
                return $result['url'];
            }
            else {
                return $result['status'];
            }
        }
    }
    
    public function getOrder($params)
    {
        $rules = $this->_rules->getRules('order_get');
        
        if ($this->_api->validateParams($params, $rules)) {
            $request = $this->_api->request('order_get', 'POST', $params);
            
            $result = Json::decode($request);
            
            return $result;
        }
    }

}

<?php

namespace app\components\os\api;

use app\components\os\ApiComponent;
use app\components\os\Rules;
use yii\helpers\Json;

/**
 *
 * @author Alexandr Mitasov
 * @date 23 окт. 2017 г.
 *
 * @brief app\web\VerteraTheme\module\components\os\api\StorageApi
 * @detailed Реализует методы взаимодействия с OS в контексте складов
 *
 */
class StorageApi
{
    /** @var ApiComponent  */
    private $_api;

    /** @var  Rules */
    private $_rules;

    /**
     * StorageApi constructor.
     * @param ApiComponent $api
     */
    public function __construct(ApiComponent $api)
    {
        $this->_api = $api;
        $this->_rules = $api->rules;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function getStorage(array $params)
    {
        $rules = $this->_rules->getRules('storage_get_profile');

        if ($this->_api->validateParams($params, $rules)) {
            $request = $this->_api->request('storage_get_profile', 'GET', $params);

            if (!$result = Json::decode($request)) {
                return false;
            }

            $storage['id'] = $result['id'];
            $storage['address'] = $result['addr'];
            $storage['is_hub'] = $result['hub'];
            $storage['is_cash'] = $result['cash_mode'];
            $storage['type'] = $result['type'];
            $storage['is_public'] = $result['is_public'] ?? 0;
            $storage['is_official'] = $result['is_official'] ?? 0;
            $storage['latitude'] = $result['lat'] ?? null;
            $storage['longitude'] = $result['lng'] ?? null;
            $storage['admin_only'] = $result['admin_only'] ?? 0;
            $storage['email'] = $result['contact_email'] ?? null;
            $storage['name'] = $result['contact_name'] ?? null;
            $storage['phone'] = $result['contact_phone'] ?? null;
            $storage['skype'] = $result['contact_skype'] ?? null;
            $storage['comment'] = $result['comment'] ?? null;
            $storage['state'] = $result['state'];

            return $storage;
        }

        return false;
    }

    /**
     * @param bool $mini
     * @return array|bool
     */
    public function getStorageList($mini = false)
    {
        $request = $this->_api->request('storage_get_list', 'GET');

        if (!$list = Json::decode($request)) {
            return false;
        }

        if ($mini) {
            return $list['storage'];
        }

        $result = [];

        foreach ($list as $key => $item) {
            if (isset($item['id'])) {
                $result[(int)$item['id']] = $item;
            }
        }

        return $result;
    }
}
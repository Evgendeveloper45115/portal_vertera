<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;
use app\helpers\AR;
use app\behaviors\TranslationBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $image_id
 * @property string $title
 * @property string $description
 * @property string $sub_description
 * @property string $start_date
 * @property int $in_top
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class News extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'image_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING,
            'description' => 'longtext',
            'sub_description' => Schema::TYPE_TEXT,
            'start_date' => Schema::TYPE_DATETIME,
            'in_top' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_TIMESTAMP,
            'updated_at' => Schema::TYPE_TIMESTAMP,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'start_date'], 'required', 'on' => 'admin-edit'],
            [['title'], 'string', 'max' => 255, 'on' => 'admin-edit'],
            [['description', 'sub_description'], 'string'],
            [['status', 'in_top'], 'integer'],
            [['title', 'description', 'sub_description', 'start_date', 'in_top', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $t = 'models/' . static::shortName();

        return [
            'id' => Yii::t($t, 'ID'),
            'image_id' => 'Изображение',
            'title' => 'Заглавие',
            'description' => 'Описание',
            'sub_description' => 'Краткое описание',
            'start_date' => 'Дата',
            'in_top' => 'На главной',
            'status' => 'Статус',
            'created_at' => 'Создана',
            'updated_at' => 'Обневлена',
        ];
    }

    /**
     * @return ObjectFile
     */
    public function getImage()
    {
        /** @var ObjectFile $o */
        if ($o = $this->hasOne(ObjectFile::class, ['id' => 'image_id'])->one()) {
            $o->setOwnerId($this->id);
        }

        return $o;
    }
}
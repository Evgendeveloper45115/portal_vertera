<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;
use app\helpers\AR;

/**
 * Class FieldTranslation
 *
 * @property integer $id
 * @property integer $rel_id
 * @property integer $model_key
 * @property integer $language
 *
 * @property string $field_name
 * @property string $field_value
 *
 * @property string $created_at
 * @property string $updated_at
 *
 * @package app\models
 */
class FieldTranslation extends AR
{
    const MODEL_CLASS_PRODUCT = 1;
    const MODEL_CLASS_PRODUCT_CATEGORY = 2;
    const MODEL_CLASS_PRODUCT_SUB_CATEGORY = 3;
    const MODEL_CLASS_BUSINESS = 4;
    const MODEL_CLASS_PROMOTION = 5;
    const MODEL_CLASS_NEWS = 6;
    const MODEL_CLASS_SPEAKER = 7;
    const MODEL_CLASS_CONTACT = 8;
    const MODEL_CLASS_QUESTION = 9;
    const MODEL_CLASS_FAQ = 10;
    const MODEL_CLASS_FAQ_CATEGORY = 11;
    const MODEL_CLASS_MAIN_SLIDER_HEADER = 12;

    const LANGUAGE_RU = 1;
    const LANGUAGE_EN = 2;
    const LANGUAGE_DE = 3;
    const LANGUAGE_UK = 4;

    /**
     * @var array
     */
    public static $model_keys = [
        self::MODEL_CLASS_PRODUCT => Product::class,
        self::MODEL_CLASS_PRODUCT_CATEGORY => ProductCategory::class,
        self::MODEL_CLASS_PRODUCT_SUB_CATEGORY => ProductSubCategory::class,
        self::MODEL_CLASS_BUSINESS => Business::class,
        self::MODEL_CLASS_PROMOTION => Promotion::class,
        self::MODEL_CLASS_NEWS => News::class,
        self::MODEL_CLASS_SPEAKER => Speaker::class,
        self::MODEL_CLASS_CONTACT => Contact::class,
        self::MODEL_CLASS_QUESTION => Question::class,
        self::MODEL_CLASS_FAQ => Faq::class,
        self::MODEL_CLASS_FAQ_CATEGORY => FaqCategory::class,
    ];

    /**
     * @var array
     */
    public static $short_names = [
        self::LANGUAGE_RU => 'ru',
        self::LANGUAGE_EN => 'en',
        self::LANGUAGE_DE => 'de',
        self::LANGUAGE_UK => 'uk',
    ];

    /**
     * @param $name
     * @return int|null
     */
    public static function getLangCode($name)
    {
        return array_search($name, static::$short_names);
    }

    /**
     * @param $code
     * @return int|null
     */
    public static function getLangName($code)
    {
        return static::$short_names[$code] ?? null;
    }

    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'rel_id' => Schema::TYPE_INTEGER,
            'model_key' => Schema::TYPE_INTEGER,
            'language' => Schema::TYPE_INTEGER,
            'field_name' => Schema::TYPE_STRING,
            'field_value' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_TIMESTAMP,
            'updated_at' => Schema::TYPE_TIMESTAMP,
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'field_translation';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['field_value'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'rel_id' => 'ID записи',
            'model_key' => 'Клас модели',
            'language' => 'Язык',
            'field_name' => 'Поле',
            'field_value' => 'Текст поля',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * @return string
     */
    public function getFullLanguage()
    {
        return array_search(static::getLangName($this->language), YII_LANGUAGES);
    }
}
<?php

namespace app\models;

use yii\db\Schema;
use app\helpers\AR;

/**
 * Class Config
 *
 * @property integer $id
 * @property string $property_name
 * @property string $property_value
 */
class Config extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'property_name' => Schema::TYPE_STRING,
            'property_value' => Schema::TYPE_TEXT,
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['property_name', 'property_value'], 'safe', 'on' => 'admin-edit'],
            ['property_name', 'unique', 'on' => 'admin-edit'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'property_name' => 'Властивість',
            'property_value' => 'Значення',

            'description' => 'Опис',
        ];
    }

    /**
     * @param string $name
     * @param mixed $defaultValue
     * @return string|null
     */
    public static function getValue($name, $defaultValue = null)
    {
        /** @var Config $config */
        if($config = static::find()->where(['property_name' => $name])->one()) {
            return $config->property_value;
        }

        return $defaultValue;
    }

    /**
     * @param string $name
     * @param string $value
     * @return bool
     */
    public static function updateValue($name, $value)
    {
        /** @var Config $config */
        if($config = static::find()->where(['property_name' => $name])->one()) {
            $config->property_value = $value;
            return $config->save();
        }

        return false;
    }
}
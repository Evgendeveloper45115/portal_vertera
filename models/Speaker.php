<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;
use app\helpers\AR;
use app\behaviors\TranslationBehavior;

/**
 * Class Speaker
 *
 * @property integer $id
 * @property string $image_id
 * @property string $full_name
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Speaker extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'image_id' => Schema::TYPE_INTEGER,
            'full_name' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_TIMESTAMP,
            'updated_at' => Schema::TYPE_TIMESTAMP,
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'speaker';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['full_name', 'status'], 'safe'],
            [['full_name'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'image_id' => 'Изображение',
            'full_name' => 'ФИО',
            'status' => 'Статус',

            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return ObjectFile
     */
    public function getImage()
    {
        /** @var ObjectFile $o */
        if ($o = $this->hasOne(ObjectFile::class, ['id' => 'image_id'])->one()) {
            $o->setOwnerId($this->id);
        }

        return $o;
    }
}
<?php

namespace app\models;

use app\behaviors\TranslationBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;
use app\helpers\AR;

/**
 * This is the model class for table "promotion".
 *
 * @property int $id
 * @property int $image_id
 * @property int $product_id
 * @property string $title
 * @property string $description
 * @property string $sub_description
 * @property int $status
 * @property string $start_date
 * @property string $end_date
 * @property string $created_at
 * @property string $updated_at
 */
class Promotion extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'image_id' => Schema::TYPE_INTEGER,
            'product_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'sub_description' => Schema::TYPE_TEXT,
            'start_date' => Schema::TYPE_DATETIME,
            'end_date' => Schema::TYPE_DATETIME,
            'status' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'created_at' => Schema::TYPE_TIMESTAMP,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promotion';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'start_date', 'end_date', 'sub_description'], 'string'],
            [['product_id', 'status'], 'safe'],
            [['start_date'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Продукт',
            'image_id' => 'Изображение',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'sub_description' => 'Сокращенное описание',
            'start_date' => 'Дата старта',
            'end_date' => 'Дата окончания',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }

    /**
     * @return ObjectFile
     */
    public function getImage()
    {
        /** @var ObjectFile $o */
        if ($o = $this->hasOne(ObjectFile::class, ['id' => 'image_id'])->one()) {
            $o->setOwnerId($this->id);
        }

        return $o;
    }
}
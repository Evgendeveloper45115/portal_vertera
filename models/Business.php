<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;
use goodizer\helpers\DbSyncHelper;
use app\helpers\AR;
use app\behaviors\TranslationBehavior;

/**
 * This is the model class for table "business".
 *
 * @property int $id
 * @property string $image_id
 * @property string $title
 * @property string $description
 * @property string $sub_description
 * @property int $type
 * @property int $conference_type
 * @property string $button_text
 * @property string $button_url
 * @property string $city_name
 * @property string $start_date
 * @property string $start_date_text
 * @property int $in_top
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Speaker[] $speakers
 * @property BusinessHasSpeaker[] $businessHasSpeakers
 */
class Business extends AR
{
    const TYPE_EVENT = 1;
    const TYPE_CALENDAR = 2;
    const TYPE_BOTH = 3;

    const SEMINAR = 1;
    const WEBINAR = 2;

    public $speaker_id;
    public $speakers = [];

    public static $types = [
        self::TYPE_EVENT => 'event',
        self::TYPE_CALENDAR => 'calendar',
        self::TYPE_BOTH => 'both',
    ];
    public static $conference_types = [
        self::SEMINAR => 'Семинар',
        self::WEBINAR => 'Вебинар',
    ];

    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'image_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'sub_description' => Schema::TYPE_STRING,
            'button_text' => Schema::TYPE_STRING,
            'button_url' => Schema::TYPE_STRING,
            'city_name' => Schema::TYPE_STRING,
            'start_date' => Schema::TYPE_DATETIME,
            'start_date_text' => Schema::TYPE_TEXT,
            'type' => Schema::TYPE_INTEGER,
            'conference_type' => Schema::TYPE_INTEGER,
            'in_top' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_TIMESTAMP,
            'updated_at' => Schema::TYPE_TIMESTAMP,

//            'test_category_id' => Schema::TYPE_INTEGER,
//
//            DbSyncHelper::ADD_CONSTRAINT_REFERENCES => [
//                static::tableName(), 'test_category_id', ProductCategory::tableName(), 'id', 'CASCADE',
//            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'business';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'sub_description', 'start_date_text'], 'string', 'on' => 'admin-edit'],
            [['type', 'conference_type', 'in_top', 'status'], 'integer', 'on' => 'admin-edit'],
            [['title', 'start_date'], 'required', 'on' => 'admin-edit'],
            [['title', 'button_text', 'button_url', 'city_name'], 'string', 'max' => 255, 'on' => 'admin-edit'],

            [['title', 'speakers', 'type', 'conference_type', 'city_name', 'start_date', 'start_date_text', 'in_top', 'status', 'speaker_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $t = 'models/' . static::shortName();

        return [
            'id' => Yii::t($t, 'ID'),
            'image_id' => Yii::t($t, 'Image'),
            'title' => Yii::t($t, 'Title'),
            'description' => Yii::t($t, 'Description'),
            'sub_description' => 'Краткое описание',
            'type' => Yii::t($t, 'Type'),
            'conference_type' => Yii::t($t, 'Conference Type'),
            'button_text' => Yii::t($t, 'Button Text'),
            'button_url' => Yii::t($t, 'Button Url'),
            'city_name' => 'Город',
            'start_date' => Yii::t($t, 'Start Date'),
            'start_date_text' => 'Дата старта(текст)',
            'in_top' => 'На главной',
            'status' => Yii::t($t, 'Status'),
            'created_at' => Yii::t($t, 'Created At'),
            'updated_at' => Yii::t($t, 'Updated At'),

            'speakers' => Yii::t($t, 'Speakers'),
            'total_speakers' => Yii::t($t, 'Total speakers'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessHasSpeakers()
    {
        return $this->hasMany(BusinessHasSpeaker::class, ['business_id' => 'id']);
    }

    /**
     * @return Speaker[]
     */
    public function getSpeakers()
    {
        if($this->isNewRecord) {
            return [];
        }

        return $this->getSpeakersQuery()->where(['bhs.business_id' => $this->id])->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getSpeakersQuery()
    {
        return Speaker::find()
            ->alias('s')
            ->leftJoin(['bhs' => BusinessHasSpeaker::tableName()], 's.id = bhs.speaker_id')
            ->select([
                's.*',
            ])
            ->groupBy([
                's.id',
            ]);
    }

    /**
     * @return ObjectFile
     */
    public function getImage()
    {
        /** @var ObjectFile $o */
        if ($o = $this->hasOne(ObjectFile::class, ['id' => 'image_id'])->one()) {
            $o->setOwnerId($this->id);
        }

        return $o;
    }

}

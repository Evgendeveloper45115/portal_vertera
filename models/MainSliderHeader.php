<?php

namespace app\models;

use himiklab\sortablegrid\SortableGridBehavior;
use yii\db\Schema;
use app\helpers\AR;

/**
 * This is the model class for table "main_slider_header".
 *
 * @property int $id
 * @property string $image_id
 * @property string $url
 * @property string $start_date
 * @property string $end_date
 * @property integer $sort_order
 * @property integer $status
 */
class MainSliderHeader extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'image_id' => Schema::TYPE_INTEGER,
            'url' => Schema::TYPE_STRING,
            'start_date' => Schema::TYPE_DATETIME,
            'end_date' => Schema::TYPE_DATETIME,
            'sort_order' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_slider_header';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'sort_order'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'start_date', 'end_date', 'sort_order', 'status'], 'safe'],
            [['sort_order'], 'unique', 'on' => 'admin-edit'],
            [['url', 'start_date', 'end_date', 'sort_order', 'status'], 'required', 'on' => 'admin-edit'],
            [['url'], 'string', 'max' => 255, 'on' => 'admin-edit'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_id' => 'Изображение',
            'url' => 'Ссылка',
            'start_date' => 'Дата старта',
            'end_date' => 'Дата окончания',
            'sort_order' => 'Позиция',
            'status' => 'Статус',
        ];
    }

    /**
     * @return ObjectFile
     */
    public function getImage()
    {
        /** @var ObjectFile $o */
        if ($o = $this->hasOne(ObjectFile::class, ['id' => 'image_id'])->one()) {
            $o->setOwnerId($this->id);
        }

        return $o;
    }
}

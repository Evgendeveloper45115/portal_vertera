<?php

namespace app\models;

use app\behaviors\TranslationBehavior;
use app\helpers\AR;
use Yii;
use yii\db\Schema;

/**
 * This is the model class for table "faq_category".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 *
 * @property Faq[] $faqs
 */
class FaqCategory extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_category';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'safe'],
            [['status'], 'integer', 'on' => 'admin-edit'],
            [['name'], 'string', 'max' => 255, 'on' => 'admin-edit'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqs()
    {
        return $this->hasMany(Faq::class, ['category_id' => 'id']);
    }
}

<?php

namespace app\models;

use Yii;
use yii\db\Schema;
use app\helpers\AR;

/**
 * This is the model class for table "business_has_speaker".
 *
 * @property int $id
 * @property int $business_id
 * @property int $speaker_id
 *
 * @property Business $business
 * @property Speaker $speaker
 */
class BusinessHasSpeaker extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'business_id' => Schema::TYPE_INTEGER,
            'speaker_id' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'business_has_speaker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['business_id', 'speaker_id'], 'required'],
            [['business_id', 'speaker_id'], 'integer'],
            [['business_id'], 'exist', 'skipOnError' => true, 'targetClass' => Business::class, 'targetAttribute' => ['business_id' => 'id']],
            [['speaker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Speaker::class, 'targetAttribute' => ['speaker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business_id' => 'Business ID',
            'speaker_id' => 'Speaker ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusiness()
    {
        return $this->hasOne(Business::class, ['id' => 'business_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeaker()
    {
        return $this->hasOne(Speaker::class, ['id' => 'speaker_id']);
    }
}

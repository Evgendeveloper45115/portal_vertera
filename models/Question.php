<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;
use app\helpers\AR;
use app\behaviors\TranslationBehavior;

/**
 * This is the model class for table "question".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $subject
 * @property int $type
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class Question extends AR
{
    /**
     * @var array
     */
    public static $types = [
        1 => 'Продукт',
        2 => 'Бизнес'
    ];

    /**
     * @param $type
     * @return mixed|null
     */
    public static function getTypes($type) {
        return static::$types[$type] ?? null;
    }

    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING,
            'subject' => Schema::TYPE_STRING,
            'type' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'created_at' => Schema::TYPE_DATETIME,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'email', 'on' => 'client-create'],
            [['email', 'name', 'type', 'subject'], 'required', 'on' => 'client-create'],
            [['email', 'name', 'type', 'subject'], 'string', 'max' => 100, 'on' => 'client-create'],
            [['subject'], 'string', 'max' => 255, 'on' => 'client-create'],

            [['email', 'name', 'type', 'subject', 'status'], 'required', 'on' => 'admin-edit'],
            [['created_at', 'updated_at'], 'safe', 'on' => 'admin-edit'],

            [['email', 'name', 'type', 'subject', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $t = 'models/' . static::shortName();

        return [
            'id' => Yii::t($t, 'ID'),
            'name' => Yii::t($t, 'Name'),
            'email' => Yii::t($t, 'Email'),
            'subject' => Yii::t($t, 'Question'),
            'type' => Yii::t($t, 'Type'),
            'status' => Yii::t($t, 'Статус'),
            'created_at' => Yii::t($t, 'Created At'),
            'updated_at' => Yii::t($t, 'Updated At'),
        ];
    }
}
<?php

namespace app\models;

use yii\db\Schema;
use app\helpers\AR;
use app\behaviors\TranslationBehavior;

/**
 * This is the model class for table "product_category".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 *
 * @property Product[] $products
 */
class ProductCategory extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'status'], 'required', 'on' => 'admin-edit'],
            [['status'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['category_id' => 'id']);
    }
}

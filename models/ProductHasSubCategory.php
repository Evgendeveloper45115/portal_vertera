<?php

namespace app\models;

use yii\db\Schema;
use app\helpers\AR;

/**
 * This is the model class for table "product_has_files".
 *
 * @property int $id
 * @property int $product_id
 * @property int $sub_category_id
 *
 * @property Business $business
 * @property Speaker $speaker
 */
class ProductHasSubCategory extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER,
            'sub_category_id' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_has_sub_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'sub_category_id' => 'Sub category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }
    public function getSubCategory()
    {
        return $this->hasOne(ProductSubCategory::class, ['id' => 'sub_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(ProductSubCategory::class, ['id' => 'sub_category_id']);
    }
}

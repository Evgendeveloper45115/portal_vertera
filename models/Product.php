<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;
use yii\helpers\ArrayHelper;
use app\helpers\AR;
use app\behaviors\TranslationBehavior;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $api_product_id
 * @property string $title
 * @property string $title_description
 * @property double $cost
 * @property double $cost_original
 * @property double $point
 * @property int $amount
 * @property int $vendor_code
 *
 * @property string $tab_description
 * @property string $tab_composition
 * @property string $tab_use
 *
 * @property string $image_id
 * @property string $description_image_id
 * @property string $info_image_id
 * @property string $info_text
 * @property int $in_top
 * @property string $url_name
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductCategory $productCategory
 */
class Product extends AR
{
    const STATUS_NEW = 2;

    public $certificates;
    public $category_id;
    public $categories;

    /**
     * @param null $status
     * @return array
     */
    public static function getStatuses($status = null)
    {
        $statuses = [
            static::STATUS_ACTIVE => Yii::t('system', 'Active'),
            static::STATUS_DISABLED => Yii::t('system', 'Disabled'),
            static::STATUS_NEW => 'Новый(API)',
        ];

        if (sizeof($args = func_get_args())) {
            return ArrayHelper::getValue($statuses, $args[0]);
        }

        if (null !== $status) {
            return ArrayHelper::getValue($statuses, $status);
        }

        return $statuses;
    }

    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'api_product_id' => Schema::TYPE_INTEGER,

            'image_id' => Schema::TYPE_INTEGER,
            'description_image_id' => Schema::TYPE_INTEGER,
            'info_image_id' => Schema::TYPE_INTEGER,

            'title' => Schema::TYPE_STRING,
            'title_description' => Schema::TYPE_TEXT,
            'cost' => Schema::TYPE_FLOAT,
            'cost_original' => Schema::TYPE_FLOAT,
            'point' => Schema::TYPE_INTEGER,
            'amount' => Schema::TYPE_STRING,
            'vendor_code' => Schema::TYPE_STRING,

            'tab_description' => Schema::TYPE_TEXT,
            'tab_composition' => Schema::TYPE_TEXT,
            'tab_use' => Schema::TYPE_TEXT,

            'info_text' => Schema::TYPE_TEXT,
            'status' => Schema::TYPE_INTEGER,
            'in_top' => Schema::TYPE_INTEGER,

            'url_name' => Schema::TYPE_STRING,

            'created_at' => Schema::TYPE_TIMESTAMP,
            'updated_at' => Schema::TYPE_TIMESTAMP,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'categories', 'cost'], 'required', 'on' => 'admin-edit'],
            [['title'], 'unique', 'on' => 'admin-edit'],
            [['api_product_id', 'status', 'in_top'], 'integer', 'on' => 'admin-edit'],
            [['title_description', 'tab_description', 'tab_composition', 'tab_use', 'info_text'], 'string', 'on' => 'admin-edit'],
            [['cost', 'cost_original', 'point'], 'number', 'on' => 'admin-edit'],
            [['title', 'amount', 'vendor_code', 'url_name'], 'string', 'max' => 255, 'on' => 'admin-edit'],

            [['title', 'cost', 'cost_original', 'amount', 'url_name', 'point', 'vendor_code', 'in_top', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $t = 'models/' . static::shortName();

        return [
            'id' => Yii::t($t, 'ID'),
            'api_product_id' => '№ продукта',
            'category_id' => 'Категория',
            'categories' => 'Подкатегории',

            'image_id' => Yii::t($t, 'Product image'),
            'description_image_id' => Yii::t($t, 'Description image'),
            'info_image_id' => Yii::t($t, 'Information image'),

            'title' => Yii::t($t, 'Title'),
            'title_description' => 'Краткое описание',
            'cost' => Yii::t($t, 'Cost'),
            'cost_original' => 'Начальная цена',
            'point' => Yii::t($t, 'Point'),
            'amount' => Yii::t($t, 'Amount'),
            'vendor_code' => Yii::t($t, 'Vendor code'),

            'tab_description' => Yii::t($t, 'Tab description'),
            'tab_composition' => Yii::t($t, 'Tab composition'),
            'tab_use' => Yii::t($t, 'Tab use'),

            'info_text' => Yii::t($t, 'Information text'),
            'status' => Yii::t($t, 'Status'),
            'in_top' => 'На главной',
            'url_name' => 'Название в ссылке',
            'created_at' => Yii::t($t, 'Created at'),
            'updated_at' => Yii::t($t, 'Updated at'),


            'certificates' => Yii::t($t, 'Certificates'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCategory()
    {
        return $this->hasMany(ProductHasSubCategory::class, ['product_id' => 'id']);
    }

    /**
     * @return ObjectFile
     */
    public function getImage()
    {
        /** @var ObjectFile $o */
        if ($o = $this->hasOne(ObjectFile::class, ['id' => 'image_id'])->one()) {
            $o->setOwnerId($this->id);
        }

        return $o;
    }

    /**
     * @return ObjectFile
     */
    public function getDescription_image()
    {
        /** @var ObjectFile $o */
        if ($o = $this->hasOne(ObjectFile::class, ['id' => 'description_image_id'])->one()) {
            $o->setOwnerId($this->id);
        }

        return $o;
    }

    /**
     * @return ObjectFile
     */
    public function getInfo_image()
    {
        /** @var ObjectFile $o */
        if ($o = $this->hasOne(ObjectFile::class, ['id' => 'info_image_id'])->one()) {
            $o->setOwnerId($this->id);
        }

        return $o;
    }

    /**
     * @return ObjectFile[]|array|\yii\db\ActiveRecord[]
     */
    public function getCertificates()
    {
        $q = ObjectFile::find()
            ->alias('o')
            ->leftJoin(['phf' => ProductHasFiles::tableName()], 'o.id = phf.file_id')
            ->select(['o.*'])
            ->where([
                'o.type' => ObjectFile::TYPE_PRODUCT_CERTIFICATE_FILE,
                'phf.product_id' => $this->id,
            ])
            ->groupBy(['o.id']);

        return $q->all();
    }
    
    public function getCertificatesPreview()
    {
        $certicates = $this->getCertificates();
        
        $out = [];
        
        foreach ($certicates as $certicate) {
            $pathArr = explode('web', $certicate->getPath());
            $out[] = $pathArr[1];
        }
        
        return $out;
    }

    public function getCategories()
    {
        if ($this->isNewRecord) {
            return [];
        }

        return $this->getCategoriesQuery()->where(['phsc.product_id' => $this->id])->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getCategoriesQuery()
    {
        return ProductHasSubCategory::find()
            ->alias('phsc')
            ->leftJoin(['p' => Product::tableName()], 'phsc.product_id = p.id')
            ->select([
                'phsc.*',
            ])
            ->groupBy([
                'phsc.id',
            ]);
    }

}
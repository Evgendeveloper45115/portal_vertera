<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;
use app\helpers\AR;
use app\behaviors\TranslationBehavior;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $country_name
 * @property string $city_name
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $map_lat
 * @property string $map_lng
 * @property string $map_enable
 * @property string $type
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Contact extends AR
{
    const MAP_ENABLED = 1;
    const MAP_DISABLED = 0;

    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'country_name' => Schema::TYPE_STRING,
            'city_name' => Schema::TYPE_STRING,
            'address' => Schema::TYPE_STRING,
            'phone' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING,
            'map_lat' => Schema::TYPE_STRING,
            'map_lng' => Schema::TYPE_STRING,
            'map_enable' => Schema::TYPE_INTEGER,
            'type' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'created_at' => Schema::TYPE_DATETIME,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_name', 'city_name', 'address', 'phone', 'map_enable', 'status'], 'required', 'on' => 'admin-edit'],
            [['country_name', 'city_name', 'address', 'map_lat', 'map_lng', 'phone', 'email'], 'string', 'max' => 255],
            [['country_name', 'city_name', 'address', 'phone', 'email', 'map_lat', 'map_lng', 'map_enable', 'type', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_name' => 'Страна',
            'city_name' => 'Город',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'email' => 'Email',
            'map_enable' => 'С меткой на карте',
            'type' => 'Представительство',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }
}

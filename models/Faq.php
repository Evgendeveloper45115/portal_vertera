<?php

namespace app\models;

use app\behaviors\TranslationBehavior;
use app\helpers\AR;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property int $category_id
 * @property string $theme
 * @property string $question
 * @property string $answer
 * @property string $client_information
 * @property int $in_top
 * @property string $status
 *
 * @property string $created_at
 * @property string $updated_at
 *
 * @property FaqCategory $faq
 */
class Faq extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'category_id' => Schema::TYPE_INTEGER,
            'theme' => Schema::TYPE_STRING,
            'question' => Schema::TYPE_TEXT,
            'answer' => Schema::TYPE_TEXT,
            'client_information' => Schema::TYPE_STRING,
            'in_top' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'created_at' => Schema::TYPE_DATETIME,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['theme', 'client_information', 'question', 'answer', 'category_id', 'status'], 'required', 'on' => 'admin-edit'],
            [['category_id', 'in_top', 'status'], 'integer', 'on' => 'admin-edit'],
            [['theme', 'question', 'answer'], 'string', 'on' => 'admin-edit'],
            [['theme', 'client_information'], 'string', 'max' => 255, 'on' => 'admin-edit'],

            [['theme', 'category_id', 'question', 'answer', 'client_information', 'in_top', 'status', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'theme' => 'Тема',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'client_information' => 'Информация о клиенте',
            'in_top' => 'На главной',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }
    public function getCategory()
    {
        return $this->hasOne(FaqCategory::class, ['id' => 'category_id']);
    }

}

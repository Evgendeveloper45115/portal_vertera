<?php

namespace app\models;

use yii\db\Schema;
use app\behaviors\TranslationBehavior;
use app\helpers\AR;

/**
 * This is the model class for table "product_sub_category".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $description
 * @property int $status
 *
 * @property Product[] $products
 */
class ProductSubCategory extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'parent_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_sub_category';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => TranslationBehavior::class,
                'default_language' => YII_DEFAULT_LANGUAGE
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'name', 'description', 'status'], 'required', 'on' => 'admin-edit'],
            [['parent_id', 'status'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Категория',
            'name' => 'Наименование',
            'description' => 'Описание',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['sub_category_id' => 'id']);
    }

    /**
     * @return ProductSubCategory|array|null|\yii\db\ActiveRecord
     */
    public function getParent()
    {
        return $this->hasMany(ProductCategory::class, ['id' => 'parent_id'])->one();
    }
}

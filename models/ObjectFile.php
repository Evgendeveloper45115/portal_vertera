<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\base\UnknownClassException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Schema;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\web\UploadedFile;
use himiklab\thumbnail\EasyThumbnailImage;
use app\helpers\AR;

/**
 * Class ObjectFile
 *
 * @property integer $id
 * @property string $name
 * @property string $original_name
 * @property integer $type
 * @property string $ext
 * @property string $updated_at
 * @property string $created_at
 */
class ObjectFile extends AR
{
    const TYPE_PRODUCT_IMAGE = 1;
    const TYPE_PRODUCT_DESCRIPTION_IMAGE = 2;
    const TYPE_PRODUCT_INFO_IMAGE = 3;
    const TYPE_PRODUCT_CERTIFICATE_FILE = 4;

    const TYPE_BUSINESS_IMAGE = 5;

    const TYPE_SPEAKER_IMAGE = 6;

    const TYPE_PROMOTION_IMAGE = 7;

    const TYPE_MAIN_SLIDER_IMAGE = 8;

    const TYPE_NEWS_IMAGE = 9;

    const EXT_IMAGE = 1;
    const EXT_FILE = 2;

    private $_ownerId;

    public $current_language;

    /**
     * @var array Field map for dynamic single|multiple files
     */
    public static $types = [
        'Product' => [
            'image_id' => self::TYPE_PRODUCT_IMAGE,
            'description_image_id' => self::TYPE_PRODUCT_DESCRIPTION_IMAGE,
            'info_image_id' => self::TYPE_PRODUCT_INFO_IMAGE,
            'certificates' => self::TYPE_PRODUCT_CERTIFICATE_FILE,
        ],
        'Business' => [
            'image_id' => self::TYPE_BUSINESS_IMAGE,
        ],
        'Speaker' => [
            'image_id' => self::TYPE_SPEAKER_IMAGE,
        ],
        'Promotion' => [
            'image_id' => self::TYPE_PROMOTION_IMAGE,
        ],
        'MainSliderHeader' => [
            'image_id' => self::TYPE_MAIN_SLIDER_IMAGE,
        ],
        'News' => [
            'image_id' => self::TYPE_NEWS_IMAGE,
        ],
    ];
    public static $fileExt = ['txt', 'xml', 'csv', 'xls', 'xlsx', 'json', 'pdf'];
    public static $mb = 6;
    public static $imageExt = [
        'jpeg', 'jpg', //(Joint Photographic Experts Group)
        'png', //(Portable Network Graphics)
        'gif', //(Portable Network Graphics)
    ];

    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'original_name' => Schema::TYPE_STRING,
            'type' => Schema::TYPE_INTEGER,
            'ext' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_TIMESTAMP,
            'created_at' => Schema::TYPE_DATETIME,
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'object_file';
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $filePath = $this->getPath();
            if (file_exists($filePath)) {
                unlink($filePath);
            }
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => new Expression('now()')
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'original_name', 'type', 'ext', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'original_name' => 'Название(оригинал)',
            'type' => 'Тип',
            'ext' => 'Расширение',
            'updated_at' => 'Обновлен',
            'created_at' => 'Создан',
        ];
    }

    /**
     * @param null|integer $mb
     * @return integer number of Bytes
     */
    public static function getMaxSize($mb = null)
    {
        return 1024 * 1024 * ($mb ?: static::$mb);
    }

    /**
     * @param null|integer $mb
     * @return integer number of K/b
     */
    public static function maxKb($mb = null)
    {
        return 1024 * ($mb ?: static::$mb);
    }

    /**
     * @param mixed $extType
     * @param mixed $ext
     * @return array|null
     */
    public static function getAllowedExtensions($extType = null, $ext = null)
    {
        $extTypes = [
            static::EXT_IMAGE => static::$imageExt,
            static::EXT_FILE => static::$fileExt,
        ];

        if (null !== $extType) {
            if (isset($extTypes[$extType])) {
                if (null !== $ext) {
                    $ext = strtolower($ext);
                    return in_array($ext, $extTypes[$extType]) ? $ext : null;
                }

                return $extTypes[$extType];
            }

            return null;
        }

        return static::$imageExt + static::$fileExt;
    }

    /**
     * @param null $type
     * @return array|string|null
     */
    public static function getFolders($type = null)
    {
        $folders = [
            static::TYPE_PRODUCT_IMAGE => 'upload/images/{lang}/products/image/{name}/',
            static::TYPE_PRODUCT_DESCRIPTION_IMAGE => 'upload/images/{lang}/products/description/{name}/',
            static::TYPE_PRODUCT_INFO_IMAGE => 'upload/images/{lang}/products/info/{name}/',
            static::TYPE_PRODUCT_CERTIFICATE_FILE => 'upload/images/{lang}/products/certificates/',

            static::TYPE_BUSINESS_IMAGE => 'upload/images/{lang}/business/image/{name}/',

            static::TYPE_SPEAKER_IMAGE => 'upload/images/{lang}/speaker/image/{name}/',

            static::TYPE_PROMOTION_IMAGE => 'upload/images/{lang}/promotion/image/{name}/',

            static::TYPE_MAIN_SLIDER_IMAGE => 'upload/images/{lang}/main-slider/image/{name}/',
            static::TYPE_NEWS_IMAGE => 'upload/images/{lang}/news/{name}/',
        ];

        if (null !== $type) {
            return isset($folders[$type]) ? $folders[$type] : null;
        }

        return $folders;
    }

    /**
     * @var string
     */
    public static $no_image_path = 'web/images/no_picture.gif';

    /**
     * @param $absolute bool
     * @return string
     */
    public static function getNoImage($absolute = true)
    {
        $f = static::$no_image_path;
        return $absolute ? \Yii::getAlias("@app/{$f}") : \Yii::getAlias("@{$f}");
    }

    /**
     * @param $file UploadedFile
     * @param $type
     * @param $ownerId
     * @param $ext
     * @param bool $deleteTempFile
     *
     * @return bool|ObjectFile
     */
    public static function createFile($file, $type, $ownerId, $ext = ObjectFile::EXT_IMAGE, $deleteTempFile = true)
    {
        $objectFile = new static();
        $objectFile->setOwnerId($ownerId);
        $objectFile->name = md5(microtime(true) . $file->tempName) . '.' . $file->getExtension();
        $objectFile->original_name = $file->name;
        $objectFile->type = $type;
        $objectFile->ext = $ext;

        $path = $objectFile->getPath();

        if (!is_dir(dirname($path))) {
            mkdir(dirname($path), 0755, true);
        }

        if ($file->saveAs($path, $deleteTempFile) && $objectFile->save()) {
            return $objectFile;
        }

        return false;
    }

    /**
     * @param AR $model
     * @param $prop
     * @return bool
     */
    public static function saveByLanguage($model, $prop)
    {
        /** @var $o static */
        if (!$o = $model->{'get' . str_replace('_id', '', $prop)}()) {
            return false;
        }

        $saved = true;

        foreach (FieldTranslation::$short_names as $lang) {
            if ($lang == YII_DEFAULT_LANGUAGE
                || !$lang_instance = UploadedFile::getInstanceByName("FieldTranslation[{$lang}][{$prop}]")
            ) {
                continue;
            }

            $o->current_language = $lang;
            $path = $o->getPath();

            if (!is_dir(dirname($path))) {
                mkdir(dirname($path), 0755, true);
            }

            $saved &= $lang_instance->saveAs($path, true);
        }

        return $saved;
    }

    /**
     * @param $model ActiveRecord|static
     * @return bool
     */
    public static function dynamicSave($model)
    {
        $saved = true;
        foreach ($_FILES[$model::shortName(true)]['name'] as $attr => $name_img) {
            if (empty($name_img) || is_array($name_img) && empty($name_img[0])) {
                continue;
            }
            if (is_array($name_img)) {
                $saved &= !empty(ObjectFile::saveInstancesImage(
                    $model,
                    $attr,
                    ObjectFile::$types[$model::shortName(true)][$attr]
                ));
            } else {
                $saved &= false !== ObjectFile::saveInstanceImage(
                        $model,
                        $attr,
                        ObjectFile::$types[$model::shortName(true)][$attr],
                        $model->id,
                        str_replace('_id', '', $attr)
                    );
            }
        }

        return $saved;
    }

    /**
     * @param mixed|ActiveRecord $model
     * @param UploadedFile | string $instance
     * @param $type
     * @param $folderName
     * @param $prop
     *
     * @return bool|ObjectFile
     */
    public static function saveInstanceImage($model, $instance, $type, $folderName, $prop = 'file')
    {
        if (($instance instanceof UploadedFile || ($instance = UploadedFile::getInstance($model, $instance)))
            && static::validateInstance($instance)
        ) {
            if ($objectFile = $model->{'get' . $prop}()) {
                /** @var static $objectFile */
                $objectFile->delete();
                $model->{$prop . '_id'} = null;
            }

            if ($objectFile = ObjectFile::createFile($instance, $type, $folderName)) {
                $model->{$prop . '_id'} = $objectFile->id;

                return $objectFile;
            }
        }

        return false;
    }

    /**
     * @param $model mixed|ActiveRecord
     * @param $instanceName
     * @param $type
     * @param null $relCls
     * @return array
     * @throws FileValidateException
     * @throws UnknownClassException
     */
    public static function saveInstancesImage($model, $instanceName, $type, $relCls = null)
    {
        $objectFiles = [];
        $instances = UploadedFile::getInstances($model, $instanceName);
        if (!empty($instances)) {
            foreach ($instances as $instance) {
                static::validateInstance($instance);
            }

            $cls = $model->shortName(true);
            $relCls = $relCls ?: ('app\\models\\' . $cls . 'HasFiles');

            if (!class_exists($relCls)) {
                throw new UnknownClassException("Unknown class: '{$relCls}'");
            }

            foreach ($instances as $instance) {
                if ($objectFile = ObjectFile::createFile($instance, $type, $model->id)) {
                    /** @var ActiveRecord $relModel */
                    $relModel = new $relCls();

                    $relModel->{Inflector::camel2id($cls, '_') . '_id'} = $model->id;
                    $relModel->{'file_id'} = $objectFile->id;
                    if ($relModel->save()) {
                        $objectFiles[] = $objectFile;
                    }
                }
            }
        }

        return $objectFiles;
    }

    /**
     * @param UploadedFile $instance
     * @param integer|null $extType
     *
     * @return true
     * @throws FileValidateException
     */
    public static function validateInstance(UploadedFile $instance, $extType = null)
    {
        if (null === static::getAllowedExtensions($extType, $instance->getExtension())) {
            throw new FileValidateException(
                'Изображение должно иметь расширения - ' . implode(' ,', static::getAllowedExtensions($extType))
            );
        } elseif ($instance->size > static::getMaxSize()) {
            throw new FileValidateException('Максимальный размер изображения - ' . static::getMaxSize() . ' kb');
        }

        return true;
    }

    /**
     * @param $id
     */
    public function setOwnerId($id)
    {
        $this->_ownerId = $id;
    }

    /**
     * @return string
     */
    public function getOwnerId()
    {
        return $this->_ownerId;
    }

    /**
     * @param bool $absolute
     * @param bool $checkExist
     * @return string
     */
    public function getPath($absolute = true, $checkExist = false)
    {
        $path = DIRECTORY_SEPARATOR;

        if ($absolute) {
            $path = \Yii::getAlias('@app/web/');
        }

        $path .= strtr(static::getFolders($this->type), [
                '{lang}' => $this->current_language ?: Yii::$app->language,
                '{name}' => $this->getOwnerId(),
            ]) . $this->name;

        $path = FileHelper::normalizePath($path);

        if ($checkExist && !file_exists($path)) {
            $path = DIRECTORY_SEPARATOR;

            if ($absolute) {
                $path = \Yii::getAlias('@app/web/');
            }

            $path .= strtr(DIRECTORY_SEPARATOR . static::getFolders($this->type), [
                    '{lang}' => YII_DEFAULT_LANGUAGE,
                    '{name}' => $this->getOwnerId(),
                ]) . $this->name;
            $path = FileHelper::normalizePath($path);

            if (!file_exists($path)) {
                return static::getNoImage($absolute);
            }
        }


        return $path;
    }

    /**
     * @param mixed $model
     * @param string $prop
     * @param bool $checkExist
     * @return string|null
     */
    public static function getAssetPath($model, $prop = 'file', $checkExist = false)
    {
        /** @var $o static */
        $o = $model->{'get' . $prop}();
        if (null === $o || !file_exists($o->getPath(true, $checkExist))) {
            return !$checkExist ? \Yii::$app->getAssetManager()->publish(static::getNoImage())[1] : null;
        }

        return \Yii::$app->getAssetManager()->publish($o->getPath(true, $checkExist))[1];
    }

    /**
     * @param $model
     * @param string $prop
     * @param array $sizes
     * @param bool $checkExist
     * @return mixed
     */
    public static function getThumbnail($model, $prop = 'file', $sizes = [90, 90], $checkExist = false)
    {
        /** @var $o static */
        if (null === ($o = $model->{'get' . ucfirst($prop)}()) || !file_exists($o->getPath())) {
            if(!$checkExist) {
                return null;
            }
            return \Yii::$app->getAssetManager()->publish(static::getNoImage())[1];
        }

        return $o->getThumbAssetPath($sizes);
    }

    /**
     * @param array $sizes
     * @return string
     */
    public function getThumbAssetPath($sizes = [90, 90])
    {
        /** @var $o static */
        if (!file_exists($this->getPath())) {
            $path = EasyThumbnailImage::thumbnailFile(
                static::getNoImage(), $sizes[0], $sizes[1], EasyThumbnailImage::THUMBNAIL_INSET
            );
        } else {
            $path = EasyThumbnailImage::thumbnailFile(
                $this->getPath(), $sizes[0], $sizes[1], EasyThumbnailImage::THUMBNAIL_INSET
            );
        }

        return preg_replace('/\\\/', '/', preg_replace('/^.+([\\\|\/]assets.+)/', '$1', FileHelper::normalizePath($path)));
    }
}

class FileValidateException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'FileValidateException';
    }

}
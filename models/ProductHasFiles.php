<?php

namespace app\models;

use app\helpers\AR;
use Yii;
use yii\db\Schema;

/**
 * This is the model class for table "product_has_files".
 *
 * @property int $id
 * @property int $product_id
 * @property int $file_id
 *
 * @property Business $business
 * @property Speaker $speaker
 */
class ProductHasFiles extends AR
{
    /**
     * @return array
     */
    public function attributeTypes()
    {
        return [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER,
            'file_id' => Schema::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_has_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'file_id' => 'File ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(ObjectFile::class, ['id' => 'file_id']);
    }
}

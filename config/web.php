<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru_RU',
    'timeZone' => 'Europe/Moscow',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => 'WybZgmjWGeqD47KPaE_2UMlvJDOxnF0x',
            'class' => 'klisl\languages\Request'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db + [
                'class' => 'yii\db\Connection',
                'enableSchemaCache' => true,
                'enableQueryCache' => true,
                'queryCacheDuration' => 1,
                'charset' => 'utf8',
                'on afterOpen' => function ($event) {
                    $event->sender->createCommand("SET time_zone='+00:00';")->execute();
                },
            ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'app\components\UrlManager',
            'rules' => [
                'languages' => 'languages/default/index', //для модуля мультиязычности

                'admin' => 'admin/product',
                'admin/<controller:.+>' => 'admin/<controller>',
                'admin/<controller:.+>/<action:.+>' => 'admin/<controller>/<action>',

                [
                    'pattern' => '<action:vertera_gel|business-vertera|world-of-retail|university|production|vertera-gel-sport|privacy-policy>',
                    'route' => 'site/<action>',
                    'suffix' => '',
                ],

                [
                    'pattern' => 'catalog/<cat_id:\w+>/<sub_cat_id:\w+>/<id:\w+>',
                    'route' => 'product/view',
                    'suffix' => '/',
                ],

                [
                    'pattern' => 'catalog/<cat_id:\w+>/<id:\w+>',
                    'route' => 'product/view',
                    'suffix' => '/',
                ],

                [
                    'pattern' => '<controller:(category)>/<id:\w+>/<sub_id:\w+>',
                    'route' => '<controller>/view',
                    'suffix' => '/',
                ],
                [
                    'pattern' => '<controller:(category)>/<id:\w+>',
                    'route' => '<controller>/view',
                    'suffix' => '/',
                ],

                [
                    'pattern' => '<controller:(calendar)>/<id:\w+>',
                    'route' => '<controller>/view',
                    'suffix' => '/',
                ],

                [
                    'pattern' => '<controller:(event)>/<id:\w+>',
                    'route' => '<controller>/view',
                    'suffix' => '/',
                ],

                [
                    'pattern' => '<controller:(news)>/<id:\w+>',
                    'route' => '<controller>/view',
                    'suffix' => '/',
                ],

                [
                    'pattern' => '<controller:(promotion)>/<id:\w+>',
                    'route' => '<controller>/view',
                    'suffix' => '/',
                ],

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'system',
                ],
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'defaultRoute' => 'admin/product/create',
        ],
        'languages' => [
            'class' => 'klisl\languages\Module',
            'languages' => YII_LANGUAGES,
            'default_language' => YII_DEFAULT_LANGUAGE,
            'show_default' => true,
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = ['class' => 'yii\debug\Module'];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = ['class' => 'yii\gii\Module'];
}

return $config;
<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru',
    'timeZone' => 'Europe/Moscow',
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'api' => [
            'class' => 'app\components\os\ApiComponent',
            'magicKey' => '12321',
            'osEngine' => '/api/engine.php',
            'osBaseUrl' => 'https://os.verteraorganic.com'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' =>  $db + [
                'class' => 'yii\db\Connection',
                'enableSchemaCache' => true,
                'enableQueryCache' => true,
                'queryCacheDuration' => 1,
                'charset' => 'utf8',
                'on afterOpen' => function ($event) {
                    $event->sender->createCommand("SET time_zone = '" . date('P') . "'")->execute();
                },
            ],
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
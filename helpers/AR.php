<?php

namespace app\helpers;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * Class AR
 *
 * @property int $id
 *
 * @package extensions
 */
class AR extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;

    /**
     * @param null $status
     * @return array
     */
    public static function getStatuses($status = null)
    {
        $statuses = [
            static::STATUS_ACTIVE => Yii::t('system', 'Active'),
            static::STATUS_DISABLED => Yii::t('system', 'Disabled'),
        ];

        if (sizeof($args = func_get_args())) {
            return ArrayHelper::getValue($statuses, $args[0]);
        }

        if (null !== $status) {
            return ArrayHelper::getValue($statuses, $status);
        }

        return $statuses;
    }

    /**
     * @param $id
     * @return static
     * @throws BadRequestHttpException
     */
    public static function findById($id)
    {
        if (!$id || null === ($model = static::findOne($id))) {
            throw new BadRequestHttpException("Object not find!", 403);
        }

        return $model;
    }

    /**
     * @param null $id
     * @return static
     * @throws BadRequestHttpException
     */
    public static function getModel($id = null)
    {
        if (null === $id) {
            $model = new static();
        } elseif (null === $model = static::findOne($id)) {
            throw new BadRequestHttpException("Access denied!", 403);
        }

        return $model;
    }

    /**
     * @param null $id
     * @return static
     * @throws BadRequestHttpException
     */
    public static function getActive($id = null)
    {
        if (null === $model = static::find()->where(['id' => $id, 'status' => static::STATUS_ACTIVE])->one()) {
            throw new BadRequestHttpException("Access denied!", 403);
        }

        return $model;
    }

    /**
     * @param mixed $condition
     * @param bool $asArray
     * @return static[]|array
     */
    public static function getModels($condition = null, bool $asArray = false): array
    {
        return static::find()->where($condition)->asArray($asArray)->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeTypes()
    {
        return [];
    }

    /**
     * @param bool $upper
     * @return string
     */
    public static function shortName($upper = false)
    {
        $n = (new \ReflectionClass(static::class))->getShortName();
        return $upper ? $n : lcfirst($n);
    }
}

<?php

namespace app\helpers;

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use dosamigos\ckeditor\CKEditor;
use app\models\FieldTranslation;
use app\models\ObjectFile;
use yii\helpers\Url;

/**
 * Class H
 * @package app\helpers
 */
class H
{
    /**
     * @param $text
     * @return string
     */
    static function mb_ucfirst($text)
    {
        return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
    }

    /**
     * @param ActiveForm $form
     * @param AR|mixed $model
     * @param string $attribute
     */
    static function render_language_file_field($form, $model, $attribute)
    {
        /** @var ObjectFile $objectFile */
        $objectFile = $model->{'get' . str_replace('_id', '', $attribute)}();

        echo $form->field($model, $attribute)->widget(FileInput::class, [
            'options' => ['accept' => 'image/*'],
            'objectFiles' => $model->getImage(),
        ])->label($model->getAttributeLabel($attribute) . ' - ' . Html::button('Загрузка по языкам', [
                'class' => 'btn btn-warning btn-xs',
                'data-toggle' => 'modal',
                'data-target' => "#modal-{$attribute}-language-file-fields",
            ]));

        Modal::begin([
            'header' => '<h3>Изображения по языкам</h3>',
            'options' => ['id' => "modal-{$attribute}-language-file-fields"],
        ]);

        foreach (FieldTranslation::$short_names as $lang) {
            if ($lang == YII_DEFAULT_LANGUAGE)
                continue;

            if ($objectFile) {
                $objectFile->current_language = $lang;
            }

            echo '<div class="row"><div class="col-md-12">';
            echo FileInput::widget([
                'label' => array_search($lang, YII_LANGUAGES),
                'name' => "FieldTranslation[{$lang}][{$attribute}]",
                'objectFiles' => $objectFile,
            ]);
            echo '</div></div>';
        }

        echo '<div class="row"><div class="col-md-12">';
        echo Html::button('Принять', ['class' => 'btn btn-success', 'data-dismiss' => 'modal']);
        echo '</div></div>';

        Modal::end();
    }

    /**
     * @param ActiveForm $form
     * @param AR $model
     * @param $attribute
     * @return mixed
     */
    static function render_languages_editor_field($form, $model, $attribute)
    {
        $html_parts = ['<span style="font-weight: normal;">Переводы</span>'];

        foreach (FieldTranslation::$short_names as $lang) {
            if (YII_DEFAULT_LANGUAGE == $lang)
                continue;

            $html_parts[] = Html::button($lang, [
                'class' => 'btn btn-warning btn-xs',
                'data' => [
                    'label' => $model->getAttributeLabel($attribute),
                    'translate-url' => Url::to([
                        'translate-single',
                        'id' => $model->id,
                        'field' => $attribute,
                        'model_key' => array_search(get_class($model), FieldTranslation::$model_keys),
                        'lang_code' => FieldTranslation::getLangCode($lang),
                    ]),
                ],
            ]);
        }

        return $form->field($model, $attribute)->widget(CKEditor::class, [
            'options' => ['rows' => 4],
            'preset' => 'full',
        ])->label($model->getAttributeLabel($attribute) . ' - ' . implode(' ', $html_parts));
    }
}
<?php
namespace app\helpers;

use app\models\ObjectFile;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;

/**
 * Class FileInput
 * @package extensions
 */
class FileInput extends \kartik\widgets\FileInput
{
    /**
     * @var ObjectFile[]|ObjectFile
     */
    public $objectFiles;

    /**
     * @var array
     */
    public $sizes = [300, 300];

    /**
     * @var null
     */
    public $ext;

    /**
     * @var string
     */
    public $label;

    /**
     * @var bool
     */
    public $withFooter = true;

    /**
     * @var callable
     */
    public $deleteUrlCallback;

    public function init()
    {
        $am = $this->getView()->getAssetManager();
        $this->ext = $this->ext ?: ObjectFile::EXT_IMAGE;
        $this->options = ArrayHelper::merge(['accept' => 'image/*'], $this->options);

        $this->pluginOptions = ArrayHelper::merge([
            'allowedFileExtensions' => ObjectFile::getAllowedExtensions($this->ext),
            'maxFileCount' => 10,
            'maxFileSize' => ObjectFile::maxKb(),
            'maxPreviewFileSize' => 0,
            'showPreview' => true,
            'showRemove' => true,
            'showUpload' => true,
            'showClose' => true,
            'overwriteInitial' => true,
            'initialCaption' => 'Файлов не выбрано...',
            'initialPreviewShowDelete' => true,
            'fileActionSettings' => [
                'showZoom' => false,
                'showDrag' => false,
            ],
            'layoutTemplates' => [],
        ], $this->pluginOptions);

        $this->pluginOptions['layoutTemplates']['main1'] = "<div class='input-group {class}'>" . "\r\n" .
            "   {caption}" . "\r\n" .
            "   <div class='input-group-btn'>" . "\r\n" .
            "       {remove}" . "\r\n" .
            "       {cancel}" . "\r\n" .
            "       {upload}" . "\r\n" .
            "       {browse}" . "\r\n" .
            "   </div>" . "\r\n" .
            "</div>" . "\r\n" .
            "<div class='kv-upload-progress hide'></div>" . "\r\n" . "{preview}";

        if ($this->withFooter === false && !isset($this->pluginOptions['layoutTemplates']['footer'])) {
            $this->pluginOptions['layoutTemplates']['footer'] = '';
        }

        $this->options['multiple'] = isset($this->options['multiple']) ? $this->options['multiple'] : false;
        if ($this->hasModel() && $this->options['multiple'] && strpos($this->attribute, '[]') !== mb_strlen($this->attribute, 'utf-8') - 2) {
            $this->attribute .= '[]';
        }

        if ($this->objectFiles) {
            if (is_object($this->objectFiles)) {
                $this->objectFiles = [$this->objectFiles];
            }

            foreach ($this->objectFiles as $k => $file) {
                if (null === $file) {
                    break;
                }

                $path = $file->getPath();

                if (!empty($this->deleteUrlCallback) && $this->ext == ObjectFile::EXT_IMAGE) {
                    if (file_exists($path)) {
                        $pubPath = \Yii::$app->getRequest()->getHostInfo() . $am->publish($path)[1];
                        $this->pluginOptions['initialPreviewAsData'] = true;
                        $this->pluginOptions['initialPreview'][$k] = $pubPath;
                        $this->pluginOptions['initialPreviewFileType'] = 'image';

                        $mimeType = preg_replace('/^\w+\/(\w+)$/', '$1', FileHelper::getMimeType($path));

                        if (in_array($mimeType, ObjectFile::$imageExt)) {
                            $mimeType = 'image';
                            $this->pluginOptions['initialPreview'][$k] = $pubPath;
                        }

                        $this->pluginOptions['initialPreviewConfig'][$k] = [
                            'type' => $mimeType,
                            'size' => filesize($path),
                            'caption' => $file->original_name,
                            'key' => $file->id,
                            'frameClass' => 'file-preview-frame-custom',
                            'url' => $this->deleteUrlCallback
                        ];
                    }
                } elseif ($this->ext == ObjectFile::EXT_FILE) {
                    if (file_exists($path)) {
                        $pubPath = \Yii::$app->getRequest()->getHostInfo() . $am->publish($path)[1];
                        $this->pluginOptions['initialPreviewAsData'] = true;
                        $this->pluginOptions['previewFileType'] = 'any';
                        $this->pluginOptions['initialPreview'][$k] = $pubPath;

                        $mimeType = preg_replace('/^\w+\/(\w+)$/', '$1', FileHelper::getMimeType($path));

                        // ['image', 'html', 'text', 'video', 'audio', 'flash', 'pdf', 'object']
                        if (!in_array($mimeType, ObjectFile::$fileExt)) {
                            $mimeType = 'text';
                            $this->pluginOptions['initialPreview'][$k] = file_get_contents($pubPath);
                        }

                        $this->pluginOptions['initialPreviewConfig'][$k] = [
                            'type' => $mimeType,
                            'size' => filesize($path),
                            'caption' => $file->original_name,
                            'key' => $file->id,
                            'frameClass' => 'file-preview-frame-custom',
//                             'url' => is_callable($this->deleteUrlCallback)
//                                 ? call_user_func($this->deleteUrlCallback, $file)
//                                 : null,
                        ];
                    }
                } else {
                    if (file_exists($path)) {
                        $new_path = EasyThumbnailImage::thumbnailFile(
                            $path,
                            $this->sizes[0], $this->sizes[1],
                            EasyThumbnailImage::THUMBNAIL_INSET
                        );

                        $this->pluginOptions['initialPreview'][$k] = Html::img(
                            preg_replace('/^.+(\/assets.+)/', '$1', $new_path),
                            ['height' => 140]
                        );
                        $this->pluginOptions['initialPreviewConfig'][$k] = ['frameClass' => 'file-preview-frame-custom'];
                    }
                }

                if (!isset($this->options['multiple']) || $this->options['multiple'] !== true) {
                    break;
                }
            }
        }

        if (!$this->hasModel() && $this->label) {
            echo Html::beginTag('div', ['class' => 'file-input-custom field field-' . strtolower($this->name)]);
            echo Html::label($this->label, $this->getId());
        }

        parent::init();

        if (!$this->hasModel() && $this->label) {
            echo Html::endTag('div');
        }
    }
}
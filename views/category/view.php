<?php

/**
 * @var $this yii\web\View
 * @var $current_cat ProductCategory
 * @var $current_sub_cat ProductSubCategory|null
 * @var $sub_id int|null
 * @var $dp \yii\data\ActiveDataProvider
 */

use yii\widgets\ListView;
use yii\helpers\Url;
use app\models\ProductCategory;
use app\models\ProductSubCategory;

$this->title = $sub_id && $current_sub_cat ? $current_sub_cat->name : $current_cat->name;
?>
<div class="b-descr-page b-descr-page-for-catalog">
    <div class="b-descr-page__bg">
        <img src="<?= Yii::getAlias('@web/images/bgs/bg-1.jpg') ?>" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="b-descr-page__substrate-bg">
                    <div class="b-descr-page-substrate-bg">
                        <h1 class="b-descr-page__title"><?= $this->title ?></h1>

                        <p class="b-descr-page__text"><?= $sub_id && $current_sub_cat
                                ? $current_sub_cat->description
                                : $current_cat->description ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="b-catalog js-catalog">
    <div class="container">
        <div class="row">

            <div class="col-sm-3">
                <aside class="b-sidebar js-sidebar">
                    <ul>
                        <li>
                            <a href="<?= Url::to(['/site/vertera_gel']) ?>" class="js-item"><?= Yii::t('labels', 'Vertera Gel') ?></a>
                        </li>

                        <?php
                        $categories = ProductCategory::find()->where([
                            'status' => ProductCategory::STATUS_ACTIVE,
                        ])->all();
                        foreach ($categories as $category) {
                            $sub_categories = ProductSubCategory::find()->where([
                                'parent_id' => $category->id,
                                'status' => ProductSubCategory::STATUS_ACTIVE,
                            ])->all();
                            ?>
                            <li>
                                <a href="<?= Url::to(['/category/view', 'id' => $category->id]) ?>"
                                   class="js-item <?= $current_cat->id == $category->id ? '-active' : '' ?>"><?= $category->name ?></a>
                                <!-- second level -->
                                <ul>
                                    <?php foreach ($sub_categories as $sub_category) { ?>
                                        <li>
                                            <a
                                               href="<?= Url::to(['/category/view', 'id' => $category->id, 'sub_id' => $sub_category->id]) ?>"><?= $sub_category->name ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>

                </aside>
            </div>

            <div class="col-sm-9">
                <?php
                $cnt = $dp->getTotalCount();
                echo ListView::widget([
                    'dataProvider' => $dp,
                    'options' => [
                        'class' => 'row',
                    ],
                    'itemView' => '/category/_list_item',
                    'layout' => "{items}",
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'viewParams' => [
                        'current_cat' => $current_cat,
                        'current_sub_cat' => $current_sub_cat,
                    ],
                    'beforeItem' => function ($model, $key, $index) use ($cnt) {
                        return $index % 3 == 0 ? "<div class='row'>" : null;
                    },
                    'afterItem' => function ($model, $key, $index) use ($cnt) {
                        return ($index + 1) % 3 == 0 || $index + 1 == $cnt ? "</div>" : null;
                    },
                ]) ?>
            </div>

        </div>
    </div>
    <div class="b-parallax b-parallax-catalog"></div>
</div>

<?php
/**
 * @var $this yii\web\View
 * @var $model Product
 * @var $current_cat \app\models\ProductCategory
 * @var $current_sub_cat \app\models\ProductSubCategory
 */

use yii\helpers\Url;
use app\models\Product;
use app\models\ObjectFile;
use app\models\ProductSubCategory;
use app\models\ProductHasSubCategory;

if (!$current_sub_cat) {
    if ($phsc = ProductHasSubCategory::find()->where([
        'product_id' => $model->id,
    ])->one()) {
        $current_sub_cat = ProductSubCategory::findOne($phsc->sub_category_id);
    }
}

$url = Url::to(array_filter([
    '/product/view',
    'cat_id' => $current_cat->id,
    'sub_cat_id' => $current_sub_cat ? $current_sub_cat->id : null,
    'id' => $model->id,
]));
?>
<div class="col-sm-4">
    <a href="<?= $url ?>" class="b-catalog__item">
        <div class="b-catalog__item_img">
            <!-- image -->
            <?php if ($path = ObjectFile::getThumbnail($model, 'image', [450, 420])) { ?>
                <img src="<?= $path ?>" alt="">
            <?php } ?>
            <div class="b-catalog__item_img-bals">
                <div class="b-catalog__item_img-bals-wrap">
                    <span class="b-catalog__item_img-bals-num"><?= $model->point ?></span>
                    <span class="b-catalog__item_img-bals-b">PV</span>
                </div>
            </div>
        </div>
        <!-- subtitle -->
        <div class="b-catalog__item_subtitle"><?= $model->title ?></div>
        <!-- text -->
        <p class="b-catalog__item_text"><?= $model->info_text ?></p>
        <!-- price -->
        <div class="b-catalog__item_price"><?= $model->cost ?> <span>₽</span></div>
    </a>
</div>
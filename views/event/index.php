<?php

/**
 * @var $this yii\web\View
 * @var $dp \yii\data\ActiveDataProvider
 */

use yii\widgets\ListView;
use yii\helpers\Url;

$this->title = Yii::t('labels', 'Events');
?>

<div class="b-news-list">
    <div class="container">
        <div class="b-breadcrumbs">
            <ul>
                <li><a href="<?= Url::to(['/']) ?>"><?= Yii::t('labels', 'Main') ?></a></li>
                <li><?= $this->title ?></li>
            </ul>
        </div>
        <div class="b-news-list__title"><?= $this->title ?></div>
        <div class="b-news-list-items">
            <?php
            $cnt = $dp->getTotalCount();
            echo ListView::widget([
                'dataProvider' => $dp,
                'options' => [
                    'class' => 'row',
                ],
                'itemView' => '/event/_list_item',
                'layout' => "{items}",
                'itemOptions' => [
                    'tag' => false,
                ],
                'beforeItem' => function ($model, $key, $index) use ($cnt) {
                    return $index % 2 == 0 ? "<div class='row'>" : null;
                },
                'afterItem' => function ($model, $key, $index) use ($cnt) {
                    return ($index + 1) % 2 == 0 || $index + 1 == $cnt ? "</div>" : null;
                },
            ]) ?>
        </div>
    </div>
</div>
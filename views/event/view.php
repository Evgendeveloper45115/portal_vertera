<?php
/**
 * @var $this \yii\web\View
 * @var $model Business
 * @var $models Business[]
 */

use yii\helpers\Url;
use app\models\Business;
use app\models\ObjectFile;

$this->title = $model->title;
$fmt = Yii::$app->getFormatter();
$fmt->defaultTimeZone = $fmt->timeZone;
$speakers = $model->getSpeakers();
?>
<div class="b-news">
    <div class="container">

        <div class="row">
            <div class="col-xs-12">
                <div class="b-breadcrumbs">
                    <ul>
                        <li><a href="/"><?= Yii::t('labels', 'Main') ?></a></li>
                        <li><a href="<?= Url::to(['/event']) ?>"><?= Yii::t('labels', 'Events') ?></a></li>
                        <li><?= $this->title ?></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="<?= sizeof($speakers) ? 'col-sm-9 col-xs-12' : 'col-md-12' ?>">

                <div class="b-news-link-back">
                    <a href="<?= Url::to(['/event']) ?>"><?= Yii::t('labels', 'Back to the list') ?></a>
                </div>
                <div class="b-news__title"><?= $model->title ?></div>

                <?php if ($model->button_text && $model->button_url) { ?>
                    <div class="site-event-info" style="margin-bottom: 20px;">
                        <div class="site-event-info-cat-date">
                            <div class="site-event-info-cat">
                                <a href="<?= $model->button_url ?>">
                                    <div class="site-event-info-cat"
                                         style="padding: 5px 10px;"><?= $model->button_text ?></div>
                                </a>
                            </div>

                            <div class="site-event-info-date"><?= $fmt->asDate($model->start_date, 'long') ?></div>
                            <?php if(date('H:i:s', strtotime($model->start_date)) != '00:00:00') { ?>
                                <div class="site-event-info-time">
                                    <?= date('H:i', strtotime($model->start_date)) ?>
                                    <?= Yii::t('labels', 'MSK') ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="b-news__date"><?= $fmt->asDate($model->start_date, 'long') ?></div>
                <?php } ?>

                <?php if ($path = ObjectFile::getAssetPath($model, 'image', true)) { ?>
                    <div class="b-news__img"><img src="<?= $path ?>" alt=""></div>
                <?php } ?>

                <div class="b-news__text">
                    <?= $model->description ?>
                    <p class="b-news__text">
                        <span style="font-size: 24px;"><?= $model->sub_description ?></span>
                    </p>
                    <div class="b-news__text-big"><br></div>
                </div>
                <div class="b-news-soc-icons">
                    <p>Поделиться</p>
                    <?= \bigpaulie\social\share\Share::widget([
                        'type' => \bigpaulie\social\share\Share::TYPE_EXTRA_SMALL,
                        'tag' => 'div',
                        'title' => $model->title,
                        'image' => 'https://portal.dev.vertera.org' . ObjectFile::getAssetPath($model, 'image', true),
                        'url' => 'https://portal.dev.vertera.org' . Yii::$app->request->getUrl(),
                        'networks' => [
                            'vk' => 'http://vkontakte.ru/share.php?url={url}',
                            'facebook' => 'https://www.facebook.com/sharer/sharer.php?u={url}',
                            'odnoklassniki' => 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl={url}',
                            'google-plus' => 'https://plus.google.com/share?url={url}',
                        ],
                        'addUtm' => true,
                        'template' => '<span>{button}</span>',
                    ]) ?>
                </div>

                <?php
                if (in_array($model->type, [Business::TYPE_CALENDAR, Business::TYPE_BOTH])
                    && $model->conference_type == Business::WEBINAR
                ) { ?>
                    <div class="clearfix"></div>
                    <div class="site-event-require">
                        <div class="require-img">
                            <img src="/images/comp.png" alt="">
                        </div>
                        <div class="site-event-require-info">
                            <div class="site-event-require-info-title">
                                <?= Yii::t('labels', 'Technical requirements') ?>
                            </div>
                            <p><?= Yii::t('labels', 'To participate in the webinar you need a computer with a stable connection to the Internet at a speed of at least 700kbps and a modern web browser.') ?></p>
                        </div>
                    </div>
                <?php } ?>

            </div>

            <?php if (sizeof($speakers)) { ?>
                <div class="col-sm-3 col-xs-12">
                    <?php foreach ($speakers as $speaker) { ?>
                        <div class="site-event-author">
                            <?php if ($path = ObjectFile::getAssetPath($speaker, 'image', true)) { ?>
                                <div class="site-event-author-img"><img height="100" src="<?= $path ?>" alt=""></div>
                            <?php } ?>
                            <div class="site-event-author-name"><?= $speaker->full_name ?></div>
                            <div class="site-event-author-info"><?= Yii::t('labels', 'Speaker') ?></div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

        </div>


        <div class="row">
            <div class="col-xs-12">

                <div class="b-news-nav">

                    <div class="row">
                        <?php
                        foreach ($models as $m) {
                            $ctrl_name = $m->type == Business::TYPE_CALENDAR ? 'calendar' : 'event';
                            ?>
                            <div class="col-sm-6 col-xs-12">
                                <div class="b-news-nav-item">

                                    <?php if ($path = ObjectFile::getThumbnail($m, 'image', [460, 160])) { ?>
                                        <div class="b-news-nav-img">
                                            <a href="<?= Url::to(["/{$ctrl_name}/view", 'id' => $m->id]) ?>">
                                                <img src="<?= $path ?>" alt="">
                                            </a>
                                        </div>
                                    <?php } ?>

                                    <div class="b-news-nav-date"><?= $fmt->asDate($m->start_date, 'long') ?></div>
                                    <div class="b-news-nav-title">
                                        <a href="<?= Url::to(["/{$ctrl_name}/view", 'id' => $m->id]) ?>"><?= $m->title ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="row b-news-nav-buttons">
                        <?php
                        if ($m = $models['prev'] ?? null) {
                            $ctrl_name = $m->type == Business::TYPE_CALENDAR ? 'calendar' : 'event';
                            ?>
                            <div class="col-sm-4 col-xs-12">
                                <div class="b-news-nav-buttons-left">
                                    <a href="<?= Url::to(["/{$ctrl_name}/view", 'id' => $m->id]) ?>">
                                        <?= Yii::t('labels', 'Previous event') ?>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-sm-4 col-xs-12">
                            <div class="b-news-nav-buttons-center">
                                <a href="<?= Url::to(['/event']) ?>"><?= Yii::t('labels', 'All events') ?></a>
                            </div>
                        </div>

                        <?php
                        if ($m = $models['next'] ?? null) {
                            $ctrl_name = $m->type == Business::TYPE_CALENDAR ? 'calendar' : 'event';
                            ?>
                            <div class="col-sm-4 col-xs-12">
                                <div class="b-news-nav-buttons-right">
                                    <a href="<?= Url::to(["/{$ctrl_name}/view", 'id' => $m->id]) ?>">
                                        <?= Yii::t('labels', 'Next event') ?>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                </div>

            </div>
        </div>
    </div>

</div>
<?php
/**
 * @var $this yii\web\View
 * @var $model Business
 */

use \yii\helpers\Url;
use \app\models\Business;
use \app\models\ObjectFile;

$fmt = Yii::$app->getFormatter();
$fmt->defaultTimeZone = $fmt->timeZone;
?>
<div class="col-sm-6 col-xs-12">
    <div class="b-news-list__item">

        <?php if ($path = ObjectFile::getThumbnail($model, 'image', [600, 200])) { ?>
            <div class="b-news-list_img">
                <a href="<?= Url::to(['event/view', 'id' => $model->id]) ?>"><img src="<?= $path ?>" alt=""></a>
            </div>
        <?php } ?>

        <div class="b-news-list__date"><?= $fmt->asDate($model->start_date, 'long') ?></div>
        <div class="b-news-list__subtitle">
            <a href="<?= Url::to(['event/view', 'id' => $model->id]) ?>"><?= $model->title ?></a>
        </div>
        <div class="b-news-list__text"><?= $model->sub_description ?></div>
    </div>
</div>
<?php
/**
 * @var $this yii\web\View
 * @var $dp \yii\data\ActiveDataProvider
 * @var $current_category \app\models\FaqCategory
 * @var $categories \app\models\FaqCategory[]
 */

use yii\widgets\ListView;
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;

$this->title = 'Вопросы и ответы';
$this->registerJsFile(Yii::getAlias('@web/js/vertera-project.min.js'), [
    'depends' => [AppAsset::class]
]);
?>
<div class="b-faq js-faq">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="b-breadcrumbs">
                    <ul>
                        <li><a href="<?= Url::to(['/']) ?>"><?= Yii::t('layout', 'Main') ?></a></li>
                        <li><?= $this->title ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-xs-12">
                <div class="b-faq__title"><?= $this->title ?></div>
                <div class="b-faq__text"><?= Yii::t('faq', 'In this section, we answer questions<br>from our partners, customers and visitors.') ?></div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <a href="https://lk.wr.market/support/support/index/" class="b-button-question" target="_blank"
                   style="color: #FFFFFF;"><?= Yii::t('faq', 'Ask a question') ?></a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-sm-3 col-xs-12">
                <aside class="b-sidebar js-sidebar">
                    <ul>
                        <li>
                            <a class="<?= !$current_category ? '-active' : null ?>" href="<?= Url::to(['/faq']); ?>">Все вопросы</a>
                        </li>
                        <?php foreach ($categories as $c) { ?>
                            <li>
                                <?= Html::a($c->name, ['/faq', 'category_id' => $c->id], [
                                    'class' => $current_category && $current_category->id == $c->id ? '-active' : null,
                                ]) ?>
                            </li>
                        <?php } ?>
                    </ul>
                </aside>
            </div>

            <?= ListView::widget([
                'dataProvider' => $dp,
                'options' => [
                    'class' => 'col-sm-9 col-xs-12',
                ],
                'itemView' => '/faq/_list_item',
                'layout' => "{items}",
                'itemOptions' => [
                    'tag' => false,
                ],
            ]) ?>

        </div>
    </div>
</div>
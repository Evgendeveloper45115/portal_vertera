<?php


/**
 * @var $this yii\web\View
 * @var $model Faq
 */

use \app\models\Faq;

?>
<div class="b-faq__item js-item">
    <div class="b-faq-item__subtitle-wrapper">
        <div class="b-faq-item__subtitle b-question-expert-item__subtitle"><?= $model->question ?></div>
        <div class="b-faq-item__author"><?= $model->client_information ?></div>
    </div>
    <div class="b-faq-item__content-text">
        <p class="b-faq-item__text-descr"><?= $model->answer ?></p>
    </div>
</div>

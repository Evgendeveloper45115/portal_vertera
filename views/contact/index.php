<?php
/**
 * @var $this \yii\web\View
 * @var $question Question
 * @var $models Contact[]
 */

use app\assets\AppAsset;
use app\widgets\Alert;
use app\models\Contact;
use app\models\Question;

$this->title = Yii::t('labels', 'Vertera Official Representative Offices');

$apiKey = 'AIzaSyBnVNkAhczDSQjXtZhzQzy6laG6OX6rGMs';
$this->registerJsFile("https://maps.googleapis.com/maps/api/js?key={$apiKey}&callback=initMap", [
    'async' => true, 'defer' => true, 'depends' => [AppAsset::class]
]);
$this->registerJsFile(Yii::getAlias('@web/js/client-contact-map.js'), [
    'depends' => [AppAsset::class]
]);

$this->registerJsFile(Yii::getAlias('@web/js/contact-list-actions.js'), [
    'depends' => [AppAsset::class]
]);
$this->registerJs("$('.b-contacts-item-open').click(function(){
    if($(this).attr('status')=='close') {
        $(this).parent().parent().find('.b-contacts-item-bottom').addClass('active');
        $(this).attr('status','open').removeClass('plus').addClass('minus');
    } else if($(this).attr('status')=='open') {
        $(this).parent().parent().find('.b-contacts-item-bottom').removeClass('active');
        $(this).attr('status','close').removeClass('minus').addClass('plus');
    }
    return false;
});");
?>
<?= Alert::widget() ?>
<div class="b-contacts">

    <div class="b-descr-page b-descr-page-for-contacts">
        <div class="container">
            <div class="row">
                <div class="col_12">
                    <div class="b-descr-page__substrate-bg">
                        <div class="b-descr-page-substrate-bg">
                            <h1 class="b-descr-page__title">
                                <?= Yii::t('labels', 'Contact<br>information') ?>
                            </h1>
                            <p class="b-descr-page__text">
                                <?= Yii::t(
                                    'contact',
                                    'In this section you will find useful information about representative offices in Russia and CIS countries')
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="ask-question" class="b-contacts-content">
        <!-- CONTACTS : begin -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="b-contacts-title">
                        <?= Yii::t('contact', 'Vertera Official Representative Offices') ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9">
                    <div class="b-contacts-list">

                        <?php
                        foreach ($models as $model) {
                            if ($model->map_enable != Contact::MAP_ENABLED) {
                                continue;
                            }
                            $is_moscow = in_array(mb_strtolower($model->city_name, 'utf-8'), ['москва']);
                            ?>
                            <div class="b-contacts-item">
                                <div class="b-contacts-item-top">
                                    <div class="b-contacts-item-top-title">
                                        <span><?= $model->city_name ?></span>
                                    </div>
                                    <a href="#"
                                       class="b-contacts-item-open <?= !$is_moscow ? 'plus' : 'minus' ?>"
                                       status="<?= $is_moscow ? 'open' : 'close' ?>"></a>
                                </div>
                                <div class="b-contacts-item-bottom <?= $is_moscow ? 'active' : '' ?>">
                                    <div class="b-contacts-item-adress">
                                        <div class="b-contacts-item-img">
                                            <img src="/images/contacts-icons/1.png" alt="">
                                        </div>
                                        <div class="b-contacts-item-adress-info">
                                            <div class="b-contacts-item-adress-title">
                                                <?= $model->getAttributeLabel('address') ?>
                                            </div>
                                            <p><?= $model->address ?></p>
                                        </div>
                                    </div>

                                    <div class="b-contacts-item-phone">
                                        <div class="b-contacts-item-img">
                                            <img src="/images/contacts-icons/2.png" alt="">
                                        </div>
                                        <div class="b-contacts-item-phone-info">
                                            <div class="b-contacts-item-phone-title">
                                                <?= $model->getAttributeLabel('phone') ?>
                                            </div>
                                            <p class="hidden-xs"><?= $model->phone ?></p>
                                            <a href="tel:<?= $model->phone ?>"
                                               class="visible-xs"
                                               style="color: #58af55;"><?= $model->phone ?></a>
                                        </div>
                                    </div>

                                    <?php if (!empty($model->email)) { ?>
                                        <div class="b-contacts-item-email">
                                            <div class="b-contacts-item-img">
                                                <img src="/images/contacts-icons/3.png" alt="">
                                            </div>
                                            <div class="b-contacts-item-email-info">
                                                <div class="b-contacts-item-email-title">
                                                    <?= $model->getAttributeLabel('email') ?>
                                                </div>
                                                <p><a href="mailto:<?= $model->email ?>"><?= $model->email ?></a></p>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="b-contacts-item-bottom-map">
                                        <div class="js-contact-map"
                                             id="map-<?= $model->id ?>"
                                             data-lat="<?= $model->map_lat ?>"
                                             data-lng="<?= $model->map_lng ?>"
                                             style="width: 100%; height: 400px"></div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>

                    <!-- table search : begin -->
                    <div class="b-contacts__table-search">
                        <div class="b-contacts-table-search__title">
                            <?= Yii::t('contact', 'Representative offices of Vertera') ?>
                        </div>

                        <form id="contact-search-form" action="" class="b-contacts-table-search__form hidden-xs">
                            <input type="text" name="string"
                                   placeholder="<?= Yii::t('contact', 'Enter country or city') ?>"
                                   style="width: 50%; float:left;">
                            <button class="b-button-green">
                                <?= Yii::t('contact', 'To find') ?>
                            </button>
                        </form>

                        <table id="contact-search-list" class="b-contacts-table-search__table hidden-xs">
                            <thead>
                            <tr>
                                <th><?= Yii::t('contact', 'Country name') ?></th>
                                <th><?= Yii::t('contact', 'City name') ?></th>
                                <th><?= Yii::t('contact', 'Address') ?></th>
                                <th><?= Yii::t('contact', 'Representation') ?></th>
                                <th style="min-width: 180px;"><?= Yii::t('contact', 'Phone') ?></th>
                            </tr>
                            </thead>
                            <tbody class="js-search-result">
                            <?php
                            foreach ($models as $model) {
                                if ($model->map_enable == Contact::MAP_ENABLED) {
                                    continue;
                                }
                                ?>
                                <tr>
                                    <td class="text-left"><?= $model->country_name ?></td>
                                    <td class="text-left"><?= $model->city_name ?></td>
                                    <td class="text-left"><?= $model->address ?></td>
                                    <td class="text-left" style="width: 200px">
                                        <?= $model->type == 1
                                            ? Yii::t('contact', 'Regional representation')
                                            : ($model->type == 2 ? Yii::t('contact', 'Pick-up Center') : '') ?>
                                    </td>
                                    <td class="text-left"><?= $model->phone ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                    </div>
                    <hr>

                </div>

                <div class="col-sm-3">
                    <div class="b-contacts-form">
                        <a href="https://lk.wr.market/support/support/index/" class="b-button-question" target="_blank"
                           style="color: #FFFFFF;"><?= Yii::t('faq', 'Ask a question') ?></a></div>
                </div>

            </div>
        </div>
    </div>

</div>
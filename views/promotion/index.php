<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\widgets\ListView;

$this->title = Yii::t('layout', 'Promotions');
?>
<div class="b-descr-page b-descr-page-for-promo-list">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- substrate background : begin -->
                <div class="b-descr-page__substrate-bg">
                    <div class="b-descr-page-substrate-bg">
                        <!-- title -->
                        <div class="b-descr-page__title"><?= Yii::t('layout', 'Promotions') ?></div>
                        <!-- description -->
                        <p class="b-descr-page__text">
                            <?= Yii::t('layout', 'To our range of products, we treat carefully and consciously, since we ourselves use it. We recommend everyone, at a minimum, to introduce into their diet natural foods that do not contain GMOs, preservatives and a variety of food chemical compounds.') ?>
                        </p>
                    </div>
                </div>
                <!-- substrate background : end -->
            </div>
        </div>
    </div>
</div>

<div class="b-promo-list">
    <div class="container">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'options' => [
                'class' => 'row',
            ],
            'itemView' => '/promotion/_list_item',
            'layout' => "{items}",
            'itemOptions' => [
                'tag' => false,
            ],
        ]) ?>
    </div>
</div>
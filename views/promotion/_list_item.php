<?php
/**
 * @var $this yii\web\View
 * @var Promotion $model
 */

use \yii\helpers\Url;
use \app\models\Promotion;
use \app\models\ObjectFile;

$this->title = $model->title;
$fmt = Yii::$app->getFormatter();
$fmt->defaultTimeZone = $fmt->timeZone;
$path = ObjectFile::getAssetPath($model, 'image', true);
?>
<div class="col-sm-4 col-xs-12">
    <div class="b-promo-list__item active">
        <a href="<?= Url::to(['/promotion/view', 'id' => $model->id]) ?>" class="b-promo-list__img"><img
                    src="<?= $path ?>" alt=""></a>
        <div class="b-promo-list__date"><?= $fmt->asDate($model->start_date, 'long') ?></div>
        <div class="b-promo-list__subtitle"><?= $model->title ?></div>
        <p class="b-promo-list__text"><?= $model->sub_description ?></p>
    </div>
</div>
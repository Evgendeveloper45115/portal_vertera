<?php
/**
 * @var $model Promotion
 */

use yii\helpers\Url;
use app\models\Promotion;
use app\models\ObjectFile;

$fmt = Yii::$app->getFormatter();
$fmt->defaultTimeZone = $fmt->timeZone;
$path = ObjectFile::getAssetPath($model, 'image', true);

?>
<div class="b-card-promo-typical">
    <!-- description page : begin -->
    <div class="b-descr-page b-descr-page-for-card-promo-typical">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- substrate background : begin -->
                    <div class="b-descr-page__substrate-bg">
                        <div class="b-descr-page-substrate-bg-typical">
                            <!-- <p class="b-descr-page-promo-typical__text_top">Партнерская программа</p> -->
                            <!-- title -->
                            <div class="b-descr-page-promo-typical__title"><?= $model->title ?></div>
                            <!-- description -->

                            <!-- options : begin -->
                            <div class="b-descr-page__options">
                                <!-- date -->
                                <div class="b-descr-page__date"><?= date("Y-m-d", strtotime($model->start_date)) ?></div>
                                <!-- events -->
                                <div class="b-descr-page__events">Акция</div>
                                <div class="b-descr-page__share"></div>
                            </div>
                            <!-- options : end -->
                        </div>
                    </div>
                    <!-- substrate background : end -->
                </div>
            </div>
            <!-- banner : begin -->
            <div class="b-descr-page__banner">
                <img src="<?= $path ?>" alt="">
            </div>
            <!-- banner : end -->
        </div>
    </div>
    <!-- description page : end -->
    <!-- advantages : begin -->
    <div class="b-card-promo-typical__advantages">
        <div class="container">
            <div class="row">
                <?= htmlspecialchars_decode($model->description) ?>
                <div class="row horizontal-nav-soc">
                    <div class="col-sm-6 col-xs-12">
                        <ul>
                            <li class="fa fa-tags"></li>
                            <li>Акции</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- advantages : end -->
    </div>
    <!-- CARD PROMO : end -->
</div>
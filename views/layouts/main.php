<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\assets\AppAsset;
use app\models\ProductCategory;
use app\models\ProductSubCategory;
use app\models\MainSliderHeader;

AppAsset::register($this);

/** @var app\models\User $user */
$user = Yii::$app->getUser()->getIdentity();
$ctrl = Yii::$app->controller;
$am = Yii::$app->getAssetManager();

$service_param = 'vertera.org';
$ref_param = Yii::$app->getRequest()->get('ref');
$return_param = 'https://vertera.org/';

$reg_url = Url::to('https://id.wr.market/registration/?' . http_build_query(array_filter([
    'service' => $service_param,
    'ref' => $ref_param,
    'return' => $return_param,
    ])));

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href="/images/favicon.png" rel="shortcut icon">
        <?php $this->head() ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123323446-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-123323446-1');
        </script>
        
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="warning-popup-modal" style="display: none;">
        <div class="container">
            <div class="col-md-9 text-warning">
                <b><?= Yii::t('layout', 'We use cookies to recognize your repeated visits and preferences, evaluate the effectiveness of advertising campaigns and improve our website. For information about the settings and additional information about cookies, see our ') ?></b>
                <b><a href="<?= Url::to(['site/privacy-policy']) ?>"><?= Yii::t('layout', 'Privacy Policy.') ?></a></b>
            </div>

            <div class="col-md-3">
                <?= Html::a(Yii::t('layout', 'Agree'), 'javascript://', [
                    'class' => 'js-agreement-button b-button-green',
                ]) ?>
            </div>
        </div>
    </div>

    <header class="b-header js-header">
        <div class="container">
            <!-- top block : begin -->
            <div class="row">
                <div class="col-md-2 col-xs-12 b-header-col-1">
                    <div class="b-header__mobile-menu">
                        <a href="#"><i class="fa fa-navicon"></i> <span><?= Yii::t('layout', 'Menu') ?></span></a>
                    </div>
                    <!--
                            <div class="b-header__languages">
                                <ul class="b-header__languages-list">
                                    <li>
                                            <a class="ru active lang-active" href="#">Русский &nbsp;<i class="fa fa-caret-down"></i></a>

                                        <ul>
                                            <li><a class="ru lang-ru" href="#">Русский</a></li>
                                            <li><a class="en lang-en" href="#">English</a></li>
                                            <li><a class="bg lang-bg" href="#">Български</a></li>
                                            <li><a class="de lang-de" href="#">Deutsch</a></li>

                                            <li><a class="ua lang-ua" href="#">Українська</a></li>
                                <li><a class="fr lang-pl" href="#">Polski</a></li>
                                            <li><a class="fr lang-fr" href="#">Français</a></li>
                                            <li><a class="de lang-de" href="#">Deutsch</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                    -->
                    <!--
                        <div class="b-header__town">
                            <span class="b-header__town-title">Ваш город: </span>
                            <span class="b-header__town-active">
                                <a href="#">Тверь</a>
                            </span>
                        </div>
                    -->
                </div>
                <div class="col-md-2 col-xs-12 b-header-col-2">
                    <!-- contact -->
                    <div class="b-header__contact">
                        <span>с 09:00 до 18:00</span>
                        <span style="white-space: nowrap;">+7 (920) 191-61-87</span>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 b-header-col-3">
                    <!-- logotype -->
                    <div class="b-header__logo">
                        <a href="/">
                            <img src="/images/logo.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12 b-header-col-4">
                    <!-- button -->
                    <div class="b-header__btn">
                        <a href="<?php echo $reg_url?>"
                           class="b-button-green-border"
                           data-toggle="modal"
                           data-target="#consent-form-modal"><?= Yii::t('layout', 'Start business') ?></a>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12 b-header-col-5">
                    <div class="b-header__auth-link">
                        <a href="https://os.verteraorganic.com/lkc.php">
                            <img src="/images/auth.gif">
                        </a><a href="<?= Url::to('https://os.verteraorganic.com/lkc.php?' . http_build_query(array_filter([
                                'service' => $service_param,
                                'ref' => $ref_param,
                                'return' => $return_param,
                            ])), true) ?>" target="_blank"
                               class="hidden-xs"><?= Yii::t('layout', 'Personal Area') ?></a>

                    </div>
                </div>
            </div>
            <!-- top block : end -->
            <!-- bottom block : begin -->
            <div class="b-header__bottom">
                <div class="row">
                    <div class="b-adapt-navigation">
                        <div class="row row-1">
                            <div class="col-sm-4 col-1">
                                <!--
                                <div class="b-header__search-mobile">
                                    <div class="b-header__search-mobile-icon">
                                        <a href="#" class="fa fa-search"></a>
                                    </div>
                                </div>
                                -->
                            </div>
                            <div class="col-sm-4 col-2">
                                <!-- logotype -->
                                <div class="b-header__logo-mobile">
                                    <a href="/">
                                        <img src="/images/logo-mobile.png">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4 col-3">
                                <a id="b-adapt-navigation-close" href="#">x</a>
                            </div>
                        </div>
                        <!-- navigation : begin -->
                        <nav class="b-nav js-nav">
                            <ul>

                                <li>
                                    <!-- first level -->
                                    <a href="#"
                                       class="js-first-lvl-controller"><?= Yii::t('layout', 'Productions') ?></a>
                                    <i status="close" class="fa fa-plus-circle"></i>

                                    <!-- second level : begin -->
                                    <ul class="b-nav__second-lvl js-second-lvl">
                                        <li>
                                            <a href="<?= Url::to(['/site/vertera_gel/']) ?>"><?= Yii::t('layout', 'Vertera Gel') ?></a>
                                        </li>
                                        <?php
                                        $categories = ProductCategory::find()->where([
                                            'status' => ProductCategory::STATUS_ACTIVE,
                                        ])->all();
                                        foreach ($categories as $category) {
                                            $sub_categories = ProductSubCategory::find()->where([
                                                'parent_id' => $category->id,
                                                'status' => ProductSubCategory::STATUS_ACTIVE,
                                            ])->all();
                                            $two_columns_class = '';
                                            if (sizeof($sub_categories) >= 15) {
                                                $two_columns_class = 'two-columns';
                                            }
                                            ?>
                                            <li>
                                                <a
                                                        href="<?= Url::to(['/category/view', 'id' => $category->id]) ?>"
                                                        class="js-second-lvl-controller"><?= $category->name ?></a>
                                                <i status="close" class="fa fa-plus-circle hidden-xs"></i>

                                                <!-- third level : begin -->
                                                <ul class="visible-xs b-nav__third-lvl js-third-lvl">
                                                    <?php foreach ($sub_categories as $sub_category) {
                                                        if ($sub_category->name == 'Спортивное питание') {
                                                            ?>
                                                            <li>
                                                                <a href="<?= Url::to(['/site/vertera-gel-sport']) ?>">VERTERA
                                                                    SPORT ACTIVE</a>
                                                            </li>

                                                            <?php
                                                        }
                                                        ?>
                                                        <li>
                                                            <a href="<?= Url::to(['/category/view', 'id' => $category->id, 'sub_id' => $sub_category->id]) ?>"><?= $sub_category->name ?></a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>

                                                <ul class="hidden-xs b-nav__third-lvl js-third-lvl <?= $two_columns_class ?>">
                                                    <?php foreach ($sub_categories as $sub_category) {
                                                        if ($sub_category->name == 'Спортивное питание') {
                                                            ?>
                                                            <li>
                                                                <a href="<?= Url::to(['/site/vertera-gel-sport']) ?>">VERTERA
                                                                    SPORT ACTIVE</a>
                                                            </li>

                                                            <?php
                                                        }
                                                        ?>
                                                        <li>
                                                            <a href="<?= Url::to(['/category/view', 'id' => $category->id, 'sub_id' => $sub_category->id]) ?>"><?= $sub_category->name ?></a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>

                                                <!-- third level : end -->

                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <!-- second level : end -->
                                </li>
                                <li>
                                    <!-- first level -->
                                    <a href="#" class="js-first-lvl-controller"><?= Yii::t('layout', 'Business') ?></a>
                                    <i status="close" class="fa fa-plus-circle"></i>

                                    <ul class="b-nav__second-lvl js-second-lvl">
                                        <li>
                                            <a href="<?= Url::to(['/calendar']) ?>"><?= Yii::t('layout', 'Calendar of events') ?></a>
                                        </li>
                                        <li><a href="<?= Url::to(['/event']) ?>"><?= Yii::t('layout', 'Events') ?></a>
                                        </li>
                                        <li>
                                            <a href="<?= Url::to(['/site/business-vertera']) ?>"><?= Yii::t('layout', 'Business opportunity') ?></a>
                                        </li>
                                        <li>
                                            <a href="<?= Url::to(['/site/world-of-retail']) ?>"><?= Yii::t('layout', 'World of trade') ?></a>
                                        </li>
                                        <li>
                                            <a href="<?= Url::to('//vertera.university/') ?>" target="_blank">
                                                <?= Yii::t('layout', 'University') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?= Url::to(['/faq']) ?>"><?= Yii::t('layout', 'Questions and answers') ?></a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <!-- first level -->
                                    <a href="<?= Url::to(['/promotion']) ?>"><?= Yii::t('layout', 'Promotions') ?></a>
                                </li>

                                <li>
                                    <!-- first level -->
                                    <a href="https://vertera.market"><?= Yii::t('layout', 'Online store') ?></a>
                                </li>

                                <li>
                                    <!-- first level -->
                                    <a href="#"
                                       class="js-first-lvl-controller"><?= Yii::t('layout', 'About company') ?></a>
                                    <i status="close" class="fa fa-plus-circle"></i>

                                    <ul class="b-nav__second-lvl js-second-lvl">
                                        <li>
                                            <a href="<?= Url::to(['/news']) ?>"><?= Yii::t('layout', 'News') ?></a>
                                        </li>
                                        <li>
                                            <a href="#"
                                               class="js-second-lvl-controller"><?= Yii::t('layout', 'About company') ?></a>
                                            <i status="close" class="fa fa-plus-circle"></i>
                                            <!-- third level : begin -->
                                            <ul class="b-nav__third-lvl js-third-lvl">
                                                <li>
                                                    <a href="<?= Url::to(['/company/about']) ?>"><?= Yii::t('layout', 'About company') ?></a>
                                                </li>

                                                <li>
                                                    <a href="<?= Url::to(['/company/history']) ?>"><?= Yii::t('layout', 'History') ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?= Url::to(['/company/mission']) ?>"><?= Yii::t('layout', 'Mission, purpose and values') ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?= Url::to(['/company/philosophy']) ?>"><?= Yii::t('layout', 'Philosophy of Vertera') ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?= Url::to(['/company/strategy']) ?>"><?= Yii::t('layout', 'Company development strategy') ?></a>
                                                </li>
                                            </ul>
                                            <!-- third level : end -->
                                        </li>
                                        <li>
                                            <a href="<?= Url::to(['/site/production/']) ?>"><?= Yii::t('layout', 'Production') ?></a>
                                        </li>
                                        <li>
                                            <a href="<?= Url::to(['/site/privacy-policy/']) ?>"><?= Yii::t('layout', 'Privacy Policy') ?></a>
                                        </li>
                                        <!--
                                        <li>
                                             <a href="/leaders/">Аллея славы</a>
                                        </li>
                                        -->
                                    </ul>
                                </li>
                                <li>
                                    <!-- first level -->
                                    <a href="<?= Url::to(['/contact']) ?>"><?= Yii::t('layout', 'Contacts') ?></a>
                                </li>
                                <li class="visible-xs visible-sm">
                                    <!-- first level -->
                                    <a href="<?php echo $reg_url?>"><?= Yii::t('layout', 'Start business') ?></a>
                                </li>
                            </ul>
                        </nav>
                        <!-- navigation : end -->
                        <div class="row-2">
                            <div class="col-sm-6">
                                <div class="row-3">
                                    <div class="b-header__languages b-header__languages-mobile">
                                        <ul class="b-header__languages-list">
                                            <li>
                                                <a class="ru active" href="#">Русский &nbsp;<i
                                                            class="fa fa-caret-down"></i></a>
                                                <ul>
                                                    <li><a class="ru lang-ru lang-active" href="#">Русский</a></li>
                                                    <li><a class="en lang-en lang-active" href="#">English</a></li>
                                                    <li><a class="bg lang-bg lang-active" href="#">Български</a></li>
                                                    <li><a class="de lang-de lang-active" href="#">Deutsch</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="b-header__town b-header__town-mobile">
                                        <!--
                                        <span class="b-header__town-title">Ваш город: </span>
                                        <span class="b-header__town-active">
                                            <a href="#">Тверь</a>
                                        </span>
                                        -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <!-- contact -->
                                <div class="b-header__contact">
                                    <span>с 09:00 до 18:00</span>
                                    <span>+7 (920) 191-61-87</span>
                                </div>
                            </div>
                        </div>

                        <!-- search : begin -->
                        <!--                    <div class="b-header__search js-search">-->
                        <!--                        <div class="b-header-search-icon js-search-controller"></div>-->
                        <!--                        <form action="/search/" class="js-form">-->
                        <!--                            <input type="text" name="q" value="" placeholder="Поиск">-->
                        <!--                            <button></button>-->
                        <!--                        </form>-->
                        <!--                    </div>-->
                        <!-- search : end -->

                    </div>
                </div>
            </div>
            <!-- bottom block : end -->
        </div>
    </header>
    <?php
    if ($ctrl->id == 'site' && $ctrl->action->id == 'index') {
        /** @var MainSliderHeader[] $slider_items */
        $slider_items = MainSliderHeader::find()
            ->where(['<=', 'DATE_FORMAT(start_date, "%Y-%m-%d")', date('Y-m-d')])
            ->andWhere(['>=', 'DATE_FORMAT(end_date, "%Y-%m-%d")', date('Y-m-d')])
            ->orderBy(['sort_order' => SORT_ASC])
            ->all();
        ?>

        <div class="b-slider-full-width">
            <div class="js-slider-full-width">

                <?php foreach ($slider_items as $slider_item) { ?>
                    <div class="b-slider-full-width__item">
                        <a href="<?= $slider_item->url ?>" class="b-slider-full-width__link">
                            <?php if ($path = \app\models\ObjectFile::getAssetPath($slider_item, 'image', true)) { ?>
                                <img src="<?= $path ?>" alt="" class="b-slider-full-width__img">
                            <?php } ?>
                        </a>
                    </div>
                <?php } ?>

            </div>
        </div>
    <?php } ?>
    <div class="wrap">
        <?= $content ?>
    </div>

    <footer class="b-footer">
        <div class="container">
            <div class="row">

                <div class="col-xs-12">
                    <div class="b-footer__top">
                        <nav class="b-footer__nav">
                            <ul>
                                <li><a href="#"><?= Yii::t('layout', 'Productions') ?></a></li>
                                <li>
                                    <a href="<?= Url::to(['/calendar']) ?>"><?= Yii::t('layout', 'Calendar of events') ?></a>
                                </li>
                                <li><a href="<?= Url::to(['/promotion']) ?>"><?= Yii::t('layout', 'Promotions') ?></a>
                                </li>
                                <li><a href="https://vertera.market/"><?= Yii::t('layout', 'Online store') ?></a></li>
                                <li><a href="<?= Url::to(['/contact']) ?>"><?= Yii::t('layout', 'Contacts') ?></a></li>
                                <li>
                                    <a href="<?= Url::to('https://dev.id.vertera.org/?' . http_build_query(array_filter([
                                            'service' => $service_param,
                                            'ref' => $ref_param,
                                            'return' => $return_param,
                                        ])), true) ?>" target="_blank"
                                       class=""><?= Yii::t('layout', 'Personal Area') ?></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12">
                    <div class="b-footer__copyright">
                        © 2013 - 2018 Vertera.
                        <?= Yii::t('layout', 'Enjoy life<br> with Vertera`s natural products!') ?>
                        <div class="pull-right"><?= date('Y-m-d H:i') ?></div>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12">
                    <div class="b-footer__bottom">
                        <div class="b-footer__share">
                            <div><a href="https://www.facebook.com/verteraorganic" class="b-footer-share__facebook"></a>
                            </div>
                            <div><a href="https://vk.com/verteraorganic" class="b-footer-share__vk"></a></div>
                            <div><a href="https://www.instagram.com/vertera_company/" class="b-footer-share__instagram"></a></div>
                            <div><a href="https://www.youtube.com/user/verteraorganic?sub_confirmation=1"
                                    class="b-footer-share__youtube"></a></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </footer>

    <?php \yii\bootstrap\Modal::begin([
        'header' => '<h3 class="text-center">' . Yii::t('layout', 'Consent to the processing of personal data') . '</h3>',
        'size' => \yii\bootstrap\Modal::SIZE_LARGE,
        'options' => [
            'id' => 'consent-form-modal',
            
        ],
    ]) ?>
    <?php $form = yii\bootstrap\ActiveForm::begin(['action' => $reg_url]) ?>
    <?php echo Yii::t('layout', 'Consent_text');?>
    <?php if (0):?>
    <p class="b-history__text-tiny">
        <?= Yii::t('layout', 'You give your express consent to Vertera processing your personal data in order to provide information about our products by e-mail, also for possible further cooperation, in order to provide services for the sale of products, processing of orders received from the Partner; processing of payments due to the Partner or from the Partner of payments; control of the Partner\'s business branch and preparation of appropriate reports; preparation of information on the volume of purchases (sales) of the Partner, including its business branch, and the level reached, as well as other business information intended for the Partner, the Partner\'s Sponsor and the Partner\'s higher-level business branch in accordance with the Werther Rules; providing the name, address, telephone number and / or e-mail address of the Partner to all Independent Partners of VERTERA in the higher and lower business branches of the Partner; the organization of communication with the Partner on registration of the Partner, execution of the Partner\'s orders or payments, for additional information or response to comments or requests of the Partner; prevention of fraud; collection of amounts due; authorization of access to information, materials, goods (information about products) Werthers.') ?>
    </p>
    <p class="b-history__text-small"><?= Yii::t('layout', 'In this regard, Vertera is hereby granted the right to store your data, including name, surname, date of birth, contact phone number, registration address, e-mail address and passport number / id card. Vertera will use this data to provide and improve its services.') ?></p>
    <p class="b-history__text-small"><?= Yii::t('layout', 'After acquaintance with our Privacy Policy, we ask you to give your consent to the storage and use of your data in accordance with the updated Privacy Policy.') ?></p>
    <p class="b-history__text-small"><?= Yii::t('layout', 'This consent is granted by me for the implementation of actions with respect to my personal data, which are necessary to achieve the above objectives, including collecting, recording, ordering, storing, modifying, providing access to data, generating requests, producing statements, using data, cross-use, association, blocking, exclusion or destruction, regardless of the way in which they were committed or the means used.') ?></p>
    <p class="b-history__text-small"><?= Yii::t('layout', 'If you have any questions, write to us at:') ?> <a class="b-mailto-link" href="mailto:info@vertera.org">info@vertera.org</a> </p>
    <p class="b-history__text-small"><?= Yii::t('layout', 'Thank you for being with us.') ?></p>
    <p class="b-history__text-small"><?= Yii::t('layout', 'Your statement of consent') ?>*</p>
    <?php endif;?>
    <?= $form->field(new \app\forms\ConsentForm(), 'i_agree')
        ->label('Да, я ознакомился(-лась) с содержанием текста выше и даю своё согласие.')
        ->checkbox() ?>
    <?= Html::submitButton('Подпись', ['class' => 'btn btn-success pull-right']) ?>
    <div class="clearfix"></div>
    <?php $form->end() ?>
    <?php \yii\bootstrap\Modal::end() ?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
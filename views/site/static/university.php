<?php
/**
 * @var $this \yii\web\View
 */
$this->title = Yii::t('layout', 'University');
?>
<div class="b-university">
    <!-- description page : begin -->
    <div class="b-descr-page b-descr-page-for-university">
        <div class="container">
            <div class="row">
                <!-- left column : begin -->
                <div class="col-sm-6 col-xs-12">
                    <!-- logotype -->
                    <div class="b-descr-page-for-university__logo">
                        <img src="/images/university/logo.png" alt="">
                    </div>
                </div>
                <!-- left column : begin -->
                <!-- right column : begin -->
                <div class="col-sm-6 col-xs-12">
                    <!-- text -->
                    <p class="b-descr-page-for-university__text">
                        <?=Yii::t('university','Effective freebies')?></p>
                    <!-- title -->
                    <div class="b-descr-page-for-university__title">
                        <?=Yii::t('university','Step by step <br> training system')?></p></div>
                    <!-- icons -->
                    <div class="b-descr-page-for-university__icons">
                        <p class="b-descr-page-for-university-icons__first">
                            <?=Yii::t('university','Saves time')?></p>
                        <p class="b-descr-page-for-university-icons__second">
                            <?=Yii::t('university','Available around <br> the world')?></p>
                        <p class="b-descr-page-for-university-icons__third">
                            <?=Yii::t('university','Free of charge <br> for partners')?></p>
                    </div>
                    <!-- button -->
                    <a href="http://vertera.university/" class="b-button-brilliant-orange">
                        <?=Yii::t('university','Start training')?></a>
                </div>
                <!-- right column : begin -->
            </div>
            <!-- banner : begin -->
            <div class="b-descr-page__banner">
                <a class="youtube" href="https://www.youtube.com/watch?v=qemFwOJjvOg">
                    <img src="/images/university/example-university.png" alt="">
                </a>
            </div>
            <!-- banner : end -->
        </div>
    </div>
    <!-- description page : end -->
    <!-- wrapper : begin -->
    <div class="b-university__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- title -->
                    <div class="b-university__title">
                        <?=Yii::t('university','9 steps in 30 days')?><br><span>
                            <?=Yii::t('university','will lead you to increase your income!')?></span></div>
                    <!-- item : begin -->
                    <div class="b-university__item b-university-item-first">
                        <div class="row">
                            <!-- left column : begin -->
                            <div class="col-sm-4 col-xs-12">
                                <div class="b-university-item__left-col">
                                    <img src="/images/university/example-university-2.png" alt="" class="b-university-item__img">
                                </div>
                            </div>
                            <!-- left column : end -->
                            <div class="col-sm-8 col-xs-12">
                                <!-- right column : begin -->
                                <div class="b-university-item__right-col">
                                    <p class="b-university-item__text">
                                        <?=Yii::t('university','Education system')?>
                                        <b><?=Yii::t('university','Vertera.University')?>
                                        </b> <?=Yii::t('university',"based on the experience of thousands 
                                        of successful people who have achieved significant financial results.Already 2500 partners
                                         have passed these steps and increased their income by 200-1000%. Now it's your turn!")?></p>
                                    <p class="b-university-item__text-special">
                                        <?=Yii::t('university','Make')?> <span>
                                            <?=Yii::t('university','already now')?>
                                        </span> <?=Yii::t('university','first step')?>
                                        <br><?=Yii::t('university','to your financial independence and freedom!')?></p>
                                </div>
                                <!-- right column : end -->
                            </div>
                        </div>
                    </div>
                    <!-- item : end -->
                    <!-- item : begin -->
                    <div class="b-university__item b-university-item-second">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <!-- right column : begin -->
                                <div class="b-university-item__right-col">
                                    <img src="/images/university/example-university-3.png" alt="" class="b-university-item__img">
                                </div>
                                <!-- right column : end -->
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <!-- left column : begin -->
                                <div class="b-university-item__left-col">
                                    <p class="b-university-item__title">
                                        <?=Yii::t('university','You do not have experience, do not know, doubt in yourself? NOT A PROBLEM!')?></p>
                                    <p class="b-university-item__text">
                                        <?=Yii::t('university','In our learning system, each step is simple
                                         and straightforward. We will teach you everything and step by step we 
                                         will deduce you to the desired financial result!')?></p>
                                </div>
                                <!-- left column : end -->
                            </div>
                        </div>
                    </div>
                    <!-- item : end -->
                    <!-- item : begin -->
                    <div class="b-university__item b-university-item-third">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <!-- left column : begin -->
                                <div class="b-university-item__left-col">
                                    <img src="/images/university/example-university-4.png" alt="" class="b-university-item__img">
                                </div>
                                <!-- left column : end -->
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <!-- right column : begin -->
                                <div class="b-university-item__right-col">
                                    <p class="b-university-item__title">
                                        <?=Yii::t('university','THE TRAINING PROGRAM IS ABSOLUTELY FREE
                                                                                 OF CHARGE <br> FOR VERTERA PARTNERS!')?></p>
                                    <!-- icons -->
                                    <div class="b-university-item__icons">
                                        <p class="b-university-item-icons__first"><span>9</span>
                                            <?=Yii::t('university','steps')?></p>
                                        <p class="b-university-item-icons__second"><span>30</span>
                                            <?=Yii::t('university','days')?></p>
                                        <p class="b-university-item-icons__third"><span>х10</span>
                                            <?=Yii::t('university','incomes')?></p>
                                    </div>
                                    <!-- button -->
                                    <a href="http://vertera.university/" class="b-button-brilliant-orange">
                                        <?=Yii::t('university','Start training')?></a>
                                </div>
                                <!-- right column : end -->
                            </div>
                        </div>
                    </div>
                    <!-- item : end -->
                </div>
            </div>
        </div>
    </div>
    <!-- wrapper : end -->
</div>
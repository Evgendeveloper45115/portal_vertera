<?php
/**
 * @var $this \yii\web\View
 */
$this->title = Yii::t('production', 'Production');
?>
<div class="b-production">
    <div class="b-descr-page b-descr-page-for-production">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- substrate background : begin -->
                    <div class="b-descr-page__substrate-bg">
                        <div class="b-descr-page-substrate-bg">
                            <!-- title -->
                            <div class="b-descr-page__title"><?= $this->title ?></div>
                            <!-- description -->
                            <p class="b-descr-page__text"><?= Yii::t(
                                    'production',
                                    'The main function of the manufacturing company is to produce high-effective, natural and safe products for prevention and rehabilitation.') ?></p>
                        </div>
                    </div>
                    <!-- substrate background : end -->
                </div>
            </div>
        </div>
    </div>

    <div class="b-production-content">

        <div class="container">
            <div class="b-production-descr__title"><?= Yii::t('production', 'The company has its own production, procuring, laboratory and research facilities.') ?>
            </div>
        </div>

        <div class="b-production-two-columns">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="b-production-slider-wrap">
                            <div class="b-production-slider b-production-slider-for slick-initialized slick-slider">
                                <div aria-live="polite" class="slick-list draggable">
                                    <div class="slick-track"
                                         style="opacity: 1; width: 545px; transform: translate3d(0px, 0px, 0px);"
                                         role="listbox">
                                        <div class="slick-slide slick-current slick-active" data-slick-index="0"
                                             aria-hidden="false" style="width: 545px;" tabindex="-1" role="option"
                                             aria-describedby="slick-slide00"><img
                                                    src="/images/production/slider/1.jpg" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="b-production-descr__subtitle">
                            <?= Yii::t('production', 'LLC NPO "Biomedical Innovative Technologies"') ?>
                        </div>
                        <ul class="b-production-descr__list">
                            <li> <?= Yii::t('production', 'More than 11 years specializes exclusively in the 
                            production of products from sea brown algae, thanks to which the
                            technology of production is brought to perfection;') ?>
                            </li>
                            <li><?= Yii::t('production', 'При производстве использует технологии, применяемые 
                            в военно-космической отрасли, а также собственные разработки;') ?>
                            </li>
                            <li> <?= Yii::t('production', 'The technology of production is patented and is 
                             kept in the strictest secrecy;') ?></li>
                            <li> <?= Yii::t('production', "For many years he has been cooperating with Professor Vitaliy Naumovich Korzun, who is the scientific director of the 
                             clinical research conducted by the Company. VN Korzun created the company's fundamental product, Vertera Gel, has been 
                             studying brown seaweed for more than 40 years and is one of the world's leading experts in this field;") ?>
                            </li>
                            <li><?= Yii::t('production', 'Uses its own production facilities;') ?></li>
                            <li><?= Yii::t('production', 'Awarded numerous awards for innovations in the field 
                             of dietary health nutrition;') ?>
                            </li>
                            <li><?= Yii::t('production', 'Carried out a lot of clinical research, which proved
                             the highest effectiveness of Vertera Gel as a dietary therapeutic 
                             food product for various problems;') ?>
                            </li>
                            <li><?= Yii::t('production', 'Produces absolutely safe, natural products;') ?></li>
                            <li><?= Yii::t('production', 'It uses algae produced in the north of Russia;') ?></li>
                            <li><?= Yii::t('production', 'Every year is tested and certified for compliance with 
                             international standards (in the EC countries).') ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="b-production-banner">
            <div class="container">
                <div class="b-production-banner-img">
                    <img src="/images/production/vertera.png" alt="">
                </div>
                <div class="b-production-banner-title">
                    <?= Yii::t('production', 'The company LLC "Biomedical Innovative Technologies" <br> (Tver)
                    produces the main product Vertera Gel and cosmetic products.') ?>
                </div>
            </div>
        </div>

        <div class="row-1">
            <div class="container">
                <div class="b-production-descr__title">
                    <?= Yii::t('production', 'Open Company NPO "Biotechnologies"') ?>
                </div>
                <div class="b-production__text">
                    <p><?= Yii::t('production', 'The company LLC NPO "BIOTECHNOLOGY" (Tver) specialize in 
                     the production of dry food phytocompositions, for tea drinks. The quantitative formulation of products is carefully guarded.') ?>
                    </p>
                    <br>
                    <p><?= Yii::t('production', 'At the enterprises of NPO BIOMEDITSINSK INNOVATIVE TECHNOLOGIES LLC and NGO <br> BIOTECHNOLOGY, a food safety
                       management system was introduced, certified in accordance with the requirements of GOST R ISO 22000-2007 (ISO 22000: 2005),
                       which includes the principles of HACCP') ?></p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <a href="/images/certificate1.jpg"><img style="float: right;" height="300"
                                                            src="/images/certificate1.jpg" alt=""></a>
                </div>
                <div class="col-sm-6">
                    <a href="/images/certificate2.jpg"><img height="300" src="/images/certificate2.jpg" alt=""></a>
                </div>
            </div>
        </div>

        <div class="b-full-width-video-production b-full-video-production-for-main-page">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- date range -->
                        <div class="b-full-width-video-production__date">Vertera</div>
                        <!-- title -->
                        <div class="b-full-width-video-production__title">
                            <?= Yii::t('production', 'Quality <br> assurance') ?></div>
                        <!-- text -->
                        <p class="b-full-width-video-production__text">
                            <?= Yii::t('production', 'The main purpose of the company is to produce highly 
                            effective, natural and safe products for preserving health and
                             increasing life expectancy.') ?></p>

                        <!-- link -->
                        <!--
                        <a href="https://youtu.be/zAWOl5d-zf8" class="b-button-video">Смотреть ролик</a>
                        -->
                    </div>
                </div>
            </div>
            <!-- background -->
            <img src="/images/production/video.png" alt="" class="b-full-video-production-for-video-page__bg">
        </div>

        <div class="row-2">
            <div class="container">
                <div class="b-production-descr__title"><?= Yii::t('production', 'All the products represented
                 in the Vertera range are exclusively natural!') ?>
                </div>
            </div>
        </div>

    </div>

</div>

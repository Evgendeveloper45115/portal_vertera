<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;
use app\models\Product;

$this->title = 'Vertera Gel';
$this->registerCssFile(Yii::getAlias('@web/css/market/style.css'), [
    'depends' => [\app\assets\AppAsset::class]
]);
$this->registerCssFile('https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&subset=cyrillic', [
    'depends' => [\app\assets\AppAsset::class]
]);
$this->registerJsFile(Yii::getAlias('@web/js/vertera_gel.js'), [
    'depends' => [\app\assets\AppAsset::class]
]);
$gel = Product::findOne([
    'title' => 'Laminaria Gel',
    'status' => Product::STATUS_ACTIVE,
]);
?>
<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<div class="screen--01" style="margin-top: 10px;">
    <div class="container">
        <div class="row">

            <div class="col-sm-2 hidden-xs">
                <p>
                    <img src="/images/ico--star.png"> <br>
                    <?= Yii::t('vertera-gel', 'Basic') ?>
                    <br>
                    <?= Yii::t('vertera-gel', 'product') ?>
                    <br>
                    <?= Yii::t('vertera-gel', 'of the company') ?>
                </p>

            </div>

            <div class="col-sm-8">

                <p class="pre-h1 tac"><?= Yii::t('vertera-gel', 'Based on seaweed') ?></p>
                <h1>Vertera <br>Gel
                    <small class="hidden-sm hidden-xs">
                        <?= Yii::t('vertera-gel', 'Health') ?>
                        <br>
                        <?= Yii::t('vertera-gel', 'and longevity') ?>
                        <br>
                        <?= Yii::t('vertera-gel', 'at any age') ?>
                    </small>
                </h1>
            </div>

            <div class="col-sm-2 hidden-xs">

                <p class="tac">
                    <img src="/images/ico--natur.png"> <br>
                    <?= Yii::t('vertera-gel', 'Natural') ?>
                    <br>
                    <?= Yii::t('vertera-gel', 'product') ?>
                </p>
                <p class="tac">
                    <img src="/images/ico--check.png"> <br>
                    <?= Yii::t('vertera-gel', 'Guaranteed result') ?>
                    <br>
                    <a href="#fake"><?= Yii::t('vertera-gel', 'see reviews') ?></a>
                </p>
                <p class="tac">
                    <img src="/images/ico--list.png"> <br>
                    <?= Yii::t('vertera-gel', 'The products') ?>
                    <br>
                    <?= Yii::t('vertera-gel', 'are certified') ?>
                </p>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-7">
                <img src="/images/banka.png" class="fixed" alt="">
            </div>
            <div class="col-sm-5">

                <div class="product--price ml30">
                    <ul>
                        <li>
                            <span><?= Yii::t('vertera-gel', 'Cost') ?></span>
                            <strong><?= $gel ? $gel->cost : '1950' ?> <i class="rub">a</i></strong>
                        </li>
                        <li>
                            <span><?= Yii::t('vertera-gel', 'In the package') ?></span>
                            <strong><?= '500' ?> <?= Yii::t('vertera-gel', 'g') ?></strong>
                        </li>
                    </ul>
                    <p>
                        <a href="https://vertera.market/promo/vertera_gel" target="_blank"
                           class="bn bn-fill"><?= Yii::t('vertera-gel', 'Buy') ?></a>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="screen--02 s-text">

    <div class="container tac">
        <h2><?= Yii::t('vertera-gel', 'What is Vertera Gel?') ?></h2>
        <p><?= Yii::t('vertera-gel', 'Vertera Gel is a cellular gel of seaweed kelp.') ?>
            <br>
            <?= Yii::t('vertera-gel', 'Natural specialized food product') ?>
            <br>
            <?= Yii::t('vertera-gel', 'dietetic therapeutic nutrition from kelp') ?>
        </p>
    </div>
    <div class="tac">
        <p><img src="/images/screen--02.jpg" class="img-responsive"></p>
    </div>
</div>
<div class="screen--03 s-text">
    <div class="container tac">
        <div class="col-xs-12">
            <h2><?= Yii::t('vertera-gel', 'Why Vertera Gel?') ?></h2>
            <h3>
                <?= Yii::t('vertera-gel', ' 90% of human diseases occur because') ?>
                <br>
                <?= Yii::t('vertera-gel', 'of the slagging of the body and the lack of a wide spectrum') ?>
                <br>
                <?= Yii::t('vertera-gel', 'of micro- and macroelements.') ?>
            </h3>
        </div>

        <div class="green-icons col-md-4">
            <a href="<?= Url::to(['/site/vertera_gel', '#' => 'lnk1']) ?>" style="color: #4aa147;">
                <img src="/images/ico--03-1.png"> <br>
                <span><?= Yii::t('vertera-gel', 'Daily cleansing of the body') ?></span>
            </a>
        </div>
        <div class="green-icons col-md-4">
            <a href="<?= Url::to(['/site/vertera_gel', '#' => 'lnk2']) ?>" style="color: #4aa147;">
                <img src="/images/ico--03-2.png"> <br>
                <span><?= Yii::t('vertera-gel', 'Nutrition, energy, bioregulation') ?></span>
            </a>
        </div>
        <div class="green-icons col-md-4">
            <a href="<?= Url::to(['/site/vertera_gel', '#' => 'lnk3']) ?>" style="color: #4aa147;">
                <img src="/images/ico--03-3.png"> <br>
                <span><?= Yii::t('vertera-gel', 'Logistics of substances') ?></span>
            </a>
        </div>
    </div>
</div>
<div class="screen--04 coso s-text">
    <div class="container">
        <div class="col-md-8">

            <h2><a name="lnk1"><?= Yii::t('vertera-gel', 'Sorbent of each <br> day') ?></a></h2>

            <p><img src="/images/ico-x.png" class="in-text"> <strong><?= Yii::t('vertera-gel', 'Problems') ?></strong>
            </p>
            <ol>
                <li><?= Yii::t('vertera-gel', 'Chemical compounds in food') ?></li>
                <li><?= Yii::t('vertera-gel', 'Toxic substances in water and air') ?></li>
                <li><?= Yii::t('vertera-gel', 'Daily intoxication of the body') ?></li>
            </ol>

            <p><img src="/images/product-2.png" class="img-responsive"></p>

        </div>

        <div class="col-md-4">

            <h2 class="hidden-xs"><br><br></h2>

            <p><img src="/images/ico-ok.png" class="in-text"> <strong><?= Yii::t('vertera-gel', 'Result') ?></strong>
            </p>
            <ol>
                <li><?= Yii::t('vertera-gel', 'Maximum food safety and body cleansing from poisons and toxins') ?></li>
                <li><?= Yii::t('vertera-gel', 'Massage, moisturizing and refreshing, protective film of the gastrointestinal mucosa') ?></li>
            </ol>

            <p class="fill"><?= Yii::t('vertera-gel', 'Only 50 grams between a glass of water and a tooth brushing before going to bed or in the morning') ?></p>
        </div>

    </div>
</div>
<div class="screen--05 s-text">
    <div class="container">
        <div class="col-md-8">

            <h2><a name="lnk2"><?= Yii::t('vertera-gel', 'Food, energy, <br>bioregulation') ?></a></h2>

            <p><img src="/images/ico-x.png" class="in-text"> <strong><?= Yii::t('vertera-gel', 'Problems') ?></strong>
            </p>
            <ol>
                <li><?= Yii::t('vertera-gel', 'Wear of the body') ?></li>
                <li><?= Yii::t('vertera-gel', 'Lack of nutrients') ?></li>
                <li><?= Yii::t('vertera-gel', 'Lack of energy') ?></li>
                <li><?= Yii::t('vertera-gel', 'Excess weight') ?></li>
                <li><?= Yii::t('vertera-gel', 'Weakened immunity') ?></li>
            </ol>

        </div>

        <div class="col-md-4">

            <h2 class="hidden-xs"><br><br></h2>

            <p><img src="/images/ico-ok.png" class="in-text"> <strong><?= Yii::t('vertera-gel', 'Result') ?></strong>
            </p>
            <ol>
                <li><?= Yii::t('vertera-gel', 'Rapid recovery through the launch of self-regulation mechanisms') ?></li>
                <li><?= Yii::t('vertera-gel', 'Weight reduction and normalization of fat, carbohydrate and protein metabolism') ?></li>
                <li><?= Yii::t('vertera-gel', 'Saturation with substances recorded in the human genetic code') ?></li>
                <li><?= Yii::t('vertera-gel', 'Active longevity') ?></li>
                <li><?= Yii::t('vertera-gel', 'A healthy cardiovascular system as the main logistic system in the body. Healthy thyroid gland. Healthy gastrointestinal tract. Strong immunity') ?></li>
            </ol>

        </div>

    </div>
</div>
<div class="screen--06 coso s-text">
    <div class="container">
        <div class="col-md-8">

            <h2><a name="lnk3"><?= Yii::t('vertera-gel', 'Logistics <br>of substances') ?></a></h2>

            <p><img src="/images/ico-x.png" class="in-text"> <strong><?= Yii::t('vertera-gel', 'Problems') ?></strong>
            </p>
            <ol>
                <li><?= Yii::t('vertera-gel', 'Disruption of delivery to cells of nutrients and substances,<br> removal and removal of toxins.') ?></li>
            </ol>


        </div>

        <div class="col-md-4">

            <h2 class="hidden-xs"><br><br></h2>

            <p><img src="/images/ico-ok.png" class="in-text"> <strong><?= Yii::t('vertera-gel', 'Result') ?></strong>
            </p>
            <ol>
                <li><?= Yii::t('vertera-gel', 'The product is certified as a dietary health food for the cardiovascular system as the main transport system in the body.') ?></li>

                <li><?= Yii::t('vertera-gel', 'This is a strategic important health product:') ?>
                    <br><?= Yii::t('vertera-gel', '- hearts') ?>,
                    <br><?= Yii::t('vertera-gel', '- vessels') ?>,
                    <br><?= Yii::t('vertera-gel', '- capillaries') ?>,
                    <br><?= Yii::t('vertera-gel', 'As a consequence - nutrition, respiration and detoxification of cells.') ?>
                </li>
            </ol>
        </div>

    </div>
</div>
<div class="screen--07 s-text">
    <div class="container tac	">

        <div class="row">
            <div class="col-md-12">
                <h3><?= Yii::t('vertera-gel', 'The cardiovascular system is a transport system for nutrients, oxygen, and toxin removal.') ?></h3>
                <p><?= Yii::t('vertera-gel', '-Components of algae cells are easily delivered to the right place <br>in the body, in the right amount and at the right time.') ?></p>
            </div>
        </div>

        <div class="row">

            <div class="list--el col-sm-3">
                <p><img src="/images/ico--01.png"></p>
                <p><strong><?= Yii::t('vertera-gel', 'Substances') ?></strong></p>
                <ul class="list--shower">
                    <li><?= Yii::t('vertera-gel', 'Alginic acids') ?></li>
                    <li><?= Yii::t('vertera-gel', 'Fucoidans') ?></li>
                    <li><?= Yii::t('vertera-gel', 'Mannitol') ?></li>
                    <li><?= Yii::t('vertera-gel', 'Proteins') ?></li>
                    <li><?= Yii::t('vertera-gel', 'Lipids') ?></li>

                    <li class="list-hide"><?= Yii::t('vertera-gel', 'Fucoidans') ?></li>
                    <li class="list-hide"><?= Yii::t('vertera-gel', 'Fucoidans') ?></li>
                </ul>
                <p><a href="#fake" class="js--shower"><?= Yii::t('vertera-gel', 'Show all') ?></a></p>
            </div>


            <div class="list--el col-sm-3">
                <p><img src="/images/ico--02.png" alt=""></p>
                <p><strong><?= Yii::t('vertera-gel', 'Macro- <br>and microelements') ?></strong></p>
                <ul class="list--shower">
                    <li>Ca2 (<?= Yii::t('vertera-gel', 'calcium') ?>)</li>
                    <li>K+1 (<?= Yii::t('vertera-gel', 'potassium') ?>)</li>
                    <li>Mg+2 (<?= Yii::t('vertera-gel', 'magnesium') ?>)</li>
                    <li>I (<?= Yii::t('vertera-gel', 'iodine') ?>)</li>
                    <li>Na+1 (<?= Yii::t('vertera-gel', 'sodium') ?>)</li>
                    <li>Fe+2 (<?= Yii::t('vertera-gel', 'iron') ?>)</li>
                    <li class="list-hide">Fe+2 (<?= Yii::t('vertera-gel', 'iron') ?>)</li>
                </ul>
                <p><a href="#fake" class="js--shower"><?= Yii::t('vertera-gel', 'Show all') ?></a></p>
            </div>
            <div class="list--el col-sm-3">
                <p><img src="/images/ico--03.png" alt=""></p>
                <p><strong><?= Yii::t('vertera-gel', 'Amino acids') ?> </strong></p>
                <ul class="list--shower">
                    <li>Asp (<?= Yii::t('vertera-gel', 'aspartic') ?>),</li>
                    <li>Thr (<?= Yii::t('vertera-gel', 'theronin') ?>)</li>
                    <li>Ser (<?= Yii::t('vertera-gel', 'serine') ?>)</li>
                    <li>Glu (<?= Yii::t('vertera-gel', 'glutamine') ?>)</li>
                    <li>Pro (<?= Yii::t('vertera-gel', 'proline') ?>)</li>
                    <li>Gly (<?= Yii::t('vertera-gel', 'glycine') ?>)</li>
                    <li class="list-hide">Pro (<?= Yii::t('vertera-gel', 'proline') ?>)</li>
                </ul>
                <p><a href="#fake" class="js--shower"><?= Yii::t('vertera-gel', 'Show all') ?></a></p>
            </div>
            <div class="list--el col-sm-3">
                <p><img src="/images/ico--04.png" alt=""></p>
                <p><strong><?= Yii::t('vertera-gel', 'Amino acids') ?> </strong></p>
                <ul class="list--shower">
                    <li>Met (<?= Yii::t('vertera-gel', 'methionine') ?>)</li>
                    <li>Ile (<?= Yii::t('vertera-gel', 'isoleucine') ?>)</li>
                    <li>Leu (<?= Yii::t('vertera-gel', 'leucine') ?>)</li>
                    <li>Tyr (<?= Yii::t('vertera-gel', 'tyrosine') ?>)</li>
                    <li>Phe (<?= Yii::t('vertera-gel', 'phenylalanine') ?>)</li>
                    <li>Lys (<?= Yii::t('vertera-gel', 'lysine') ?>)</li>
                    <li class="list-hide">Tyr (<?= Yii::t('vertera-gel', 'tyrosine') ?>)</li>
                </ul>
                <p><a href="#fake" class="js--shower"><?= Yii::t('vertera-gel', 'Show all') ?></a></p>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12">
                <img src="/images/screen--09.jpg" class="img-responsive">
            </div>
        </div>


    </div>
</div>
<div class="screen--08 s-text">
    <div class="container tac	">

        <h2><?= Yii::t('vertera-gel', 'For whom is Vertera Gel?') ?></h2>

        <ul class="list--whom">
            <li>
                <img src="/images/ico-1.png"><br>
                <?= Yii::t('vertera-gel', 'Who wants to stay healthy') ?>
            </li>
            <li>
                <img src="/images/ico-2.png"><br>
                <?= Yii::t('vertera-gel', 'Who wants to stay young and beautiful for many years') ?>
            </li>
            <li>
                <img src="/images/ico-3.png"><br>
                <?= Yii::t('vertera-gel', 'Who wants to be energetic and vigorous') ?>
            </li>
            <li>
                <img src="/images/ico-4.png"><br>
                <?= Yii::t('vertera-gel', 'Who leads a healthy lifestyle') ?>
            </li>
            <li>
                <img src="/images/ico-5.png"><br>
                <?= Yii::t('vertera-gel', 'Who follows the principles of proper nutrition') ?>
            </li>

        </ul>

    </div>
</div>
<div class="screen--09 coso s-text">
    <div class="container tac">
        <div class="col-md-12">

            <h2><?= Yii::t('vertera-gel', 'Testimonials of those who have already tried <br>Vertera Gel') ?></h2>

            <div class="container-fluid">

                <div id="slider-review" class="carousel slide" data-ride="carousel">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">

                        <div class="item active container-fluid">

                            <div class="col-lg-4">
                                <div class="review--box">
                                    <p><?= Yii::t('vertera-gel', 'I take just over a week! Very satisfied! Passing back pain! Much less spine got hurt!') ?></p>
                                </div>
                                <div class="review--author">
                                    <div class="review--img">
                                        <img src="/images/avatar_vertera.jpg">
                                    </div>
                                    <div class="review--name">
                                        <p><?= Yii::t('vertera-gel', 'Likhatsky Sergey') ?></p>
                                        <p><span><?= Yii::t('vertera-gel', 'Krasnodar region') ?></span></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="review--box">
                                    <p><?= Yii::t('vertera-gel', 'After this mess, you do not want to eat the usual cereal porridge, which is sold in stores. Delicious! I recommend to all!') ?></p>
                                </div>
                                <div class="review--author">
                                    <div class="review--img">
                                        <img src="/images/avatar_vertera.jpg">
                                    </div>
                                    <div class="review--name">
                                        <p><?= Yii::t('vertera-gel', 'Borisova Fausina') ?></p>
                                        <p><span><?= Yii::t('vertera-gel', 'Nevyansk, Sverdlovsk region') ?></span></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="review--box">
                                    <p><?= Yii::t('vertera-gel', 'I can tell a lot. And better recommend buying a product, and begin to heal. Be healthy and smile.') ?></p>
                                </div>
                                <div class="review--author">
                                    <div class="review--img">
                                        <img src="/images/avatar_vertera.jpg">
                                    </div>
                                    <div class="review--name">
                                        <p><?= Yii::t('vertera-gel', 'Elvira Kamalova') ?></p>
                                        <p><span><?= Yii::t('vertera-gel', 'Nizhnekamsk') ?></span></p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <!-- Controls -->
                    <!--
                    <a class="left carousel-control2" href="#slider-review" data-slide="prev">
                      <img src="/images/left.png" alt="">
                    </a>
                    <a class="right carousel-control2" href="#slider-review" data-slide="next">
                      <img src="/images/right.png" alt="">
                    </a>
                    -->

                </div>
            </div>

        </div>


    </div>
</div>
<div class="screen--10 s-text">
    <div class="container tac	">

        <h2><?= Yii::t('vertera-gel', 'Choose your taste, <br> natural and fruit gels') ?></h2>

        <div class="row">

            <div class="col-md-3 col-sm-4">
                <div class="product">
                    <p><a href="/catalog/zdorove/umnoe_pitanie/laminaria_gel/"><img class="img-responsive"
                                                                                    src="/images/Vertera_gel_gor5LKV.png"></a>
                    </p>
                    <h4>Laminaria Gel</h4>
                    <p><?= Yii::t('vertera-gel', 'The main purpose of the gel is dietary health food. This unique product helps the processes of purification and saturation of the organism at the cellular level. The fundamental product of Vertera') ?></p>
                    <hr>
                    <p><strong>2275 <span class="rub">a</span></strong></p>
                    <p><a href="https://vertera.market/catalog/zdorove/umnoe_pitanie/laminaria-gel/" target="_blank"
                          class="bn bn-fill bn-small"><?= Yii::t('vertera-gel', 'Buy') ?></a>
                    </p>
                </div>
            </div>

            <div class="col-md-3 col-sm-4">
                <div class="product">
                    <p><a href="/catalog/zdorove/umnoe_pitanie/vertera_forte_cernaa_smorodina/"><img
                                    class="img-responsive"
                                    src="/images/forte_black_currant_GawL4Mo.png"></a>
                    </p>
                    <h4>Vertera Forte «<?= Yii::t('vertera-gel', 'Blackberry') ?>»</h4>
                    <p><?= Yii::t('vertera-gel', 'Gel of black currant based on marine brown algae. The balanced composition includes extracts of plants, including polyvitamin plant, dogrose, which provides the body with a physiological complex of water- and fat-soluble vitamins.') ?>

                    </p>
                    <hr>
                    <p><strong>2275 <span class="rub">a</span></strong></p>
                    <p><a href="https://vertera.market/catalog/zdorove/umnoe_pitanie/vertera-forte-black-currant/" target="_blank"
                          class="bn bn-fill bn-small"><?= Yii::t('vertera-gel', 'Buy') ?></a></p>
                </div>
            </div>

            <div class="col-md-3 col-sm-4">
                <div class="product">
                    <p><a href="/catalog/zdorove/umnoe_pitanie/vertera_forte_visna/"><img class="img-responsive"
                                                                                          src="/images/forte_cherry_FrIu0QU.png"></a>
                    </p>
                    <h4>Vertera Forte «<?= Yii::t('vertera-gel', 'Cherry') ?>»</h4>
                    <p><?= Yii::t('vertera-gel', 'Cherry gel based on sea kelp. The balanced composition of the product includes sea pectin, a natural sorbent that manifests itself as a means for binding toxic metals (lead, mercury, nickel, cadmium, manganese).') ?>

                    </p>
                    <hr>
                    <p><strong>2275 <span class="rub">a</span></strong></p>
                    <p><a href="https://vertera.market/catalog/zdorove/omolozenie/vertera-forte-cherry/" target="_blank"
                          class="bn bn-fill bn-small"><?= Yii::t('vertera-gel', 'Buy') ?></a></p>
                </div>
            </div>

            <div class="col-md-3 col-sm-4">
                <div class="product">
                    <p><a href="/catalog/zdorove/umnoe_pitanie/vertera_forte_abloko/"><img class="img-responsive"
                                                                                           src="/images/forte_apple_wOAR1qE.png"></a>
                    </p>
                    <h4>Vertera Forte «<?= Yii::t('vertera-gel', 'Apple') ?>»</h4>
                    <p><?= Yii::t('vertera-gel', 'Apple gel with sea kelp. The balanced composition includes plant extracts, including echinacea, containing a complex of biologically active substances-stimulants of the immune system, capable of increasing the body\'s defense against infectious agents.') ?>

                    </p>
                    <hr>
                    <p><strong>2275 <span class="rub">a</span></strong></p>
                    <p><a href="https://vertera.market/catalog/zdorove/zenskoe_zdorove/vertera-forte-apple/" target="_blank"
                          class="bn bn-fill bn-small"><?= Yii::t('vertera-gel', 'Buy') ?></a></p>
                </div>
            </div>

        </div>

    </div>
</div>
<div class="screen--11 coso s-text">
    <div class="container tac">

        <h2><?= Yii::t('vertera-gel', 'Certificates <br>and clinical trials') ?></h2>

        <div class="sertificate col-sm-3">
            <p><img src="/images/s-1.png" class="img-responsive"></p>
            <p><?= Yii::t('vertera-gel', 'Protocol for testing the gel sample for the content and composition of microelements') ?></p>
        </div>

        <div class="sertificate col-sm-3">
            <p><img src="/images/s-2.png" class="img-responsive"></p>
            <p><?= Yii::t('vertera-gel', 'Declaration of conformity') ?></p>
        </div>

        <div class="sertificate col-sm-3">
            <p><img src="/images/s-3.png" class="img-responsive"></p>
            <p><?= Yii::t('vertera-gel', 'Conclusion of sanitary and epidemiological expertise') ?></p>
        </div>

        <div class="sertificate col-sm-3">
            <p><img src="/images/s-4.png" class="img-responsive"></p>
            <p><?= Yii::t('vertera-gel', 'Certificate of state registration') ?></p>
        </div>


    </div>
</div>
<div class="screen--12">
    <div class="container">

        <div class="row">

            <div class="col-md-2 hidden-sm hidden-xs">
                <p>
                    <img src="/images/ico--star-white.png"> <br>
                    <?= Yii::t('vertera-gel', 'The basic <br> product <br> of the company') ?>
                </p>
            </div>

            <div class="col-sm-8">
                <p class="pre-h1 tac"><?= Yii::t('vertera-gel', 'Based on seaweed') ?></p>
                <h1>Vertera <br>Gel
                    <small class="hidden-xs hidden-sm"><?= Yii::t('vertera-gel', 'Health <br>& Longevity <br> at any age') ?></small>
                </h1>
            </div>

            <div class="col-md-2 hidden-sm hidden-xs">
                <p class="tac">
                    <img src="/images/ico--natur-white.png"> <br>
                    <?= Yii::t('vertera-gel', 'Natural <br> product') ?>
                </p>
                <p class="tac">
                    <img src="/images/ico--check-white.png"> <br>
                    <?= Yii::t('vertera-gel', 'Guaranteed result') ?> <br> <a
                            href="#fake"><?= Yii::t('vertera-gel', 'see reviews') ?></a>
                </p>
                <p class="tac">
                    <img src="/images/ico--list-white.png"> <br>
                    <?= Yii::t('vertera-gel', 'Products <br> certified') ?>
                </p>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-6">
                <img class="fixed img-responsive" src="/images/banka-footer.png" alt="">
            </div>

            <div class="col-sm-6">

                <div class="product--price product--price-white">
                    <ul>
                        <li>
                            <span><?= Yii::t('vertera-gel', 'Cost') ?></span>
                            <strong><?= $gel ? $gel->cost : '1950' ?> <i class="rub">a</i></strong>
                        </li>
                        <li>
                            <span><?= Yii::t('vertera-gel', 'In the package') ?></span>
                            <strong><?= '500' ?> <?= Yii::t('vertera-gel', 'g') ?></strong>
                        </li>
                    </ul>
                    <p>
                        <a href="https://vertera.market/catalog/zdorove/umnoe_pitanie/laminaria-gel/"
                           target="_blank"
                           class="bn bn-fill bn-oliva"><?= Yii::t('vertera-gel', 'Buy') ?></a>
                    </p>
                </div>
                <div class="product--price product--price-white f-right hidden-xs hidden-sm tar">
                    <h4><?= Yii::t('vertera-gel', 'Do you have<br> questions?') ?></h4>
                    <p>
                        <a href="<?= Url::to(['/contact', '#' => 'ask-question']) ?>"
                           class="bn bn-fill bn-sal"><?= Yii::t('vertera-gel', 'Ask a Question') ?></a>
                    </p>
                </div>
            </div>

        </div>

    </div>
</div>
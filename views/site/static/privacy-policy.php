<?php
/**
 * @var $this \yii\web\View
 */
$this->title = Yii::t('layout', 'Privacy Policy');
?>
<div class="b-descr-page b-descr-page-for-about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- substrate background : begin -->
                <div class="b-descr-page__substrate-bg">
                    <div class="b-descr-page-substrate-bg">
                        <!-- title -->
                        <h1 class="b-descr-page__title"><?= Yii::t('layout', 'Privacy Policy') ?></h1>
                        <!-- description -->
                        <p class="b-descr-page__text">Vertera – это лидер на рынке прямых продаж в своем сегментев России и СНГ                            </p>
                    </div>
                </div>
                <!-- substrate background : end -->
            </div>
        </div>
    </div>
</div>
<div class="b-capabilities js-capabilities" style="margin: 50px 0;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="container">
                    <div class="b-about-content-title"><?= Yii::t('privacy-policy', 'In this privacy policy, you can review your rights and information about the processing of your personal data.') ?></div>
                    <div class="b-about-content-text">
                        <p class="b-history__text"><?= Yii::t('privacy-policy', 'The Privacy Policy ("Privacy Policy") defines the principles on the basis of which Vertera OÜ ("Enterprise") protects personal data in connection with the provision of services, for which the Enterprise conducts personal data processing.') ?></p>
                        <p class="b-history__text"><?= Yii::t('privacy-policy', 'The privacy policy applies to all persons who use any of the services and the Enterprise web page and perform requests for the services of the Enterprise') ?></p>
                        <p class="b-history__text"><?= Yii::t('privacy-policy', 'Privacy policy is based on') ?>
                            <a
                                    href="https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2016.119.01.0001.01.ENG&toc=OJ:L:2016:119:TOC">Общего
                                <?= Yii::t('privacy-policy', 'Regulations for Data Protection') ?></a>
                            <?= Yii::t('privacy-policy', 'GDPR (General Data Protection Regulation), as well as the Data Protection Act in Estonia.') ?>
                        </p>
                        <p class="b-history__text"><?= Yii::t('privacy-policy', 'Privacy policy is a part of all contracts of the Enterprise. By visiting the Enterprise website, sending inquiries to the Enterprise regarding services and starting to use the services of the Enterprise, you agree to the data processing principles described in the Privacy Policy.') ?></p>
                        <p class="b-history__text"><?= Yii::t('privacy-policy', 'According to Article 4 of Part 2 of the General Regulation on Data Protection, the processing of personal data, all actions performed with personal data, either individually or together, including their collection, recording, streamlining, storage, modification, provision of access to data, query production, manufacturing statements, the use of data, their transfer, cross-use, association, blocking, exclusion or destruction, regardless of the manner in which they were committed or the means used') ?></p>
                        <p class="b-history__text">
                            <?= Yii::t('privacy-policy', 'The manager of your personal data is:') ?>
                        </p>
                        <ul class="rule-list">
                            <li>
                                <b><?= Yii::t('privacy-policy', '1. What changed after the new law came into force?') ?></b>
                                <ul class="rule-list rule-list--sub">
                                    <li><?= Yii::t('privacy-policy', '1.1. After the new regulation comes into force, we will continue to observe all the principles of the safety of personal data processing;') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '1.2.Treat Personal Data in a legal, honest and transparent manner;') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '1.3.Collect personal data in established, clearly designated and lawful purposes and not process them in a manner inconsistent with these goals;') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '1.4. Guarantee that the processed personal data are adequate, suitable and only such as are necessary to achieve the purposes in which they are processed;') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '1.5. Guarantee that the Personal Data is accurate and updated as necessary; take all reasonable steps to ensure that inaccurate personal data, subject to the processing purpose, is immediately deleted or corrected;') ?>
                                    </li>
                                    <li>
                                        <ul class="rule-list rule-list--sub">
                                            <li><?= Yii::t('privacy-policy', '1.1.5. store personal data in such a way that the identity of the data subjects can not be established for longer than necessary for the purposes in which the personal data are processed;') ?>
                                            </li>
                                            <li><?= Yii::t('privacy-policy', '1.1.6. process Personal Data in such a way that, when appropriate technical or organizational measures are taken, the appropriate security of personal data is ensured, including protection from data processing without permission or illegal processing of data and from inadvertent loss, destruction or damage') ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b><?= Yii::t('privacy-policy', '2. What personal data do you collect and process?') ?></b>
                                <ul class="rule-list rule-list--sub">
                                    <li><?= Yii::t('privacy-policy', '2.1.Your personal data we collect only in established, clearly defined and lawful purposes.') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '2.2.Your personal data processed for the purpose of concluding a contract:') ?>
                                        <ul class="rule-list rule-list--sub">
                                            <li><?= Yii::t('privacy-policy', 'Name;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'Last name;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'Middle name;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'Date of Birth;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'Passport number / id card number;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'Copy of passport / id card;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'Postal address;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'Registration address;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'E-mail address;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'Phone number;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'IP address;') ?></li>
                                            <li><?= Yii::t('privacy-policy', 'account number (banking / payment system);') ?></li>
                                            <li><br></li>
                                            <li><?= Yii::t('privacy-policy', 'You can also provide additional personal information that we will process only when you enter them and store them in your personal account.') ?>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '2.3.Your personal data processed for the purposes of drafting and executing the contract:') ?>
                                        <ul class="rule-list rule-list--sub">
                                            <li><?= Yii::t('privacy-policy', '1.8.1. In order that we could sell you the goods you selected in the e-store vertera.org, we need to process the following your personal data:') ?>
                                                <ul class="rule-list rule-list--sub">
                                                    <li><?= Yii::t('privacy-policy', 'Name;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'Last name;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'Middle name;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'Date of Birth;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'Passport number / id card number;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'Copy of passport / id card;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'Postal address;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'Registration address;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'E-mail address;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'Phone number;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'IP address;') ?></li>
                                                    <li><?= Yii::t('privacy-policy', 'account number (banking / payment system);') ?></li>
                                                    <li><br></li>
                                                    <li><?= Yii::t('privacy-policy', 'To provide the address for the delivery of goods, we ask you only if, when preparing the order, you want the goods for your convenience to be delivered to the specified address') ?>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '2.4.Your personal data processed in order to improve services and analysis of activities;') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '2.5.Relation from the processing of personal data and further use of the online store vertera.org;') ?>
                                        <ul class="rule-list rule-list--sub">
                                            <li><?= Yii::t('privacy-policy', '2.5.1. You can always disagree with the processing of your additional personal data, which is intended only for analysis and marketing purposes. But without the required data to complete the order, it is impossible to complete the purchase, since we can not fulfill such an order.') ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b><b><?= Yii::t('privacy-policy', '3. Transfer of personal data') ?></b></b>
                                <ul class="rule-list rule-list--sub">
                                    <li><?= Yii::t('privacy-policy', '3.1. Personal data is transmitted only to reliable third countries with which we have concluded contracts, and only to the extent necessary for the provision of the service. In order to properly provide services to you, we transfer your personal data to the following partners:') ?>
                                        <ul class="rule-list rule-list--sub">
                                            <li><?= Yii::t('privacy-policy', '3.1.1. Enterprises providing courier services that deliver goods to you at the addresses you indicated;') ?>
                                            </li>
                                            <li><?= Yii::t('privacy-policy', '3.1.2. Providers of goods that deliver goods directly to you; postal service') ?>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <b><?= Yii::t('privacy-policy', '3.2 Your personal data we also provide:') ?></b>
                                        <ul class="rule-list rule-list--sub">
                                            <li><?= Yii::t('privacy-policy', '3.2.1. Law enforcement authorities in accordance with the procedure provided for by legal acts of the Republic of Estonia;') ?>
                                            </li>
                                            <li><?= Yii::t('privacy-policy', '3.2.2. State bodies and courts, when such an obligation is provided for by applicable legal acts.') ?>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><b><?= Yii::t('privacy-policy', '3.3.Conservation of personal data') ?></b></li>
                                    <li><?= Yii::t('privacy-policy', '3.4.Your personal data is stored as much as necessary for the performance of the contract for the acquisition of goods, but not shorter than we are obligated by legal acts regulating our activities. Your personal data processed with your consent is processed and stored until your consent is withdrawn') ?>
                                    </li>
                                </ul>
                            </li>
                            <li><b><?= Yii::t('privacy-policy', '4. Acquaintance with my personal data') ?></b>
                                <ul class="rule-list rule-list--sub">
                                    <li><?= Yii::t('privacy-policy', '4.1. You have the right to see the personal data processed by us. With processed your personal data you can familiarize with the following ways:') ?>
                                        <ul class="rule-list rule-list--sub">
                                            <li>
                                                - <?= Yii::t('privacy-policy', 'In my personal office vertera.org') ?></li>
                                            <li>
                                                - <?= Yii::t('privacy-policy', 'Send us a request by e-mail: info@vertera.org') ?></li>
                                            <li>
                                                - <?= Yii::t('privacy-policy', 'Notifying us by phone: +7 (920) 191-61-87.') ?>
                                                Известив нас по телефону: +7 (920) 191-61-87.
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b><?= Yii::t('privacy-policy', '5. Updating / changing my personal data') ?></b>
                                <ul class="rule-list rule-list--sub">
                                    <li> - <?= Yii::t('privacy-policy', 'In my personal office vertera.org') ?></li>
                                    <li>
                                        - <?= Yii::t('privacy-policy', 'Send us a request by e-mail: info@vertera.org') ?></li>
                                    <li> - <?= Yii::t('privacy-policy', 'Notifying us by phone: +7 (920) 191-61-87.') ?>
                                        Известив нас по телефону: +7 (920) 191-61-87.
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b><?= Yii::t('privacy-policy', '6. Refusal to consent and removal of personal data') ?></b>
                                <ul class="rule-list rule-list--sub">
                                    <li><?= Yii::t('privacy-policy', '6.1. You always have the right to refuse consent to the processing of your personal data.') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '6.2. In case of refusal to consent to the processing of personal data, please send a notification to the e-mail address info@vertera.org') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '6.3.Takzhe request for the removal of your personal data you can apply in the following ways:') ?>
                                        <ul class="rule-list rule-list--sub">
                                            <li>
                                                - <?= Yii::t('privacy-policy', 'In my personal office vertera.org') ?></li>
                                            <li>
                                                - <?= Yii::t('privacy-policy', 'Send us a request by e-mail: info@vertera.org') ?></li>
                                            <li>
                                                - <?= Yii::t('privacy-policy', 'Notifying us by phone: +7 (920) 191-61-87.') ?>
                                                Известив нас по телефону: +7 (920) 191-61-87.
                                            </li>
                                        </ul>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '6.4. Your personal data will not be deleted from our database, if we receive your request, we will establish that:') ?>
                                        <ul class="rule-list rule-list--sub">
                                            <li><?= Yii::t('privacy-policy', '6.4.1. Your personal data is necessary to achieve the goals for which they were collected or processed') ?>
                                            </li>
                                            <li><?= Yii::t('privacy-policy', '6.4.2. You have not withdrawn consent to the processing of your personal data;') ?>
                                            </li>
                                            <li><?= Yii::t('privacy-policy', '6.4.3. You did not agree with the processing of your personal data, but such data processing is necessary in our legitimate interests;') ?>
                                            </li>
                                            <li><?= Yii::t('privacy-policy', '6.4.4. We are obliged to process your personal data by legal acts of the European Union and the country.') ?>
                                            </li>
                                            <li><?= Yii::t('privacy-policy', '6.4.5. We must process your data in connection with the application, execution or protection of legal requirements.') ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b><?= Yii::t('privacy-policy', '7. Restriction of the right to process personal data.') ?> </b>
                                <ul class="rule-list rule-list--sub">
                                    <li><?= Yii::t('privacy-policy', '7.1. You have the right to limit or object to the processing of your personal data at any time, if there are sufficient grounds in your particular situation, if data processing is not required by law.') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '7.2. In this case, if we can not provide convincing legal reasons for processing the data or the arguments in favor of establishing, implementing or protecting legitimate claims, we will no longer process or limit the processing of personal data.') ?>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b><?= Yii::t('privacy-policy', '8. Other Rights') ?></b>
                                <ul class="rule-list rule-list--sub">
                                    <li><?= Yii::t('privacy-policy', '8.1. This Privacy Policy contains information about which personal data we collect and how they are used.') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '8.2. In addition to the above access rights, changes and limitations or objections to processing, individuals may have other rights to the personal data we store, such as the right to delete / delete and the right to data portability.') ?>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <?= Yii::t('privacy-policy', '9. What are cookies?') ?>
                                <ul class="rule-list rule-list--sub">
                                    <li><?= Yii::t('privacy-policy', '9.1.Cookie files are small text files that are sent to your device when you visit the corresponding website. Cookie files help us identify your device and improve the functionality of our website and make it easier to use.') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '9.2. We use cookies to get information about how you use our Web site. Cookies allow us to provide the ability to use the Website in a more convenient and accessible format. Such files, as a rule, do not contain any data that would allow us to identify you as a specific individual. Your data will be processed using cookies during the period allowed by you') ?>
                                    </li>
                                    <li><?= Yii::t('privacy-policy', '9.3.Cookie - files you can manage through the settings of your browser, choosing which cookies you want to accept and which ones to delete. From cookies you can refuse at any time, removing them from your browser, in which they are embedded. The location for managing cookies depends on the browser you are using. In order to learn more about how to manage cookies on your browser or delete them, go to the web page in your browser.') ?>
                                    </li>
                                    <li><br></li>
                                    <li><?= Yii::t('privacy-policy', 'You can also at any time cancel your consent to use cookies by sending us a request by e-mail info@vertera.org or by phone +7 (920) 191-61-87. By canceling your consent to use the cookie, you must also delete cookies embedded in your browser.') ?>
                                    </li>
                                    <li><br></li>
                                    <li><?= Yii::t('privacy-policy', 'You can delete all cookies already saved in your computer, and you can set that the browser does not allow them to be saved. But in this case, you must manually specify the choice each time you visit the website, and some services and functions of the home page may not work.') ?>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <b><?= Yii::t('privacy-policy', '10. Complaints') ?></b>
                                <ul class="rule-list rule-list--sub">
                                    <li><?= Yii::t('privacy-policy', '10.1. You have the right to file a complaint with the Data Protection Authority ("DPA") if you believe that your Personal Information is not being processed correctly or your rights as a data subject have been violated. You can file a complaint by contacting:') ?>
                                        <ul class="rule-list rule-list--sub">
                                            <li>
                                                <a href="http://www.aki.ee/ru"><?= Yii::t('privacy-policy', 'Data Protection Inspectorate') ?></a>
                                                <?= Yii::t('privacy-policy', '(Väike-Amerika 19, Tallinn 10129, e-mail info@aki.ee).') ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <p class="b-history__text">
                            <b><?= Yii::t('privacy-policy', 'Additional Information') ?></b></p>
                        <p class="b-history__text"><?= Yii::t('privacy-policy', 'Vertera OÜ reserves the right to change the general terms and conditions of the privacy policy by notifying the clinic\'s website') ?>
                            <a href="https://vertera.org/"><b>vertera.org</b></a>
                        </p>
                        <p class="b-history__text"><?= Yii::t('privacy-policy', 'By visiting vertera.org, you agree to the terms of use of the site and the Vertera OÜ privacy policy.') ?></p>
                        <p class="b-history__text"><?= Yii::t('privacy-policy', 'For all questions arising from the privacy policy or the processing of data, please contact') ?></p>
                        <br>
                        <p class="b-history__text">
                            <i><?= Yii::t('privacy-policy', 'Version of the Privacy Policy of 25/05/2018') ?></i></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .b-capabilities .b-about-content-text p {
        text-align: left;
        margin-bottom: 15px;
        font-size: 16px;
    }

    .b-capabilities .b-about-content-text * {
        text-align: left;
        font-size: 16px;
        font-family: "Helvetica Neue";
    }

    .b-capabilities .rule-list {
        margin-top: 10px;
        margin-bottom: 10px;
        list-style-type: none;
    }

    .b-capabilities .rule-list--sub {
        margin-left: 15px;
    }
</style>
<?php
/**
 * @var $this \yii\web\View
 */
$this->title = Yii::t('layout', 'World of trade');
?>
<div class="world_of_retail">
    <!-- DESCRIPTION PAGE : begin -->
    <div class="b-descr-page b-descr-page-for-world-of-retail">
        <!-- background -->
        <div class="b-descr-page__bg">
            <img src="/images/worldOfRetail/bg-world-of-retail.png" alt="">
        </div>
        <div class="container">
            <!-- breadcrumbs : begin -->
            <div class="b-breadcrumbs">
                <ul>
                    <li><a href="<?= \yii\helpers\Url::to(['/']) ?>">
                            <?= Yii::t('world-of-retail', 'Main') ?></a></li>
                    <li><?= Yii::t('world-of-retail', 'World of Trade') ?></li>
                </ul>
            </div>
            <!-- breadcrumbs : end -->
            <div class="row">
                <div class="col-xs-12">
                    <!-- substrate background : begin -->
                    <div class="b-descr-page__substrate-bg">
                        <div class="b-descr-page-substrate-bg">
                            <!-- subtitle -->
                            <div class="b-descr-page__subtitle">World of retail</div>
                            <!-- title -->
                            <div class="b-descr-page__title">
                                <?= Yii::t('world-of-retail', 'Ideal MLM-business <br> together with World of Retail') ?></div>
                            <!-- description -->
                            <p class="b-descr-page__text">
                                <?= Yii::t('world-of-retail', 'Today on the Internet we are offered millions of goods and services on different resources. Tens of thousands of
       online stores offer discounts and promotions. It is impossible to keep abreast of all the news.
      The unique "World of Trade" site is not only an opportunity to purchase goods and services in one place,
       but also to get its share of cash from the world turnover of this platform.') ?></p>
                            <!-- button -->
                            <a href="https://wor.ru/" class="b-button-green">
                                <?= Yii::t('world-of-retail', 'Go to the site') ?></a>
                        </div>
                    </div>
                    <!-- substrate background : end -->
                </div>
            </div>
        </div>
    </div>
    <!-- DESCRIPTION PAGE : end -->
    <!-- WORLD OF RETAIL : begin -->
    <div class="b-world-of-retail">
        <!-- what on platform : begin -->
        <div class="b-world-of-retail__what-on-platform">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- title -->
                        <div class="b-world-of-retail-what-on-platform__title">
                            <?= Yii::t('world-of-retail', 'What is on the <br> "World of Trade" platform') ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <!-- left column : begin -->
                    <div class="col-sm-6 col-xs-12">
                        <!-- image -->
                        <img src="/images/worldOfRetail/example-whot-on-platform-1.png" alt=""
                             class="b-world-of-retail-what-on-platform__img">
                        <!-- subtitle -->
                        <div class="b-world-of-retail-what-on-platform__subtitle">
                            <?= Yii::t('world-of-retail', 'What is on the <br> Huge selection of goods') ?></div>
                        <!-- text -->
                        <p class="b-world-of-retail-what-on-platform__text">
                            <?= Yii::t('world-of-retail', "More than 20 million goods (clothing, footwear,
                             cosmetics, food, travel, financial services, goods for cars, children's
                             goods, personal care products and others)") ?></p>
                    </div>
                    <!-- left column : begin -->
                    <!-- right column : begin -->
                    <div class="col-sm-6 col-xs-12">
                        <!-- image -->
                        <img src="/images/worldOfRetail/example-whot-on-platform-2.png" alt=""
                             class="b-world-of-retail-what-on-platform__img">
                        <!-- subtitle -->
                        <div class="b-world-of-retail-what-on-platform__subtitle">
                            <?= Yii::t('world-of-retail', 'More than 250 online stores') ?></div>
                        <!-- text -->
                        <p class="b-world-of-retail-what-on-platform__text">
                            <?= Yii::t('world-of-retail', 'More than 250 of the most popular online stores,
                             including world leaders in the industry, with the best offers:
                               Sportmaster, Lamoda, OZON.RU, M.Video, AliExpress and others.') ?></p>
                    </div>
                    <!-- right column : end -->
                </div>
            </div>
            <div class="b-parallax">
                <div class="b-parallax-catalog__animate-el-15"></div>
                <div class="b-parallax-catalog__animate-el-16"></div>
            </div>
        </div>
        <!-- what on platform : end -->
        <!-- income platform : begin -->
        <div class="b-world-of-retail__income-platform">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- title -->
                        <div class="b-world-of-retail-income-platform__title">
                            <?= Yii::t('world-of-retail', 'What income is available <br> for platform?') ?>
                        </div>
                        <!-- text -->
                        <div class="b-world-of-retail-income-platform__text">
                            <?= Yii::t('world-of-retail', 'Any purchase on the "World of Trade" platform 
                            is always profitable! Receive income not only from your purchases, but also from purchases of 
                            your clients and customers of your structure, as well as from sales of your suppliers.') ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <!-- subtitle -->
                        <div class="b-world-of-retail-income-platform__subtitle b-world-of-retail-income-platform__subtitle-first">
                            <?= Yii::t('world-of-retail', 'From all personal purchases you get a cashback of 2 to 40%') ?>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <!-- subtitle -->
                        <div class="b-world-of-retail-income-platform__subtitle b-world-of-retail-income-platform__subtitle-second">
                            <?= Yii::t('world-of-retail', 'From all purchases of your first line you get 25% of the amount of their cashback') ?>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <!-- subtitle -->
                        <div class="b-world-of-retail-income-platform__subtitle b-world-of-retail-income-platform__subtitle-third">
                            <?= Yii::t('world-of-retail', "With all the purchases of your entire structure, you get a score (according to the Company's partner program)") ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- income platform : end -->
        <!-- invite platform : begin -->
        <div class="b-world-of-retail__invite-platform">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- title -->
                        <div class="b-world-of-retail-invite-platform__title">
                            <?= Yii::t('world-of-retail', 'Who can be invited to the platform? <br> Absolutely everyone!') ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- item : begin -->
            <div class="b-world-of-retail-invite-platform__item">
                <div class="container">
                    <div class="row">
                        <!-- left column : begin -->
                        <div class="col-sm-6 col-xs-12">
                            <!-- image -->
                            <img src="/images/worldOfRetail/example-invite-platform-1.jpg" alt=""
                                 class="b-world-of-retail-invite-platform-item__img">
                        </div>
                        <!-- left column : end -->
                        <!-- right column : begin -->
                        <div class="col-sm-6 col-xs-12">
                            <!-- title -->
                            <div class="b-world-of-retail-invite-platform-item__title">
                                <?= Yii::t('world-of-retail', 'Invite for lucrative <br> shopping') ?>
                            </div>
                            <!-- text -->
                            <div class="b-world-of-retail-invite-platform-item__text">
                                <?= Yii::t('world-of-retail', 'Invite friends, acquaintances and even strangers.
                                 Now you can buy everything on the Internet, and on the "World of Trade" platform, you can do this
                                  not only for the best prices, but also get a part of the money back!') ?>
                            </div>
                        </div>
                        <!-- right column : end -->
                    </div>
                </div>
            </div>
            <!-- item : end -->
            <!-- item : begin -->
            <div class="b-world-of-retail-invite-platform__item">
                <div class="container">
                    <div class="row">
                        <!-- left column : begin -->
                        <div class="col-sm-6 col-xs-12">
                            <!-- title -->
                            <div class="b-world-of-retail-invite-platform-item__title">
                                <?= Yii::t('world-of-retail', 'Invite to build a networked business on a simple and unique resource') ?>
                            </div>
                            <!-- text -->
                            <div class="b-world-of-retail-invite-platform-item__text">
                                <?= Yii::t('world-of-retail', "This is a unique platform on which everyone can earn, and building a business is much easier! Filling in the affiliate 
    program is not only due to Vertera's own products, but also from all purchases of your structure on the platform.") ?>
                            </div>
                        </div>
                        <!-- left column : end -->
                        <!-- right column : begin -->
                        <div class="col-sm-6 col-xs-12">
                            <!-- image -->
                            <img src="/images/worldOfRetail/example-invite-platform-2.jpg" alt=""
                                 class="b-world-of-retail-invite-platform-item__img">
                        </div>
                        <!-- right column : end -->
                    </div>
                </div>
            </div>
            <!-- item : end -->
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="b-world-of-retail-invite-platform__connect-to-tech">
                            <!-- title with icon -->
                            <div class="b-world-of-retail-invite-platform__title-with-icon">
                                <?= Yii::t('world-of-retail', 'Join the world of successful people <br> and new technologies!') ?>
                            </div>
                            <!-- text small -->
                            <div class="b-world-of-retail-invite-platform__text-small">
                                <?= Yii::t('world-of-retail', 'This is the best business for earning on the Internet. <br> This is the future of direct sales and Internet 
      entrepreneurship. <br> This is your future and your children.') ?>
                            </div>
                            <!-- button -->
                            <a href="https://wor.ru/" class="b-button-green">
                                <?= Yii::t('world-of-retail', 'Go to the site') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- invite platform : end -->
    </div>
    <!-- WORLD OF RETAIL : end -->
</div>

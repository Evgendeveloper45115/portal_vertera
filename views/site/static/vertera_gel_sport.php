<?php
/**
 * @var $this \yii\web\View
 */

use yii\web\View;
use app\assets\AppAsset;
$this->title = 'Vertera Gel Sport';
$this->registerCssFile(Yii::getAlias('@web/css/market/style_second.css'), [
        'position' => View::POS_HEAD,
    'depends' => [AppAsset::class]
]);
?>
<link href="https://fonts.google.com/specimen/Roboto+Condensed" rel="stylesheet">
<section class="screen--01">
    <div class="container">
        <div class="row">
            <div class="first--header col-xs-12">
                <p class="header--before">Vertera Gel sport active</p>
                <h1>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'It destroys fat,<br> not the heart with the kidneys!'
                    ) ?>
                </h1>
                <p class="header--after"><?= Yii::t(
                        'vertera-gel-sport',
                        'Specialized food product <br> for nutrition of athletes and <br> active people in gel form'
                    ) ?>
                </p>

                <div class="box--coso box--coso-yellow feb--modern">
                    <div class="box--skew">
                        <div class="box--skew-nofill">
                            <?= Yii::t(
                                'vertera-gel-sport',
                                'Volume <br> in packing'
                            ) ?>
                        </div>
                        <div class="box--skew-fill">
                            600
                            <small> <?= Yii::t(
                                    'vertera-gel-sport',
                                    'g'
                                ) ?>
                            </small>
                        </div>
                    </div>

                    <div class="box--skew">
                        <div class="box--skew-nofill">
                            <?= Yii::t(
                                'vertera-gel-sport',
                                'Price<br> per packing'
                            ) ?>
                        </div>
                        <div class="box--skew-fill">
                            1040
                            <small class="rub"> <?= Yii::t(
                                    'vertera-gel-sport',
                                    '$'
                                ) ?></small>
                        </div>
                    </div>

                    <div class="box--skew">
                        <!-- <a href="#" class="btn btn-yellow">Купить</a> -->
                    </div>
                </div>

                <div class="gel"></div>

            </div>
        </div>
    </div>
</section>


<section class="screen--02 coso coso-yellow">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-sm-offset-2 tac">
                <p class="min-height"><a href="#screen-03"><img src="/images/vertera-gel-sport/f-ico--01.png"
                                                                alt=""></a></p>
                <h3><a href="#screen-03">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Fat reduction'
                        ) ?>
                    </a></h3>
            </div>
            <div class="col-sm-3 tac">
                <p class="min-height"><a href="#screen-04"><img src="/images/vertera-gel-sport/f-ico--02.png"
                                                                alt=""></a></p>
                <h3><a href="#screen-03">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Recovery'
                        ) ?>
                    </a></h3>
            </div>
            <div class="col-sm-3 tac">
                <p class="min-height"><a href="#screen-05"><img src="/images/vertera-gel-sport/f-ico--03.png"
                                                                alt=""></a></p>
                <h3><a href="#screen-03">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Training'
                        ) ?>
                    </a></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 tac">
                <p class="min-height"><a href="#screen-06"><img src="/images/vertera-gel-sport/f-ico--3.png"
                                                                alt=""></a></p>
                <h3><a href="#screen-03">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Daily'
                        ) ?>
                    </a></h3>
            </div>
            <div class="col-sm-3 tac">
                <p class="min-height"><a href="#screen-07"><img src="/images/vertera-gel-sport/f-ico--4.png"
                                                                alt=""></a></p>
                <h3><a href="#screen-03">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Endurance'
                        ) ?>
                    </a></h3>
            </div>
            <div class="col-sm-3 tac">
                <p class="min-height"><a href="#screen-08"><img src="/images/vertera-gel-sport/f-ico--05.png"
                                                                alt=""></a></p>
                <h3><a href="#screen-03">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Slowing of catabolism'
                        ) ?>
                    </a></h3>
            </div>
            <div class="col-sm-3 tac">
                <p class="min-height"><a href="#screen-09"><img src="/images/vertera-gel-sport/f-ico--06.png"
                                                                alt=""></a></p>
                <h3><a href="#screen-03">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Natural composition'
                        ) ?>
                    </a></h3>
            </div>
        </div>
    </div>
</section>


<section class="screen--03 text" id="screen-03">
    <div class="container">
        <div class="col-sm-6 col-md-5 col-lg-6 col-sm-offset-1">
            <h2 class="h--img"><img src="/images/vertera-gel-sport/icon_01.png">
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Decreased fat mass'
                ) ?>
            </h2>
            <p>
                <?= Yii::t(
                    'vertera-gel-sport',
                    'A full meal. You can easily <br> lose weight by replacing evening meals!'
                ) ?>
            </p>

            <ul class="ul--img">
                <li>
                    <img src="/images/vertera-gel-sport/ico_ves.png" alt="">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        '200 grams of product at a time'
                    ) ?>
                    <br>
                    <span>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            '1/3 cans'
                        ) ?>
                    </span>
                </li>
                <li>
                    <img src="/images/vertera-gel-sport/ico_ml.png" alt="">
                    <span>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'The servings contain'
                        ) ?>:
                    </span>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'protein - 13 grams'
                    ) ?>
                    <br>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Carbohydrates - 22 grams'
                    ) ?>
                    <br>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'algal lipids - 4 grams'
                    ) ?>
                </li>
                <li>
                    <img src="/images/vertera-gel-sport/ico_check.png" alt="">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'In a serving only<br><strong> 144 kcal</strong> <span class="font-rc">WITHOUT SUGAR</span>'
                    ) ?>
                </li>
            </ul>
        </div>
    </div>
</section>


<section class="screen--00 text text-white coso coso-white coso-white-2" id="screen-00">
    <div class="container">
        <div class="col-sm-12">
            <h4>
                <small>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Comparison by Three Positions'
                    ) ?>
                </small>
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Comparison by Three Positionseffectiveness, accessibility and simplicity <br> of the most known methods and methods of losing weight'
                ) ?>
            </h4>
            <div class="feb--col hidden-sm hidden-xs">
                <div class="feb--head feb--head-empty"></div>
                <div class="feb--line feb--line-empty">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Efficiency'
                    ) ?>
                </div>
                <div class="feb--line feb--line-empty">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Availability'
                    ) ?>
                </div>
                <div class="feb--line feb--line-empty">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Simplicity'
                    ) ?>
                </div>

            </div>

            <div class="feb--col">
                <div class="feb--head">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Individual <br>diets'
                    ) ?>
                </div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Efficiency'
                        ) ?>
                    </div>
                    <span class="status status-good"></span></div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Availability'
                        ) ?>
                    </div>
                    <span class="status status-bad"></span></div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Simplicity'
                        ) ?>
                    </div>
                    <span class="status status-bad"></span></div>
            </div>

            <div class="feb--col">
                <div class="feb--head">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Additives that<br> "reduce" the appetite'
                    ) ?>
                </div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Efficiency'
                        ) ?>
                    </div>
                    <span class="status status-bad"></span></div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Availability'
                        ) ?>
                    </div>
                    <span class="status status-good"></span></div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Simplicity'
                        ) ?>
                    </div>
                    <span class="status status-maybe"></span></div>
            </div>

            <div class="feb--col">
                <div class="feb--head">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Cleaning <br> fees'
                    ) ?>
                </div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Efficiency'
                        ) ?>
                    </div>
                    <span class="status status-bad"></span></div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Availability'
                        ) ?>
                    </div>
                    <span class="status status-good"></span></div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Simplicity'
                        ) ?>
                    </div>
                    <span class="status status-maybe"></span></div>
            </div>

            <div class="feb--col feb--col-yellow">
                <div class="feb--head">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Vertera gel sport <br>active'
                    ) ?>
                </div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Efficiency'
                        ) ?>
                    </div>
                    <span class="status status-good"></span></div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Availability'
                        ) ?>
                    </div>
                    <span class="status status-good"></span></div>
                <div class="feb--line">
                    <div class="visible-sm visible-xs">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Simplicity'
                        ) ?>
                    </div>
                    <span class="status status-good"></span></div>
            </div>

            <div class="status--map">
                <span class="status status-small status-good"></span>
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Excellent'
                ) ?>
                <span class="status status-small status-maybe"></span>
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Good'
                ) ?>
                <span class="status status-small status-bad"></span>
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Bad'
                ) ?>
            </div>

            <div class="col-md-9">
                <p>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Using Vertera Gel Sport Active, you can, besides losing weight, solve many other tasks to improve health. 
    The stronger health, the more likely you are not only to make your figure attractive, but also to keep it for 
    years to come!'
                    ) ?>
                </p>
            </div>

        </div>

    </div>
</section>


<div class="coso-continue"></div>

<section class="screen--05 text" id="screen-04">
    <div class="container">
        <div class="row">

            <div class="col-sm-6 col-md-5 col-lg-5 col-sm-offset-1">
                <h2 class="h--img"><img src="/images/vertera-gel-sport/icon_03.png">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Recovery'
                    ) ?>
                </h2>
                <p>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Effectively restores physical working capacity by replenishing muscle glycogen stores simultaneously with the
     improvement of trophic status of muscle tissue, activation of energy metabolism, detoxification'
                    ) ?>
                </p>
                <hr>
                <p class="ttu">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'An ideal combination of macro-, micro-ultra elements, vitamins, amino acids, carbohydrates and protein with the 
    highest biological value.'
                    ) ?>
                </p>
                <br>
                <p><img src="/images/vertera-gel-sport/chel_yel.png" class="img-responsive"></p>
            </div>
        </div>
    </div>
</section>

<section class="screen--06 text text-white coso coso-plus coso-plus-06" id="screen-05">
    <div class="container">
        <div class="col-sm-6">
            <p class="relative-top">
                <!-- <img src="/images/vertera-gel-sport/girl.png"> -->
            </p>
        </div>
        <div class="col-sm-5 col-sm-offset-1 mt50">
            <h2 class="h--img"><img src="/images/vertera-gel-sport/icon_04.png">
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Training'
                ) ?>
            </h2>
            <p>
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Ideal for feeding athletes during intensive training in addition to the basic diet in accordance with the program 
    developed for these sports.'
                ) ?>
            </p>
            <p><em> <?= Yii::t(
                        'vertera-gel-sport',
                        'Pea protein (protein) in sports nutrition is a highly purified isolate with a content of
                     protein 84-88%, which has a high digestibility of 98%. Pea protein is richer than others
                     vegetable proteins by protein content, which is the most important indicator for muscle growth,
                     since after water proteins are the second major component of the structure of muscle fibers.'
                    ) ?></em>
            </p>
            <ul class="ul--img">
                <li>
                    <img src="/images/vertera-gel-sport/ico_ml_green.png" alt="">
                    <span>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Instructions'
                        ) ?>
                    </span>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'When recruiting dry mass and fat burning, eat 200 g in the morning, 30 minutes before training and at night before 
    going to bed.'
                    ) ?>
                </li>
                <li class="nogreen">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'In case of problems with falling asleep, cancel the appointment before bedtime'
                    ) ?>
                </li>
            </ul>
        </div>
    </div>
</section>


<section class="screen--07 text coso coso-07" id="screen-06">
    <div class="container">
        <div class="row">

            <div class="col-sm-6 col-md-6 col-lg-5 col-sm-offset-1">
                <h2 class="h--img"><img src="/images/vertera-gel-sport/icon_05.png">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Daily'
                    ) ?>
                </h2>
                <p>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Regular use of Vertera Gel Sport Active neutralizes the consequences of the use of chemical elements in the diet of 
    athletes, provides the health of the liver, digestive tract, thyroid and cardiovascular system. Improves skin and hair.'
                    ) ?>
                </p>
                <p>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'An additional source of soluble dietary fiber (alginates), iodine in organic form and calcium.'
                    ) ?>
                </p>

                <hr>
                <ul class="ul--img">
                    <li>
                        <img src="/images/vertera-gel-sport/ico_banka_yel.png" alt="">
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Ready to use. Nourishes, restores and exerts a powerful detoxification effect!'
                        ) ?>
                    </li>
                </ul>

                <p><img src="/images/vertera-gel-sport/chel_100.png" class="img-responsive"></p>
            </div>
        </div>
    </div>
</section>


<section class="screen--08 text text-white  " id="screen-07">
    <div class="container">
        <div class="col-sm-6">
            <p class="relative-top">
                <!-- <img src="/images/vertera-gel-sport/girl.png"> -->
            </p>
        </div>
        <div class="col-sm-7 col-md-5 col-sm-offset-1 mt50">
            <h2 class="h--img h--img-middle"><img src="/images/vertera-gel-sport/icon_06.png">
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Endurance'
                ) ?>
            </h2>
            <p>
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Arginine is the most important amino acid for those who want to build muscle, because it releases growth hormone and synthesizes creatine together with other substances.'
                ) ?>
            </p>
            <p><em>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'The pea protein contains a large content of essential amino acids, which are inevitably lost during stress, and the
     muscles suffer from the shaking. Therefore pea protein is the best reductant. The greatest amount of it contains arginine,
      which in many other products of sports nutrition is usually very small.'
                    ) ?>
                </em></p>
            <ul class="ul--img">
                <li>
                    <img src="/images/vertera-gel-sport/ico_banka_green.png" alt="">
                    <span>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Arginine'
                        ) ?>
                    </span>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Arginine is contained in an amount of 8.7% per gram of protein, which is higher than in any other source of protein'
                    ) ?>
                </li>
                <li class="nogreen">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Arginine in combination with other amino acids gives an incredible synergy effect.'
                    ) ?>
                </li>
            </ul>
        </div>
    </div>
</section>


<section class="screen--17 text" id="screen-08">
    <div class="container">
        <div class="row">

            <div class="col-sm-9 col-md-5 col-lg-6 col-sm-offset-1">
                <h2 class="h--img"><img src="/images/vertera-gel-sport/icon_03.png">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Slowing of catabolism'
                    ) ?>
                </h2>
                <p>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'The use of Vertera gel Activ after training will minimize the process of catabolism (destruction) of muscles,
     thanks to the supply of the necessary quantity of high-quality proteins and carbohydrates to the body.'
                    ) ?>
                </p>
                <hr>
                <p class="ttu">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'The product is completely safe for the body and does not have such terrible side effects as chemical drugs 
    (destruction of the cardiovascular system, metabolic disorders, development of allergies, infertility, etc.'
                    ) ?>
                </p>
                <br>
                <ul class="ul--img">
                    <li>
                        <img src="/images/vertera-gel-sport/ico_ml.png" alt="">
                        <span>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'The servings contain'
                        ) ?>:
                    </span>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'protein - 13 grams'
                        ) ?>
                        <br>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Carbohydrates - 22 grams'
                        ) ?>
                        <br>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'algal lipids - 4 grams'
                        ) ?>

                    </li>
                </ul>
                <p><img src="/images/vertera-gel-sport/chel_100.png" class="img-responsive"></p>
            </div>
        </div>
    </div>
</section>
<div class="coso-continue feb"></div>


<section class="screen--04 text text-white coso coso-white coso-white-2" id="screen-09">
    <div class="container">
        <div class="col-sm-12 col-md-6">
            <p class="relative-top">
                <img src="/images/vertera-gel-sport/gel_01.png">
            </p>
        </div>
        <div class="col-md-5 col-md-offset-1 col-sm-12 mt50">
            <h2 class="h--img"><img src="/images/vertera-gel-sport/icon_02.png">
                <?= Yii::t(
                    'vertera-gel-sport',
                    '100% Natural composition'
                ) ?>
            </h2>
            <p>
                <?= Yii::t(
                    'vertera-gel-sport',
                    'The basis of the product is a specialized food product, a seaweed gel for dietary nutritional therapy Vertera, which
     contains a colossal set of micro, macro and ultra elements necessary for us as air.'
                ) ?>
            </p>
            <span>
                <?= Yii::t(
                    'vertera-gel-sport',
                    'Product structure'
                ) ?>
            </span>
            <ul>
                <li>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Water'
                    ) ?>
                </li>
                <li>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'blackcurrant juice concentrated'
                    ) ?>
                </li>
                <li>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'a specialized food product of dietary therapeutic nutrition from Vertera kelp <span> (drinking water, kelp, ancillary
 components, citric acid, calcium gluconate) </span>'
                    ) ?>
                </li>
                <li>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'pea protein'
                    ) ?>
                </li>
                <li>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'algal alginate'
                    ) ?>
                </li>
                <li>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Black currant powder, natural food flavoring'
                    ) ?>
                </li>
            </ul>
        </div>
    </div>
</section>


<section class="screen--20 coso coso-yellow">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="tac">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Why should athletes regularly<br> use Vertera Sport Gel Active?'
                    ) ?>
                </h2>
            </div>
            <div class="col-sm-6">
                <ul>
                    <li>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Balanced on BZHU and macro-, micro- and ultra-elements, a full-time one-time meal. 1 total serving = 144 kcal. 
        The product does not contain sugar and other derivatives (fructose, maltodextrin, etc.)'
                        ) ?>
                    </li>
                    <li>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Increase immunity sporstmena in conditions of training loads. It is expressed in the restoration to normal levels of
     concentrations of immunoglobulins A and G, as well as the complement component of C3.'
                        ) ?>
                    </li>
                    <li>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Regulation of fat, protein, carbohydrate and energy metabolism'
                        ) ?>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6">
                <ul>
                    <li></li>
                    <li>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Neutralization of consequences of the use of chemical components of sports nutrition.'
                        ) ?>
                    </li>
                    <li>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Normalization of functions and protection of the cardiovascular system.'
                        ) ?>
                    </li>
                    <li>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Protection and health of the liver'
                        ) ?>
                    </li>
                    <li>
                        <?= Yii::t(
                            'vertera-gel-sport',
                            'Health of the skin, hair, nails.'
                        ) ?>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12">
                <br>
                <p class="tac">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'As a result of the use of gel occurs'
                    ) ?>
                </p>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 tac">
                <p class="min-height"><img src="/images/vertera-gel-sport/f-ico--01.png" alt=""></p>
                <h3>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Daily'
                    ) ?>
                </h3>
            </div>
            <div class="col-sm-3 tac">
                <p class="min-height"><img src="/images/vertera-gel-sport/f-ico--02.png" alt=""></p>
                <h3>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Endurance'
                    ) ?>
                </h3>
            </div>
            <div class="col-sm-3 tac">
                <p class="min-height"><img src="/images/vertera-gel-sport/f-ico--05.png" alt=""></p>
                <h3>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Slowing of catabolism'
                    ) ?>
                </h3>
            </div>
            <div class="col-sm-3 tac">
                <p class="min-height"><img src="/images/vertera-gel-sport/f-ico--4.png" alt=""></p>
                <h3>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Natural composition'
                    ) ?>
                </h3>
            </div>
        </div>
        <div class="row tac mt20">
            <div class="col-md-offset-2 col-md-8">
                <p>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'We do not compete with manufacturers of sports nutrition, we neutralize the negative effects of sports nutrition and loads!'
                    ) ?>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="screen--10">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <img src="/images/vertera-gel-sport/gel_02.png" alt="" class="img-responsive">
            </div>
            <div class="col-sm-7 promo-2">
                <p class="header--before">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Vertera Gel sport active'
                    ) ?>
                    </p>
                <h1>
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Try it and see for yourself!'
                    ) ?>
                    <img src="/images/vertera-gel-sport/sportactivebr.png">
                </h1>
                <p class="header--after">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Specialized food <br> for athletes'
                    ) ?>
                </p>

                <div class="box--coso box--coso-green">
                    <div class="box--skew">
                        <div class="box--skew-nofill">
                            <?= Yii::t(
                                'vertera-gel-sport',
                                'Volume <br> in packing'
                            ) ?>
                        </div>
                        <div class="box--skew-fill">
                            600
                            <small>
                                <?= Yii::t(
                                    'vertera-gel-sport',
                                    'g'
                                ) ?>
                            </small>
                        </div>
                    </div>

                    <div class="box--skew">
                        <div class="box--skew-nofill">
                            <?= Yii::t(
                                'vertera-gel-sport',
                                'Price<br> per packing'
                            ) ?>
                        </div>
                        <div class="box--skew-fill">
                            1040
                            <small class="rub">
                                <?= Yii::t(
                                    'vertera-gel-sport',
                                    '$'
                                ) ?>
                            </small>
                        </div>
                    </div>

                    <div class="box--skew">
                        <!-- <a href="#" class="btn btn-green">Купить</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="screen--11 text coso coso-blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="tac">
                    <?= Yii::t(
                        'vertera-gel-sport',
                        'Certificates'
                    ) ?>
                </h2>

                <div id="sertif" class="carousel slide" data-ride="carousel">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="col-md-3 col-sm-6">
                                <p><img src="/images/vertera-gel-sport/s-1.png"></p>
                                <p>
                                    <?= Yii::t(
                                        'vertera-gel-sport',
                                        'Customs Union of the Republic of Belarus, the Republic of Kazakhstan and the Russian Federation'
                                    ) ?>
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <p><img src="/images/vertera-gel-sport/s-2.png"></p>
                                <p>
                                    <?= Yii::t(
                                        'vertera-gel-sport',
                                        'Certificate of compliance with the requirements of GOST R ISO 22000-2007 (ISO 22000-2005)'
                                    ) ?>
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <p><img src="/images/vertera-gel-sport/s-3.png"></p>
                                <p>
                                    <?= Yii::t(
                                        'vertera-gel-sport',
                                        'Certificate of state registration'
                                    ) ?>
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <p><img src="/images/vertera-gel-sport/s-4.png"></p>
                                <p>
                                    <?= Yii::t(
                                        'vertera-gel-sport',
                                        'The protocol of testing the gel sample for the amino acid composition'
                                    ) ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
/**
 * @var $this \yii\web\View
 */
$this->title = Yii::t('layout', 'Business opportunity');
?>
<div class="b-capabilities js-capabilities">
    <!-- promo : begin -->
    <div class="b-capabilities__promo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- logo -->
                    <div class="b-capabilities-promo__logo"></div>
                    <!-- title -->
                    <div class="b-capabilities-promo__title"><?= Yii::t('business-vertera', 'Welcome to the team') ?>
                        <br><?= Yii::t('business-vertera', 'healthy, successful and happy') ?>
                        <br><?= Yii::t('business-vertera', 'people') ?>
                    </div>
                    <!-- text -->
                    <p class="b-capabilities-promo__text">
                        <?= Yii::t('business-vertera', 'We understand what is important to every person, and we offer unique
products for quality life and successful business. With Vertera, you can easily change your life for the better!') ?>
                    </p>
                    <!-- button -->
                    <a href="https://os.verteraorganic.com/reg/"
                       class="b-button-green"
                       data-toggle="modal"
                       data-target="#consent-form-modal">
                        <?= Yii::t('business-vertera', 'Become a partner') ?></a>
                    <div class="b-capabilities-img-mouse">
                        <img src="/images/static-business/mouse-scroll.png" alt="">
                    </div>
                </div>
                <!-- promo : end -->
                <!-- parallax block : begin -->
                <div class="b-parallax b-parallax-capabilities">
                    <div class="b-parallax-capabilities__animate-el-1 js-parallax-el-1" style="top: 346px;"></div>
                    <div class="b-parallax-capabilities__animate-el-2 js-parallax-el-2" style="top: 645px;"></div>
                    <div class="b-parallax-capabilities__animate-el-3 js-parallax-el-3" style="top: 1716px;"></div>
                    <div class="b-parallax-capabilities__animate-el-4 js-parallax-el-4" style="top: 1933px;"></div>
                    <div class="b-parallax-capabilities__animate-el-5 js-parallax-el-5" style="bottom: 3413px;"></div>
                    <div class="b-parallax-capabilities__animate-el-6 js-parallax-el-6" style="bottom: 3972px;"></div>
                    <div class="b-parallax-capabilities__animate-el-7 js-parallax-el-7" style="top: 431px;"></div>
                    <div class="b-parallax-capabilities__animate-el-8 js-parallax-el-8" style="top: 604px;"></div>
                    <div class="b-parallax-capabilities__animate-el-9 js-parallax-el-9" style="top: 732px;"></div>
                    <div class="b-parallax-capabilities__animate-el-10 js-parallax-el-10" style="top: 647px;"></div>
                    <div class="b-parallax-capabilities__animate-el-11 js-parallax-el-11" style="bottom: 3888px;"></div>
                    <div class="b-parallax-capabilities__animate-el-12 js-parallax-el-12" style="bottom: 3435px;"></div>
                </div>
                <!-- parallax block : end -->
            </div>
        </div>
    </div>
    <!-- opportunities : begin -->
    <div class="b-capabilities__opportunities">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- title -->
                    <div class="b-capabilities-opportunities__title"><?= Yii::t('business-vertera', "It's time to start embodying <br> a life of cherished dreams!") ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-12">
                    <!-- item : begin -->
                    <div class="b-capabilities-opportunities__item">
                        <img src="/images/static-business/example-opportunities-1.png" alt=""
                             class="b-capabilities-opportunities__img">
                        <p class="b-capabilities-opportunities__text">
                            <?= Yii::t('business-vertera', 'Buy a new <br> car') ?></p>
                    </div>
                    <!-- item : end -->
                </div>
                <div class="col-sm-3 col-xs-12">
                    <!-- item : begin -->
                    <div class="b-capabilities-opportunities__item">
                        <img src="/images/static-business/example-opportunities-2.png" alt=""
                             class="b-capabilities-opportunities__img">
                        <p class="b-capabilities-opportunities__text">
                            <?= Yii::t('business-vertera', 'Refresh the atmosphere <br> in the house') ?></p>
                    </div>
                    <!-- item : end -->
                </div>
                <div class="col-sm-3 col-xs-12">
                    <!-- item : begin -->
                    <div class="b-capabilities-opportunities__item">
                        <img src="/images/static-business/example-opportunities-3.png" alt=""
                             class="b-capabilities-opportunities__img">
                        <p class="b-capabilities-opportunities__text">
                            <?= Yii::t('business-vertera', 'Pay for education for <br> yourself and your children') ?></p>
                    </div>
                    <!-- item : end -->
                </div>
                <div class="col-sm-3 col-xs-12">
                    <!-- item : begin -->
                    <div class="b-capabilities-opportunities__item">
                        <img src="/images/static-business/example-opportunities-4.png" alt=""
                             class="b-capabilities-opportunities__img">
                        <p class="b-capabilities-opportunities__text">
                            <?= Yii::t('business-vertera', 'Embark on a journey <br> around the world') ?></p>
                    </div>
                    <!-- item : end -->
                </div>
            </div>
        </div>
    </div>
    <!-- opportunities : end -->
    <!-- cash income : begin -->
    <div class="b-capabilities__cash-income">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- title -->
                    <div class="b-capabilities-cash-income__title">
                        <?= Yii::t('business-vertera', 'Your opportunities are limited only by <br> your ambitions, desires and priorities!') ?>
                    </div>
                    <!-- text -->
                    <p class="b-capabilities-cash-income__text">
                        <?= Yii::t('business-vertera', 'You determine how long you are willing to invest <br> in your business and how much to earn!') ?>
                    </p>
                    <!-- title small -->

                    <div class="b-capabilities-cash-income__title-small">
                        <?= Yii::t('business-vertera', 'Choose for yourself a comfortable regime for <br> development and cooperation with the Company.') ?>
                    </div>
                </div>
                <!-- parallax block : begin -->
                <div class="b-parallax b-parallax-capabilities">
                    <div class="b-parallax-capabilities__animate-el-1 js-parallax-el-1" style="top: 346px;"></div>
                    <div class="b-parallax-capabilities__animate-el-2 js-parallax-el-2" style="top: 645px;"></div>
                    <div class="b-parallax-capabilities__animate-el-3 js-parallax-el-3" style="top: 1716px;"></div>
                    <div class="b-parallax-capabilities__animate-el-4 js-parallax-el-4" style="top: 1933px;"></div>
                    <div class="b-parallax-capabilities__animate-el-5 js-parallax-el-5" style="bottom: 3413px;"></div>
                    <div class="b-parallax-capabilities__animate-el-6 js-parallax-el-6" style="bottom: 3972px;"></div>
                    <div class="b-parallax-capabilities__animate-el-7 js-parallax-el-7" style="top: 431px;"></div>
                    <div class="b-parallax-capabilities__animate-el-8 js-parallax-el-8" style="top: 604px;"></div>
                    <div class="b-parallax-capabilities__animate-el-9 js-parallax-el-9" style="top: 732px;"></div>
                    <div class="b-parallax-capabilities__animate-el-10 js-parallax-el-10" style="top: 647px;"></div>
                    <div class="b-parallax-capabilities__animate-el-11 js-parallax-el-11" style="bottom: 3888px;"></div>
                    <div class="b-parallax-capabilities__animate-el-12 js-parallax-el-12" style="bottom: 3435px;"></div>
                </div>
                <div class="b-parallax b-parallax-capabilities">
                    <div class="b-parallax-capabilities__animate-el-13"></div>
                    <div class="b-parallax-capabilities__animate-el-14"></div>
                </div>
                <!-- parallax block : end -->
            </div>
            <div class="row">
                <div class="b-capabilities-cash-income__wrapper">
                    <div class="col-sm-4 col-xs-12">
                        <!-- item : begin -->
                        <div class="b-capabilities-cash-income__item">
                            <!-- subtitle -->
                            <div class="b-capabilities-cash-income-item__subtitle"
                                 data-mh="b-capabilities-cash-income-item-subtitle-group" style="height: 72px;">
                                <span>
                                    <?= Yii::t('business-vertera', 'Additional income') ?>
                                </span>
                            </div>
                            <!-- price -->
                            <div class="b-capabilities-cash-income-item__price">
                                <?= Yii::t('business-vertera', 'up to') ?>
                                55000<span><?= Yii::t('business-vertera', '$') ?></span></div>
                            <!-- text -->
                            <p class="b-capabilities-cash-income-item__text"
                               data-mh="b-capabilities-cash-income-item-text-group" style="height: 158px;">
                                <?= Yii::t('business-vertera', 'You are interested in additional income, provided that you can give this business several hours a week') ?>
                            </p>
                            <!-- text selected -->
                            <p class="b-capabilities-cash-income-item__text-selected"
                               data-mh="b-capabilities-cash-income-item-text-selected-group" style="height: 256px;">
                                <?= Yii::t('business-vertera', 'You use the product of the Company, tell about it to your relatives, friends and relatives. Thus you have your own group of regular customers. At the same time, you spend several hours a week motivating the most active consumers from this group, who also work with their closest associates.') ?>
                            </p>
                            <!-- button -->
                            <a href="http://vertera.university/" class="b-button-green" target="_blank">
                                <?= Yii::t('business-vertera', 'Start training') ?></a>
                        </div>
                        <!-- item : end -->
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <!-- item : begin -->

                        <div class="b-capabilities-cash-income__item b-capabilities-cash-income__item-selected col-2">
                            <!-- subtitle -->
                            <div class="b-capabilities-cash-income-item__subtitle"
                                 data-mh="b-capabilities-cash-income-item-subtitle-group" style="height: 72px;">
                                <span>
                                    <?= Yii::t('business-vertera', 'Basic <br> income') ?></span>
                            </div>
                            <!-- price -->
                            <div class="b-capabilities-cash-income-item__price">55 000 <span>$</span> – 250 000
                                <span>$</span>
                            </div>
                            <!-- text -->
                            <p class="b-capabilities-cash-income-item__text"
                               data-mh="b-capabilities-cash-income-item-text-group" style="height: 158px;">
                                <?= Yii::t('business-vertera', 'You want to do business with the company as your main job and receive a corresponding income of 55,000 rubles a month or more') ?>
                            </p>
                            <!-- text selected -->
                            <p class="b-capabilities-cash-income-item__text-selected"
                               data-mh="b-capabilities-cash-income-item-text-selected-group" style="height: 256px;">
                                <?= Yii::t('business-vertera', "You decided to devote yourself seriously to cooperation with the company, which means it's time to discover the main part of the company's marketing plan. Your goal is to realize yourself as a leader, talented, purposeful and ambitious person. And most importantly, help in this and your team-mates.") ?>
                                <!-- button -->
                            </p>
                            <a href="http://vertera.university/" class="b-button-green" target="_blank">
                                <?= Yii::t('business-vertera', 'Start training') ?></a>
                        </div>
                        <!-- item : end -->
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <!-- item : begin -->
                        <div class="b-capabilities-cash-income__item">
                            <!-- subtitle -->
                            <div class="b-capabilities-cash-income-item__subtitle"
                                 data-mh="b-capabilities-cash-income-item-subtitle-group" style="height: 72px;">
                                <span><?= Yii::t('business-vertera', 'Stable <br> international <br> business') ?></span>
                            </div>
                            <!-- price -->
                            <div class="b-capabilities-cash-income-item__price"><?= Yii::t('business-vertera', 'of') ?>
                                250 000 <span></span><?= Yii::t('business-vertera', '$') ?></div>
                            <!-- text -->
                            <p class="b-capabilities-cash-income-item__text"
                               data-mh="b-capabilities-cash-income-item-text-group" style="height: 158px;">
                                <?= Yii::t('business-vertera', 'You want to invest your time and energy in business with the company, achieve financial independence, determine a stable and successful future for many years for you and your family') ?>
                            </p>
                            <!-- text selected -->
                            <p class="b-capabilities-cash-income-item__text-selected"
                               data-mh="b-capabilities-cash-income-item-text-selected-group" style="height: 256px;">
                                <?= Yii::t('business-vertera', 'A huge number of people of very different ages and professions have already reached millions of checks! They did it, you will get it! We are ready to offer you a working plan, how to create your business in order to realize all your dreams, be successful, beautiful, healthy and abundant. Take your chance right now! The exact choice means success!') ?>
                            </p>
                            <!-- button -->
                            <a href="http://vertera.university/" class="b-button-green" target="_blank">
                                <?= Yii::t('business-vertera', 'Start training') ?></a>
                        </div>
                        <!-- item : end -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <!-- icon -->
                    <div class="b-capabilities-cash-income__icon"></div>
                    <!-- title small -->
                    <div class="b-capabilities-cash-income__title-small-margin-small">
                        <?= Yii::t('business-vertera', 'Take the first step to <br> a successful future today!') ?>
                    </div>
                    <!-- text -->
                    <p class="b-capabilities-cash-income__text">
                        <?= Yii::t('business-vertera', 'Start your business with the step-by-step tutorial <br> system "Vertera University"') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- cash income : end -->
    <!-- advantages : begin -->
    <div class="b-capabilities__advantages">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- title -->
                    <div class="b-capabilities-advantages__title">

                        <?= Yii::t('business-vertera', 'Benefits of business <br>  with Vertera') ?></div>
                </div>
            </div>
            <div class="b-capabilities-advantages__wrapper">
                <div class="row row-1">
                    <!-- item -->
                    <div class="b-capabilities-advantages__item">
                        <div class="b-capabilities-advantages-item__icon-first"></div>
                        <p class="b-capabilities-advantages-item__text">
                            <?= Yii::t('business-vertera', 'Income from three <br> sources') ?></p>
                        <!-- additional information : begin -->
                        <div class="b-capabilities-advantages-item__add-info">
                            <div class="b-capabilities-advantages-item__icon-first"></div>
                            <p class="b-capabilities-advantages-item__text">
                                <?= Yii::t('business-vertera', 'Income from three <br> sources') ?></p>
                            <p class="b-capabilities-advantages-item__descr">
                                <?= Yii::t('business-vertera', 'You receive rewards not only with the sale and promotion of Vertera products, but also with the distribution of customers to you from the advertising campaign of products in the media, the Internet provided by Hermes promontories, and also through the clients of the World of Trade platform.') ?>
                            </p>
                        </div>
                        <!-- additional information : end -->
                    </div>

                    <!-- item -->
                    <div class="b-capabilities-advantages__item">
                        <div class="b-capabilities-advantages-item__icon-second"></div>
                        <p class="b-capabilities-advantages-item__text">
                            <?= Yii::t('business-vertera', 'Money immediately') ?></p>
                        <!-- additional information : begin -->
                        <div class="b-capabilities-advantages-item__add-info">
                            <div class="b-capabilities-advantages-item__icon-second"></div>
                            <p class="b-capabilities-advantages-item__text">
                                <?= Yii::t('business-vertera', 'Money immediately') ?></p>
                            <p class="b-capabilities-advantages-item__descr">
                                <?= Yii::t('business-vertera', 'Start earning immediately! Instant accruals on the Partner bonus immediately upon payment of the product by the partner.') ?>
                            </p>
                        </div>
                        <!-- additional information : end -->
                    </div>

                    <!-- item -->
                    <div class="b-capabilities-advantages__item">
                        <div class="b-capabilities-advantages-item__icon-third"></div>
                        <p class="b-capabilities-advantages-item__text">
                            <?= Yii::t('business-vertera', 'Loyalty <br> program') ?></p>
                        <!-- additional information : begin -->
                        <div class="b-capabilities-advantages-item__add-info">
                            <div class="b-capabilities-advantages-item__icon-third"></div>
                            <p class="b-capabilities-advantages-item__text">
                                <?= Yii::t('business-vertera', 'Loyalty <br> program') ?></p>
                            <p class="b-capabilities-advantages-item__descr">
                                <?= Yii::t('business-vertera', 'The returnable commodity discount is 25% of the value of personal volume for the reporting period. You can collect the accrued amount at the trusted Service Center of the Company in the next calendar month.') ?>
                            </p>
                        </div>
                        <!-- additional information : end -->
                    </div>

                    <!-- item -->
                    <div class="b-capabilities-advantages__item">
                        <div class="b-capabilities-advantages-item__icon-fourth"></div>
                        <p class="b-capabilities-advantages-item__text">
                            <?= Yii::t('business-vertera', 'Affiliate <br> compensation') ?></p>
                        <!-- additional information : begin -->
                        <div class="b-capabilities-advantages-item__add-info">
                            <div class="b-capabilities-advantages-item__icon-fourth"></div>
                            <p class="b-capabilities-advantages-item__text">
                                <?= Yii::t('business-vertera', 'Affiliate <br> compensation') ?></p>
                            <p class="b-capabilities-advantages-item__descr">
                                <?= Yii::t('business-vertera', 'Even minimal personal activity is enough to start receiving the Partner Bonus from 5 levels (20%, 5%, 5%, 5%, 10%). You need to make a minimum purchase of only 30 points!') ?>
                            </p>
                        </div>
                        <!-- additional information : end -->
                    </div>

                    <!-- item -->
                    <div class="b-capabilities-advantages__item">
                        <div class="b-capabilities-advantages-item__icon-fifth"></div>
                        <p class="b-capabilities-advantages-item__text">
                            <?= Yii::t('business-vertera', 'Leader\'s <br> reward') ?></p>
                        <!-- additional information : begin -->
                        <div class="b-capabilities-advantages-item__add-info">
                            <div class="b-capabilities-advantages-item__icon-fifth"></div>
                            <p class="b-capabilities-advantages-item__text">
                                <?= Yii::t('business-vertera', 'Leader\'s <br> reward') ?></p>
                            <p class="b-capabilities-advantages-item__descr">
                                <?= Yii::t('business-vertera', "To receive the Leader's Bonus from 8 levels: 1%, 5%, 5%, 6%, 6%, 7%, 7%, 8%, as well as all marketing plan privileges, you need to make a minimum purchase of everything from 60 points to receive a reward or 120 points per month for participation in the automotive and housing programs, travel program.") ?>
                            </p>
                        </div>
                        <!-- additional information : end -->
                    </div>
                </div>
                <div class="row row-2">
                    <!-- item -->
                    <div class="b-capabilities-advantages__item">
                        <div class="b-capabilities-advantages-item__icon-sixth"></div>
                        <p class="b-capabilities-advantages-item__text">
                            <?= Yii::t('business-vertera', 'Unlimited <br> payments') ?></p>
                        <!-- additional information : begin -->
                        <div class="b-capabilities-advantages-item__add-info">
                            <div class="b-capabilities-advantages-item__icon-sixth"></div>
                            <p class="b-capabilities-advantages-item__text">
                                <?= Yii::t('business-vertera', 'Unlimited <br> payments') ?></p>
                            <p class="b-capabilities-advantages-item__descr">
                                <?= Yii::t('business-vertera', "On the leader's ranks you receive income not only from your structure, but also from the entire international turnover of the company.") ?>
                            </p>
                        </div>
                        <!-- additional information : end -->
                    </div>

                    <!-- item -->
                    <div class="b-capabilities-advantages__item">
                        <div class="b-capabilities-advantages-item__icon-seventh"></div>
                        <p class="b-capabilities-advantages-item__text"><?= Yii::t('business-vertera', 'Dynamic<br>level<br>compression') ?></p>
                        <!-- additional information : begin -->
                        <div class="b-capabilities-advantages-item__add-info">
                            <div class="b-capabilities-advantages-item__icon-seventh"></div>
                            <p class="b-capabilities-advantages-item__text"><?= Yii::t('business-vertera', 'Dynamic<br>level<br>compression') ?></p>
                            <p class="b-capabilities-advantages-item__descr">
                                <?= Yii::t(
                                    'business-vertera',
                                    'In the company Vertera there is no interruption in the payment of bonuses! The action of double dynamic compression is a significant additional income!'
                                ) ?>
                            </p>
                        </div>
                        <!-- additional information : end -->
                    </div>

                    <!-- item -->
                    <div class="b-capabilities-advantages__item">
                        <div class="b-capabilities-advantages-item__icon-eighth"></div>
                        <p class="b-capabilities-advantages-item__text"><?= Yii::t('business-vertera', 'Special<br>projects<br>for partners') ?></p>
                        <!-- additional information : begin -->
                        <div class="b-capabilities-advantages-item__add-info">
                            <div class="b-capabilities-advantages-item__icon-eighth"></div>
                            <p class="b-capabilities-advantages-item__text"><?= Yii::t('business-vertera', 'Special<br>projects<br>for partners') ?></p>
                            <p class="b-capabilities-advantages-item__descr">
                                • <?= Yii::t('business-vertera', 'Automotive and Residential Program') ?>;<br>
                                • <?= Yii::t('business-vertera', 'Traveling at company expense') ?>;<br>
                                • <?= Yii::t('business-vertera', 'Learning system "University of Vertera" - a clear step-by-step action plan to achieve success') ?>
                                ;<br>
                                • <?= Yii::t('business-vertera', 'The "World of Trade" platform is a global project with a huge perspective for the next few decades in the field of e-commerce') ?>
                                .
                            </p>
                        </div>
                        <!-- additional information : end -->
                    </div>
                    <!-- item -->
                    <div class="b-capabilities-advantages__item">
                        <div class="b-capabilities-advantages-item__icon-ninth"></div>
                        <p class="b-capabilities-advantages-item__text"><?= Yii::t('business-vertera', 'Family<br>Business') ?></p>
                        <!-- additional information : begin -->
                        <div class="b-capabilities-advantages-item__add-info">
                            <div class="b-capabilities-advantages-item__icon-ninth"></div>
                            <p class="b-capabilities-advantages-item__text"><?= Yii::t('business-vertera', 'Family<br>Business') ?></p>
                            <p class="b-capabilities-advantages-item__descr"><?= Yii::t('business-vertera', 'Receiving residual income and building a family business system for inheritance ID in the Company') ?>
                                .
                            </p>
                        </div>
                        <!-- additional information : end -->
                    </div>
                </div>
            </div>
            <div class="row row-3">
                <div class="col-xs-12">
                    <!-- title small -->
                    <div class="b-capabilities-advantages__title-small">
                        <?= Yii::t('business-vertera', 'You are ready to create your own business') ?>?
                    </div>
                    <!-- button -->
                    <div>
                        <a href="https://os.verteraorganic.com/reg/"
                           data-toggle="modal"
                           data-target="#consent-form-modal"
                           class="b-button-green"><?= Yii::t('business-vertera', 'Create a business') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
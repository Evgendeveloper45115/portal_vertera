<?php
/**
 * @var $this yii\web\View
 * @var $events Business[]
 * @var $news News[]
 * @var $products Product[]
 * @var $faq_all Faq[]
 */

use yii\helpers\Url;
use app\assets\AppAsset;
use app\models\{
    ObjectFile,
    Product,
    ProductSubCategory,
    ProductHasSubCategory,
    Business,
    Faq,
    News
};

$this->title = Yii::t('labels', 'Official website of company Vertera');
$fmt = Yii::$app->formatter;

$this->registerJsFile(Yii::getAlias('@web/js/vertera-project.min.js'), [
    'depends' => [AppAsset::class]
]);

$this->registerCssFile(Yii::getAlias('@web/css/slick.css'), [
    'depends' => [AppAsset::class]
]);
$this->registerJsFile(Yii::getAlias('@web/js/slick.min.js'), [
    'depends' => [AppAsset::class]
]);

$this->registerCssFile(Yii::getAlias('@web/css/highslide.css'), [
    'depends' => [AppAsset::class]
]);
$this->registerJsFile(Yii::getAlias('@web/js/highslide-full.min.js'), [
    'depends' => [AppAsset::class]
]);
$this->registerCssFile(Yii::getAlias('@web/css/owl.carousel.css'), [
    'depends' => [AppAsset::class]
]);
$this->registerJsFile(Yii::getAlias('@web/js/owl.carousel.min.js'), [
    'depends' => [AppAsset::class]
]);
$this->registerJs("hs.graphicsDir = '/images/highslide/';
hs.align = 'center';
hs.transitions = ['expand', 'crossfade'];
hs.wrapperClassName = 'dark borderless floating-caption';
hs.fadeInOut = true;
hs.dimmingOpacity = .75;

// Add the controlbar
if (hs.addSlideshow) hs.addSlideshow({
    //slideshowGroup: 'group1',
    interval: 5000,
    repeat: false,
    useControls: false,
    fixedControls: 'fit',
    overlayOptions: {
        opacity: .6,
        position: 'bottom center',
        hideOnMouseOut: true
    }
});

var owl = $(\".b-owl-slider-mobile\");
owl.owlCarousel({
    items : 1, //10 items above 1000px browser width
    itemsDesktop : [1000,1], //5 items between 1000px and 901px
    itemsDesktopSmall : [900,1], // 3 items betweem 900px and 601px
    itemsTablet: [600,1], //2 items between 600 and 0;
    itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
});
$('.b-question-expert-mobile-btn').click(function(){

    if($(this).attr('status') == 'close') {
        $(this).parent().find('.b-question-expert-mobile-item__text-descr.-invisible').show();
        $(this).attr('status','open').removeClass('plus').addClass('minus');
    } else if($(this).attr('status') == 'open') {
        $(this).parent().find('.b-question-expert-mobile-item__text-descr.-invisible').hide();
        $(this).attr('status','close').removeClass('minus').addClass('plus');
    }

    return false;
});");
?>
<!-- NEWS LAST MOBILE : begin -->
<div class="container b-owl-slider-mobile-wrap b-news-last-mobile">
    <div class="b-owl-slider-mobile">

        <?php foreach ($news as $n) { ?>
            <div class="item">
                <div class="b-news-last-mobile-title">Новости</div>
                <?php if ($path = ObjectFile::getThumbnail($n, 'image', [300, 103])) { ?>
                    <div class="b-news-last-mobile-img">
                        <a href="<?= Url::to(['/news/view', 'id' => $n->id]) ?>" class="b-news-feed__img">
                            <img src="<?= $path ?>" alt="">
                        </a>
                    </div>
                <?php } ?>
                <div class="b-news-last-mobile-date"><?= $fmt->asDate($n->start_date, 'long') ?></div>
                <div class="b-news-last-mobile-subtitle">
                    <a href="<?= Url::to(['/news/view', 'id' => $n->id]) ?>"><?= $n->title ?></a>
                </div>
                <p class="b-news-last-mobile-text"><?= $n->sub_description ?></p>
            </div>
        <?php } ?>

    </div>
    <div class="b-news-last-mobile-more">
        <a href="/news/">Все новости</a>
    </div>
</div>
<!-- NEWS LAST MOBILE : end -->

<!-- NEWS LAST MOBILE : begin -->
<div class="container b-owl-slider-mobile-wrap b-news-last-mobile">
    <div class="b-owl-slider-mobile">

        <?php foreach ($events as $event) { ?>
            <div class="item">
                <div class="b-news-last-mobile-title">Мероприятия</div>
                <?php if ($path = ObjectFile::getThumbnail($event, 'image', [300, 103])) { ?>
                    <div class="b-news-last-mobile-img">
                        <a href="<?= Url::to(['/news/view', 'id' => $event->id]) ?>" class="b-news-feed__img">
                            <img src="<?= $path ?>" alt="">
                        </a>
                    </div>
                <?php } ?>
                <div class="b-news-last-mobile-date"><?= $fmt->asDate($event->start_date, 'long') ?></div>
                <div class="b-news-last-mobile-subtitle">
                    <a href="<?= Url::to(['/news/view', 'id' => $event->id]) ?>"><?= $event->title ?></a>
                </div>
                <p class="b-news-last-mobile-text"><?= $event->sub_description ?></p>
            </div>
        <?php } ?>

    </div>
    <div class="b-news-last-mobile-more">
        <a href="/news/">Все новости</a>
    </div>
</div>
<!-- NEWS LAST MOBILE : end -->

<!-- DIRECTION MOBILE : begin -->
<div class="container b-owl-slider-mobile-wrap b-direction-mobile">
    <div class="b-owl-slider-mobile">
        <div class="item">
            <a href="<?= Url::to(['/category/view', 'id' => 1, 'sub_id' => 14]) ?>">
                <!-- img -->
                <img src="/images/examples/example-directions-2.jpg" alt="" class="b-direction-mobile-img">
                <div class="b-direction-mobile-body b-directions-item__slimming">
                    <!-- icon -->
                    <div class="b-direction-mobile-icon"></div>
                    <!-- title -->
                    <div class="b-direction-mobile-title"><?= Yii::t('index', 'Weight Loss') ?></div>
                    <!-- text -->
                    <div class="b-direction-mobile-text" style="color: rgba(255, 255, 255, 255);">
                        <p><?= Yii::t('index', 'Lightness and excellent mood without extra calories') ?></p>
                    </div>
                </div>
            </a>
        </div>
        <div class="item">
            <a href="<?= Url::to(['/category/view', 'id' => 1]) ?>">
                <!-- img -->
                <img src="/images/examples/example-directions-1.jpg" alt="" class="b-direction-mobile-img">
                <div class="b-direction-mobile-body b-direction-mobile-health">
                    <!-- icon -->
                    <div class="b-direction-mobile-icon"></div>
                    <!-- title -->
                    <div class="b-direction-mobile-title"><?= Yii::t('index', 'Health') ?></div>
                    <!-- text -->
                    <div class="b-direction-mobile-text" style="color: rgba(255, 255, 255, 255);">
                        <p><?= Yii::t('index', 'Proper nutrition for good health each day') ?></p>
                    </div>
                </div>
            </a>
        </div>
        <div class="item">
            <a href="<?= Url::to(['/category/view', 'id' => 3]) ?>">
                <!-- img -->
                <img src="/images/examples/example-directions-3.jpg" alt="" class="b-direction-mobile-img">
                <div class="b-direction-mobile-body b-directions-item__slimming">
                    <!-- icon -->
                    <div class="b-direction-mobile-icon"></div>
                    <!-- title -->
                    <div class="b-direction-mobile-title"><?= Yii::t('index', 'Sport') ?></div>
                    <!-- text -->
                    <div class="b-direction-mobile-text" style="color: rgba(255, 255, 255, 255);">
                        <p><?= Yii::t('index', 'Beyond energy for new records') ?></p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- DIRECTION MOBILE : end -->

<div class="b-news-feed">
    <div class="container">
        <div class="row row-1">
            <div class="col-sm-6 col-xs-12">
                <div class="row row-1-1">
                    <!-- title -->
                    <div class="b-news-feed__title"><?= Yii::t('index', 'News') ?></div>
                    <!-- all news link -->
                    <div class="b-news-feed__all-news-link">
                        <a href="<?= Url::to(['/news']) ?>"><?= Yii::t('index', 'All news') ?></a>
                    </div>
                </div>
                <div class="row row-1-2">
                    <?php foreach ($news as $n) { ?>
                        <div class="col-sm-6 col-xs-12">
                            <!-- item : begin -->
                            <div class="b-news-feed__item">
                                <!-- image -->
                                <?php if ($path = ObjectFile::getThumbnail($n, 'image', [490, 170])) { ?>
                                    <a href="<?= Url::to(['/news/view', 'id' => $n->id]) ?>" class="b-news-feed__img">
                                        <img src="<?= $path ?>" alt="">
                                    </a>
                                <?php } ?>
                                <!-- date -->
                                <div class="b-news-feed__date"><?= $fmt->asDate($n->start_date, 'long') ?></div>
                                <!-- subtitle -->
                                <div class="b-news-feed__subtitle">
                                    <a href="<?= Url::to(['/news/view', 'id' => $n->id]) ?>"><?= $n->title ?></a>
                                </div>
                                <!-- text -->
                                <p class="b-news-feed__text"><?= $n->sub_description ?></p>
                            </div>
                            <!-- item : end -->
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="row row-1-3">
                    <!-- title -->
                    <div class="b-news-feed__title"><?= Yii::t('index', 'Events') ?></div>
                    <!-- all news link -->
                    <div class="b-news-feed__all-news-link">
                        <a href="<?= Url::to(['/event']) ?>"><?= Yii::t('index', 'Events') ?></a>
                    </div>
                </div>
                <div class="row row-1-4">
                    <?php foreach ($events as $event) { ?>
                        <div class="col-sm-6 col-xs-12">
                            <!-- item : begin -->
                            <div class="b-news-feed__item">
                                <!-- image -->
                                <?php if ($path = ObjectFile::getThumbnail($event, 'image', [490, 170])) { ?>
                                    <a href="<?= Url::to(['/event/view', 'id' => $event->id]) ?>"
                                       class="b-news-feed__img">
                                        <img src="<?= $path ?>" alt="">
                                    </a>
                                <?php } ?>
                                <!-- date -->
                                <div class="b-news-feed__date"><?= $fmt->asDate($event->start_date, 'long') ?></div>
                                <!-- subtitle -->
                                <div class="b-news-feed__subtitle">
                                    <a href="<?= Url::to(['/news/view', 'id' => $event->id]) ?>"><?= $event->title ?></a>
                                </div>
                                <!-- text -->
                                <p class="b-news-feed__text"><?= $event->sub_description ?></p>
                            </div>
                            <!-- item : end -->
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="b-directions">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <!-- one item : begin -->
                <a href="<?= Url::to(['/category/view', 'id' => 1, 'sub_id' => 14]) ?>" style="text-decoration: none;">
                    <!-- img -->
                    <img src="/images/examples/example-directions-2.jpg" alt="" class="b-directions__img">
                    <div class="b-directions__item b-directions-item__slimming">

                        <!-- icon -->
                        <div class="b-directions__icon"></div>

                        <!-- title -->
                        <div class="b-directions__title"
                             style="margin: 30px 0 14px;"><?= Yii::t('index', 'Weight Loss') ?></div>

                        <!-- text -->
                        <div class="b-directions__text"
                             style="color: rgba(255, 255, 255, 255);"><?= Yii::t('index', 'Lightness and excellent mood without extra calories') ?>
                        </div>

                    </div>
                </a>
                <!-- one item : end -->
            </div>
            <div class="col-sm-4">
                <!-- one item : begin -->
                <a href="<?= Url::to(['/category/view', 'id' => 1]) ?>" style="text-decoration: none;">
                    <!-- img -->
                    <img src="/images/examples/example-directions-1.jpg" alt="" class="b-directions__img">
                    <div class="b-directions__item b-directions-item__health">
                        <!-- icon -->
                        <div class="b-directions__icon"></div>

                        <!-- title -->
                        <div class="b-directions__title"
                             style="margin: 30px 0 14px;"><?= Yii::t('index', 'Health') ?></div>

                        <!-- text -->
                        <div class="b-directions__text"
                             style="color: rgba(255, 255, 255, 255);"><?= Yii::t('index', 'Proper nutrition for good health each day') ?>
                        </div>

                    </div>
                </a>
                <!-- one item : end -->
            </div>
            <div class="col-sm-4">
                <!-- one item : begin -->
                <a href="<?= Url::to(['/category/view', 'id' => 3]) ?>" style="text-decoration: none;">
                    <!-- img -->
                    <img src="/images/examples/example-directions-3.jpg" alt="" class="b-directions__img">
                    <div class="b-directions__item b-directions-item__sport">
                        <!-- icon -->
                        <div class="b-directions__icon"></div>

                        <!-- title -->
                        <div class="b-directions__title"
                             style="margin: 30px 0 14px;"><?= Yii::t('index', 'Sport') ?></div>

                        <!-- text -->
                        <div class="b-directions__text"
                             style="color: rgba(255, 255, 255, 255);"><?= Yii::t('index', 'Beyond energy for new records') ?>
                        </div>

                    </div>
                </a>
                <!-- one item : end -->
            </div>
        </div>
    </div>
</div>

<div class="b-sales-leaders">
    <div class="container">
        <div class="row">
            <div class="col-xa-12">
                <div class="b-sales-leaders__title"><?= Yii::t('index', 'Leaders of sells') ?></div>
            </div>
        </div>
    </div>

    <!-- For default view -->
    <div class="container hidden-xs">
        <?php
        foreach ($products as $index => $product) {
            if (!($phsc = ProductHasSubCategory::find()->where(['product_id' => $product->id])->one())
                || !$sub_category = ProductSubCategory::findOne($phsc->sub_category_id)
            ) {
                continue;
            }

            $open_row = $index % 4 == 0 ? "<div class='row'>" : null;
            $close_row = ($index + 1) % 4 == 0 || $index + 1 == sizeof($products) ? "</div>" : null;
            $url = Url::to(array_filter([
                '/product/view',
                'cat_id' => $sub_category->parent_id,
                'sub_cat_id' => $sub_category->id,
                'id' => $product->id,
            ]));
            ?>
            <?= $open_row ?>
            <div class="col-sm-3">
                <!-- one product : begin -->
                <a href="<?= $url ?>" class="b-catalog__item">
                    <!-- image -->
                    <div class="b-catalog__item_img">
                        <?php if ($path = ObjectFile::getAssetPath($product, 'image', true)) { ?>
                            <img src="<?= $path ?>" alt="">
                        <?php } ?>
                        <div class="b-catalog__item_img-bals">
                            <div class="b-catalog__item_img-bals-wrap">
                                <span class="b-catalog__item_img-bals-num"><?= $product->point ?></span>
                                <span class="b-catalog__item_img-bals-b">PV</span>
                            </div>
                        </div>
                    </div>
                    <!-- subtitle -->
                    <div class="b-catalog__item_subtitle b-catalog__item_title-wrap"><?= $product->title ?></div>
                    <!-- text -->
                    <div class="b-catalog__item_text-wrap">
                        <p class="b-catalog__item_text"><?= $product->info_text ?></p>
                        <div class="b-catalog__item_text-bg"></div>
                    </div>
                    <!-- price -->
                    <div class="b-catalog__item_price"><?= $product->cost ?> <span>₽</span></div>
                </a>
                <!-- one product : end -->
            </div>
            <?= $close_row ?>
            <?php
        }
        ?>
    </div>

    <!-- For mobile view -->
    <div class="container hidden-sm hidden-md hidden-lg">
        <?php
        foreach ($products as $index => $product) {
            if (!($phsc = ProductHasSubCategory::find()->where(['product_id' => $product->id])->one())
                || !$sub_category = ProductSubCategory::findOne($phsc->sub_category_id)
            ) {
                continue;
            }

            $open_row = $index % 2 == 0 ? "<div class='row'>" : null;
            $close_row = ($index + 1) % 2 == 0 || $index + 1 == sizeof($products) ? "</div>" : null;
            $current_sub_cat = ProductSubCategory::findOne($phsc->sub_category_id);
            $url = Url::to(array_filter([
                '/product/view',
                'cat_id' => $current_sub_cat->parent_id,
                'sub_cat_id' => $current_sub_cat->id,
                'id' => $product->id,
            ]));
            ?>
            <?= $open_row ?>
            <div class="col-sm-3">
                <!-- one product : begin -->
                <a href="<?= $url ?>" class="b-catalog__item">
                    <!-- image -->
                    <div class="b-catalog__item_img">
                        <?php if ($path = ObjectFile::getAssetPath($product, 'image', true)) { ?>
                            <img src="<?= $path ?>" alt="">
                        <?php } ?>
                        <div class="b-catalog__item_img-bals">
                            <div class="b-catalog__item_img-bals-wrap">
                                <span class="b-catalog__item_img-bals-num"><?= $product->point ?></span>
                                <span class="b-catalog__item_img-bals-b">PV</span>
                            </div>
                        </div>
                    </div>
                    <!-- subtitle -->
                    <div class="b-catalog__item_subtitle b-catalog__item_title-wrap"><?= $product->title ?></div>
                    <!-- text -->
                    <div class="b-catalog__item_text-wrap">
                        <p class="b-catalog__item_text"><?= $product->info_text ?></p>
                        <div class="b-catalog__item_text-bg"></div>
                    </div>
                    <!-- price -->
                    <div class="b-catalog__item_price"><?= $product->cost ?> <span>₽</span></div>
                </a>
                <!-- one product : end -->
            </div>
            <?= $close_row ?>
            <?php
        }
        ?>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="horizontal-line-link horizontal-line-label">
                    <div class="row-1"><a href="https://vertera.market/"><?= Yii::t('index', 'Online store') ?></a>
                    </div>
                    <div class="row-2">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="b-reviews-video-list">
    <div class="container">
        <div class="row row-1">
            <div class="col-xs-12">
                <!-- title -->
                <div class="b-reviews-video-list-title"><?= Yii::t('index', 'Results of the use of Vertera Gel') ?></div>
            </div>
        </div>
        <div class="row row-2">
            <div class="col-sm-3 col-xs-12">
                <div class="b-reviews-video-list-img">
                    <img src="/images/02-1_review.jpg" alt="">
                    <div class="b-reviews-video-list-img-play">
                        <a class="youtube" href="https://www.youtube.com/watch?v=5IfH_wf81Gw?rel=0&amp;showinfo=0">
                            <img src="/images/play.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="b-reviews-video-list-name">
                    <?= Yii::t('index', 'Skin cleansed') ?>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="b-reviews-video-list-img">
                    <img src="/images/02-2_review.jpg" alt="">
                    <div class="b-reviews-video-list-img-play">
                        <a class="youtube" href="https://www.youtube.com/watch?v=TBR78LTLPL0?rel=0&amp;showinfo=0">
                            <img src="/images/play.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="b-reviews-video-list-name">
                    <?= Yii::t('index', 'Rapid recovery from a knee injury') ?>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12">
                <div class="b-reviews-video-list-img">
                    <img src="/images/02-3_review.jpg" alt="">
                    <div class="b-reviews-video-list-img-play">
                        <a class="youtube" href="https://www.youtube.com/watch?v=gSzlz2NSG_c?rel=0&amp;showinfo=0">
                            <img src="/images/play.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="b-reviews-video-list-name">
                    <?= Yii::t('index', 'Weight loss without diets') ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- REVIEWS-VIDEO-LIST MOBILE : begin -->
<div class="container b-owl-slider-mobile-wrap b-reviews-video-list-mobile">
    <div class="b-reviews-video-list-mobile-title"><?= Yii::t('index', 'Results of the use of Vertera Gel') ?></div>
    <div class="b-owl-slider-mobile">
        <div class="item">
            <div class="b-reviews-video-list-mobile-img">
                <img src="/images/02-1_review.jpg" alt=""/>
                <div class="b-reviews-video-list-mobile-play">
                    <a href="https://www.youtube.com/embed/5IfH_wf81Gw?rel=0&showinfo=0">
                        <img src="/images/play.png" alt=""/>
                    </a>
                </div>
            </div>
            <div class="b-reviews-video-list-mobile-name">
                <?= Yii::t('index', 'Skin cleansed') ?>
            </div>
        </div>
        <div class="item">
            <div class="b-reviews-video-list-mobile-img">
                <img src="/images/02-2_review.jpg" alt=""/>
                <div class="b-reviews-video-list-mobile-play">
                    <a href="https://www.youtube.com/embed/TBR78LTLPL0?rel=0&showinfo=0">
                        <img src="/images/play.png" alt=""/>
                    </a>
                </div>
            </div>
            <div class="b-reviews-video-list-mobile-name">
                <?= Yii::t('index', 'Rapid recovery from a knee injury') ?>
            </div>
        </div>
        <div class="item">
            <div class="b-reviews-video-list-mobile-img">
                <img src="/images/02-3_review.jpg" alt=""/>
                <div class="b-reviews-video-list-mobile-play">
                    <a href="https://www.youtube.com/embed/gSzlz2NSG_c?rel=0&showinfo=0">
                        <img src="/images/play.png" alt=""/>
                    </a>
                </div>
            </div>
            <div class="b-reviews-video-list-mobile-name">
                <?= Yii::t('index', 'Weight loss without diets') ?>
            </div>
        </div>
    </div>
</div>
<!-- REVIEWS-VIDEO-LIST MOBILE : end -->

<!-- QUESTION EXPERT MOBILE : begin -->
<div class="container b-owl-slider-mobile-wrap b-question-expert-mobile">
    <div class="b-question-expert-mobile-title"><?= Yii::t('index', 'Ask a specialist') ?></div>
    <div class="b-owl-slider-mobile">

        <?php foreach ($faq_all as $faq) { ?>
            <div class="item">
                <div class="b-question-expert__item">
                    <div class="b-question-expert-mobile-item__wrapper">
                        <div class="b-question-expert-mobile-item__date"><?= $fmt->asDate($faq->created_at) ?></div>
                        <div class="b-question-expert-mobile-item__subtitle"><?= $faq->theme ?></div>
                        <p class="b-question-expert-mobile-item__text-descr"><?= $faq->question ?></p>
                        <p class="b-question-expert-mobile-item__text-descr -invisible"
                           data-text="invisible"><?= $faq->answer ?></p>
                        <div class="b-question-expert-mobile-item__author"><?= $faq->client_information ?></div>
                    </div>
                    <a href="#" class="b-question-expert-mobile-btn plus" status="close"></a>
                </div>
            </div>
        <?php } ?>

    </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- buttons -->
            <div class="b-question-expert__btns">
                <a href="<?= Url::to(['/faq']) ?>" class="b-button-green b-question-expert-btns__all-questions">
                    <?= Yii::t('index', 'All questions') ?>
                </a>
                <!-- <a href="#" class="b-button-green b-question-expert-btns__ask-question">Задать вопрос</a> -->
            </div>
        </div>
    </div>
</div>
<!-- QUESTION EXPERT MOBILE : end -->

<div class="b-question-expert js-question-expert">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- title -->
                <div class="b-question-expert__title"><?= Yii::t('index', 'Ask a specialist') ?></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">

                <?php foreach ($faq_all as $faq) { ?>
                    <div class="b-question-expert__item js-item">
                        <div class="b-question-expert-item__wrapper">
                            <div class="b-question-expert-item__date"><?= $fmt->asDate($faq->created_at) ?></div>
                            <div class="b-question-expert-item__subtitle"><?= $faq->theme ?></div>
                            <p class="b-question-expert-item__text-descr"><?= $faq->question ?></p>
                            <p class="b-question-expert-item__text-descr -invisible"
                               data-text="invisible"><?= $faq->answer ?></p>
                            <div class="b-question-expert-item__author"><?= $faq->client_information ?></div>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <!-- buttons -->
                <div class="b-question-expert__btns">
                    <a href="<?= Url::to(['/faq']) ?>" class="b-button-green b-question-expert-btns__all-questions">
                        <?= Yii::t('index', 'All questions') ?>
                    </a>
                    <!-- <a href="#" class="b-button-green b-question-expert-btns__ask-question">Задать вопрос</a> -->
                </div>
            </div>
        </div>
    </div>
</div>
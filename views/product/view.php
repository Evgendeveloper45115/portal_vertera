<?php
/**
 * @var $this \yii\web\View
 * @var $model Product
 * @var $products Product[]
 * @var $cat ProductCategory
 * @var $sub_cat ProductSubCategory
 */

use yii\helpers\Url;
use bigpaulie\social\share\Share;
use app\assets\AppAsset;
use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductSubCategory;
use app\models\ProductHasSubCategory;
use app\models\ObjectFile;

$this->title = $model->title;

$this->registerJsFile(Yii::getAlias('@web/js/vertera-project.min.js'), [
    'depends' => [AppAsset::class]
]);
?>
<div class="b-product-full js-product-full">
    <div class="b-product-full-top">
        <div class="container">

            <div class="b-breadcrumbs">
                <ul>
                    <li><a href="/"><?= Yii::t('labels', 'Main') ?></a></li>
                    <li><a href="<?= Url::to(['category/view', 'id' => $cat->id]) ?>"><?= $cat->name ?></a></li>
                    <li>
                        <a href="<?= Url::to(['category/view', 'id' => $cat->id, 'sub_id' => $sub_cat->id]) ?>"><?= $sub_cat->name ?></a>
                    </li>
                    <li><?= $model->title ?></li>
                </ul>
            </div>

            <div class="row">

                <div class="col-sm-6 col-xs-12">
                    <div class="b-product-full__back-to-list">
                        <a href="<?= Url::to(['category/view', 'id' => $cat->id]) ?>"><?= Yii::t('labels', 'Back to the list') ?></a>
                    </div>
                    <?php if ($path = ObjectFile::getAssetPath($model, 'image', true)) { ?>
                        <div class="b-product-full__image"><img src="<?= $path ?>" alt=""></div>
                    <?php } ?>
                </div>

                <div class="col-sm-6 col-xs-12">
                    <div class="b-product-full__title" style="font-size: 50px;"><?= $model->title ?></div>
                    <p class="b-product-full__shot-descr"><?= $model->title_description ?></p>
                    <div class="b-product-full__price-number-share">
                        <div class="b-product-full__price">
                            <p><?= Yii::t('labels', 'Cost') ?></p>
                            <p><?= $model->cost ?> <span>₽</span></p>
                        </div>
                        <div class="b-product-full__balls">
                            <p><?= Yii::t('labels', 'Points') ?></p>
                            <p><?= $model->point ?><span>PV</span></p>
                        </div>
                        <div class="b-product-full__number hidden-xs">
                            <p><?= Yii::t('labels', 'In the package') ?></p>
                            <p><?= $model->amount ?></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="b-product-full__share">
                            <?php
                            ?>
                            <p><?= Yii::t('labels', 'Share') ?></p>
                            <?= Share::widget([
                                'type' => Share::TYPE_EXTRA_SMALL,
                                'tag' => 'div',
                                'title' => $model->title,
                                'description' => $model->title_description,
                                'image' => 'https://portal.dev.vertera.org' . $path,
                                'url' => 'https://portal.dev.vertera.org' . Yii::$app->request->getUrl(),
                                'networks' => [
                                    'vk' => 'http://vkontakte.ru/share.php?url={url}',
                                    'facebook' => 'https://www.facebook.com/sharer/sharer.php?u={url}',
                                    'odnoklassniki' => 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl={url}',
                                    'google-plus' => 'https://plus.google.com/share?url={url}',
                                ],
                                'addUtm' => true,
                                'template' => '<span>{button}</span>',
                            ]) ?>
                        </div>
                    </div>
                    <?php
                    //$dev = YII_ENV_DEV ? 'dev.' : null;
                    $parts = explode('/', preg_replace(array_map(function ($v) {
                        return "/^{$v}/";
                    }, array_values(YII_LANGUAGES)), '', trim(Url::to(), '/')));
                    $l = array_pop($parts);
                    $parts[] = $model->url_name;
                    $out_url = "https://vertera.market" . implode('/', $parts);
                    ?>
                    <div class="b-product-full__button-article">
                        <div class="b-product-full__button">
                            <a href="<?= $out_url ?>"
                               target="_blank"
                               class="b-button b-button-transparent"><?= Yii::t('labels', 'Buy from e-shop') ?></a>
                        </div>
                        <div class="b-product-full__article">
                            <p><?= Yii::t('labels', 'Vendor code') ?>: <?= $model->vendor_code ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row b-product-full-info">
            <div class="col-sm-7 col-xs-12">
                <div class="b-product-full__tabs">

                    <div class="b-product-full-tabs__top-mobile">
                        <div class="col-xs-12">
                            <div class="b-sorting__select-style">
                                <select>
                                    <option value="1" class="js-tab-controller">
                                        <?= Yii::t('labels', 'Description') ?>
                                    </option>
                                    <option value="2" class="js-tab-controller">
                                        <?= Yii::t('labels', 'Consist') ?>
                                    </option>
                                    <option value="3" class="js-tab-controller">
                                        <?= Yii::t('labels', 'Use') ?>
                                    </option>
                                    <!-- <option value="4" class="js-tab-controller">Отзывы</option> -->
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="b-product-full-tabs__top">
                        <ul>
                            <li><a href="#" class="b-button-green js-tab-controller -active"
                                   data-tab="1"><?= Yii::t('labels', 'Description') ?></a>
                            </li>
                            <li><a href="#" class="b-button-green js-tab-controller"
                                   data-tab="2"><?= Yii::t('labels', 'Consist') ?></a></li>
                            <li><a href="#" class="b-button-green js-tab-controller"
                                   data-tab="3"><?= Yii::t('labels', 'Use') ?></a></li>
                        </ul>
                    </div>

                    <div class="b-product-full-tabs__bottom">
                        <div class="b-product-full-tabs__wrapper js-tab-content -active" data-tab="1">
                            <?= $model->tab_description ?>
                        </div>
                        <div class="b-product-full-tabs__wrapper js-tab-content" data-tab="2">
                            <?= $model->tab_composition ?>
                        </div>
                        <div class="b-product-full-tabs__wrapper js-tab-content" data-tab="3">
                            <?= $model->tab_use ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-5 col-xs-12">
                <?php if ($path = ObjectFile::getAssetPath($model, 'description_image', true)) { ?>
                    <div class="b-product-full-info-img"><img src="<?= $path ?>" alt=""></div>
                <?php } ?>
                <!--
                <div class="b-product-full-info-icons">
                    <div class="row">
                        <img src="/vertera/img/product-icons/1.png" alt=""/>
                        <p>Не содержит глютена</p>
                    </div>
                    <div class="row">
                        <img src="/vertera/img/product-icons/2.png" alt=""/>
                        <p>Натуральный продукт</p>
                    </div>
                    <div class="row">
                        <img src="/vertera/img/product-icons/3.png" alt=""/>
                        <p>Российское сырье</p>
                    </div>
                    <div class="row">
                        <img src="/vertera/img/product-icons/4.png" alt=""/>
                        <p>Продукция сертифицирована</p>
                    </div>
                </div>
                -->
            </div>

        </div>
    </div>
    <?php
    $path = ObjectFile::getAssetPath($model, 'info_image', true);
    ?>
    <div class="b-product-full__add-info-bg"
         style="background: url(<?= $path ?>) no-repeat top center; background-size: cover;">
        <div class="container">
            <div class="row">

                <div class="col-sm-6 col-xs-12">
                    <!-- image big -->
                </div>

                <div class="col-sm-6 col-xs-12">
                    <img src="/images/examples/example-flower-round.png" alt=""
                         class="b-product-full-add-info-bg__img-small">
                    <div class="b-product-full-add-info-bg__title"><?= $model->title ?></div>
                    <div class="b-product-full-add-info-bg__text"><?= $model->info_text ?></div>
                </div>
            </div>
        </div>
    </div>

    <div class="b-certificates">
        <div class="container">
            <div class="b-reviews__title"><?= Yii::t('labels', 'Certificates') ?></div>

            <div class="col-sm-12">
                <div class="row">

                    <?php foreach ($model->getCertificates() as $file) {
                        ?>
                        <div class="col-sm-3">
                            <div class="b-certificates__wrapper">
                                <div class="b-certificates__item">
                                    <?php
                                    if (file_exists($file->getPath(true, true))) {
                                        $url = $this->getAssetManager()->publish($file->getPath(true, true))[1]
                                        ?>
                                        <img src="<?= $url ?>">
                                        <div class="b-certificates__item-title">
                                            <a class="b-certificates__item-title" target="_blank" href="<?= $url ?>">
                                                <?= $file->original_name ?>
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="b-product-full__mnml-catalog">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="b-product-full-mnml-catalog__title"><?= Yii::t('labels', 'More effect <br>in the complex') ?></div>
            </div>
        </div>

        <div class="row">

            <?php
            foreach ($products as $product) {
                if (!$phsc = ProductHasSubCategory::find()->where(['product_id' => $product->id])->one()) {
                    continue;
                }

                $current_sub_cat = ProductSubCategory::findOne($phsc->sub_category_id);
                $url = Url::to(array_filter([
                    '/product/view',
                    'cat_id' => $current_sub_cat->parent_id,
                    'sub_cat_id' => $current_sub_cat->id,
                    'id' => $product->id,
                ]));
                ?>
                <div class="col-sm-3">
                    <div class="b-catalog__item">
                        <?php if ($path = ObjectFile::getAssetPath($product, 'image', true)) { ?>
                            <a href="<?= $url ?>" class="b-catalog__item_img">
                                <img src="<?= $path ?>" alt="">
                            </a>
                        <?php } ?>
                        <div class="b-catalog__item_subtitle"><?= $product->title ?></div>
                        <p class="b-catalog__item_text"><?= $product->info_text ?></p>
                        <div class="b-catalog__item_price"><?= $product->cost ?> <span>₽</span></div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</div>
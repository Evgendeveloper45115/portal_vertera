<?php
/**
 * @var $model News
 * @var $models News[]
 */

use yii\helpers\Url;


$this->title = $model->title;
$fmt = Yii::$app->getFormatter();
$fmt->defaultTimeZone = $fmt->timeZone;
?>
<div class="b-news">
    <div class="container">
        <div class="b-breadcrumbs">
            <ul>
                <li><a href="<?= Url::to(['/']) ?>"><?= Yii::t('labels', 'Main') ?></a></li>
                <li><a href="<?= Url::to(['/news/index']) ?>"><?= Yii::t('labels', 'News') ?></a></li>
                <li><?= $model->title ?></li>
            </ul>
        </div>
        <div class="b-news-link-back">
            <a href="<?= Url::to(['/news/index']) ?>"><?= Yii::t('labels', 'Back to the list') ?></a>
        </div>
        <div class="b-news__title"><?= $model->title ?></div>
        <div class="b-news__date"><?= $fmt->asDate($model->start_date, 'long') ?></div>

        <?php if ($path = ObjectFile::getAssetPath($model, 'image', true)) { ?>
            <div class="b-news__img"><img src="<?= $path ?>" alt=""></div>
        <?php } ?>

        <div><?= htmlspecialchars_decode($model->description) ?></div>
        <?= \bigpaulie\social\share\Share::widget([
            'type' => \bigpaulie\social\share\Share::TYPE_EXTRA_SMALL,
            'tag' => 'div',
            'title' => $model->title,
            'image' => 'https://portal.dev.vertera.org' . $path,
            'url' => 'https://portal.dev.vertera.org' . Yii::$app->request->getUrl(),
            'networks' => [
                'vk' => 'http://vkontakte.ru/share.php?url={url}',
                'facebook' => 'https://www.facebook.com/sharer/sharer.php?u={url}',
                'odnoklassniki' => 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl={url}',
                'google-plus' => 'https://plus.google.com/share?url={url}',
            ],
            'addUtm' => true,
            'template' => '<span>{button}</span>',
        ]) ?>

        <div class="b-news-nav">
            <div class="row">
                <?php foreach ($models as $m) { ?>
                    <div class="col-sm-6 col-xs-12">
                        <div class="b-news-nav-item">

                            <?php if ($path = ObjectFile::getAssetPath($m, 'image', true)) { ?>
                                <div class="b-news-nav-img">
                                    <a href="<?= Url::to(['/news/view', 'id' => $m->id]) ?>"><img src="<?= $path ?>" alt=""></a>
                                </div>
                            <?php } ?>

                            <div class="b-news-nav-date"><?= $fmt->asDate($m->start_date, 'long') ?></div>
                            <div class="b-news-nav-title">
                                <a href="<?= Url::to(['/news/view', 'id' => $m->id]) ?>"><?= $m->title ?></a>
                            </div>
                            <div class="b-news-nav-buttons-left">
                                <a href="<?= Url::to(['/news/view', 'id' => $m->id]) ?>">
                                    <?= Yii::t('labels', 'Previous news') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="row b-news-nav-buttons">
                <?php if ($m = $models['prev'] ?? null) { ?>
                    <div class="col-sm-4 col-xs-12">
                        <div class="b-news-nav-buttons-left">
                            <a href="<?= Url::to(['/news/view', 'id' => $m->id]) ?>"><?= Yii::t('labels', 'Previous news') ?></a>
                        </div>
                    </div>
                <?php } ?>

                <div class="col-sm-4 col-xs-12">
                    <div class="b-news-nav-buttons-center">
                        <a href="<?= Url::to(['/news']) ?>"><?= Yii::t('labels', 'All news') ?></a>
                    </div>
                </div>

                <?php if ($m = $models['next'] ?? null) { ?>
                    <div class="col-sm-4 col-xs-12">
                        <div class="b-news-nav-buttons-right">
                            <a href="<?= Url::to(['/news/view', 'id' => $m->id]) ?>"><?= Yii::t('labels', 'Next news') ?></a>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>

<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

$this->title = Yii::t('strategy', 'Company development strategy');
?>
<div class="b-mission">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- breadcrumbs : begin -->
                <div class="b-breadcrumbs">
                    <ul>
                        <li><a href="/">
                                <?= Yii::t(
                                    'strategy',
                                    'Home')
                                ?>
                            </a>
                        </li>
                        <li><?= $this->title ?></li>
                    </ul>
                </div>
                <!-- breadcrumbs : end -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-12">
                <aside class="b-sidebar js-sidebar">
                    <ul>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/history']) ?>">
                                <?= Yii::t(
                                    'strategy',
                                    'History')
                                ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/mission']) ?>">
                                <?= Yii::t(
                                    'strategy',
                                    'Mission, purpose and values')
                                ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/philosophy']) ?>">
                                <?= Yii::t(
                                    'strategy',
                                    'Philosophy of Vertera')
                                ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/strategy']) ?>">
                                <?= Yii::t(
                                    'strategy',
                                    'Company development strategy')
                                ?>
                            </a>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="col-sm-9 col-xs-12">
                <!-- title -->
                <h1 class="b-strategy__title"><?= $this->title ?></h1>

                <!-- text -->
                <p class="b-strategy__text">
                    <?= Yii::t(
                        'strategy',
                        'The development strategy of Vertera for 2017-2025.')
                    ?>
                </p>

                <!-- item : begin -->
                <div class="b-strategy__item">


                    <!-- table : begin -->
                    <table class="b-strategy-item__table">

                        <!-- head : begin -->
                        <thead>
                        <tr>
                            <th>
                                <?= Yii::t(
                                    'strategy',
                                    'Plan for 2017')
                                ?>
                            </th>
                            <th>
                                <?= Yii::t(
                                    'strategy',
                                    'Goal for 3 years')
                                ?>
                            </th>
                            <th>
                                <?= Yii::t(
                                    'strategy',
                                    'After 10 years')
                                ?>
                            </th>
                        </tr>
                        </thead>
                        <!-- head : end -->

                        <!-- body : begin -->
                        <tbody>
                        <tr>
                            <td><?= Yii::t(
                                    'strategy',
                                    '- Year of technology Vertera.')
                                ?>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Expansion of the product line.')
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Start of sales of mycelium food concentrates.')
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- The line of professional cosmetics Vertera Cosmetics Professional.')
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Line of personal care products.')
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Start of the global trading platform World of Retail.')
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    "- Opening of the Company's own offices in the largest cities of the CIS.")
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Opening of the European market.')
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Launching the training resource Vertera.university.')
                                ?>

                            </td>
                            <td> <?= Yii::t(
                                    'strategy',
                                    "- Cover 70% of each family's needs with Vertera's own products and purchases made online via the World of Retail platform.")
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Create a powerful socially-oriented business. Vertera leaders are leaders in business communities.')
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Business clubs Vertera as serious promotion tools.')
                                ?>
                            </td>
                            <td><?= Yii::t(
                                    'strategy',
                                    '- Satisfy the needs of each client in purchasing their own goods, as well as goods and services of third-party suppliers on the Internet through the World of Retail platform.')
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Becoming an economy of peace.')
                                ?>
                                <br>
                                <br>
                                <?= Yii::t(
                                    'strategy',
                                    '- Vertera is a global player in the social and political space of Russia and the World.')
                                ?>

                            </td>

                        </tr>
                        </tbody>
                        <!-- body : end -->

                    </table>
                    <!-- table : end -->

                </div>
                <!-- item : end -->


            </div>
        </div>
    </div>
</div>
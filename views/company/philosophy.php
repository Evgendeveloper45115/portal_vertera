<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

$this->title = Yii::t('philosophy', 'Philosophy of Vertera');
?>
<div class="b-mission">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- breadcrumbs : begin -->
                <div class="b-breadcrumbs">
                    <ul>
                        <li><a href="/">
                                <?= Yii::t(
                                    'philosophy',
                                    'Home')
                                ?>
                            </a>
                        </li>
                        <li><?= $this->title ?></li>
                    </ul>
                </div>
                <!-- breadcrumbs : end -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-12">
                <aside class="b-sidebar js-sidebar">
                    <ul>
                        <li>
                            <a href="<?= Url::to(['/company/history']) ?>">
                                <?= Yii::t(
                                    'philosophy',
                                    'History')
                                ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/mission']) ?>">
                                <?= Yii::t(
                                    'philosophy',
                                    'Mission, purpose and values')
                                ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/philosophy']) ?>">
                                <?= Yii::t(
                                    'philosophy',
                                    'Philosophy of Vertera')
                                ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/strategy']) ?>">
                                <?= Yii::t(
                                    'philosophy',
                                    'Company development strategy')
                                ?>
                            </a>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="col-sm-9 col-xs-12">
                <!-- title -->
                <h1 class="b-philosophy__title"><?= $this->title ?></h1>

                <!-- text -->
                <p class="b-philosophy__text">
                    <?= Yii::t(
                        'philosophy',
                        'We have united in our activities the adherence to traditions, values and, at the same time, high technologies of business. We care about people and help them achieve their goals in protecting family health and prosperity of a kind.')
                    ?>
                    .</p>

                <!-- text small -->
                <p class="b-philosophy__text-small">
                    <?= Yii::t(
                        'philosophy',
                        'Our philosophy is based on 4 major principles')
                    ?>
                </p>

                <!-- principles : begin -->
                <div class="b-philosophy__principles">

                    <!-- list : begin -->
                    <ul class="b-philosophy-principles__list">
                        <li>
                            <div class="b-philosophy-principles__icon-people"></div>
                            <div class="b-philosophy-principles__list-wrapper">
                                <h2 class="b-philosophy-principles__subtitle">
                                    <?= Yii::t(
                                        'philosophy',
                                        'People')
                                    ?>
                                </h2>
                                <p class="b-philosophy-principles__text-small">
                                    <?= Yii::t(
                                        'philosophy',
                                        'The culture, values and business of Vertera are aimed at satisfying the interests of our partners and customers. We consider every partner as a member of our family, team and in every possible way to promote the development of his personality and business.')
                                    ?>
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="b-philosophy-principles__icon-products"></div>
                            <div class="b-philosophy-principles__list-wrapper">
                                <h2 class="b-philosophy-principles__subtitle">
                                    <?= Yii::t(
                                        'philosophy',
                                        'Products')
                                    ?>
                                </h2>
                                <p class="b-philosophy-principles__text-small">
                                    <?= Yii::t(
                                        'philosophy',
                                        'Our products are based on the technologies of bionics, the science of how to correctly see solutions in living nature. Everything that we create is based on the integrity, unity of man and nature.')
                                    ?>
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="b-philosophy-principles__icon-business"></div>
                            <div class="b-philosophy-principles__list-wrapper">
                                <h2 class="b-philosophy-principles__subtitle">
                                    <?= Yii::t(
                                        'philosophy',
                                        'Business')
                                    ?>
                                </h2>
                                <p class="b-philosophy-principles__text-small">
                                    <?= Yii::t(
                                        'philosophy',
                                        'We offer a powerful business for any person who aspires to success. The partner program, the system of training and motivation from Vertera allow our partners to effectively and promptly reach the residual income and subsequently, if desired, transfer the business by inheritance or sell.')
                                    ?>
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="b-philosophy-principles__icon-technology"></div>
                            <div class="b-philosophy-principles__list-wrapper">
                                <h2 class="b-philosophy-principles__subtitle">
                                    <?= Yii::t(
                                        'philosophy',
                                        'Technologies')
                                    ?>
                                </h2>
                                <p class="b-philosophy-principles__text-small">
                                    <?= Yii::t(
                                        'philosophy',
                                        "Vertera's business, not only in the nature of its products, but also in the way it is conducted, is truly innovative and has no analogues in the world. We are in step with the times and we consider it our duty to take care of the future. That's why our wor.ru internet marketplace is aimed at cooperation with a new generation of young people and those who do not represent their lives without the Internet. Vertera is a company of the future that already exists.")
                                    ?>
                                </p>
                            </div>
                        </li>
                    </ul>
                    <!-- list : end -->

                    <!-- text -->
                    <p class="b-philosophy__text-small">
                        <?= Yii::t(
                            'philosophy',
                            'Today, thousands of people around the world are grateful to Vertera for its products and technologies, for choosing the right business and life priorities for itself.')
                        ?>
                    </p>

                    <!-- button -->
                    <p class="text-center">
                        <a href="https://os.verteraorganic.com/reg/"
                           data-toggle="modal"
                           data-target="#consent-form-modal"
                           class="b-button-green"><?= Yii::t(
                                'philosophy',
                                'Become a partner')
                            ?></a>
                    </p>
                    <br>
                    <br>

                    <!-- text -->
                    <p class="b-philosophy__text-big">
                        <?= Yii::t(
                            'philosophy',
                            'We are happy to see you in our business family and will do everything possible to ensure that cooperation with Vertera is the basis for your successful and happy life!')
                        ?>
                    </p>

                </div>
                <!-- principles : end -->
            </div>
        </div>
    </div>
</div>
<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

$this->title = 'О компании Vertera';
?>
<div class="b-about">

    <div class="b-descr-page b-descr-page-for-about">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- substrate background : begin -->
                    <div class="b-descr-page__substrate-bg">
                        <div class="b-descr-page-substrate-bg">
                            <!-- title -->
                            <h1 class="b-descr-page__title"><?= Yii::t('about', 'about company') ?><br>Vertera</h1>
                            <!-- description -->
                            <p class="b-descr-page__text"><?= Yii::t(
                                    'about',
                                    'Vertera is the leader in the direct sales market in its segment in Russia and the CIS'
                                ) ?>
                            </p>
                        </div>
                    </div>
                    <!-- substrate background : end -->
                </div>
            </div>
        </div>
    </div>

    <div class="b-about-content">
        <div class="container">
            <div class="b-about-content-title"><?= Yii::t(
                    'about',
                    "Vertera is a group of companies united by a common philosophy, a socially oriented business in the sphere of supporting each person's striving for health, longevity and a better quality of life"
                ) ?>
            </div>
            <div class="b-about-content-text">
                <p><?= Yii::t(
                        'about',
                        'Vertera is a group of companies united by a common philosophy, socially oriented business, the sphere of supporting each person\'s striving for health, longevity and the best quality of life. We have
own research laboratory for the development of products for dietary nutrition, a powerful
production base, uniting scientists and specialists from all over the world, a business specializing in
e-commerce World Of Retail (wor.ru) and multifunctional information
Centre. The most advanced high-tech, large-scale tasks and focus on long-term
prospects - these are the advantages that make cooperation with us profitable, stable and
promising.'
                    ) ?>
                </p>
                <hr>
                <p>
                    <?= Yii::t('about', 'ul.Punane house, 6 office 322, Tallinn, Estonia') ?>,13619. Vertera OÜ<br>
                    Reg.nr. 14137122<br>
                    KMKR EE101948883<br>
                    Swedbank AS<br>
                    HABAEE2X EE52 2200 2210 6600 8100<br>
                </p>
            </div>
            <div class="b-about-content-banners">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <div class="b-about-content-banners-img">
                                <a href="<?= Url::to(['company/mission']) ?>">
                                    <img src="/images/about/1.png" alt="">
                                </a>
                            </div>
                            <div class="b-about-content-banners-title">
                                <a href="<?= Url::to(['company/mission']) ?>"><?= Yii::t(
                                        'about',
                                        'Mission, purpose and values'
                                    ) ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="b-about-content-banners-img">
                                <a href="<?= Url::to(['company/history']) ?>">
                                    <img src="/images/about/2.png" alt="">
                                </a>
                            </div>
                            <div class="b-about-content-banners-title">
                                <a href="<?= Url::to(['company/history']) ?>"><?= Yii::t(
                                        'about',
                                        'History of the company'
                                    ) ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="b-about-content-banners-img">
                                <a href="<?= Url::to(['company/philosophy']) ?>">
                                    <img src="/images/about/3.png" alt="">
                                </a>
                            </div>
                            <div class="b-about-content-banners-title">
                                <a href="<?= Url::to(['company/philosophy']) ?>"><?= Yii::t(
                                        'about',
                                        'Philosophy of Vertera'
                                    ) ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="b-about-content-banners-img">
                                <a href="<?= Url::to(['company/strategy']) ?>">
                                    <img src="/images/about/4.png" alt="">
                                </a>
                            </div>
                            <div class="b-about-content-banners-title">
                                <a href="<?= Url::to(['company/strategy']) ?>"><?= Yii::t(
                                        'about',
                                        'Development Strategy'
                                    ) ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="b-about-content-title"><?= Yii::t(
                    'about',
                    'The main advantage of Vertera is to offer ready-made solutions to customers, which have no analogues in any other direct sales company in the world.'
                ) ?>
            </div>
        </div>
        <div class="b-about-full-image">
            <div class="container">
                <div class="b-about-full-image-inner">
                    <div class="b-about-full-image-item b-about-full-image-item-1">
                        <img src="/images/about/logo.png">
                        <div class="b-about-full-image-item-title">
                            <?= Yii::t(
                                'about',
                                'Vertera Company'
                            ) ?>
                        </div>
                    </div>
                    <div class="b-about-full-image-item b-about-full-image-item-2">
                        <img src="/images/about/icon-1.png">
                        <div class="b-about-full-image-item-title">
                            <?= Yii::t(
                                'about',
                                'Direct Sales Department'
                            ) ?>
                        </div>
                        <div class="b-about-full-image-item-popup">
                            <?= Yii::t(
                                'about',
                                'Own call-center, the largest in Russia'
                            ) ?>
                        </div>
                    </div>
                    <div class="b-about-full-image-item b-about-full-image-item-3">
                        <img src="/images/about/icon-2.png">
                        <div class="b-about-full-image-item-title">
                            <?= Yii::t(
                                'about',
                                'Promotional packages "Hermes"'
                            ) ?>
                        </div>
                        <div class="b-about-full-image-item-popup">
                            <?= Yii::t(
                                'about',
                                'Points from direct sales of the company are distributed in the marketing plan under active partners for the advertising packages "HERMES"'
                            ) ?>
                        </div>
                    </div>
                    <div class="b-about-full-image-item b-about-full-image-item-4">
                        <img src="/images/about/icon-3.png">
                        <div class="b-about-full-image-item-title">
                            <?= Yii::t(
                                'about',
                                'Own production'
                            ) ?>
                        </div>
                        <div class="b-about-full-image-item-popup">
                            <?= Yii::t(
                                'about',
                                'Own extraction, processing of raw materials, development and Russian production of eco products'
                            ) ?>
                        </div>
                    </div>
                    <div class="b-about-full-image-item b-about-full-image-item-5">
                        <img src="/images/about/icon-4.png">
                        <div class="b-about-full-image-item-title">
                            <?= Yii::t(
                                'about',
                                'Household <br> chemicals'
                            ) ?>
                        </div>
                        <div class="b-about-full-image-item-popup">
                            <?= Yii::t(
                                'about',
                                'Natural, organic products for cleaning the house'
                            ) ?>
                        </div>
                    </div>
                    <div class="b-about-full-image-item b-about-full-image-item-6">
                        <img src="/images/about/icon-5.png">
                        <div class="b-about-full-image-item-title">
                            <?= Yii::t(
                                'about',
                                'Cosmetic <br> line'
                            ) ?>
                        </div>
                        <div class="b-about-full-image-item-popup">
                            <?= Yii::t(
                                'about',
                                'Natural ecocosmetics for face and body based on seaweed'
                            ) ?>
                        </div>
                    </div>
                    <div class="b-about-full-image-item b-about-full-image-item-7">
                        <img src="/images/about/icon-6.png">
                        <div class="b-about-full-image-item-title">
                            <?= Yii::t(
                                'about',
                                'Smart food'
                            ) ?>
                        </div>
                        <div class="b-about-full-image-item-popup">
                            <?= Yii::t(
                                'about',
                                'Ecological products for medical and dietary nutrition based on seaweed'
                            ) ?>
                        </div>
                    </div>
                    <div class="b-about-full-image-item b-about-full-image-item-8">
                        <img src="/images/about/icon-7.png">
                        <div class="b-about-full-image-item-title">
                            <?= Yii::t(
                                'about',
                                'IT department'
                            ) ?>
                        </div>
                    </div>
                    <div class="b-about-full-image-item b-about-full-image-item-9">
                        <img src="/images/about/icon-8.png">
                        <div class="b-about-full-image-item-title">
                            <?= Yii::t(
                                'about',
                                'World of Trade'
                            ) ?><br>(World Of Retail)
                        </div>
                        <div class="b-about-full-image-item-popup">
                            <?= Yii::t(
                                'about',
                                'A unique e-commerce platform (millions of goods and services, all coupons, stocks of leading stores)'
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

$this->title = Yii::t('history', 'History of the company');
?>
<div class="b-mission">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- breadcrumbs : begin -->
                <div class="b-breadcrumbs">
                    <ul>
                        <li><a href="/">
                                <?= Yii::t('history', 'Home') ?>
                            </a>
                        </li>
                        <li><?= $this->title ?></li>
                    </ul>
                </div>
                <!-- breadcrumbs : end -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-12">
                <aside class="b-sidebar js-sidebar">
                    <ul>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/history']) ?>">
                                <?= Yii::t(
                                    'history',
                                    'History'
                                ) ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/mission']) ?>">
                                <?= Yii::t(
                                    'history',
                                    'Mission, purpose and values'
                                ) ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/philosophy']) ?>">
                                <?= Yii::t(
                                    'history',
                                    'Philosophy of Vertera'
                                ) ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/strategy']) ?>">
                                <?= Yii::t(
                                    'history',
                                    'Company development strategy'
                                ) ?>
                            </a>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="col-sm-9 col-xs-12">
                <div class="b-history__title"><?= $this->title ?></div>
                <div class="row-1">
                    <p class="b-history__text">
                        <?= Yii::t(
                            'history',
                            'The history of Vertera began in 2005, when for the first time in the world A unique biotechnology has been created that makes it possible to obtain a complex of active natural compounds from cells of seaweed kelp. The cellular contents of the plant were isolated, and on its basis a gel was obtained. The gel composition included the cellular composition of the kelp, which retains undestroyed state of the molecular bond of natural substances. Vertera Gel is certified as A specialized food for dietary therapeutic nutrition.'
                        ) ?>
                    </p>
                    <p>
                        <a class="b-button-green" href="<?= Url::to(['site/vertera_gel']) ?>">
                            <?= Yii::t(
                                'history',
                                'More'
                            ) ?>
                        </a>
                        <br><br>
                    </p></div>
                <div class="row-2">
                    <p class="b-history__text-small">
                        <?= Yii::t(
                            'history',
                            'It is a holistic product, not divided artificially into separate organic and mineral elements. This is his main advantage. That is why substances in Vertera gel are perfectly perceived by the body, and the level of their digestibility can reach 93%.'
                        ) ?>
                    </p>
                    <p class="b-history__text-small">
                        <?= Yii::t(
                            'history',
                            "Laminaria gel is the basis of almost all products and natural complexes of Vertera. This is a 'live food', thanks to which our body receives a complex of vitally important substances. The composition of the components of the product corresponds to the code of organic and mineral compounds evolutionarily fixed in the organism's genotype."
                        ) ?>
                    </p>
                </div>
                <div class="row-3">
                    <div class="b-history__subtitle">
                        <?= Yii::t(
                            'history',
                            'Research'
                        ) ?>
                    </div>
                </div>
                <div class="row-4">
                    <p class="b-history__text-small">
                        <?= Yii::t(
                            'history',
                            'Work on technology improvement and clinical research of Vertera products continued under the leadership of Anatoly Khitrov, who is still President of the Company.'
                        ) ?>
                    </p>
                    <p class="b-history__text-small">
                        <?= Yii::t(
                            'history',
                            "It was he who united a team of like-minded people, modern scientists, technologists and other high-level specialists with world experience. A graduate of the Edinburgh School of Business, Edinburgh Business School, A. Khitrov appreciated the highest potential of domestic science and defined his business as the creation of innovative products for the nation's recovery, accessible to most consumers."
                        ) ?>
                    </p>
                </div>
                <div class="row-5">
                    <div class="b-history__text-big">
                        <?= Yii::t(
                            'history',
                            'The main principle of Vertera is the creation of products to help people in the modern technogenic world. Man is an integral, original, unique and unusually important part of the most complex mechanism called nature. That is why, for a natural healthy existence, each of us needs integral food products, rather than substances separated from them, endowed with magical properties.'
                        ) ?>
                    </div>
                </div>
                <br>
                <div class="row-6">
                    <div class="b-history__subtitle-color">2010</div>
                    <p class="b-history__text-small">
                        <?= Yii::t(
                            'history',
                            'Since 2010, Vertera has significantly expanded the range of its products. Professional pharmacologist and phytotherapeutist, Honored Scientist of Russia, Head of the Department of Clinical Pharmacology, TGMA, Doctor of Medicine, Professor Gennady Bazanov became the author of phytoproducts recipes. He developed and formulated Verona phytonadiation, leading the research division of the company. The innovative nature of this direction was that the created composite phytostases were intended not only for replenishing the missing vital ingredients of nutrition and for balancing the complex of organic and mineral substances, but also for directed physiological effects on organs and systems, including activation of detoxification and excretory processes.'
                        ) ?>
                    </p>
                </div>
                <div class="row-7">
                    <div class="b-history__subtitle-color">2014</div>
                    <p class="b-history__text-small">
                        <?= Yii::t(
                            'history',
                            'December 7, 2014 the company Vertera started as a direct sales company.'
                        ) ?>
                    </p>
                </div>
                <div class="row-8">
                    <div class="b-history__subtitle-color">2015-2016</div>
                    <p class="b-history__text-small">
                        <?= Yii::t(
                            'history',
                            'The company is active in the markets of Russia, the Republic of Belarus, Kyrgyzstan, Kazakhstan, Ukraine, Azerbaijan.  <br> <Span> 867 partner offices are open. </ Span> <br> Passed certification in the EU.'
                        ) ?>
                    </p>
                </div>
                <div class="row-9">
                    <div class="b-history__subtitle-color">2017</div>
                    <p class="b-history__text-small"><?= Yii::t(
                            'history',
                            'On January 6, 2017, Vertera opened an additional business line for Partners - World of Retail.'
                        ) ?></p>
                    <a class="b-button-green" href="https://wor.ru/">World of Retail</a>
                </div>
            </div>
        </div>
    </div>
</div>

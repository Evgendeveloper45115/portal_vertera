<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

$this->title = Yii::t('mission', 'Mission, purpose, values');
?>
<div class="b-mission">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- breadcrumbs : begin -->
                <div class="b-breadcrumbs">
                    <ul>
                        <li><a href="/">
                                <?= Yii::t(
                                        'mission',
                                        'Home')
                                ?>
                            </a>
                        </li>
                        <li><?= $this->title ?></li>
                    </ul>
                </div>
                <!-- breadcrumbs : end -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-12">
                <aside class="b-sidebar js-sidebar">
                    <ul>
                        <li>
                            <a href="<?= Url::to(['/company/history']) ?>">
                                <?= Yii::t(
                                    'mission',
                                    'History')
                                ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/mission']) ?>">
                                <?= Yii::t(
                                    'mission',
                                    'Mission, purpose and values')
                                ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/philosophy']) ?>">
                                <?= Yii::t(
                                    'mission',
                                    'Philosophy of Vertera')
                                ?>
                            </a>
                        </li>
                        <!-- first level -->
                        <li>
                            <a href="<?= Url::to(['/company/strategy']) ?>">
                                <?= Yii::t(
                                    'mission',
                                    'Company development strategy')
                                ?>
                            </a>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="col-sm-9 col-xs-12">
                <!-- right column : begin -->
                <!-- title -->
                <div class="b-mission__title"><?= $this->title ?></div>

                <!-- text -->
                <p class="b-mission__text">
                    <?= Yii::t(
                        'mission',
                        'The main business tools of Vertera are natural products and new technologies. We support the aspiration of every person to health, longevity and help in achieving the set goals.')
                    ?>
                </p>

                <!-- text small -->
                <p class="b-mission__text-small">
                    <?= Yii::t(
                        'mission',
                        'Vertera is a large international holding uniting several businesses within the framework of a single philosophy. We have our own research laboratory for the development of products for dietary nutrition, a powerful manufacturing base that unites scientists and specialists from all over the world, a business in the field of DT (Digital Technologies), specializing in e-commerce and software applications development to support partners, a multifunctional information and reference center . New high technology, large-scale tasks and focus on long-term prospects are the advantages that make cooperation with us profitable, stable and promising.')
                    ?>
                </p>

                <!-- title -->
                <div class="b-mission__title-normal-transform">
                    <?= Yii::t(
                        'mission',
                        'Our mission')
                    ?>
                </div>

                <!-- text small -->
                <p class="b-mission__text-small">
                    <?= Yii::t(
                        'mission',
                        'Create a powerful social movement, consisting of people who share the values of our company. All our efforts are aimed at improving the demographic situation, the long-term development of society on the principle of family or family, when each person is responsible not only for his life, but for the future of his country and future generations. First of all, our business is socially oriented. We are fighting for the lives of people, reducing the risks of death from cardiovascular diseases, and we care about the future of the nation, increasing the birth rate.')
                    ?>
                </p>

                <!-- title -->
                <div class="b-mission__title-normal-transform">
                    <?= Yii::t(
                        'mission',
                        'Our goal')
                    ?>
                </div>

                <!-- text small -->
                <p class="b-mission__text-small">
                    <?= Yii::t(
                    'mission',
                    'Become the No. 1 company in the world, the global leader in the direct sales and e-commerce market, representing products and technologies for the development of healthy humanity.')
                    ?>
                </p>

                <!-- title -->
                <div class="b-mission__title-normal-transform">
                    <?= Yii::t(
                        'mission',
                        'Our values')
                    ?>
                </div>

                <!-- image -->
                <img src="/images/examples/example-mission-1.png" alt="" class="b-mission__img">

                <!-- title small -->
                <div class="b-mission__title-small">
                    <?= Yii::t(
                        'mission',
                        'Family')
                    ?>
                </div>

                <!-- text small -->
                <p class="b-mission__text-small">
                    <?= Yii::t(
                        'mission',
                        'The family is the most valuable thing that is given to a person. This is our first principle, support and support, love and happiness. We are building a business on these principles, and this is our strength and our power!')
                    ?>
                </p>

                <!-- two cols : begin -->
                <div class="b-mission__two-cols">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <!-- image -->
                            <img src="/images/examples/example-mission-2.png" alt="" class="b-mission__img">
                            <!-- title small -->
                            <div class="b-mission__title-small">
                                <?= Yii::t(
                                    'mission',
                                    'Health')
                                ?>
                            </div>
                            <!-- text small -->
                            <p class="b-mission__text-small">
                                <?= Yii::t(
                                    'mission',
                                    'Health is the guarantee of a happy life for every person. We are glad that thanks to our products people can enjoy data from nature with good health!')
                                ?>
                            </p>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <!-- image -->
                            <img src="/images/examples/example-mission-3.png" alt="" class="b-mission__img">
                            <!-- title small -->
                            <div class="b-mission__title-small">
                                <?= Yii::t(
                                    'mission',
                                    'Beauty')
                                ?>
                            </div>
                            <!-- text small -->
                            <p class="b-mission__text-small">
                                <?= Yii::t(
                                    'mission',
                                    'Beauty is the driving force of the world. All that we do is aimed at improving the quality of life of every person and creating a harmonious society.')
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- two cols : end -->

                <!-- two cols : begin -->
                <div class="b-mission__two-cols">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <!-- image -->
                            <img src="/images/examples/example-mission-4.png" alt="" class="b-mission__img">
                            <!-- title small -->
                            <div class="b-mission__title-small">
                                <?= Yii::t(
                                    'mission',
                                    'Freedom')
                                ?>
                            </div>
                            <!-- text small -->
                            <p class="b-mission__text-small">
                                <?= Yii::t(
                                    'mission',
                                    'We help people to have freedom of choice, to determine their future. Vertera is not only a unique business, but an opportunity to help yourself and people in the realization of personal and professional goals. Vertera is inner freedom.')
                                ?>
                            </p>
                        </div>

                        <div class="col-sm-6 col-xs-12">
                            <!-- image -->
                            <img src="/images/examples/example-mission-5.png" alt="" class="b-mission__img">
                            <!-- title small -->
                            <div class="b-mission__title-small">
                                <?= Yii::t(
                                    'mission',
                                    'Creation')
                                ?>
                            </div>
                            <!-- text small -->
                            <p class="b-mission__text-small">
                                <?= Yii::t(
                                    'mission',
                                    'We are constantly looking for non-standard and effective solutions that help people to open their potential and achieve their goals.')
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- two cols : end -->

                <!-- title small -->
                <div class="b-mission__title-small">
                    <?= Yii::t(
                        'mission',
                        'Possessing powerful intellectual potential and great opportunities, Vertera sets ambitious goals - reviving cultural traditions, improving the nation, improving the quality of life, creating conditions for a harmonious life and the continuous development of everyone who wants it.')
                    ?>
                </div>
                <!-- right column : end -->
            </div>
        </div>
    </div>
</div>
<?php

/**
 * @var $this yii\web\View
 * @var $model Business
 */

use yii\helpers\Url;
use app\models\Business;
use app\models\ObjectFile;

$fmt = Yii::$app->getFormatter();
$fmt->defaultTimeZone = $fmt->timeZone;
$speakers = $model->getSpeakers();
$f_speaker = sizeof($speakers) ? $speakers[0] : null;
$css_cls = $model->conference_type == Business::SEMINAR ? 'sem' : 'web';
$str_date = !empty($model->start_date_text)
    ? $model->start_date_text
    : $fmt->asDate($model->start_date, 'long');
$url = Url::to(['/calendar/view', 'id' => $model->id]);
if(!$model->button_text && !$model->button_url && $model->conference_type == Business::SEMINAR) {
    $url = Url::to(['/event/view', 'id' => $model->id]);
}
?>
<div class="col-sm-3 col-xs-12">
    <!-- item start -->
    <div class="b-calendar__item-web b-calendar__item">
        <div class="b-calendar__item-<?= $css_cls ?>-title"><?= Business::$conference_types[$model->conference_type] ?></div>

        <div class="b-calendar__item-web-date"><?= $str_date ?></div>

        <div class="b-calendar__item-<?= $css_cls ?>-subtitle">
            <a data-pjax="0" href="<?= $url ?>"><?= $model->title ?></a>
        </div>

        <?php if (date('H:i', strtotime($model->start_date)) != '00:00') { ?>
            <!-- date -->
            <div class="b-calendar__item-web-time"><?= date('H:i', strtotime($model->start_date)) ?></div>
        <?php } ?>

        <?php if (!empty($model->city_name) ) { ?>
            <div class="b-calendar__item-sem-town"><?= $model->city_name ?></div>
        <?php } ?>

        <?php if ($model->button_text && $model->button_url) { ?>
            <div class="b-calendar__item-sem-btn">
                <a data-pjax="0" class="b-button-green" href="<?= $model->button_url ?>"><?= $model->button_text ?></a>
            </div>
        <?php } else if($model->conference_type == Business::SEMINAR) { ?>
            <div class="b-calendar__item-sem-btn">
                <a data-pjax="0" class="b-button-green" href="<?= $url ?>">
                    <?= Yii::t('labels', 'Watch program') ?>
                </a>
            </div>
       <?php } ?>

        <hr>

        <?php if (sizeof($speakers)) { ?>
            <table class="b-calendar__item-web-speaker">
                <tbody>
                <tr>
                    <td>
                        <?php if ($path = ObjectFile::getAssetPath($f_speaker, 'image', true)) { ?>
                            <div class="b-calendar__item-web-speaker-ava">
                                <img height="60" src="<?= $path ?>" alt="">
                            </div>
                        <?php } ?>
                    </td>
                    <td>
                        <div class="b-calendar__item-web-speaker-name"><?= $f_speaker->full_name ?></div>
                        <div class="b-calendar__item-web-speaker-info"><?= Yii::t('labels', 'Speaker') ?></div>
                    </td>
                </tr>
                </tbody>
            </table>
        <?php } ?>
    </div>
    <!-- item end -->
</div>
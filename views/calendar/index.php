<?php
/**
 * @var $this \yii\web\View
 * @var $search_model Business
 * @var $dp \yii\data\ActiveDataProvider
 * @var $speakers array
 * @var $dates array
 */

use yii\widgets\{
    Pjax,
    ListView
};
use yii\helpers\{
    Url,
    Html
};
use app\helpers\H;
use app\assets\AppAsset;
use app\models\Business;

$this->title = 'Календарь мероприятий';
$this->registerJsFile(Yii::getAlias('@web/js/calendar-list-actions.js'), [
    'depends' => [AppAsset::class]
]);
$fmt = Yii::$app->getFormatter();
$fmt->defaultTimeZone = $fmt->timeZone;

$active_start_date = empty($search_model->start_date)
    ? Yii::t('labels', 'All period')
    : ($search_model->start_date == 'upcoming'
        ? Yii::t('labels', 'Current')
        : H::mb_ucfirst($fmt->asDate($search_model->start_date, 'LLLL Y')));

$active_type = empty($search_model->type)
    ? Yii::t('labels', 'All subjects')
    : ([
            1 => Yii::t('labels', 'Business'),
            2 => Yii::t('labels', 'Product'),
        ][$search_model->type] ?? "");

$active_speaker_id = empty($search_model->speaker_id)
    ? Yii::t('labels', 'All speakers')
    : ($speakers[$search_model->speaker_id] ?? "");

$active_conference_type = empty($search_model->conference_type)
    ? Yii::t('labels', 'All types')
    : (Business::$conference_types[$search_model->type] ?? "");

?>
<div class="b-calendar">
    <div class="row">
        <div class="col-xs-12">
            <div class="b-breadcrumbs">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li><?= $this->title ?></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="b-calendar__title"><?= $this->title ?></div>
        </div>
    </div>

    <div class="row">
        <div class="b-sorting-calendar">
            <form id="calendar-form" action="<?= Url::to(['/calendar/index']) ?>">
                <div class="row">

                    <div class="col-sm-3 col-xs-12">
                        <div class="sort sort-dropdown-input">
                            <label for="" class="sort-calendar__label">
                                <?= Yii::t('labels', 'Period') ?>
                                <ul class="sort__select">
                                    <li>
                                        <a class="active" href="javascript://"><?= $active_start_date ?></a>
                                        <ul class="sort__select-drop">
                                            <li><a data-value="upcoming"
                                                   href="javascript://"><?= Yii::t('labels', 'Current') ?></a></li>
                                            <li><a data-value=""
                                                   href="javascript://"><?= Yii::t('labels', 'All period') ?></a>
                                            </li>
                                            <li class="disabled">
                                                <span><?= Yii::t('labels', 'Archive') ?></span>
                                                <hr>
                                            </li>

                                            <?php foreach ($dates as $date) { ?>
                                                <li><a data-value="<?= $date ?>" href="javascript://">
                                                        <?= H::mb_ucfirst($fmt->asDate($date, 'LLLL Y')) ?>
                                                    </a></li>
                                            <?php } ?>

                                        </ul>
                                    </li>
                                </ul>
                            </label>
                            <?= Html::activeHiddenInput($search_model, 'start_date', [
                                'class' => 'sort__input-selected',
                            ]) ?>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="sort sort-dropdown-input">
                            <label for="" class="sort-calendar__label">
                                <?= Yii::t('labels', 'Subjects') ?>
                                <ul class="sort__select">
                                    <li>
                                        <a class="active" href="javascript://"><?= $active_type ?></a>
                                        <ul class="sort__select-drop">
                                            <li><a data-value=""
                                                   href="javascript://"><?= Yii::t('labels', 'All subjects') ?></a>
                                            </li>
                                            <li><a data-value="1"
                                                   href="javascript://"><?= Yii::t('labels', 'Business') ?></a></li>
                                            <li><a data-value="2"
                                                   href="javascript://"><?= Yii::t('labels', 'Product') ?></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </label>
                            <?= Html::activeHiddenInput($search_model, 'type', [
                                'class' => 'sort__input-selected',
                            ]) ?>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="sort sort-dropdown-input">
                            <label for="" class="sort-calendar__label">
                                <?= Yii::t('labels', 'Speaker') ?>
                                <ul class="sort__select">
                                    <li>
                                        <a class="active" href="javascript://"><?= $active_speaker_id ?></a>
                                        <ul class="sort__select-drop">
                                            <li><a data-value=""
                                                   href="javascript://"><?= Yii::t('labels', 'All speakers') ?></a>
                                            </li>

                                            <?php foreach ($speakers as $id => $name) { ?>
                                                <li><a data-value="<?= $id ?>" href="javascript://"><?= $name ?></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                </ul>
                            </label>
                            <?= Html::activeHiddenInput($search_model, 'speaker_id', [
                                'class' => 'sort__input-selected',
                            ]) ?>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="sort sort-dropdown-input">
                            <!-- one sorting block -->
                            <label for="" class="sort-calendar__label">
                                <?= Yii::t('labels', 'Types') ?>
                                <ul class="sort__select">
                                    <li>
                                        <a class="active" href="javascript://"><?= $active_conference_type ?></a>
                                        <ul class="sort__select-drop">
                                            <li><a data-value=""
                                                   href="javascript://"> <?= Yii::t('labels', 'All types') ?></a>
                                            </li>
                                            <?php foreach (Business::$conference_types as $k => $name) { ?>
                                                <li><a data-value="<?= $k ?>" href="javascript://"><?= $name ?></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                </ul>
                            </label>
                            <?= Html::activeHiddenInput($search_model, 'conference_type', [
                                'class' => 'sort__input-selected',
                            ]) ?>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="b-calendar-list">
                <?php
                $cnt = $dp->getTotalCount();
                Pjax::begin(['formSelector' => '#calendar-form']);
                echo '<div class="pjax-container-loader"></div>';
                echo ListView::widget([
                    'dataProvider' => $dp,
                    'options' => [
                        'class' => 'calendar-list-view',
                    ],
                    'itemView' => '/calendar/_list_item',
                    'layout' => "{items}",
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'beforeItem' => function ($model, $key, $index) use ($cnt) {
                        return $index % 4 == 0 ? "<div class='row'>" : null;
                    },
                    'afterItem' => function ($model, $key, $index) use ($cnt) {
                        return ($index + 1) % 4 == 0 || $index + 1 == $cnt ? "</div>" : null;
                    },
                ]);
                Pjax::end();
                ?>
            </div>
        </div>
    </div>

</div>
<?php

namespace app\controllers;

use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\web\Controller;

/**
 * Class CompanyController
 * @package app\controllers
 */
class CompanyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * @return string
     */
    public function actionHistory()
    {
        return $this->render('history');
    }

    /**
     * @return string
     */
    public function actionMission()
    {
        return $this->render('mission');
    }

    /**
     * @return string
     */
    public function actionPhilosophy()
    {
        return $this->render('philosophy');
    }

    /**
     * @return string
     */
    public function actionStrategy()
    {
        return $this->render('strategy');
    }
}

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\forms\LoginForm;
use app\models\Business;
use app\models\Faq;
use app\models\News;
use app\models\Product;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return mixed
     */
    public function beforeAction($action)
    {
        if (stristr(Yii::$app->request->url, 'ru_RU')) {
            return $this->redirect(['/'])->send();
        }
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $news = News::find()
            ->where([
                'in_top' => 1,
                'status' => News::STATUS_ACTIVE,
            ])
            ->orderBy(['start_date' => SORT_DESC])
            ->all();
        $events = Business::find()
            ->where([
                'type' => [Business::TYPE_EVENT, Business::TYPE_BOTH],
                'in_top' => 1,
                'status' => Business::STATUS_ACTIVE,
            ])
            ->orderBy(['start_date' => SORT_DESC])
            ->all();
        $products = Product::find()->where([
                'in_top' => 1,
                'status' => Product::STATUS_ACTIVE,
            ])->all();
        $faq_all = Faq::find()->where([
                'in_top' => 1,
                'status' => Faq::STATUS_ACTIVE,
            ])->all();

        return $this->render('index', [
            'news' => $news,
            'events' => $events,
            'products' => $products,
            'faq_all' => $faq_all,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/admin/product']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    //static begin

    /**
     * @return string
     */
    public function actionVertera_gel()
    {
        return $this->render('static/vertera_gel');
    }

    public function actionVerteraGelSport()
    {
        return $this->render('static/vertera_gel_sport');
    }

    /**
     * @return string
     */
    public function actionBusinessVertera()
    {
        return $this->render('static/business');
    }

    /**
     * @return string
     */
    public function actionProduction()
    {
        return $this->render('static/production');
    }

    /**
     * @return string
     */
    public function actionWorldOfRetail()
    {
        return $this->render('static/worldOfRetail');
    }

    /**
     * @return string
     */
    public function actionUniversity()
    {
        return $this->render('static/university');
    }

    /**
     * @return string
     */
    public function actionPrivacyPolicy()
    {
        return $this->render('static/privacy-policy');
    }
}

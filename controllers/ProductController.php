<?php

namespace app\controllers;

use app\models\ProductHasSubCategory;
use app\models\ProductSubCategory;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use app\models\Product;
use yii\web\HttpException;

/**
 * Class ProductController
 * @package app\controllers
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = Product::getActive($id);
        $products = Product::find()
            ->where(['!=', 'id', $model->id])
            ->andWhere(['status' => Product::STATUS_ACTIVE])
            ->orderBy(new Expression('rand()'))
            ->limit(4)
            ->all();

        if (!$sub = ProductHasSubCategory::find()->where([
            'product_id' => $model->id
        ])->one()) {
            throw new HttpException('Error');
        }
        $sub_cat = ProductSubCategory::find()->where([
            'id' => $sub->sub_category_id,
        ])->one();
        $cat = $sub_cat->getParent();
        return $this->render('view', [
            'model' => $model,
            'products' => $products,
            'cat' => $cat,
            'sub_cat' => $sub_cat,
        ]);
    }
}
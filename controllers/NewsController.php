<?php

namespace app\controllers;

use app\models\ObjectFile;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use app\models\News;
use yii\web\Response;

/**
 * Class NewsController
 * @package app\controllers
 */
class NewsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actionIndex()
    {
        $q = News::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->orderBy(['start_date' => SORT_DESC])
        ;
        $dataProvider = new ActiveDataProvider([
            'query' => $q,
            'pagination' => false
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = News::getActive($id);
        $models = [];

        if ($prev = News::find()
            ->where(['<', 'start_date', "$model->start_date"])
            ->orderBy(['start_date' => SORT_DESC])->one()
        ) {
            $models['prev'] = $prev;
        }
        if ($next = News::find()
            ->where(['>', 'start_date', "$model->start_date"])
            ->orderBy(['start_date' => SORT_ASC])->one()
        ) {
            $models['next'] = $next;
        }

        return $this->render('view', [
            'model' => $model,
            'models' => $models,
        ]);
    }

    /**
     * Exports all Newa data in json format
     * @return array
     */
    public function actionExport()
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        $models = News::find()->orderBy(['start_date' => SORT_DESC])->all();
        $calendars_data = [];

        foreach ($models as $i => $model) {
            $calendars_data[$i]['attributes'] = $model->attributes;
            $calendars_data[$i]['image_url'] = ObjectFile::getAssetPath($model, 'image', true);
        }

        return $calendars_data;
    }
}
<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use app\models\{
    Product, ProductCategory, ProductHasSubCategory, ProductSubCategory
};

/**
 * Class CategoryController
 * @package app\controllers
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param $id
     * @param null $sub_id
     * @return string
     */
    public function actionView($id, $sub_id = null)
    {
        $current_cat = ProductCategory::getActive($id);
        $current_sub_cat = null;

        $q = Product::find()->where([
            'status' => Product::STATUS_ACTIVE,
        ]);

        if($sub_id && ($current_sub_cat = ProductSubCategory::find()->where([
                'id' => $sub_id,
                'parent_id' => $current_cat->id,
                'status' => ProductSubCategory::STATUS_ACTIVE,
            ])->one())) {
            $phsc = ProductHasSubCategory::find()->where([
                'sub_category_id' => $sub_id,
            ])->all();
            $q->andWhere(['id' => ArrayHelper::map($phsc, 'product_id', 'product_id')]);
        } else {
            $current_sub_cats = ProductSubCategory::find()->where([
                'parent_id' => $current_cat->id,
                'status' => ProductSubCategory::STATUS_ACTIVE,
            ])->all();
            if(sizeof($current_sub_cats)) {
                $phsc = ProductHasSubCategory::find()->where([
                    'sub_category_id' => ArrayHelper::map($current_sub_cats, 'id', 'id'),
                ])->all();
                $q->andWhere(['id' => ArrayHelper::map($phsc, 'product_id', 'product_id')]);
            }
        }

        $dp = new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);

        return $this->render('view', [
            'current_cat' => $current_cat,
            'current_sub_cat' => $current_sub_cat,
            'sub_id' => $sub_id,
            'dp' => $dp,
        ]);
    }
}

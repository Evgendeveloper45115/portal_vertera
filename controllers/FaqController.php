<?php

namespace app\controllers;

use yii\data\ActiveDataProvider;
use yii\web\Controller;
use app\models\Faq;
use app\models\FaqCategory;

/**
 * Class FaqController
 * @package app\controllers
 */
class FaqController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param null $category_id
     * @return string
     */
    public function actionIndex($category_id = null)
    {
        $category = null;

        if ($category_id)
            $category = FaqCategory::getActive($category_id);

        $categories = FaqCategory::findAll(['status' => Faq::STATUS_ACTIVE]);
        $query = Faq::find()->filterWhere([
            'status' => Faq::STATUS_ACTIVE,
            'category_id' => $category_id,
        ]);
        $dp = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('index', [
            'dp' => $dp,
            'current_category' => $category,
            'categories' => $categories,
        ]);
    }
}

<?php

namespace app\controllers;

use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use app\models\Promotion;

/**
 * Class PromotionController
 * @package app\controllers
 */
class PromotionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Promotion::find()
                ->where(['<=', 'start_date', date('Y:m:d H:i:s')])
                ->andWhere(['>=', 'end_date', date('Y:m:d')])
                ->andWhere(['status' => Promotion::STATUS_ACTIVE])
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionView($id)
    {
        if (!$id) {
            throw new BadRequestHttpException('Неверный адрес.');
        }

        if (!$model = Promotion::findOne($id)) {
            return $this->redirect(['/site']);
        }

        return $this->render('view', ['model' => $model]);
    }
}
<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use app\models\{
    Product, ProductCategory, ProductHasSubCategory, ProductSubCategory
};

/**
 * Class CategoryController
 * @package app\controllers
 */
class ExternalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param $session_key
     * @return \yii\web\Response
     */
    public function actionAuthorization($session_key)
    {
        return $this->redirect(['/']);
    }
}

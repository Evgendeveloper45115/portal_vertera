<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\{
    Business, BusinessHasSpeaker, ObjectFile, Speaker
};
use yii\web\Response;

/**
 * Class CalendarController
 * @package app\controllers
 */
class CalendarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $search_model = new Business();
        $search_model->start_date = 'upcoming';

        $q = Business::find()->alias('b')->where([
            'b.type' => [
                Business::TYPE_CALENDAR,
                Business::TYPE_BOTH,
            ],
            'b.status' => Business::STATUS_ACTIVE,
        ]);

        if ($search_model->load(Yii::$app->getRequest()->get())) {
            if ((int)$search_model->type) {
                //$q->andWhere(['b.type' => $search_model->type]);
            }
        }

        if ($search_model->start_date == 'upcoming') {
            $d = new \DateTime();
            $q->andWhere([
                '>',
                'b.start_date',
                $d->format('Y-m-d'),
            ]);
        } elseif ($search_model->start_date) {
            $d = new \DateTime($search_model->start_date);
            $q->andWhere([
                'between',
                'b.start_date',
                $d->modify('first day of this month')->format('Y-m-d'),
                $d->modify('last day of this month')->format('Y-m-d'),
            ]);
        }

        if ((int)$search_model->conference_type) {
            $q->andWhere([
                'b.conference_type' => $search_model->conference_type,
            ]);
        }

        if ((int)$search_model->speaker_id) {
            $q->leftJoin(['bhs' => BusinessHasSpeaker::tableName()], 'b.id = bhs.business_id')->andWhere([
                'bhs.speaker_id' => $search_model->speaker_id,
            ]);
        }

        $q->orderBy(['start_date' => SORT_ASC]);

        $dp = new ActiveDataProvider([
            'query' => $q,
            'pagination' => false,
        ]);
        $speakers = Speaker::find()->where(['status' => Speaker::STATUS_ACTIVE])->orderBy(['full_name' => SORT_ASC])->all();
        $dates = Business::find()
            ->select([
                'DATE_FORMAT(start_date, \'%Y-%m-%d\') start_date',
            ])
            ->where([
                'type' => [
                    Business::TYPE_CALENDAR,
                    Business::TYPE_BOTH,
                ],
                'status' => Business::STATUS_ACTIVE,
            ])
            ->groupBy(new Expression('DATE_FORMAT(start_date, \'%Y-%m\')'))
            ->orderBy(['start_date' => SORT_DESC])
            ->column();

        if (Yii::$app->getRequest()->getIsAjax()) {
            return $this->renderAjax('index', [
                'search_model' => $search_model,
                'dp' => $dp,
                'dates' => $dates,
                'speakers' => ArrayHelper::map($speakers, 'id', 'full_name'),
            ]);
        }

        return $this->render('index', [
            'search_model' => $search_model,
            'dp' => $dp,
            'dates' => $dates,
            'speakers' => ArrayHelper::map($speakers, 'id', 'full_name'),
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = Business::getActive($id);
        $models = [];

        if ($prev = Business::find()
            ->where(['<', 'start_date', "$model->start_date"])
            ->andWhere([
                'type' => [Business::TYPE_CALENDAR, Business::TYPE_BOTH],
                'status' => Business::STATUS_ACTIVE,
            ])
            ->orderBy(['start_date' => SORT_DESC])->one()
        ) {
            $models['prev'] = $prev;
        }
        if ($next = Business::find()
            ->where(['>', 'start_date', "$model->start_date"])
            ->andWhere([
                'type' => [Business::TYPE_CALENDAR, Business::TYPE_BOTH],
                'status' => Business::STATUS_ACTIVE,
            ])
            ->orderBy(['start_date' => SORT_ASC])->one()
        ) {
            $models['next'] = $next;
        }

        return $this->render('/event/view', [
            'model' => $model,
            'models' => $models,
        ]);
    }

    /**
     * Exports all Business data in json format
     * @return array
     */
    public function actionExport()
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        $models = Business::find()->orderBy(['start_date' => SORT_ASC])->all();

        $calendars_data = [];

        foreach ($models as $i => $model) {
            $speakers_data = [];
            $image_url = null;

            if ($path = ObjectFile::getAssetPath($model, 'image', true)) {
                $image_url = $path;
            }

            foreach ($model->getSpeakers() as $k => $speaker) {
                $speakers_data[$k]['attributes'] = $speaker->attributes;
                $speakers_data[$k]['image_url'] = null;

                if ($path = ObjectFile::getAssetPath($speaker, 'image', true)) {
                    $speakers_data[$k]['image_url'] = $path;
                }
            }

            $calendars_data[$i]['attributes'] = $model->attributes;
            $calendars_data[$i]['image_url'] = $image_url;
            $calendars_data[$i]['speakers_data'] = $speakers_data;
        }

        return $calendars_data;
    }
}

<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Question;
use app\models\Contact;

/**
 * Class Contact Controller
 * @package app\controllers
 */
class ContactController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $question = new Question();
        $question->setScenario('client-create');

        if($question->load(\Yii::$app->getRequest()->post())) {
            $question->status = Question::STATUS_ACTIVE;
            if($question->save()) {
                \Yii::$app->getSession()->setFlash('success', \Yii::t('system', 'Success'));
            } else {
                \Yii::$app->getSession()->setFlash('error', \Yii::t('system', 'Error'));
            }
            return $this->refresh();

        }

        $models = Contact::find()->filterWhere([
            'status' => Contact::STATUS_ACTIVE,
        ])->all();

        return $this->render('index', [
            'question' => $question,
            'models' => $models,
        ]);
    }
}

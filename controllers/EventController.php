<?php

namespace app\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Business;

/**
 * Class EventController
 * @package app\controllers
 */
class EventController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actionIndex()
    {
        $q = Business::find()->where([
            'type' => [Business::TYPE_EVENT, Business::TYPE_BOTH],
            'status' => Business::STATUS_ACTIVE,
        ])->andWhere(['not', [
            'image_id' => null,
        ]])->orderBy([
            'start_date' => SORT_DESC,
        ]);
        $dp = new ActiveDataProvider([
            'query' => $q,
            'pagination' => false,
        ]);

        return $this->render('index', [
            'dp' => $dp,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = Business::getActive($id);
        $models = [];

        if($prev = Business::find()
            ->where(['<', 'start_date', "$model->start_date"])
            ->andWhere([
                'type' => [Business::TYPE_EVENT, Business::TYPE_BOTH],
                'status' => Business::STATUS_ACTIVE,
            ])
            ->orderBy(['start_date' => SORT_DESC])->one()) {
            $models['prev'] = $prev;
        }
        if($next = Business::find()
            ->where(['>', 'start_date', "$model->start_date"])
            ->andWhere([
                'type' => [Business::TYPE_EVENT, Business::TYPE_BOTH],
                'status' => Business::STATUS_ACTIVE,
            ])
            ->orderBy(['start_date' => SORT_ASC])->one()) {
            $models['next'] = $next;
        }

        return $this->render('view', [
            'model' => $model,
            'models' => $models,
        ]);
    }
}

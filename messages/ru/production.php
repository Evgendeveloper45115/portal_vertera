<?php
return [
    'The main function of the manufacturing company is to produce high-effective, natural and safe products for prevention and rehabilitation.' => 'Главная функция производственной компании — производить <br> высокоэффективную, натуральную и безопасную 
  продукцию для профилактики и реабилитации.',
    'Production' => 'Производство',
    'Quality <br> assurance' => 'Гарантия <br> качества',
    'Open Company NPO "Biotechnologies"' => 'ООО НПО «Биотехнологии»',
    'All the products represented
                 in the Vertera range are exclusively natural!' => 'Вся продукция, представленная в линейке Vertera, является исключительно натуральной!',
    'The main purpose of the company is to produce highly 
                            effective, natural and safe products for preserving health and
                             increasing life expectancy.' => 'Главное назначение компании - производить высокоэффективную,<br>натуральную
     и безопасную продукцию для сохранения здоровья и увеличения продолжительности жизни.',
    'At the enterprises of NPO BIOMEDITSINSK INNOVATIVE TECHNOLOGIES LLC and NGO <br> BIOTECHNOLOGY, a food safety
                       management system was introduced, certified in accordance with the requirements of GOST R ISO 22000-2007 (ISO 22000: 2005),
                       which includes the principles of HACCP' => 'На предприятиях ООО НПО «БИОМЕДИЦИНСКИЕ ИННОВАЦИОННЫЕ ТЕХНОЛОГИИ» и ООО НПО<br> «БИОТЕХНОЛОГИИ»
      внедрена система менеджмента безопасности пищевой продукции, сертифицирована в соответствии с<br> требованиями 
      ГОСТ Р ИСО 22000-2007 (ИСО 22000:2005), включающий принципы ХАССП',
    'The company LLC "Biomedical Innovative Technologies" <br> (Tver)
                    produces the main product Vertera Gel and cosmetic products.' => 'Компания ООО НПО «БИОТЕХНОЛОГИИ» (г. Тверь)
      специализируются на производстве сухих пищевых фитокомпозиций,<br> чайных напитков. Количественная рецептура продуктов тщательно оберегается.',
    'The company LLC NPO "BIOTECHNOLOGY" (Tver) specialize in 
                     the production of dry food phytocompositions, for tea drinks. The quantitative formulation of products is carefully guarded.' => 'Компания ООО НПО «Биомедицинские Инновационные Технологии»<br>(г. Тверь) производит главный продукт 
     Vertera Gel и косметические<br> средства.',
    'Every year is tested and certified for compliance with 
                             international standards (in the EC countries).' => 'Ежегодно проходит проверку и аттестацию на соответствие международным стандартам (в странах EC).',
    'It uses algae produced in the north of Russia;' => 'Использует водоросли, добываемые на севере России;',
    'Produces absolutely safe, natural products;' => 'Производит абсолютно безопасную, натуральную продукцию;',
    'Carried out a lot of clinical research, which proved
                             the highest effectiveness of Vertera Gel as a dietary therapeutic 
                             food product for various problems;' => 'Провела массу клинических исследований, которые доказали высочайшую эффективность Vertera Gel как
     диетического лечебного продукта питания при различной проблематике;',
    'Awarded numerous awards for innovations in the field 
                             of dietary health nutrition;' => 'Удостоена многочисленных наград
     за инновации в области диетического лечебного питания;',
    'Uses its own production facilities;' => 'Использует собственные производственные мощности;',
    "For many years he has been cooperating with Professor Vitaliy Naumovich Korzun, who is the scientific director of the 
                             clinical research conducted by the Company. VN Korzun created the company's fundamental product, Vertera Gel, has been 
                             studying brown seaweed for more than 40 years and is one of the world's leading experts in this field;" => 'Долгие годы 
    сотрудничает с профессором Виталием Наумовичем Корзуном, который является научным руководителем проводимых Компанией 
    клинических исследований. В.Н.Корзун создал фундаментальный продукт Компании, Vertera Gel, более 40 лет занимается 
    исследованием бурых морских водорослей и является одним из ведущих специалистов мирового уровня в этой области;',
    'The technology of production is patented and is 
                             kept in the strictest secrecy;' => 'Технология производства запатентована и держится в строжайшем секрете;',
    'In production, it uses technologies used in the military space industry, as well as its own developments;' => 'При производстве 
    использует технологии, применяемые в военно-космической отрасли, а также собственные разработки;',
    'More than 11 years specializes exclusively in the 
                            production of products from sea brown algae, thanks to which the
                            technology of production is brought to perfection;' => 'Более 11 лет специализируется исключительно на производстве 
     продукции из морских бурых водорослей, благодаря чему технология производства продукции доведена до совершенства;',
    'The company has its own production, procuring, laboratory and research facilities.' => 'Компания имеет собственные 
    производственную, заготовительную, лабораторную и научно-исследовательскую базы.',
    'LLC NPO "Biomedical Innovative Technologies"' => 'ООО НПО «Биомедицинские Инновационные Технологии»',
];
<?php
return [
    'Main' => 'Главная',
    'Registration' => 'Регистрация',
    'Login' => 'Войти',

    'Administrator' => 'Администратор',
    'Vertera Gel' => 'Вертера гель',

    'Declaration of conformity' => 'Декларация о соответствии',
    'Back to the list' => 'Обратно к списку',
    'Certificates' => 'Сертификаты',

    'Official website of company Vertera' => 'Официальный сайт компании Vertera',

    'News' => 'Новости',
    'All news' => 'Все новости',
    'Previous news' => 'Предыдущая новость',
    'Next news' => 'Следующая новость',

    'Archive' => 'Архив',
    'Period' => 'Период',
    'Current' => 'Текущий',
    'All period' => 'Весь период',

    'Subjects' => 'Тематика',
    'All subjects' => 'Все тематики',
    'Business' => 'Бизнес',
    'Product' => 'Продукт',

    'Cost' => 'Цена',
    'Points' => 'Баллы',
    'In the package' => 'В упаковке',
    'Share' => 'Поделиться',
    'Buy from e-shop' => 'Купить в интернет-магазине',
    'Vendor code' => 'Артикул',
    'Description' => 'Описание',
    'Consist' => 'Состав',
    'Use' => 'Применение',

    'Speaker' => 'Спикер',
    'All speakers' => 'Все спикеры',

    'Types' => 'Типы',
    'All types' => 'Все типы',
    'MSK' => 'МСК',

    'Events' => 'Мероприятия',
    'All events' => 'Все мероприятие',
    'Previous event' => 'Предыдущие мероприятие',
    'Next event' => 'Следующее мероприятие',

    'Watch program' => 'Смотреть программу',

    'Technical requirements' => 'Технические требования',
    'To participate in the webinar you need a computer with a stable connection to the Internet at a speed of at least 700kbps and a modern web browser.' => 'Для участия в вебинаре нужны компьютер с устойчивым подключением к интернету на скорости не менее 700kbps и современный веб-браузер.',

    'Vertera Official Representative Offices' => 'Официальные Представительства Vertera',
    'Contact<br>information' => 'Контактная<br>информация',
    'More effect <br>in the complex' => 'Больше эффекта <br>в комплексе',
];
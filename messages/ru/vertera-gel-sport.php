<?php
return [
    'It destroys fat,<br> not the heart with the kidneys!' => 'Уничтожает жир,<br>а не сердце с почками!',
    'Specialized food product <br> for nutrition of athletes and <br> active people in gel form' => 'Специализированный пищевой продукт <br>для питания спортсменов и <br> активных людей в гелевой форме',
    'Volume <br> in packing' => 'Объем <br>в упаковке',
    'g' => 'г',
    '$' => 'a',
    'Price<br> per packing' => 'Цена <br>за упаковку',
    'Fat reduction' => 'Снижение жира',
    'Recovery' => 'Восстановление',
    'Training' => 'Тренировки',
    'Daily' => 'Ежедневно',
    'Endurance' => 'Выносливость',
    'Slowing of catabolism' => 'Замедление катаболизма',
    'Natural composition' => 'Натуральный состав',
    'Decreased fat mass' => 'Снижение жировой массы',
    'A full meal. You can easily <br> lose weight by replacing evening meals!' => 'Полноценный прием пищи. Вы сможете легко<br> похудеть, заменив вечерние приемы пищи!',
    '200 grams of product at a time' => '200 грамм продукта на один прием',
    '1/3 cans' => '1/3 банки',
    'The servings contain' => 'В порции содержится',
    'protein - 13 grams' => 'белка — 13 грамм',
    'Carbohydrates - 22 grams' => 'Углеводов — 22 грамма',
    'algal lipids - 4 grams' => 'водорослевых липидов — 4 грамма',
    'In a serving only<br><strong> 144 kcal</strong> <span class="font-rc">WITHOUT SUGAR</span>' => '
     В порции лишь <br>
                    <strong>144 ккал</strong>
                    <span class="font-rc">БЕЗ САХАРА</span>',
    'Comparison by Three Positions' => 'Сравнение по Трем позициям',
    'Comparison by Three Positionseffectiveness, accessibility and simplicity <br> of the most known methods and methods of losing weight'
    => 'эффективность, доступность и простота<br>наиболее известных способов и методов похудения',
    'Efficiency' => 'Эффективность',
    'Availability' => 'Доступность',
    'Simplicity' => 'Простота',
    'Individual <br>diets' => 'Индивидуальные <br>диеты',
    'Additives that<br> "reduce" the appetite' => 'Добавки,<br>«снижающие» аппетит',
    'Cleaning <br> fees' => 'Очистительные<br>сборы',
    'Vertera gel sport <br>active' => 'Vertera gel sport <br>active',
    'Excellent' => 'Отлично',
    'Good' => 'Хорошо',
    'Bad' => 'Плохо',
    'Using Vertera Gel Sport Active, you can, besides losing weight, solve many other tasks to improve health. 
    The stronger health, the more likely you are not only to make your figure attractive, but also to keep it for 
    years to come!' => 'Употребляя Vertera Gel Sport Active, вы можете,
     кроме похудения, решать и многие другие задачи по укреплению здоровья.
      Чем крепче здоровье, тем у вас больше шансов не только сделать свою фигуру привлекательной, 
      но и сохранить ее на долгие годы!',
    'Effectively restores physical working capacity by replenishing muscle glycogen stores simultaneously with the
     improvement of trophic status of muscle tissue, activation of energy metabolism, detoxification'
    =>
        'Эффективно восстанавливает физическую работоспособность путем пополнения запасов гликогена мышц одновременно с
     улучшением трофического статуса мышечной ткани, активацией энергетического обмена,  детоксикации',
    'An ideal combination of macro-, micro-ultra elements, vitamins, amino acids, carbohydrates and protein with the 
    highest biological value.'
    =>
        'Идеальное сочетание макро-, микро- ультраэлементов, витаминов, аминокислот, углеводов и
                    белка с высочайшей биологической ценностью.',
    'Ideal for feeding athletes during intensive training in addition to the basic diet in accordance with the program 
    developed for these sports.'
    =>
        'Идеально для питания спортсменов в период интенсивных тренировок в дополнение к основному рациону в
                соответствии с программой, разработанной для данных видов спорта.',
    'Pea protein (protein) in sports nutrition is a highly purified isolate with a content of
                     protein 84-88%, which has a high digestibility of 98%. Pea protein is richer than others
                     vegetable proteins by protein content, which is the most important indicator for muscle growth,
                     since after water proteins are the second major component of the structure of muscle fibers.'
    =>
        'Белок гороха (протеин) в спортивном питании представляет собой высокоочищенный изолят с содержанием
                    белка 84-88 %, обладающий высокой усвояемостью, равной 98 %. Гороховый протеин богаче других
                    растительных протеинов по содержанию белка, что является важнейшим для роста мышц показателем,
                    поскольку после воды белки являются вторым основным компонентом структуры мышечных волокон.',
    'Instructions' => 'Инструкция',
    'When recruiting dry mass and fat burning, eat 200 g in the morning, 30 minutes before training and at night before 
    going to bed.' => 'При наборе сухой массы и жиросжигании съедать 200 г утром, за 30 минут до тренировки и на ночь перед сном.',
    'In case of problems with falling asleep, cancel the appointment before bedtime' => 'В случае проблем с засыпанием, отменить прием перед сном',
    'Regular use of Vertera Gel Sport Active neutralizes the consequences of the use of chemical elements in the diet of 
    athletes, provides the health of the liver, digestive tract, thyroid and cardiovascular system. Improves skin and hair.'
    => 'Регулярное употребление Vertera Gel Sport Active нейтрализует последствия употребления химических
                    элементов в питании спортсменов, обеспечивает здоровье печени, ЖКТ, щитовидной железы и
                    сердечно-сосудистой системы. Улучшает состояние кожи и волос.',
    'An additional source of soluble dietary fiber (alginates), iodine in organic form and calcium.' =>
        'Дополнительный источник растворимых пищевых волокон (альгинатов), йода в органической форме и
                    кальция.',
    'Ready to use. Nourishes, restores and exerts a powerful detoxification effect!' =>
        'Готов к употреблению. Питает, восстанавливает и оказывает мощный Детоксикационный эффект!',
    'Arginine is the most important amino acid for those who want to build muscle, because it releases growth hormone and synthesizes creatine together with other substances.' =>
        'Аргинин – наиважнейшая аминокислота для тех, кто хочет нарастить мышцы, потому как он высвобождает гормон
                роста и синтезирует вместе с другими веществами креатин.',
    'The pea protein contains a large content of essential amino acids, which are inevitably lost during stress, and the
     muscles suffer from the shaking. Therefore pea protein is the best reductant. The greatest amount of it contains arginine,
      which in many other products of sports nutrition is usually very small.'
    => 'В гороховом белке содержится большое содержание незаменимых аминокислот, которые неизбежно
                    утрачиваются во время нагрузок, и мышцы страдают от крепатуры. Поэтому гороховый протеин является
                    лучшим восстановителем. Наибольшее количество в нем содержится аргинина, которого во многих других
                    продуктах производства спортивного питания обычно очень мало.',
    'Arginine' => 'Аргинин',
    'Arginine is contained in an amount of 8.7% per gram of protein, which is higher than in any other source of protein' =>
        'Аргинин содержится в количестве - 8,7%
                    на грамм белка, что выше, чем в любом другом источнике белка',
    'Arginine in combination with other amino acids gives an incredible synergy effect.' => 'Аргинин в сочетании с другими аминокислотами дает невероятный эффект синергии.',
    'The use of Vertera gel Activ after training will minimize the process of catabolism (destruction) of muscles,
     thanks to the supply of the necessary quantity of high-quality proteins and carbohydrates to the body.' =>
        'Употребление Vertera gel Activ после тренировок сведет к минимуму процесс катаболизма (разрушения)
                    мыщц, благодаря поставкам в организм необходимого количества качественных белков и углеводов.',
    'The product is completely safe for the body and does not have such terrible side effects as chemical drugs 
    (destruction of the cardiovascular system, metabolic disorders, development of allergies, infertility, etc.' =>
        'Продукт полностью безопасен для организма и не имеет такие страшные побочные эффекты, как
                    химические препараты (разрушение сердечно-сосудистой системы, нарушение обмена веществ, развитие
                    аллергии, бесплодия и тд',
    '100% Natural composition' => '100% Натуральный состав',
    'The basis of the product is a specialized food product, a seaweed gel for dietary nutritional therapy Vertera, which
     contains a colossal set of micro, macro and ultra elements necessary for us as air.' =>
        'Основа продукта – специализированный пищевой продукт, гель из морских водорослей для диетического
                лечебного питания Vertera, который содержит колоссальный набор микро-, макро- и ультраэлементов
                необходимых нам как воздух.',
    'Product structure' => 'Cостав продукта',
    'Water' => 'Вода',
    'blackcurrant juice concentrated' => 'сок черной смородины концентрированный',
    'a specialized food product of dietary therapeutic nutrition from Vertera kelp <span> (drinking water, kelp, ancillary
 components, citric acid, calcium gluconate) </span>' =>
        'специализированный пищевой продукт диетического лечебного питания из ламинарии Vertera <span>(вода питьевая, ламинария, вспомогательные компоненты, кислота лимонная , глюконат кальция)</span>',
    'pea protein' => 'гороховый белок',
    'algal alginate' => 'водорослевый альгинат',
    'Black currant powder, natural food flavoring' => 'Порошок черной смородины, ароматизатор натуральный пищевой',
    'Why should athletes regularly<br> use Vertera Sport Gel Active?' => 'Почему спортсменам необходимо регулярно <br>употреблять Vertera Sport Gel Active?',
    'Balanced on BZHU and macro-, micro- and ultra-elements, a full-time one-time meal. 1 total serving = 144 kcal. 
        The product does not contain sugar and other derivatives (fructose, maltodextrin, etc.)' =>
        'Сбалсированный по БЖУ и макро-, микро- и ультраэлементам, полноценный разовый прием пищи. 1
                        полная порция = 144 ккалории. Продукт не содержит сахара и прочих его производных (фруктоза,
                        мальтодекстрин и тд)',
    'Increase immunity sporstmena in conditions of training loads. It is expressed in the restoration to normal levels of
     concentrations of immunoglobulins A and G, as well as the complement component of C3.' =>
    'Повышение иммунитета спорстмена в условиях тренировочных нагрузок. Выражается в восстановлении
                        до нормального уровня концентраций иммуноглобулинов А и G, а также компонента комплемента СЗ.',
    'Regulation of fat, protein, carbohydrate and energy metabolism' => 'Регуляция жирового, белкового, углеводного и энергетического обмена',
    'Neutralization of consequences of the use of chemical components of sports nutrition.' => 'Нейтрализация последствий употребления химических компонентов спортивного питания.',
    'Normalization of functions and protection of the cardiovascular system.' => 'Нормализации функций и защиты сердечно-сосудистой системы.',
    'Protection and health of the liver.' => 'Защита и здоровье печени.',
    'Health of the skin, hair, nails.' => 'Здоровья кожи, волос, ногтей.',
    'As a result of the use of gel occurs' => 'В результате употребления геля происходит',
    'We do not compete with manufacturers of sports nutrition, we neutralize the negative effects of sports nutrition and loads!'
    => 'Мы не конкурируем с производителями спортивного питания, мы нейтрализуем негативные последствия
                    спортивного питания и нагрузок!',
    'Try it and see for yourself!' => 'Попробуйте и убедитесь сами! ',
    'Specialized food <br> for athletes' => 'Специализированный пищевой<br>продукт для питания спортсменов',
    'Certificates' => 'Сертификаты',
    'Customs Union of the Republic of Belarus, the Republic of Kazakhstan and the Russian Federation' => 'Таможенный союз Республики Беларусь, Республики Казахстан и Российской Федерации',
    'Certificate of compliance with the requirements of GOST R ISO 22000-2007 (ISO 22000-2005)' => 'Сертификат соответствия требованиям ГОСТ Р ИСО 22000-2007 (ИСО 22000-2005)',
    'Certificate of state registration' => 'Свидетельство о государственной регистрации',
    'The protocol of testing the gel sample for the amino acid composition' => 'Протокол испытаний образца геля на аминокислотный состав',
];
<?php
return [
    'Welcome to the team' => 'Добро пожаловать в команду',
    'healthy, successful and happy' => 'здоровых, успешных и счастливых',
    'Start your business with the step-by-step tutorial <br> system "Vertera University"' => 'Начните ваш бизнес с пошаговой обучающей <br> системы «Vertera Университет»',
    'Unlimited <br> payments' => 'Неограниченные <br> выплаты',
    "On the leader's ranks you receive income not only from your structure, but also from the entire international turnover of the company."
    => 'На лидерских рангах Вы получаете доход не только от своей структуры, но и от всего международного товарооборота компании.',
    'people' => 'людей!',
    'of' => 'От',
    'Dynamic<br>level<br>compression' => 'Динамическая<br>компрессия<br>уровней',
    "To receive the Leader's Bonus from 8 levels: 1%, 5%, 5%, 6%, 6%, 7%, 7%, 8%, as well as all marketing plan privileges, you need to make a minimum purchase of everything from 60 points to receive a reward or 120 points per month for participation in the automotive and housing programs, travel program." => 'Чтобы получать Лидерский бонус с 8-ми уровней:
  1%,5%,5%,6%,6%,7%,7%,8%, а также все привилегии по маркетинг-плану, Вам нужно делать минимальную закупку всего 
  от 60 баллов для получения вознаграждения или от 120 баллов в месяц для участия в автомобильной и жилищной 
  программах, программе путешествий.',
    "Leader's <br> reward" => 'Лидерское <br> вознаграждение',
    'Even minimal personal activity is enough to start receiving the Partner Bonus from 5 levels (20%, 5%, 5%, 5%, 10%). You need to make a minimum purchase of only 30 points!' => 'Даже минимальной личной активности достаточно, чтобы начать
 получать Партнерский бонус с 5-ти уровней (20%, 5%, 5%, 5%,10%). Вам нужно делать минимальную закупку 
 всего в 30 баллов!',
    'Affiliate <br> compensation' => 'Партнерское <br> вознаграждение',
    'Loyalty <br> program' => 'Программа <br> лояльности',
    'The returnable commodity discount is 25% of the value of personal volume for the reporting period. You can collect the accrued amount at the trusted Service Center of the Company in the next calendar month.'
    => 'Возвратная товарная скидка – 25% от стоимости личного объема по итогам отчетного периода. Вы можете отоварить начисленную
 сумму на доверенном Центре обслуживания Компании в следующем календарном месяце',
    'Start earning immediately! Instant accruals on the Partner bonus immediately upon payment of the product by the partner.'
    => 'Начните зарабатывать немедленно! Моментальные начисления по Партнерскому бонусу сразу по факту оплаты продукта партнером.',
    'Money immediately' => 'Деньги сразу',
    'Take the first step to <br> a successful future today!' => 'Сделайте первый шаг к успешному <br> будущему уже сегодня!',
    'You want to invest your time and energy in business with the company, achieve financial independence, determine a stable and successful future for many years for you and your family' => 'Вы хотите инвестировать свое время и энергию в бизнес 
 с компанией, достичь финансовой независимости, определить стабильное и успешное будущее на долгие года для Вас и Вашей 
 семьи',
    'Start training' => 'Начать обучение!',
    'You receive rewards not only with the sale and promotion of Vertera products, but also with the distribution of customers to you from the advertising campaign of products in the media, the Internet provided by Hermes promontories, and also through the clients of the World of Trade platform.' => 'Вы получаете вознаграждения не только с продажи и продвижения продуктов Vertera, но и с распределения Вам клиентов с рекламной кампании продуктов в СМИ, Интернете, обеспечиваемой промонаборами 
  «Гермес», а также благодаря клиентам платформы «Мир торговли».',
    'Benefits of business <br>  with Vertera' => 'Преимущества бизнеса <br> с Vertera',
    'Income from three <br> sources' => 'Доход из трех <br> источников',
    'You want to invest your time and energy in business with the company, 
achieve financial independence, 
determine a stable and successful future for many years for you and your family' => 'Вы хотите инвестировать свое время и энергию в бизнес с компанией, достичь финансовой независимости, определить стабильное и успешное будущее на долгие года для Вас и Вашей семьи',
    'Stable <br> international <br> business' => 'Стабильный <br> международный <br> бизнес',
    "You decided to devote yourself seriously to cooperation with the company, which means it's time to discover the main part of the company's marketing plan. Your goal is to realize yourself as a leader, talented, purposeful and ambitious person. And most importantly, help in this and your team-mates." => 'Вы решили серьезно посвятить себя сотрудничеству с компанией, а значит, пришло время открыть для себя главную часть маркетинг-плана компании. Ваша цель — реализовать себя как лидера, талантливого, целенаправленного и амбициозного человека. А главное, помочь в этом и Вашим Партнерам по команде.',
    'Basic <br> income' => 'Основной <br> доход',
    'You want to do business with the company as your main job and receive a corresponding income of 55,000 rubles a month or more' => 'Вы хотите заниматься бизнесом с компанией в качестве своей основной работы и получать соответствующий доход от 55 000 рублей в месяц и более',
    'up to' => 'до',
    'A huge number of people of very different ages and professions have already reached millions of checks! They did it, you will get it! We are ready to offer you a working plan, how to create your business in order to realize all your dreams, be successful, beautiful, healthy and abundant. Take your chance right now! The exact choice means success!' => 'Огромное количество людей самого разного возраста и профессий уже достигли миллионных чеков! У них это получилось, получится и у Вас! Мы готовы предложить Вам работающий план, как создать своё дело, чтобы осуществить все свои мечты, быть успешным, красивым, здоровым и изобильным. Воспользуйтесь своим шансом прямо сейчас! Точный выбор означает успех!',
    '$' => '₽',
    'You use the product of the Company, tell about it to your relatives, friends and relatives. Thus you have your own group of regular customers. At the same time, you spend several hours a week motivating the most active consumers from this group, who also work with their closest associates.' => 'Вы пользуетесь продуктом Компании, рассказываете о нем своим родным, друзьям и близким. Таким образом у Вас формируется своя группа постоянных Клиентов. При этом Вы тратите несколько часов в неделю для мотивации из этой группы наиболее активных потребителей, которые также работают со своим ближайшим окружением.',
    'You are interested in additional income, provided that you can give this business several hours a week' => 'Вас интересует дополнительный доход при условии, что Вы можете уделять этому бизнесу несколько часов в неделю',
    'Additional income' => 'Дополнительный <br> доход',
    'Choose for yourself a comfortable regime for <br> development and cooperation with the Company.' => 'Выберите для себя комфортный режим<br>развития и сотрудничества с Компанией.',
    'You determine how long you are willing to invest <br> in your business and how much to earn!' => 'Вы сами определяете, сколько времени готовы инвестировать <br> в свой бизнес и сколько зарабатывать!',
    'Your opportunities are limited only by <br> your ambitions, desires and priorities!' => 'Ваши возможности ограничены только <br> вашими амбициями, желаниями и приоритетами!',
    'Embark on a journey <br> around the world' => 'Отправьтесь в путешествие <br> по всему миру',
    'Pay for education for <br> yourself and your children' => 'Оплатите образование <br> себе и своим детям',
    'Refresh the atmosphere <br> in the house' => 'Обновите обстановку <br> в доме',
    'Buy a new <br> car' => 'Купите новый <br> автомобиль',
    'Become a partner' => 'Стать партнером',
    "It's time to start embodying <br> a life of cherished dreams!" => 'Пришло время начать воплощать <br> в жизнь заветные мечты!',
    'We understand what is important to every person, and we offer unique
products for quality life and successful business. With Vertera, you can easily change your life for the better!'
    => 'Мы понимаем, что важно каждому человеку, и предлагаем уникальные
продукты для качественной жизни и успешного бизнеса. С компанией Vertera Вы можете легко изменить свою жизнь к лучшему!',
    'In the company Vertera there is no interruption in the payment of bonuses! The action of double dynamic compression is a significant additional income!'
    => 'В компании Vertera нет прерывания в выплате бонусов! Действие двойной динамической компрессии — значительный дополнительный доход!',
    'Special<br>projects<br>for partners' => 'Специальные<br>проекты<br>для партнеров',
    'Automotive and Residential Program' => 'Автомобильная и жилищная программа',
    'Traveling at company expense' => 'Путешествия за счет компании',
    'Learning system "University of Vertera" - a clear step-by-step action plan to achieve success' => 'Система обучения «Университет Vertera» — четкий пошаговый план действий к достижению успеха',
    'The "World of Trade" platform is a global project with a huge perspective for the next few decades in the field of e-commerce' => 'Платформа «Мир торговли» — глобальный проект с огромной перспективой на ближайшие десятки лет в сфере электронной коммерции',
    'Family<br>Business' => 'Cемейный<br>бизнес',
    'Receiving residual income and building a family business system for inheritance ID in the Company' => 'Получение остаточного дохода и построение системы семейного бизнеса для передачи по наследству ID в Компании',
    'You are ready to create your own business' => 'Вы готовы создать свой бизнес',
    'Create a business' => 'Создать бизнес',
];
<?php
return [
    'ID' => '#',
    'Image' => 'Изображение',
    'Title' => 'Название',
    'Description' => 'Описание',
    'Type' => 'Тип',
    'Conference Type' => 'Тип конференции',

    'Button Text' => 'Текст кнопки',
    'Button Url' => 'Ссылка кнопки',
    'Start Date' => 'Назначенная дата',

    'Status' => 'Статус',
    'Created at' => 'Создано',
    'Updated at' => 'Обновлено',

    'Speakers' => 'Спикеры',
    'Total speakers' => 'Всего спикеров',
];
<?php
return [
    'ID' => '#',
    'Name' => 'Имя',
    'Email' => 'Ел. почта',
    'Question' => 'Вопрос',

    'Type' => 'Тип',
    'Status' => 'Статус',
    'Created at' => 'Создано',
    'Updated at' => 'Обновлено',
];
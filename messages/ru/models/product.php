<?php
return [
    'ID' => '#',
    'Product category' => 'Категория продукта',

    'Product image' => 'Изображение продукта',
    'Description image' => 'Изображение для описания',
    'Information image' => 'Изображение для информации',

    'Title' => 'Название',
    'Description title' => 'Заголовок описания',
    'Cost' => 'Стоимость',
    'Point' => 'Баллы',
    'Amount' => 'В упаковке',
    'Vendor code' => 'Артикул',

    'Tab description' => 'Вкладка "Описание"',
    'Tab composition' => 'Вкладка "Состав"',
    'Tab use' => 'Вкладка "Применения"',

    'Information text' => 'Текст для информации',
    'Status' => 'Статус',
    'Created at' => 'Создано',
    'Updated at' => 'Обновлено',

    'Certificates' => 'Сертификаты',
];
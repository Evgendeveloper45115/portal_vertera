<?php
return[
    'Home' => 'Главная',
    'History' => 'История',
    'Mission, purpose and values' => 'Миссия, цель и ценности',
    'Philosophy of Vertera' => 'Философия Vertera',
    'Company development strategy' => 'Стратегия развития компании',
    'We have united in our activities the adherence to traditions, values and, at the same time, high technologies of business. We care about people and help them achieve their goals in protecting family health and prosperity of a kind.' => 'Мы объединили в своей деятельности следование традициям, ценностям и, в то
                    же время, высокие технологии бизнеса. Мы заботимся о людях и помогаем им достичь целей в защите
                    здоровья семьи и процветании своего рода.',
    'Our philosophy is based on 4 major principles' => 'Наша философия основана на 4 важнейших принципах',
    'People' => 'Люди',
    'The culture, values and business of Vertera are aimed at satisfying the interests of our partners and customers. We consider every partner as a member of our family, team and in every possible way to promote the development of his personality and business.' => 'Культура, ценности и бизнес компании
                                    Vertera направлены на удовлетворение интересов наших партнеров и клиентов. Мы
                                    считаем каждого партнера членом нашей семьи, команды и всячески содействуем развитию
                                    его личности и бизнеса.',
    'Products' => 'Продукты',
    'Our products are based on the technologies of bionics, the science of how to correctly see solutions in living nature. Everything that we create is based on the integrity, unity of man and nature.' => 'Наша продукция создана по технологиям
                                    бионики, науки о том, как правильно видеть решения у живой природы. Все, что мы
                                    создаем, основано на целостности, единстве человека и природы.',
    'Business' => 'Бизнес',
    'We offer a powerful business for any person who aspires to success. The partner program, the system of training and motivation from Vertera allow our partners to effectively and promptly reach the residual income and subsequently, if desired, transfer the business by inheritance or sell.' => 'Мы предлагаем мощный бизнес для любого
                                    человека, который стремится к успеху. Партнерская программа, система обучения и
                                    мотивации от Vertera позволяют нашим партнерам эффективно и в кратчайшие сроки выйти
                                    на получение остаточного дохода и впоследствии, при желании, передать бизнес по
                                    наследству либо продать.',
    'Technologies' => 'Технологии',
    "Vertera's business, not only in the nature of its products, but also in the way it is conducted, is truly innovative and has no analogues in the world. We are in step with the times and we consider it our duty to take care of the future. That's why our wor.ru internet marketplace is aimed at cooperation with a new generation of young people and those who do not represent their lives without the Internet. Vertera is a company of the future that already exists." => 'Бизнес Vertera, не только по характеру
                                    выпускаемых продуктов, но и по способу его ведения, является по-настоящему
                                    инновационным и не имеет аналогов в мире. Мы идем в ногу со временем и считаем своим
                                    долгом заботиться о будущем. Именно поэтому наша платформа интернет-торговли wor.ru
                                    направлена на сотрудничество с новым поколением молодых людей и тех, кто не
                                    представляет свою жизнь без Интернета. Vertera – компания будущего, которая уже
                                    существует.',
    'Today, thousands of people around the world are grateful to Vertera for its products and technologies, for choosing the right business and life priorities for itself.' => 'Сегодня тысячи людей во всем мире благодарны компании Vertera за
                        ее продукты и технологии, за правильный выбор для себя бизнеса и жизненных приоритетов.',
    'Become a partner' => 'Стать
                            партнером',
    'We are happy to see you in our business family and will do everything possible to ensure that cooperation with Vertera is the basis for your successful and happy life!' => 'Мы счастливы видеть Вас в нашей бизнес-семье и сделаем все
                        возможное, чтобы сотрудничество с Vertera стало основой для Вашей успешной и счастливой
                        жизни!',
];
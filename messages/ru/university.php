<?php
return [
'Effective freebies' => 'Эффективная бесплатная',
'Saves time' => 'Экономит <br> время',
'Education system' => 'Система обучения',
'Vertera.University' => 'Vertera.Университет',
'Start training' => 'Начать обучение',
'steps' => 'шагов',
'incomes' => 'доходы',
'days' => 'дней',
'Make' => 'Сделайте',
'already now' => 'уже сейчас',
'first step' => 'первый шаг',
'will lead you to increase your income!' => 'приведут вас к увеличению вашего дохода!',
'9 steps in 30 days' => '9 шагов за 30 дней',
'Available around <br> the world' => 'Доступно по <br> всему миру',
'Step by step <br> training system' => 'Пошаговая <br> система обучения',
'Free of charge <br> for partners' => 'Бесплатно <br> для партнеров',
'to your financial independence and freedom!' => 'к вашей финансовой независимости и свободе!',
'You do not have experience, do not know, doubt in yourself? NOT A PROBLEM!' => 'У ВАС НЕТ ОПЫТА, НЕТ ЗНАНИЙ, СОМНЕВАЕТЕСЬ <br> В СВОИХ СИЛАХ? НЕ ПРОБЛЕМА!',
'THE TRAINING PROGRAM IS ABSOLUTELY FREE
                                                                                 OF CHARGE <br> FOR VERTERA PARTNERS!' => 'ПРОГРАММА ОБУЧЕНИЯ АБСОЛЮТНО БЕСПЛАТНА <br> ДЛЯ ПАРТНЕРОВ VERTERA!',
'In our learning system, each step is simple
                                         and straightforward. We will teach you everything and step by step we 
                                         will deduce you to the desired financial result!' => 'В нашей системе обучения каждый шаг прост и понятен. Мы научим всему и пошагово 
 выведем Вас на желаемый финансовый результат!',
    'based on the experience of thousands 
                                        of successful people who have achieved significant financial results.Already 2500 partners
                                         have passed these steps and increased their income by 200-1000%. Now it\'s your turn!' => 'Система обучения Vertera.Университет построена на опыте тысяч успешных людей, которые достигли значительных финансовых результатов. Уже 2500 партнеров прошли эти шаги и увеличили свой доход на 200-1000%. Теперь Ваша очередь!'
];
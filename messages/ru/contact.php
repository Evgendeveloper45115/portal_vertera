<?php
return[
    'In this section you will find useful information about representative offices in Russia and CIS countries' => 'В данном разделе вы найдете полезную информацию о
                                представительствах в России и странах СНГ',
    'Vertera Official Representative Offices' => 'Официальные Представительства Vertera',
    'Representative offices of Vertera' => 'Представительства компании Vertera',
    'Regional representation' => 'Региональное представительство',
    'Pick-up Center' => 'Пикап-Центр',
    'Ask a Question' => 'Задать вопрос',
    'Send' => 'Отправить',
    'To find' => 'Найти',
    'Representation' => 'Представительство',
    'Enter country or city' => 'Введите страну или город',

    'Country name' => 'Страна',
    'City name' => 'Город',
    'Address' => 'Адрес',
    'Phone' => 'Телефон',
];
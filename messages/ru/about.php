<?php
return [
    'about company' => 'О КОМПАНИИ',
    'Vertera is the leader in the direct sales market in its segment in Russia and the CIS' => 'Vertera – это лидер на рынке прямых продаж в своем сегментев России и СНГ',
    "Vertera is a group of companies united by a common philosophy, a socially oriented business in the sphere of supporting each person's striving for health, longevity and a better quality of life" => 'Vertera – это группа компаний, объединенных общей философией, социально ориентированный бизнес в сфере поддержания стремления каждого человека к здоровью, долголетию и лучшему качеству жизни',
    'Vertera is a group of companies united by a common philosophy, socially oriented business, the sphere of supporting each person\'s striving for health, longevity and the best quality of life. We have
own research laboratory for the development of products for dietary nutrition, a powerful
production base, uniting scientists and specialists from all over the world, a business specializing in
e-commerce World Of Retail (wor.ru) and multifunctional information
Centre. The most advanced high-tech, large-scale tasks and focus on long-term
prospects - these are the advantages that make cooperation with us profitable, stable and
promising.' => 'Vertera – это группа компаний, объединенных общей философией, социально ориентированный бизнес в
                    сфере поддержания стремления каждого человека к здоровью, долголетию и лучшему качеству жизни. У нас
                    собственная научная лаборатория по разработке продуктов для диетического питания, мощная
                    производственная база, объединяющая ученых и специалистов со всего мира, бизнес, специализирующийся
                    на электронной коммерции World Of Retail (wor.ru) и многофункциональный информационно-справочный
                    центр. Самые передовые наукоемкие технологии, масштабные задачи и нацеленность на долгосрочные
                    перспективы – вот преимущества, которые делают сотрудничество с нами выгодным, стабильным и
                    перспективным.',
    'ul.Punane house, 6 office 322, Tallinn, Estonia' => 'ул.Пунане дом, 6 офис 322, Таллин,Эстония',
    'Mission, purpose and values' => 'Миссия, цель и ценности',
    'History of the company' => 'История компании',
    'Philosophy of Vertera' => 'Философия Vertera',
    'Development Strategy' => 'Стратегия развития',
    'The main advantage of Vertera is to offer ready-made solutions to customers, which have no analogues in any other direct sales company in the world.' => 'Главное преимущество Vertera – предлагать клиентам готовые решения, аналогов которым нет ни в одной другой компании прямых продаж в мире.',
    'Vertera Company' => 'Компания Vertera',
    'Direct Sales Department' => 'Департамент прямых продаж',
    'Own call-center, the largest in Russia' => 'Собственный call-центр, самый крупный в России',
    'Promotional packages "Hermes"' => 'Рекламные пакеты «Гермес»',
    'Points from direct sales of the company are distributed in the marketing plan under active partners for the advertising packages "HERMES"' => 'Баллы с прямых продаж компании распределяются в маркетинговый план под активных партнеров по
                            рекламным пакетам «ГЕРМЕС»',
    'Own production' => 'Собстевнное производство',
    'Own extraction, processing of raw materials, development and Russian production of eco products' => 'Собственная добыча, переработка сырья, разработка и российское производство экопродукции',
    'Household <br> chemicals' => 'Бытовая <br> химия',
    'Natural, organic products for cleaning the house' => 'Натуральные, органические средства для уборки дома',
    'Cosmetic <br> line' => 'Косметическая <br> линия',
    'Natural ecocosmetics for face and body based on seaweed' => 'Натуральная экокосметика для лица и тела на основе морских водорослей',
    'Smart food' => 'Умное питание',
    'Ecological products for medical and dietary nutrition based on seaweed' => 'Экопродукты для лечебно-диетического питания на основе морских водорослей',
    'IT department' => 'IT-департамент',
    'World of Trade' => 'Мир торговли',
    'A unique e-commerce platform (millions of goods and services, all coupons, stocks of leading stores)' => 'Уникальная платформа электронной коммерции (миллионы товаров и услуг, все купоны, акции
                            ведущих магазинов)',
];
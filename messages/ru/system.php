<?php
return [
	'Failed' => 'Не удалось сохранить!',
	'Success' => 'Сохранение прошло успешно!',
	'Not found' => 'Запрошенная информация не найдена!',

	'Profile' => 'Профиль',
	'Logout' => 'Выйти',

	'Active' => 'Активный',
	'Disabled' => 'Отключен',
	'Edit' => 'Редактировать',
];
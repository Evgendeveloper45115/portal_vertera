<?php
return [
    'Main' => 'Главная',
    'Go to the site' => 'Перейти на сайт',
    'This is the best business for earning on the Internet. <br> This is the future of direct sales and Internet 
      entrepreneurship. <br> This is your future and your children.' => 'Это лучший бизнес для заработка в Интернете.<br>Это будущее
      прямых продаж и интернет-предпринимательства.<br> Это Ваше будущее и Ваших детей.',
    'Join the world of successful people <br> and new technologies!' => 'Присоединяйтесь к миру успешных людей<br>и новых технологий!',
    "This is a unique platform on which everyone can earn, and building a business is much easier! Filling in the affiliate 
    program is not only due to Vertera's own products, but also from all purchases of your structure on the platform." => 'Это уникальная площадка,
     на которой может зарабатывать каждый, а строить бизнес намного проще! Наполнение партнерской программы происходит
      не только за счет собственных продуктов Vertera, но и со всех покупок Вашей структуры на платформе.',
    'Invite to build a networked business on a simple and unique resource' => 'Приглашайте строить сетевой бизнес на простом и уникальном ресурсе',
    'Today on the Internet we are offered millions of goods and services on different resources. Tens of thousands of
       online stores offer discounts and promotions. It is impossible to keep abreast of all the news.
      The unique "World of Trade" site is not only an opportunity to purchase goods and services in one place,
       but also to get its share of cash from the world turnover of this platform.' => 'Сегодня в Интернете нам предлагаются
        миллионы товаров и услуг на разных ресурсах. Десятки тысяч интернет-магазинов предлагают скидки и акции. 
        Невозможно быть в курсе всех новостей. Уникальная площадка “Мир Торговли” — это не только возможность приобретать
         товары и услуги в одном месте, но и получать свою часть денежных средств от всемирного товарооборота
          этой платформы.',
    'From all personal purchases you get a cashback of 2 to 40%' => 'Со всех личных покупок Вы получаете кэшбэк от 2 до 40%',
    'From all purchases of your first line you get 25% of the amount of their cashback' => 'Со всех покупок Вашей первой линии Вы получаете 25% от суммы их кэшбэка',
    'World of Trade' => 'Мир торговли',
    'More than 250 online stores' => 'Более 250 интернет-магазинов',
    'Invite for lucrative <br> shopping' => 'Приглашайте за выгодными <br> покупками',
    'Invite friends, acquaintances and even strangers.
                                 Now you can buy everything on the Internet, and on the "World of Trade" platform, you can do this
                                  not only for the best prices, but also get a part of the money back!' => 'Приглашайте друзей, 
     знакомых и даже незнакомых людей. Сейчас абсолютно все можно купить в Интернете, а на платформе «Мир торговли» это 
     можно делать не только по лучшим ценам, но и получать часть денег обратно!',
    'Who can be invited to the platform? <br> Absolutely everyone!' => 'Кого можно приглашать <br> на платформу? Абсолютно всех!',
    "With all the purchases of your entire structure, you get a score (according to the Company's partner program)" => 'Со всех 
    покупок всей Вашей структуры Вы получаете балловое наполнение (согласно партнерской программы Компании)',
    'Any purchase on the "World of Trade" platform 
                            is always profitable! Receive income not only from your purchases, but also from purchases of 
                            your clients and customers of your structure, as well as from sales of your suppliers.' => 'Любые покупки
       на платформе «Мир торговли» всегда выгодны! Получайте доход не только от своих покупок, но и от 
       покупок Ваших клиентов и клиентов Вашей структуры, а также с продаж заведенных Вами поставщиков.',
    'What income is available <br> for platform?' => 'Какой доход можно получить <br> на платформе?',
    'More than 250 of the most popular online stores,
                             including world leaders in the industry, with the best offers:
                               Sportmaster, Lamoda, OZON.RU, M.Video, AliExpress and others.' => 'Более 250 популярнейших интернет-магазинов,включая
       мировых лидеров индустрии, с лучшими предложениями: Спортмастер, Lamoda, OZON.РУ, М.Видео, AliExpress и другие.',
    'What is on the <br> Huge selection of goods' => 'Огромный выбор товаров',
    "More than 20 million goods (clothing, footwear,
                             cosmetics, food, travel, financial services, goods for cars, children's
                             goods, personal care products and others)" => 'Более 20 млн. товаров (одежда, обувь, косметика, продукты питания, 
       путешествия, финансовые услуги, товары для автомобилей, детские товары, товары личной гигиены и другие)',
    'What is on the <br> "World of Trade" platform' => 'Что есть на платформе <br> «Мир Торговли»',
    'Ideal MLM-business <br> together with World of Retail' => 'Идеальный MLM-бизнес <br> вместе с World of Retail',
];
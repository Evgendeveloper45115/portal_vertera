<?php
return [
    'History of the company' => 'История компании',
    'Home' => 'Главная',
    'History' => 'История',
    'Mission, purpose and values' => 'Миссия, цель и ценности',
    'Philosophy of Vertera' => 'Философия Vertera',
    'Company development strategy' => 'Стратегия развития компании',
    'The history of Vertera began in 2005, when for the first time in the world A unique biotechnology has been created that makes it possible to obtain a complex of active natural compounds from cells of seaweed kelp. The cellular contents of the plant were isolated, and on its basis a gel was obtained. The gel composition included the cellular composition of the kelp, which retains undestroyed state of the molecular bond of natural substances. Vertera Gel is certified as A specialized food for dietary therapeutic nutrition.' => 'История компании Vertera началась в 2005 году, когда впервые в мире была
                        создана уникальная биотехнология, позволяющая получить комплекс активных природных соединений из
                        клеток морской водоросли ламинарии. Было выделено клеточной содержимое растения, а на его основе
                        был получен гель. В состав геля вошел клеточный состав ламинарии, который сохраняет в
                        неразрушенном состоянии молекулярную связь природных веществ. Vertera Gel сертифицирован как
                        специализированный пищевой продукт для диетического лечебного питания.',
    'More' => 'Подробнее',
    'It is a holistic product, not divided artificially into separate organic and mineral elements. This is his main advantage. That is why substances in Vertera gel are perfectly perceived by the body, and the level of their digestibility can reach 93%.' => 'Это целостный продукт, а не разделенный искусственно на отдельные
                        органические и минеральные элементы. В этом его главное преимущество. Именно поэтому вещества в
                        геле Vertera прекрасно воспринимаются организмом, а уровень их усвояемости может достигать
                        93%.',
    "Laminaria gel is the basis of almost all products and natural complexes of Vertera. This is a 'live food', thanks to which our body receives a complex of vitally important substances. The composition of the components of the product corresponds to the code of organic and mineral compounds evolutionarily fixed in the organism's genotype." => 'Гель ламинарии является основой почти всех продуктов и природных
                        комплексов Vertera. Это «живая» пища, благодаря которой наш организм получает комплекс жизненно
                        необходимых веществ. Состав компонентов продукта соответствует эволюционно закрепленному в
                        генотипе организма коду органических и минеральных соединений.',
    'Research' => 'Исследования',
    'Work on technology improvement and clinical research of Vertera products continued under the leadership of Anatoly Khitrov, who is still President of the Company.' => 'Работы над усовершенствованием технологий и клинические
                        исследования продуктов Vertera продолжились под руководством Анатолия Хитрова, который и сегодня
                        является президентом Компании.',
    "It was he who united a team of like-minded people, modern scientists, technologists and other high-level specialists with world experience. A graduate of the Edinburgh School of Business, Edinburgh Business School, A. Khitrov appreciated the highest potential of domestic science and defined his business as the creation of innovative products for the nation's recovery, accessible to most consumers." => 'Именно он объединил команду единомышленников, современных ученых,
                        технологов и других специалистов высокого уровня с мировым опытом работы. Выпускник Эдинбургской
                        школы бизнеса, Edinburgh Business School, А. Хитров оценил высочайший потенциал отечественной
                        науки и делом своей жизни обозначил создание инновационных продуктов для оздоровления нации,
                        доступного для большинства потребителей.',
    'The main principle of Vertera is the creation of products to help people in the modern technogenic world. Man is an integral, original, unique and unusually important part of the most complex mechanism called nature. That is why, for a natural healthy existence, each of us needs integral food products, rather than substances separated from them, endowed with magical properties.' => 'Основной принцип компании Vertera – создание продуктов для помощи человеку в современном
                        техногенном мире. Человек является целостной, самобытной, уникальной и необычайно важной частью
                        сложнейшего механизма, называемого природой. Именно поэтому для естественного здорового
                        существования каждому из нас необходимы целостные продукты питания, а не выделенные из них
                        вещества, наделенные волшебными свойствами.',
    'Since 2010, Vertera has significantly expanded the range of its products. Professional pharmacologist and phytotherapeutist, Honored Scientist of Russia, Head of the Department of Clinical Pharmacology, TGMA, Doctor of Medicine, Professor Gennady Bazanov became the author of phytoproducts recipes. He developed and formulated Verona phytonadiation, leading the research division of the company. The innovative nature of this direction was that the created composite phytostases were intended not only for replenishing the missing vital ingredients of nutrition and for balancing the complex of organic and mineral substances, but also for directed physiological effects on organs and systems, including activation of detoxification and excretory processes.' => 'С 2010 года компания Vertera существенно расширяет линейку
                        производимой продукции. Профессиональный фармаколог и фитотерапевт, заслуженный деятель науки
                        РФ, глава кафедры клинической фармакологии ТГМА, доктор медицинских наук, профессор Геннадий
                        Базанов стал автором рецептур фитопродуктов. Он разработал и сформулировал фитонаправление
                        Vertera, возглавив научно-исследовательское подразделение компании. Инновационность этого
                        направления заключалось в том, что созданные композиционные фитосоставы предназначались не
                        только для восполнения недостающих жизненно необходимых ингредиентов питания и
                        сбалансированности комплекса органических и минеральных веществ, но и для направленного
                        физиологического воздействия на органы и системы, в том числе и в отношении активации
                        дезинтоксикационных и выделительных процессов.',
    'December 7, 2014 the company Vertera started as a direct sales company.' => '7 декабря 2014 года состоялся старт компании Vertera как компании
                        прямых продаж.',
    'The company is active in the markets of Russia, the Republic of Belarus, Kyrgyzstan, Kazakhstan, Ukraine, Azerbaijan.  <br> <span> 867 partner offices are open. </span> <br> Passed certification in the EU.' => 'Компания активно функционирует на рынках России, Республики
                        Беларусь, Кыргызстане, Казахстане, Украине, Азербайджане.<br><span>Открыто 867 партнерских офисов.</span><br>Пройдена
                        сертификация в ЕС.',
    'On January 6, 2017, Vertera opened an additional business line for Partners - World of Retail.' => '6 января 2017 году компания Vertera открыла дополнительное
                        направление бизнеса для Партнеров - World of Retail.',
];
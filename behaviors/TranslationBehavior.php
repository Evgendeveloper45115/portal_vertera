<?php

namespace app\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\helpers\Url;
use app\models\FieldTranslation;

/**
 * Class TranslationBehavior
 *
 * ```php
 * use yii\behaviors\TranslationBehavior;
 * public function behaviors()
 * {
 *     return [
 *         'translation' => [
 *             'class' => TranslationBehavior::class,
 *             'language' => Yii::$app->language,
 *             'default_language' => 'en',
 *          ],
 *     ];
 * }
 * ```
 *
 * @package app\behaviors
 */
class TranslationBehavior extends Behavior
{
    /**
     * Application language
     *
     * @var string
     */
    public $language;

    /**
     * Application default language. If set, it will be skipped
     *
     * @var string
     */
    public $default_language;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if(!$this->language) {
            $this->language = Yii::$app->language;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if (!$this->default_language || $this->default_language != Yii::$app->language) {
            /* @var $owner ActiveRecord|object */
            $owner = $this->owner;
            $model_key = array_search(get_class($owner), FieldTranslation::$model_keys);
            $code = FieldTranslation::getLangCode(Yii::$app->language);

            foreach (FieldTranslation::find()->where([
                'rel_id' => (int)$owner->id,
                'model_key' => (int)$model_key,
                'language' => (int)$code,
            ])->all() as $tr) {
                if ($owner->hasAttribute($tr->field_name) && !empty($tr->field_value)) {
                    $owner->{$tr->field_name} = $tr->field_value;
                }
            }
        }
    }

    /**
     * @param $attribute
     * @param null $key
     * @return array|mixed
     */
    public function buildOptions($attribute, $key = null)
    {
        /** @var ActiveRecord|object $model */
        $model = $this->owner;
        $options = [
            'class' => 'form-control',
            'data' => [
                'label' => $model->getAttributeLabel($attribute),
                'translate-url' => Url::to([
                    'translate',
                    'id' => $model->id,
                    'field' => $attribute,
                    'model_key' => array_search(get_class($model), FieldTranslation::$model_keys)
                ]),
            ],
        ];

        return $options[$key] ?? $options;
    }
}